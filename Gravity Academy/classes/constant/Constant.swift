//
//  Constant.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 06/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//


// Training Plan = Program
// Skills = Technique
// Workout = Workout
// Exercise = Exercise

import Foundation
import UIKit


// Common
var iOS = "2"
let deviceID = UIDevice.current.identifierForVendor!.uuidString
let errorMessage: String = "something went wrong"
let noInternet: String = "No internet connection"
let ebookPurchaseURL = "https://gravity-academy.de/liegestuetze-anleitung"
let shadowImage = UIImage(named: "black_shadow")!

var APP_THEME_ORANGE_COLOR = "#E64C2F"
var tabBarColor = "#000000" //"#252525"
var filterSelected = "#000000"
var filterUnselected = "#EFEFEF"
var NOEQUIPMENT = "NO EQUIPMENT"


//Like & Dislike
let likeFill = UIImage(named: "heart-fill")
let likeUnfill = UIImage(named: "heart-unfill")

// Gender
var male = "1"
var female = "2"

// Height
var heightUnitCM = "1"
var heightUnitInch = "2"

// Weight
var weightUnitKG = "1"
var weightUnitLBS = "2"


// Fitness and Goals
var elementOne = "1"
var elementTwo = "2"
var elementThree = "3"
var elementFour = "4"
var normalBorderWith: CGFloat = 1.0
var updateBorderWith: CGFloat = 2.0


enum Gender: String, Codable {
    case male
    case female
}

// Fonts
enum AppFont:String {
    case Bold = "Roboto-Bold"
    case Medium = "Roboto-Medium"
    case Light = "Roboto-Light"
    case Regular = "Roboto-Regular"
    case Thin = "Roboto-Thin"
    case Black = "Roboto-Black"
}

var Roboto_Bold = "Roboto-Bold"
var Roboto_Medium = "Roboto-Medium"
var Roboto_Light = "Roboto-Light"
var Roboto_Regular = "Roboto-Regular"
var Roboto_Thin = "Roboto-Thin"
var Roboto_Black = "Roboto-Black"


let safeAreaInsets: UIEdgeInsets = UIApplication.shared.keyWindow!.safeAreaInsets
let placeHolderImage: UIImage = UIImage()
let startTimerIcon = UIImage(named: "exercise-play-icon")
let pauseTimerIcon = UIImage(named: "exercise-pause-icon")
let workoutStartIcon = UIImage(named: "workout-start")
let workoutPauseIcon = UIImage(named: "workout-pause")


enum AppFitnessLevel:String {
    case new = "1"
    case beginner = "2"
    case intermediate = "3"
    case advanced = "4"
}

func fitnessLevelTitle(_ level:String) -> String {
    switch level {
    case "1":
        return Fitness.beginner.rawValue
    case "2":
        return Fitness.intermediate.rawValue
    case "3":
        return Fitness.advanced.rawValue
    default:
        return ""
    }
}

func getFitnessLevel(_ level:String) -> FitnessLevel? {
    if let data = UserStore.masterData {
        return (data.fitnessLevel.filter { $0.fnLevelID == level }).first
    } else {
        return nil
    }
}

func getGoal(_ goalID:String) -> Goal? {
    if let data = UserStore.masterData {
        return (data.goals.filter { $0.goalID == goalID }).first
    } else {
        return nil
    }
}

func getMuscleGroup(_ muscleID:String) -> MuscleGroup? {
    if let data = UserStore.masterData {
        return (data.muscleGroup.filter { $0.muscleID == muscleID }).first
    } else {
        return nil
    }
}
