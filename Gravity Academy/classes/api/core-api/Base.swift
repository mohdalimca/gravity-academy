//
//  Base.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 3/22/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import UIKit

/// API Enviroment
public enum Environment {
   var  APIVersion:String { return "user/" }
   var host: String {
    switch self {
    case .dev:
        return "http://gravity-admin.com"
    case .staging:
        return "http://gravity-admin.com"
    case .production:
        return "http://gravity-admin.com"
    }
   }
   case staging
   case dev
   case production
}

let APIEnvironment: Environment = .production
typealias APIResult<T:Codable> = (_ result:T?, _ error:APIError?) -> Void


public enum HTTPMethod: String {
   case post = "POST"
   case get = "GET"
   case put = "PUT"
   case patch = "PATCH"
   case delete = "DELETE"
}

public enum Parameters {
   case none
   case body(data: Data?)
   case url(_: [String: String])
}

final class SessionDispatcher: NSObject, URLSessionDelegate {
   var headers: [String: String] = [:]
   let host: String
  
   var session: URLSession {
      let config = URLSessionConfiguration.default
      config.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
      let s = URLSession(configuration: config)
      return s
   }
   
   func execute<T:Decodable>(requst: RequestRepresentable, modeling _: T.Type, completion:@escaping APIResult<T>) {
    
    if !Reachability.isConnectedToNetwork() {
        completion(nil, APIError(errorCode: .network, responseData: nil, statusCode: 0))
        print("not connected with internet")
        return
    }
    
    switch requst.parameters {
    case let .body (data):
        if let obj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) {
            print("parameters of request",obj)
        }
    default:
        break
    }
    print("headers of request", requst.headers as Any)
     let  task = session.dataTask(with: prepareRequest(request: requst), completionHandler: { data, http, err in
      if err != nil {
        completion(nil, APIError(errorCode: .uknown, responseData: nil, statusCode: 0))
        return
        }
      guard let  resp = http as? HTTPURLResponse else {
         completion(nil, APIError(errorCode: .uknown, responseData: nil, statusCode: 0))
         return
      }
      
      guard let data = data else {
         completion(nil, APIError(errorCode: .uknown, responseData: nil, statusCode: resp.statusCode))
         return
      }
        if let obj = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) {
            print(obj)
        }

      self.handleReponse(data: data, response: resp, completion: completion)
     })
   
   
   task.resume()
      
   }

   override init() {
      self.host = APIEnvironment.host
   }
   
   func handleReponse<T:Codable>(data:Data, response:HTTPURLResponse,completion:@escaping APIResult<T>) {
     print("received response status code:\(response.statusCode)")
      let (ok, code) = statusOK(response: response)
      if !ok {
         let error = APIError(errorCode: code, responseData: APIErrorResponseData.from(data: data), statusCode: response.statusCode)
         completion(nil, error)
         return
      }
      
      let (model, err) = Parser<T>.from(data)
      if err == nil {
         completion(model, nil)
         return
      }
      
      if let obj = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) {
         print(obj)
      }
      
      completion(nil, APIError(errorCode: .parsing, responseData: nil, statusCode: response.statusCode))
    
   }
   
   func statusOK(response:HTTPURLResponse)-> (Bool, ErrorCode) {
      
      var code:ErrorCode
      var ok:Bool
      switch response.statusCode {
      case 200,203,201:
         code = .uknown
         ok = true
      case 401,403:
         code = .authorize
         ok = false
      case 400:
         code = .badRequest
         ok = false
      case 500:
         code = .server
       ok = false
      default:
         code  = .uknown
         ok = false
      }
      
      return (ok, code)
   }
   
   private func prepareRequest(request: RequestRepresentable) -> URLRequest {
      let s = "\(host)/\(APIEnvironment.APIVersion)\(request.endpoint)"
      let scaped = s.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
      let url = URL(string: scaped!)
      var r = URLRequest(url: url!)
      headers(in: request, for: &r)
      params(in: request, for: &r)
      print("sending request:", s)
      return r
   }
   
   private func headers(in request: RequestRepresentable, for urlRequest: inout URLRequest) {
      urlRequest.httpMethod = request.method.rawValue
      addDefaultHeaders()
      headers.forEach({ key, value in
         urlRequest.setValue(value, forHTTPHeaderField: "\(key)")
      })
      request.headers?.forEach({ key, value in
         urlRequest.setValue(value, forHTTPHeaderField: "\(key)")
      })
   }

   private func addDefaultHeaders() {
    print("current user token", UserStore.token as Any)
      headers["Content-Type"] = "application/json"
         if let token = UserStore.token {
            headers["sessionkey"] = "\(token)"
         }
   }

   private func params(in request: RequestRepresentable, for urlRequest: inout URLRequest) {
      switch request.parameters {
      case let .body(data):
         urlRequest.httpBody = data
      case let .url(urlencoded):
         var urlComponents = URLComponents(url: urlRequest.url!, resolvingAgainstBaseURL: true)
         urlencoded.forEach { key, value in
            let query = URLQueryItem(name: key, value: value)
            urlComponents?.queryItems?.append(query)
         }
         urlRequest = URLRequest(url: (urlComponents?.url)!)
      case .none: break
      }
   }
}
