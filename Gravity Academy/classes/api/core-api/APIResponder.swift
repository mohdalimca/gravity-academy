//
//  APIResponder.swift
//  HabitSuite
//
//  Created by Mohd Ali Khan on 4/4/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


protocol RequestRepresentable {
   var endpoint: String { get }
   var method: HTTPMethod { get }
   var parameters: Parameters { get }
   var headers: [String: String]? { get }
}

extension RequestRepresentable {
   var headers:[String:String]? { return nil   }
   var method: HTTPMethod         { return .get  }
   var parameters: Parameters   { return .none }
   
   func encodeBody<T: Codable>(data: T) -> Data? {
      let data = try? JSONEncoder().encode(data.self)
      return data
   }
   
   func encode(body:[String:Any]) ->Data? {
      return try? JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions())
   }
   
   func encode(body:Any) -> Data? {
      return try? JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions())
   }
}

public enum Response<T: Decodable> {
   case success(date: T)
   case failed(error: APIError)
}
