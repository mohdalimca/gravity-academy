//
//  APIParser.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 3/22/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

public struct Parser<T: Decodable> {
   static func from(_ data: Data) -> (T?, Error?) {
      do {
         let decodedModel = try JSONDecoder().decode(T.self, from: data)
         return (decodedModel, nil)
      } catch {
        print("Parsing error is ==== \(error)")
         return (nil, error)
      }
   }
    
    static func fromData(_ data: Data) -> ([T]?, Error?) {
        do {
            let decodeModel = try JSONDecoder().decode(Array<T>.self, from: data)
            return (decodeModel, nil)
        } catch  {
            return (nil, error)
        }
    }
    
   static func from(_ data: Data) -> T? {
      return try? JSONDecoder().decode(T.self, from: data)
   }
   
   static func to<T: Encodable>(_ model: T) -> (Data?, Error?) {
      do { return (try JSONEncoder().encode(model), nil) } catch { return (nil, error) }
   }
   static func to<T:Encodable>(_ model:T) -> Data? {
      return try? JSONEncoder().encode(model)
   }
}
