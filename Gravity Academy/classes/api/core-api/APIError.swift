//
//  File.swift
//  HabitSuite
//
//  Created by Mohd Ali Khan on 4/4/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


//struct APIErroResponseData:Codable {
//    let message:String?
//    let name:String?
//    let error:String?
//    static func from(data:Data) -> APIErroResponseData? {
//      return try? JSONDecoder().decode(APIErroResponseData.self, from: data)
//   }
//}

public struct APIError:Error {
   let errorCode:ErrorCode?
   var responseData:APIErrorResponseData?
   var statusCode:Int?
}

public enum ErrorCode {
   case badRequest
   case uknown
   case network
   case server
   case authorize
   case parsing
}
