//
//  Params.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 10/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum UserParams {
    
    struct Register: Codable {
        let gender:String
        var device_type:String = iOS
        let height:String
        let height_unit:String
        let weight:String
        let weight_unit:String
        let fn_level_id:String
        let goals:[String]
        let max_pullups:String
        let max_pushups:String
        let max_squats:String
        let max_dips:String
        let user_name:String
        let email:String
        let name:String
        let password:String
        var device_id:String = UUID().uuidString
    }
    
    struct Login:Codable {
        let email:String
        let password:String
        var device_type:String = iOS
        var device_id:String = deviceID
    }
    
    struct Logout:Codable {
        let sessionkey:String
    }
    
    struct Forgot:Codable {
        let email:String
    }
    
    struct NewPassword:Codable {
        let password:String
        var key:String
    }
    
    struct ValidateOTP:Codable {
        let email:String
        let otp:String
        var device_type:String = iOS
        var device_id:String = deviceID
    }
    
    struct ValidateEmail:Codable {
        let email:String
    }
}

enum ExerciseParams {
    
    struct ExerciseLibrary:Codable {
        let page_size:String
        let page_no:String
        let keyword:String
        let fn_level_id:String
        let equipment_required:String
        let muscle_id:String
    }
    
    struct ExerciseLike:Codable {
        var type:String
        var type_id:String
    }
    
    struct Filter:Codable {
        var equipment_required: String?
        var fn_level_id: String?
        var muscle_id:String?
    }
}

enum WorkoutsParams {
    struct WorkoutsDetail:Codable {
        let workout_id:String?
        let fn_level_id:String?
    }
    
    struct Complete:Codable {
        let workout_id:String?
    }
    
    struct CompleteWokrout:Codable {
        let page_no:String?
        var page_size: String = "10"
    }
}

enum ProgramsParams {
    struct FitnessLevel:Codable {
        let fn_level_id:String?
    }
    
    struct Weeks:Codable {
        let tp_id:String?
    }
    
    struct WeekData:Codable {
        let tp_week_id:String?
    }
    
    struct CompleteProgram:Codable {
        let page_no:String?
        var page_size: String = "10"
    }
}

enum TechniquesParam {
    struct SkillWeeks:Codable {
        let skill_id: String?
    }
    
    struct SkillWeekDetail:Codable {
        let skill_week_id: String?
    }
    
    struct CompleteSkill:Codable {
        let page_no:String?
        let page_size: String = "10"
    }
}


enum LikeUnlikeType:Int,Codable {
    case exercise = 1
    case workout = 2
    case trainingPlan = 3
    case skills = 4
}

struct LikeDislikeParam:Codable {
    let type:LikeUnlikeType?
    let type_id:String?
}

struct FavouriteList:Codable {
    let page_no:String?
    let page_size:String?
}

public enum AppAPI: String {
    case login
    case registration
    case forgotPassword
    case verifyEmail
    case requestOTP
    case masterData
    case workouts
    case likeUnlike
    case favouritesData
}
