//
//  UserTask.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 10/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class UserTask {
    private let dispatcher = SessionDispatcher()
    
    func masterData<T:Codable>(modeling: T.Type, completion: @escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.masterData, modeling: modeling, completion: completion)
    }
    
    func register<T:Codable>(param:UserParams.Register, modeling: T.Type, completion: @escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.register(params: param), modeling: modeling, completion: completion)
    }
    
    func login<T:Codable>(param:UserParams.Login, responseModel:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.login(params: param), modeling: responseModel, completion: completion)
    }
    
    func forgot<T:Codable>(param:UserParams.Forgot, responseModel:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.forgot(params: param), modeling: responseModel, completion: completion)
    }
    
    func validateOTP<T:Codable>(param:UserParams.ValidateOTP, responseModel:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.validateOTP(params: param), modeling: responseModel, completion: completion)
    }
    
    func validateEmail<T:Codable>(param:UserParams.ValidateEmail, responseModel:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.validateEmail(params: param), modeling: responseModel, completion: completion)
    }
    
    func newPassword<T:Codable>(param:UserParams.NewPassword, responseModel:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.newPassword(params: param), modeling: responseModel, completion: completion)
    }
    
    func getProfile<T:Codable>(responseModel:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.getProfile, modeling: responseModel, completion: completion)
    }
    
    func updateProfile<T:Codable>(responseModel:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.updateProfile, modeling: responseModel, completion: completion)
    }
    
    func profileImage<T:Codable>(responseModel:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.profileImage, modeling: responseModel, completion: completion)
    }
    
    func completeWorkout<T:Codable>(param:WorkoutsParams.CompleteWokrout, responseModel:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.completeWorkout(params: param), modeling: responseModel, completion: completion)
    }
    
    func completeProgram<T:Codable>(param:ProgramsParams.CompleteProgram, responseModel:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.completeProgram(params: param), modeling: responseModel, completion: completion)
    }
    
    func completeSkill<T:Codable>(param:TechniquesParam.CompleteSkill, responseModel:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.completeSkill(params: param), modeling: responseModel, completion: completion)
    }
    
    func logout<T:Codable>(param:UserParams.Logout, responseModel:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.logout(params: param), modeling: responseModel, completion: completion)
    }
}
