//
//  TechniquesTask.swift
//  Gravity Academy
//
//  Created by Apple on 16/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class TechniquesTask {
    private let dispatcher = SessionDispatcher()
    
    func list(completion: @escaping APIResult<SuccessResponseModel>) {
        dispatcher.execute(requst: TechniquesRequest.list, modeling: SuccessResponseModel.self, completion: completion)
    }

    func likeDislike(param:LikeDislikeParam, completion: @escaping APIResult<SuccessResponseModel>) {
        dispatcher.execute(requst: TechniquesRequest.like(params: param), modeling: SuccessResponseModel.self, completion: completion)
    }
    
    func skillWeeks(param:TechniquesParam.SkillWeeks, completion: @escaping APIResult<SuccessResponseModel>) {
        dispatcher.execute(requst: TechniquesRequest.skillWeeks(params: param), modeling: SuccessResponseModel.self, completion: completion)
    }
    
    func skillWeeekDetail(param:TechniquesParam.SkillWeekDetail, completion: @escaping APIResult<SuccessResponseModel>) {
        dispatcher.execute(requst: TechniquesRequest.skillWeeekDetail(params: param), modeling: SuccessResponseModel.self, completion: completion)
    }
}
