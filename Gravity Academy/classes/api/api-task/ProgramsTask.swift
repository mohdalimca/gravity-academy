//
//  ProgramsTask.swift
//  Gravity Academy
//
//  Created by Apple on 24/06/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

final class ProgramsTask {
    private let dispatcher = SessionDispatcher()
    
    func list<T:Codable>(modeling: T.Type, completion: @escaping APIResult<T>) {
        dispatcher.execute(requst: ProgramsRequest.library, modeling: modeling, completion: completion)
    }

    func likeDislike(param:LikeDislikeParam, completion: @escaping APIResult<SuccessResponseModel>) {
        dispatcher.execute(requst: ProgramsRequest.like(params: param), modeling: SuccessResponseModel.self, completion: completion)
    }
    
    func weekList(param:ProgramsParams.Weeks, completion: @escaping APIResult<SuccessResponseModel>) {
        dispatcher.execute(requst: ProgramsRequest.programWeeks(params: param), modeling: SuccessResponseModel.self, completion: completion)
    }
    
    func programWeekData(param:ProgramsParams.WeekData, completion: @escaping APIResult<SuccessResponseModel>) {
        dispatcher.execute(requst: ProgramsRequest.programWeekData(params: param), modeling: SuccessResponseModel.self, completion: completion)
    }
}
