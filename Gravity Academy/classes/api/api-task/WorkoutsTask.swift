
//
//  WorkoutsTask.swift
//  Gravity Academy
//
//  Created by Apple on 08/12/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class WorkoutsTask {
    private let dispatcher = SessionDispatcher()
    
    func masterData<T:Codable>(modeling: T.Type, completion: @escaping APIResult<T>) {
        dispatcher.execute(requst: AuthRequests.masterData, modeling: modeling, completion: completion)
    }

    func workoutOfTheDay<T:Codable>(modeling: T.Type, completion: @escaping APIResult<T>) {
        dispatcher.execute(requst: WorkoutsRequest.workoutOfTheDay, modeling: modeling, completion: completion)
    }
    
    func workoutLibrary<T:Codable>(modeling: T.Type, completion: @escaping APIResult<T>) {
        dispatcher.execute(requst: WorkoutsRequest.workoutLibrary, modeling: modeling, completion: completion)
    }
    
    func workoutDetail<T:Codable>(param:WorkoutsParams.WorkoutsDetail, modeling:T.Type, completion:@escaping APIResult<T>) {
        dispatcher.execute(requst: WorkoutsRequest.workoutDetail(params: param), modeling: modeling, completion: completion)
    }
    
    func likeDislike(param:LikeDislikeParam, completion: @escaping APIResult<SuccessResponseModel>) {
        dispatcher.execute(requst: WorkoutsRequest.like(params: param), modeling: SuccessResponseModel.self, completion: completion)
    }
    
    func dashBoardData<T:Codable>(modeling: T.Type, completion: @escaping APIResult<T>) {
        dispatcher.execute(requst: WorkoutsRequest.dashBoardData, modeling: modeling, completion: completion)
    }

    func equipments<T:Codable>(modeling: T.Type, completion: @escaping APIResult<T>) {
        dispatcher.execute(requst: WorkoutsRequest.equipments, modeling: modeling, completion: completion)
    }

    func completeWorkout(param:WorkoutsParams.Complete, completion: @escaping APIResult<SuccessResponseModel>) {
        dispatcher.execute(requst: WorkoutsRequest.complete(params: param), modeling: SuccessResponseModel.self, completion: completion)
    }
    
    func favourites(completion: @escaping APIResult<SuccessResponseModel>) {
        dispatcher.execute(requst: WorkoutsRequest.favourites, modeling: SuccessResponseModel.self, completion: completion)
    }
    
    
    func favouriteList(param:FavouriteList, scene:Scenes, completion: @escaping APIResult<SuccessResponseModel>) {
        switch scene {
        case .favouriteTrainingPlan:
            dispatcher.execute(requst: WorkoutsRequest.favouriteTP(params: param), modeling: SuccessResponseModel.self, completion: completion)
        case .favouriteWorkout:
            dispatcher.execute(requst: WorkoutsRequest.favouriteWO(params: param), modeling: SuccessResponseModel.self, completion: completion)
        case .favouriteSkills:
            dispatcher.execute(requst: WorkoutsRequest.favouriteSkill(params: param), modeling: SuccessResponseModel.self, completion: completion)
        case .favouriteExercise:
            dispatcher.execute(requst: WorkoutsRequest.favouriteExercise(params: param), modeling: SuccessResponseModel.self, completion: completion)
        default:
            break
        }
    }
}
