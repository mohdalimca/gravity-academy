//
//  ExerciseTask.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 04/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class ExerciseTask {
    private let dispatcher = SessionDispatcher()
    
    func exerciseLibrary<T:Codable>(modeling: T.Type, completion: @escaping APIResult<T>) {
        dispatcher.execute(requst: ExerciseRequest.exerciseLibrary, modeling: modeling, completion: completion)
    }
    
    func exerciseLike<T:Codable>(params: LikeDislikeParam, modeling:T.Type, completion: @escaping APIResult<T> ) {
        dispatcher.execute(requst: ExerciseRequest.like(params: params), modeling: modeling, completion: completion)
    }
    
    func exerciseFilter<T:Codable>(params: ExerciseParams.Filter, modeling:T.Type, completion: @escaping APIResult<T> ) {
        dispatcher.execute(requst: ExerciseRequest.filter(params: params), modeling: modeling, completion: completion)
    }

}
