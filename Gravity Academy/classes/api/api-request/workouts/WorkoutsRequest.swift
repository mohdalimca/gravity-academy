//
//  WorkoutsRequest.swift
//  Gravity Academy
//
//  Created by Apple on 08/12/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum WorkoutsRequest: RequestRepresentable {
    
    var method: HTTPMethod {
        return .post
    }
    
    var endpoint: String {
        switch self {
        case .dashBoardData:
            return "exercise/dashboard"
        case .workoutLibrary:
            return "exercise/workout_library"
        case .workoutDetail:
            return "exercise/workout_detail"
        case .workoutOfTheDay:
            return "exercise/wo_of_the_day"
        case .like:
            return "exercise/toggle_like"
        case .complete:
            return "exercise/complete_wo"
        case .equipments:
            return "exercise/equipment_list"
        case .favourites:
            return "exercise/fav_data"
        case .favouriteWO:
            return "exercise/fav_workouts"
        case .favouriteTP:
            return "exercise/fav_tp"
        case . favouriteSkill:
            return "exercise/fav_skills"
        case .favouriteExercise:
            return "exercise/fav_exercise"
        }
    }
    
    var parameters: Parameters {
        switch self {
        case let .like(params):
            return .body(data: encodeBody(data: params))
        case let .workoutDetail(params):
            return .body(data: encodeBody(data: params))
        case let .complete(params: params):
            return .body(data: encodeBody(data: params))
        case let .favouriteWO(params), let .favouriteTP(params), let .favouriteExercise(params), let .favouriteSkill(params):
            return .body(data: encodeBody(data: params))
        default:
            return .none
        }
    }
    
    case workoutLibrary
    case dashBoardData
    case equipments
    case favourites
    case favouriteWO(params: FavouriteList)
    case favouriteTP(params: FavouriteList)
    case favouriteExercise(params: FavouriteList)
    case favouriteSkill(params: FavouriteList)
    case workoutDetail(params: WorkoutsParams.WorkoutsDetail)
    case workoutOfTheDay
    case like(params: LikeDislikeParam)
    case complete(params: WorkoutsParams.Complete)
}
