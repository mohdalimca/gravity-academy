//
//  ExerciseRequest.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 04/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum ExerciseRequest: RequestRepresentable {
    
    var method: HTTPMethod {
        return .post
    }
    
    var endpoint: String {
        switch self {
        case .exerciseLibrary:
            return "exercise/exercise_library"
        case .workoutLibrary:
            return "exercise/workout_library"
        case .workoutDetail:
            return "exercise/workout_detail"
        case .workoutOfTheDay:
            return "exercise/wo_of_the_day"
        case .filter:
            return "exercise/exercise_library"
        case .like:
            return "exercise/toggle_like"
        }
    }
    
    var parameters: Parameters {
        switch self {
        case let .like(params):
            return .body(data: encodeBody(data: params))
        case let .filter(params):
            return .body(data: encodeBody(data: params))
        default:
            return .none
        }
    }
    
    case exerciseLibrary
    case workoutLibrary
    case workoutDetail
    case workoutOfTheDay
    case like(params: LikeDislikeParam)
    case filter(params: ExerciseParams.Filter)
}
