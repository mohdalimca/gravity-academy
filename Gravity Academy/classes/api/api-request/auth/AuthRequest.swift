//
//  AuthRequest.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 09/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
enum AuthRequests:RequestRepresentable {
    var method: HTTPMethod {
        return .post
    }
    
    var endpoint: String {
        switch self {
        case .masterData:
            return "auth/get_master_data"
        case .dashBoardData:
            return "auth/get_master_data"
        case .register:
            return "auth/signup"
        case .login:
            return "auth/login"
        case .forgot:
            return "auth/forgot_password"
        case .validateOTP:
            return "auth/validate_otp"
        case .validateEmail:
            return "auth/resend_email_verification_code"
        case .validateForotOTP:
            return "auth/resend_email_verification_code"
        case .newPassword:
            return "auth/change_password"
        case .getProfile:
            return "my_profile/get_my_profile"
        case .updateProfile:
            return "my_profile/update_profile"
        case .profileImage:
            return "my_profile/do_upload"
        case .completeWorkout:
            return "exercise/completed_wo"
        case .completeSkill:
            return "exercise/completed_wo"
        case .completeProgram:
            return "exercise/completed_wo"
        case .logout:
            return "auth/logout"
        }
    }
    
    var parameters: Parameters {
        switch self {
        case let .register(params):
            return .body(data: encodeBody(data: params))
        case let .login(params):
            return .body(data: encodeBody(data: params))
        case let .forgot(params):
            return .body(data: encodeBody(data: params))
        case let .validateOTP(params):
            return .body(data: encodeBody(data: params))
        case let .validateEmail(params):
            return .body(data: encodeBody(data: params))
        case let .validateForotOTP(params):
            return .body(data: encodeBody(data: params))
        case let .newPassword(params):
            return .body(data: encodeBody(data: params))
        case let .completeWorkout(params):
            return .body(data: encodeBody(data: params))
        case let .completeSkill(params):
            return .body(data: encodeBody(data: params))
        case let .completeProgram(params):
            return .body(data: encodeBody(data: params))
        case let .logout(params):
            return .body(data: encodeBody(data: params))
        default : return .none
        }
    }
    
    case masterData
    case dashBoardData
    case logout(params:UserParams.Logout)
    case register(params:UserParams.Register)
    case login(params:UserParams.Login)
    case forgot(params:UserParams.Forgot)
    case validateOTP(params:UserParams.ValidateOTP)
    case validateEmail(params:UserParams.ValidateEmail)
    case validateForotOTP(params:UserParams.ValidateEmail)
    case newPassword(params:UserParams.NewPassword)
    case getProfile
    case updateProfile
    case profileImage
    case completeWorkout(params:WorkoutsParams.CompleteWokrout)
    case completeProgram(params:ProgramsParams.CompleteProgram)
    case completeSkill(params:TechniquesParam.CompleteSkill)
}
