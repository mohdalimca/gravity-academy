//
//  TechniquesRequest.swift
//  Gravity Academy
//
//  Created by Apple on 16/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

enum TechniquesRequest: RequestRepresentable {
    
    var method: HTTPMethod {
        return .post
    }
    
    var endpoint: String {
        switch self {
        case .list:
            return "exercise/skill_list"
        case .like:
            return "exercise/toggle_like"
        case .skillWeeks:
            return "exercise/skill_weeks"
        case .skillWeeekDetail:
            return "exercise/skill_week_detail"
        }
    }
    
    var parameters: Parameters {
        switch self {
        case let .like(params):
            return .body(data: encodeBody(data: params))
        case let .skillWeeks(params):
            return .body(data: encodeBody(data: params))
        case let .skillWeeekDetail(params):
            return .body(data: encodeBody(data: params))
        default:
            return .none
        }
    }
    
    case list
    case like(params: LikeDislikeParam)
    case skillWeeks(params:TechniquesParam.SkillWeeks)
    case skillWeeekDetail(params:TechniquesParam.SkillWeekDetail)
}

