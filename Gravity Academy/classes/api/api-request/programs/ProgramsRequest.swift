//
//  ProgramsRequest.swift
//  Gravity Academy
//
//  Created by Apple on 24/06/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

enum ProgramsRequest: RequestRepresentable {
    
    var method: HTTPMethod {
        return .post
    }
    
    var endpoint: String {
        switch self {
        case .library:
            return "exercise/tp_list"
        case .like:
            return "exercise/toggle_like"
        case .programWeeks:
            return "exercise/tp_weeks"
        case .programWeekData:
            return "exercise/tp_week_detail"
        }
    }
    
    var parameters: Parameters {
        switch self {
        case let .like(params):
            return .body(data: encodeBody(data: params))
        case let .programWeeks(params):
            return .body(data: encodeBody(data: params))
        case let .programWeekData(params):
            return .body(data: encodeBody(data: params))
        default:
            return .none
        }
    }
    
    case library
    case programWeeks(params:ProgramsParams.Weeks)
    case programWeekData(params:ProgramsParams.WeekData)
    case like(params: LikeDislikeParam)
}

