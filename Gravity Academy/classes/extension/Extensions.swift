//
//  Extensions.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import UIKit
import NotificationBannerSwift
import NVActivityIndicatorView
import AVFoundation
import AVKit

extension UITableView {
    private struct AssociatedObjectKey {
        static var registeredCells = "registeredCells"
    }
    
    private var registeredCells: Set<String> {
        get {
            if objc_getAssociatedObject(self, &AssociatedObjectKey.registeredCells) as? Set<String> == nil {
                self.registeredCells = Set<String>()
            }
            return objc_getAssociatedObject(self, &AssociatedObjectKey.registeredCells) as! Set<String>
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedObjectKey.registeredCells, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func registerReusable<T: UITableViewCell>(_: T.Type) where T: Reusable {
        let bundle = Bundle(for: T.self)
        
        if bundle.path(forResource: T.reuseIndentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: T.reuseIndentifier, bundle: bundle)
            register(nib, forCellReuseIdentifier: T.reuseIndentifier)
        } else {
            register(T.self, forCellReuseIdentifier: T.reuseIndentifier)
        }
    }
    
    func dequeueReusable<T: UITableViewCell>(_ indexPath: IndexPath) -> T where T: Reusable {
        if registeredCells.contains(T.reuseIndentifier) == false {
            registeredCells.insert(T.reuseIndentifier)
            registerReusable(T.self)
        }
        
        guard let reuseCell = self.dequeueReusableCell(withIdentifier: T.reuseIndentifier, for: indexPath) as? T else { fatalError("Unable to dequeue cell with identifier \(T.reuseIndentifier)") }
        return reuseCell
    }
    
    func registerReusable<T: UITableViewCell>(_: T.Type) where T: ReusableReminder {
        let bundle = Bundle(for: T.self)
        
        if bundle.path(forResource: T.reuseIndentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: T.reuseIndentifier, bundle: bundle)
            register(nib, forCellReuseIdentifier: T.reuseIndentifier)
        } else {
            register(T.self, forCellReuseIdentifier: T.reuseIndentifier)
        }
    }
    
    func dequeueReusable<T: UITableViewCell>(_ indexPath: IndexPath) -> T where T: ReusableReminder {
        if registeredCells.contains(T.reuseIndentifier) == false {
            registeredCells.insert(T.reuseIndentifier)
            registerReusable(T.self)
        }
        
        guard let reuseCell = self.dequeueReusableCell(withIdentifier: T.reuseIndentifier, for: indexPath) as? T else { fatalError("Unable to dequeue cell with identifier \(T.reuseIndentifier)") }
        return reuseCell
    }
}

extension UICollectionView {
    private struct AssociatedObjectKey {
        static var registeredCells = "registeredCells"
    }
    
    private var registeredCells: Set<String> {
        get {
            if objc_getAssociatedObject(self, &AssociatedObjectKey.registeredCells) as? Set<String> == nil {
                self.registeredCells = Set<String>()
            }
            return objc_getAssociatedObject(self, &AssociatedObjectKey.registeredCells) as! Set<String>
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedObjectKey.registeredCells, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func registerReusable<T: UICollectionViewCell>(_: T.Type) where T: Reusable {
        let bundle = Bundle(for: T.self)
        
        if bundle.path(forResource: T.reuseIndentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: T.reuseIndentifier, bundle: bundle)
            register(nib, forCellWithReuseIdentifier: T.reuseIndentifier)
            
        } else {
            register(T.self, forCellWithReuseIdentifier: T.reuseIndentifier)
        }
    }
    
    func dequeueReusable<T: UICollectionViewCell>(_ indexPath: IndexPath) -> T where T: Reusable {
        if registeredCells.contains(T.reuseIndentifier) == false {
            registeredCells.insert(T.reuseIndentifier)
            registerReusable(T.self)
        }
        guard let reuseCell = self.dequeueReusableCell(withReuseIdentifier: T.reuseIndentifier, for: indexPath) as? T else {
            fatalError("Unable to dequeue cell with identifier \(T.reuseIndentifier)")
        }
        return reuseCell
    }
    
    func registerReusable<T: UICollectionViewCell>(_: T.Type) where T: ReusableReminder {
        let bundle = Bundle(for: T.self)
        
        if bundle.path(forResource: T.reuseIndentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: T.reuseIndentifier, bundle: bundle)
            register(nib, forCellWithReuseIdentifier: T.reuseIndentifier)
            
        } else {
            register(T.self, forCellWithReuseIdentifier: T.reuseIndentifier)
        }
    }
    
    func dequeueReusable<T: UICollectionViewCell>(_ indexPath: IndexPath) -> T where T: ReusableReminder {
        if registeredCells.contains(T.reuseIndentifier) == false {
            registeredCells.insert(T.reuseIndentifier)
            registerReusable(T.self)
        }
        guard let reuseCell = self.dequeueReusableCell(withReuseIdentifier: T.reuseIndentifier, for: indexPath) as? T else {
            fatalError("Unable to dequeue cell with identifier \(T.reuseIndentifier)")
        }
        return reuseCell
    }
}

extension UIView {
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func addShadowToView() {
        self.layer.backgroundColor = UIColor.clear.cgColor
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1.5)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowRadius = 10
    }
    
    func addShadowAround( shadowSize: CGFloat, color: UIColor) {
        DispatchQueue.main.async {
            let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                       y: 0,
                                                       width: self.layer.frame.size.width + (shadowSize),
                                                       height: self.layer.frame.size.height + shadowSize))
            self.layer.masksToBounds = false
            
            self.layer.shadowColor = color.cgColor
            self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            self.layer.shadowOpacity = 0.05
            self.layer.shadowPath = shadowPath.cgPath
        }
    }
    
    func createDashedLine(from point1: CGPoint, to point2: CGPoint, color: UIColor, strokeLength: NSNumber, gapLength: NSNumber, width: CGFloat) {
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = width
        shapeLayer.lineDashPattern = [strokeLength, gapLength]
        
        let path = CGMutablePath()
        path.addLines(between: [point1, point2])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
    
    func setGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedScreen(tap:)))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
    }
    
    @objc func tappedScreen(tap:UITapGestureRecognizer) {
        self.endEditing(true)
    }
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    //    @IBInspectable var cornerRadius: CGFloat {
    //        get {
    //            return layer.cornerRadius
    //        } set {
    //            layer.cornerRadius = newValue
    //            layer.masksToBounds = newValue > 0
    //        }
    //    }
    
}

extension UIViewController: NVActivityIndicatorViewable {
    static func from<T>(storyboard: Storyboard) -> T {
        
        guard let controller = UIStoryboard(name: storyboard.rawValue, bundle: nil).instantiateInitialViewController() as? T else {
            fatalError("unable to instantiate view controller")
        }
        
        return controller
    }
    
    static func from<T>(from storyboard: Storyboard, with identifier: StoryboardIdentifier) -> T {
        guard let controller = UIStoryboard(name: storyboard.rawValue, bundle: nil).instantiateViewController(withIdentifier: identifier.rawValue) as? T else {
            fatalError("unable to instantiate view controller")
        }
        
        return controller
    }
    
    func showBannerWith(text:String, style:BannerStyle ) {
        let banner = StatusBarNotificationBanner(title: text, style: style)
        banner.show()
    }
    
    func gravityAlert(controller: UIViewController) {
        let alert = GravityAcademyAlert(nibName: "GravityAcademyAlert", bundle: nil)
        if !GravityAcademyAlertData.isDouble {
//alert.delegate = (controller as! MemberShipViewController)
        } else {
            alert.alertTitle = "ARE YOU SURE?"
            alert.alertMessage = "Do you want to logout?"
            alert.delegate = (controller as! SettingsViewController)
        }
        
        let pop = CustomPopup.shared.showPopup(controller: alert, buttons: [])
        controller.present(pop, animated: true, completion: nil)
    }
    
    func restAlert(controller: UIViewController) {
        let alert = RestAlert(nibName: "RestAlert", bundle: nil)
        alert.delegate = (controller as! SessionViewController)
        let pop = CustomPopup.shared.showPopup(controller: alert, buttons: [])
        controller.present(pop, animated: true, completion: nil)
    }
    
    func startAnimation() {
        startAnimating(type: .circleStrokeSpin)
    }
    
    func playVideo(_ url:URL) {
        let player = AVPlayer(url: url)
        let playerVC = AVPlayerViewController()
        playerVC.player = player
        present(playerVC, animated: true) {
            playerVC.player?.play()
        }
    }
}

extension String {
    
    var isInteger: Bool { return Int(self) != nil }
    var isDouble: Bool { return Double(self) != nil }
    
    var databaseToUnix: TimeInterval {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: self)
        return Date().timeIntervalSince(date!)
    }
    
    func gravityDate(format: String = "dd. MMM yyyy") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date!)
    }
    
    
    func journalFormattedDate(format: String = "HH:mm") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date!)
    }
    
    func imageFromBase64() -> UIImage? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return UIImage(data: data)
    }
    
    func toDouble() -> Double {
        return NumberFormatter().number(from: self)?.doubleValue ?? 0
    }
    
    var toInt: Int {
        return NumberFormatter().number(from: self)?.intValue ?? 0
    }
    
    func getSizeOfText(_ width:CGFloat, _ height:CGFloat) -> CGSize {
        let lbl = UILabel()
        lbl.text = self
        lbl.textAlignment = .center
        lbl.sizeToFit()
        return CGSize(width: lbl.frame.size.width + width, height: height)
    }
    
    func size(OfFont font: UIFont) -> CGSize {
        return  self.size(withAttributes:[.font: font])
    }
}

extension TimeInterval {
    var unixToDatabase: String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return dateFormatter.string(from: date)
    }
}

extension Calendar {
    static let gregorian = Calendar(identifier: .gregorian)
}

extension Date {
    var startOfDay: Date {
        let calendar = Calendar.current
        let unitFlags = Set<Calendar.Component>([.year, .month, .day])
        let components = calendar.dateComponents(unitFlags, from: self)
        return calendar.date(from: components)!
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        let date = Calendar.current.date(byAdding: components, to: self.startOfDay)
        return date!
    }
    
    var startOfWeek: Date {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
    }
    
    var endOfWeek: Date {
        var components = DateComponents()
        components.day = 7
        return Calendar.gregorian.date(byAdding: components, to: self.startOfWeek)!
    }
    
    var startOfMonth: Date {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.year, .month], from: Calendar.gregorian.startOfDay(for: self)))!
    }
    
    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        return Calendar.gregorian.date(byAdding: components, to: self.startOfMonth)!
    }
    
    var startOfYear: Date {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.year], from: Calendar.gregorian.startOfDay(for: self)))!
    }
    
    var endOfYear: Date {
        var components = DateComponents()
        components.year = 1
        return Calendar.gregorian.date(byAdding: components, to: self.startOfYear)!
    }
    
    var currentMonthNumber: Int {
        let calendar = Calendar.current
        let compomnents = calendar.dateComponents([.month], from: self)
        return compomnents.year ?? 0
    }
    
    func currentMonth() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        //        return dateFormatter.monthSymbols[currentMonthNumber - 1]
        return dateFormatter.string(from: self).uppercased()
    }
    
    var monthYear: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        return dateFormatter.string(from: self).capitalized
    }
    
    var currentYear: Int {
        let calendar = Calendar.current
        let compomnents = calendar.dateComponents([.year], from: self)
        return compomnents.year ?? 0
    }
    
    var dateFrom1970: Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.date(from: "01/01/1970")!
    }
    
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func adding(hours: Int) -> Date {
        return Calendar.current.date(byAdding: .hour, value: hours, to: self)!
    }
    
    func adding(days: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
    
    func adding(months: Int) -> Date {
        return Calendar.current.date(byAdding: .month, value: months, to: self)!
    }
    
    func adding(years: Int) -> Date {
        return Calendar.current.date(byAdding: .year, value: years, to: self)!
    }
    
    func UTCToLocal(outGoingFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = outGoingFormat
        
        return dateFormatter.string(from: self)
    }
    
    func getElapsedInterval() -> String {
        let interval = Calendar.current.dateComponents([.year, .month, .weekOfMonth, .day, .hour, .minute], from: self, to: Date())
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" :
                "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" :
                "\(month)" + " " + "months ago"
        } else if let week = interval.weekOfMonth, week > 0 {
            return week == 1 ? "\(week)" + " " + "week ago" :
                "\(week)" + " " + "weeks ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" :
                "\(day)" + " " + "days ago"
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago" :
                "\(hour)" + " " + "hours ago"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "minute ago" :
                "\(minute)" + " " + "minutes ago"
        } else {
            return "a moment ago"
        }
    }
    
    func journalCountFormattedDate(format: String = "HH:mm") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    func journalHeaderFormattedDate(format: String = "E , MMM d YYYY") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    var isCurrentDate : Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    func getDaysInMonth() -> Int{
        let calendar = Calendar.current
        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        let date = calendar.date(from: dateComponents)!
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        return numDays
    }
    
    func getRangeForCurrentMonth() -> String {
        var range = ""
        var date =  Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        range.append("\(formatter.string(from: date)) ⏤ ")
        date = Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: date)!
        range.append(formatter.string(from: date))
        return range.uppercased()
    }
    
    func getRangeForCurrentWeek() -> String {
        var range = ""
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return ""  }
        //        var date = gregorian.date(byAdding: .day, value: 1, to: sunday)
        var date = sunday
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        range.append("\(formatter.string(from: date)) ⏤ ")
        date = gregorian.date(byAdding: .day, value: 7, to: sunday)!
        range.append(formatter.string(from: date))
        return range.uppercased()
    }
    
    func getSummaryRangeForWeek() -> String {
        var range = ""
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return ""  }
        //        var date = gregorian.date(byAdding: .day, value: 1, to: sunday)
        var date = sunday
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d"
        range.append("\(formatter.string(from: date)) ⏤ ")
        date = gregorian.date(byAdding: .day, value: 7, to: sunday)!
        range.append(formatter.string(from: date))
        range.append(", \(currentYear)")
        return range.uppercased()
    }
    
    //    func getRangeForCurrentYear() -> String {
    //        let formatter = DateFormatter()
    //        formatter.dateFormat = "yyyy"
    //        let range = ("JAN \(formatter.string(from: CountViewModel.countDate)) ⏤ DEC \(formatter.string(from: CountViewModel.countDate))")
    //        return range
    //    }
    
    func getMonthNumber() -> String {
        let date =  Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
        let formatter = DateFormatter()
        formatter.dateFormat = "M"
        return formatter.string(from: date)
    }
    
    func counterDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.string(from: self)
    }
}

extension Double {
    func unixToUTC() -> Date {
        let unixTimestamp = self
        return Date(timeIntervalSince1970: unixTimestamp)
    }
    
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
        //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}

extension UIImage {
    func toBase64() -> String? {
        guard let imageData = self.pngData() else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func noir() -> UIImage {
        let context = CIContext(options: nil)
        let currentFilter = CIFilter(name: "CIPhotoEffectNoir")
        currentFilter!.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        let output = currentFilter!.outputImage
        let cgimg = context.createCGImage(output!, from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!, scale: scale, orientation: imageOrientation)
        return processedImage
    }
}

enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

extension UIImage {
    func base64(format: ImageFormat) -> String? {
        var imageData: Data?
        
        switch format {
        case .png: imageData = self.pngData()
        //case .jpeg(let compression): imageData = UIImageJPEGRepresentation(self, compression)
        case .jpeg(let compression): imageData = self.jpegData(compressionQuality: compression)
        }
        
        return imageData?.base64EncodedString()
    }
}


extension FloatingPoint {
    var isInt: Bool {
        return floor(self) == self
    }
}

extension UIColor {
    
    convenience init(hex: String, alpha:CGFloat = 1.0) {
        var hexInt: UInt32 = 0
        let scanner = Scanner(string: hex)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        scanner.scanHexInt32(&hexInt)
        
        let red = CGFloat((hexInt & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexInt & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexInt & 0xff) >> 0) / 255.0
        let alpha = alpha
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    convenience init(r: CGFloat, g:CGFloat, b:CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
    func isEqualTo(_ color: UIColor) -> Bool {
        var red1: CGFloat = 0, green1: CGFloat = 0, blue1: CGFloat = 0, alpha1: CGFloat = 0
        getRed(&red1, green:&green1, blue:&blue1, alpha:&alpha1)
        
        var red2: CGFloat = 0, green2: CGFloat = 0, blue2: CGFloat = 0, alpha2: CGFloat = 0
        color.getRed(&red2, green:&green2, blue:&blue2, alpha:&alpha2)
        
        return red1 == red2 && green1 == green2 && blue1 == blue2 && alpha1 == alpha2
    }
}


extension DateFormatter {
    static var formatter = DateFormatter()
}

extension UITableView {
    
    func isLast(for indexPath: IndexPath) -> Bool {
        let indexOfLastSection = numberOfSections > 0 ? numberOfSections - 1 : 0
        let indexOfLastRowInLastSection = numberOfRows(inSection: indexOfLastSection) - 1
        return indexPath.section == indexOfLastSection && indexPath.row == indexOfLastRowInLastSection
    }
}
extension UISwitch {
    
    func set(width: CGFloat, height: CGFloat) {
        
        let standardHeight: CGFloat = 31
        let standardWidth: CGFloat = 51
        
        let heightRatio = height / standardHeight
        let widthRatio = width / standardWidth
        
        transform = CGAffineTransform(scaleX: widthRatio, y: heightRatio)
    }
}

let imageCache = NSCache<AnyObject, AnyObject>()
extension UIImageView {
    func downloadImageFrom(urlString: String?, with placeHolder:UIImage) {
        let url = URL(string: urlString ?? "")
        self.image = nil
        // check for the cached image first
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = cachedImage
            return
        }
        self.image = placeHolder
        // otherwise cache the images to decrease the use of internet, new download of the image will be done
        if urlString != nil && urlString != "" {
            URLSession.shared.dataTask(with: url!) { (data, response, error) in
                if error != nil {
                    print(error as Any)
                    //                self.image = UIImage(named: "defaultUser-Male")
                    return
                }
                // downloaded the images succesfully
                DispatchQueue.main.async {
                    if let downloadedImage = UIImage(data: data!) {
                        imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                        self.image = downloadedImage
                    }
                }
            }.resume()
        }
    }
    
    func makeCircular() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.masksToBounds = true
    }
}

extension Notification.Name {
    static let reloadWorkoutOfTheDay = Notification.Name("reloadWorkoutOfTheDay")
    static let handleExercisLikeDislike = Notification.Name("handleExercisLikeDislike")
    static let handleWorkoutLikeDislike = Notification.Name("handleWorkoutLikeDislike")
    static let handleProgramLikeDislike = Notification.Name("handleProgramLikeDislike")
    static let handleTrainingLikeDislike = Notification.Name("handleTrainingLikeDislike")
    static let handleSkillsLikeDislike = Notification.Name("handleSkillsLikeDislike")
    static let reloadWorkoutLibrary = Notification.Name("reloadWorkoutLibrary")

}

class CustomShadowView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        if self.tag == 95 {
            DispatchQueue.main.async {
                
                let shadowSize : CGFloat = 0.1
                let shadowPath = UIBezierPath(roundedRect:CGRect(x: -shadowSize / 2,
                                                                 y: -shadowSize / 2,
                                                                 width: self.frame.size.width + shadowSize,
                                                                 height: self.frame.size.height + shadowSize),  cornerRadius: self.bounds.height/2)
                self.layer.cornerRadius = self.frame.height/2
                self.layer.masksToBounds = false
                self.layer.shadowColor = UIColor.darkGray.cgColor
                self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
                self.layer.shadowOpacity = 0.5
                self.layer.shadowPath = shadowPath.cgPath
            }
        } else if self.tag == 94 {
            DispatchQueue.main.async {
                           
                           let shadowSize : CGFloat = 0.1
                           let shadowPath = UIBezierPath(roundedRect:CGRect(x: -shadowSize / 6,
                                                                            y: -shadowSize / 6,
                                                                            width: self.frame.size.width + shadowSize,
                                                                            height: self.frame.size.height + shadowSize),  cornerRadius: self.bounds.height/6)
                           self.layer.cornerRadius = self.frame.height/6
                           self.layer.masksToBounds = false
                           self.layer.shadowColor = UIColor.darkGray.cgColor
                           self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
                           self.layer.shadowOpacity = 0.5
                           self.layer.shadowPath = shadowPath.cgPath
                       }
        } else if self.tag == 96 {
            DispatchQueue.main.async {
                
                let shadowSize : CGFloat = 0.1
                let shadowPath = UIBezierPath(roundedRect:CGRect(x: -shadowSize / 16,
                                                                 y: -shadowSize / 16,
                                                                 width: self.frame.size.width + shadowSize,
                                                                 height: self.frame.size.height + shadowSize),  cornerRadius: self.bounds.height/16)
                self.layer.cornerRadius = self.frame.height/16
                self.layer.masksToBounds = false
                self.layer.shadowColor = UIColor.darkGray.cgColor
                self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
                self.layer.shadowOpacity = 0.5
                self.layer.shadowPath = shadowPath.cgPath
            }
        } else {
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowOpacity = 0.3
            self.layer.shadowOffset = CGSize.zero
            self.layer.shadowRadius = 6
        }
    }
}
