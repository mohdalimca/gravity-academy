//
//  Scenes.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum Scenes: String {    
    case landing
    case nointernet
    case login
    case register
    case forgot
    case registrationPageController
    case gender
    case height
    case weight
    case fitness
    case goals
    case performance
    case userInfo
    case terms
    case contactus
    case home
    case homeTab
    case workouts
    case trainingPlan
    case skills
    case timeline
    case favourite
    case profile
    case membership
    case dayWorkout
    case programs
    case techniques
    case guide
    case settings
    case text
    case aboutus
    case logout
    case delete
    case workoutsList
    case workoutLibrary
    case programLibrary
    case programsList
    case techniquesList
    case techniqueDetail
    case newPassword
    case detail
    case dayWorkoutDetail
    case categoryList
    case verifyEmail
    case verifyOTP
    case support
    case notification
    case changePassword
    case edit
    case createWorkout
    case activityDetail
    case workoutDetail
    case session
    case startActivity
    case exercise
    case allExercise
    case savedExercise
    case filter
    case equipments
    case timer
    case athlete
    case gravityAcademy
    case academyCourse
    case proMember
    case ebook
    case ebookDetail
    case activate
    case completeWorkout
    case programDetail
    case techniqueWorkouts
    case username
    case email
    case name
    case country
    case state
    case city
    case bio
    case maxPullups
    case maxPushups
    case maxSquats
    case maxDips
    case version
    case muscle
    case favouritesList
    case favouriteWorkout
    case favouriteSkills
    case favouriteTrainingPlan
    case favouriteExercise
    case browser
}

enum Storyboard: String {
    case landing = "Landing"
    case login = "Login"
    case register = "Registration"
    case forgot = "ForgotPassword"
    case home = "Home"
    case workouts = "Workouts"
    case programs = "Programs"
    case techniqueGuide = "TechniqueGuide"
    case settings = "Settings"
    case common = "Common"
    case exercise = "Exercise"
    case ebook = "Ebook"
    case gravityAcademy = "GravityAcademy"
    case favourite = "Favourite"
}

enum StoryboardIdentifier: String {
    case landing
    case nointernet
    case login
    case register
    case forgot
    case home
    case homeTab
    case workouts
    case trainingPlan
    case skills
    case timeline
    case favourite
    case profile
    case membership
    case registrationPageController
    case gender
    case height
    case weight
    case fitness
    case goals
    case performance
    case userInfo
    case terms
    case contactus
    case text
    case settings
    case aboutus
    case logout
    case delete
    case workoutsList
    case workoutLibrary
    case programLibrary
    case programsList
    case techniquesList
    case techniqueDetail
    case newPassword
    case detail
    case categoryList
    case verifyEmail
    case verifyOTP
    case support
    case changePassword
    case edit
    case createWorkout
    case activityDetail
    case workoutDetail
    case session
    case startActivity
    case exercise
    case allExercise
    case savedExercise
    case filter
    case equipments
    case timer
    case athlete
    case gravityAcademy
    case academyCourse
    case proMember
    case ebook
    case ebookDetail
    case activate
    case completeWorkout
    case programDetail
    case techniqueWorkouts
    case favouritesList
    case browser
}

