//
//  Toggle.swift
//  Treebetty
//
//  Created by Marlon Monroy on 4/20/19.
//  Copyright © 2019 Treebetty, LLC. All rights reserved.
//

import UIKit

final class Toggle: UIControl {
    private let generator = UISelectionFeedbackGenerator()
    var buttons = [UIButton]()
    var selector: UIView!
    
    var indexPos: Int? // ((Int) -> Void)?
    
    @IBInspectable
    var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    var commaSeparatedButtonTitles: String = "" {
        didSet {
            updateView()
        }
    }
    
    // Default Color
    @IBInspectable
    var textColor: UIColor = .black {
        didSet {
            updateView()
        }
    }
    
    // Selector Color
    @IBInspectable
    var selectorColor: UIColor = .red {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable
    var selectorTextColor: UIColor = .blue {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable
    var fontSize: CGFloat = 12 {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable
    var fontName: String = "Roboto-Regular" {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        buttons.removeAll()
        subviews.forEach { $0.removeFromSuperview() }
        buttons = commaSeparatedButtonTitles.components(separatedBy: ",").map { (title) -> UIButton in
            let button = UIButton(type: .system)
            button.setTitle(title, for: .normal)
            button.setTitleColor(textColor, for: .normal)
            button.titleLabel?.font = UIFont(name: fontName, size: fontSize)
            button.contentHorizontalAlignment = .center
            button.addTarget(self, action: #selector(buttonTapped(button:)), for: .touchUpInside)
            return button
        }
        
        let selectorWidth = frame.width / CGFloat(buttons.count)
        selector = UIView(frame: CGRect(x: 0, y: 0, width: selectorWidth, height: frame.height))
        selector.layer.cornerRadius = frame.height / 2

        selector.backgroundColor = selectorColor
        
        addSubview(selector)
        
        buttons[0].setTitleColor(selectorTextColor, for: .normal)
        
        let sv = UIStackView(arrangedSubviews: buttons)
        sv.axis = .horizontal
        sv.alignment = .fill
        sv.distribution = .fillEqually
        addSubview(sv)
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.topAnchor.constraint(equalTo: topAnchor).isActive = true
        sv.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        sv.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        sv.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    override func draw(_: CGRect) {
        layer.cornerRadius = frame.height / 2
        updateView()
    }
    
    // Button function
    @objc func buttonTapped(button: UIButton) {
        self.selector.isHidden = false
        generator.selectionChanged()
        for (buttonIndex, btn) in buttons.enumerated() {
            btn.setTitleColor(textColor, for: .normal)
            
            if btn == button {
                let selectorStartPosition = frame.width / CGFloat(buttons.count) * CGFloat(buttonIndex)
                UIView.animate(withDuration: 0.3, animations: {
                    self.selector.frame.origin.x = selectorStartPosition
                })
                
                btn.setTitleColor(selectorTextColor, for: .normal)
                indexPos = buttonIndex
                //            indexPos!(buttonIndex)
            }
        }
        sendActions(for: .valueChanged)
    }
    
    func setSelected(index: Int) {
      
        DispatchQueue.main.async {
            self.selector.isHidden = false
            self.buttons[index].setTitleColor(self.textColor, for: .normal)
            let selectorStartPosition = self.frame.width / CGFloat(self.buttons.count) * CGFloat(index)
            self.selector.frame.origin.x = selectorStartPosition
            self.buttons[index].setTitleColor(self.selectorTextColor, for: .normal)
            self.indexPos = index
        }
    }
    
    func hideSelector() {
        self.selector.isHidden = true
    }
    
}
