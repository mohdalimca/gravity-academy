//
//  CustomPopup.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 17/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import PopupDialog

class CustomPopup {
    static let shared = CustomPopup()
    
    func showPopup(controller: UIViewController, buttons: [PopupDialogButton]) -> PopupDialog {
        let popup: PopupDialog = PopupDialog(viewController: controller, buttonAlignment: .horizontal, transitionStyle: .fadeIn, tapGestureDismissal: false)
        
        let containerAppearance = PopupDialogContainerView.appearance()
        containerAppearance.cornerRadius = 20
        
//        if BackgroundViewModel.background == .dark {
//            containerAppearance.backgroundColor = .grayBackground
//        }
        
        popup.addButtons(buttons)
        return popup
    }
    
}
