
//
//  Responses.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 10/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

struct SuccessResponseModel:Codable {
    var serviceName, message, globalError, error: String?
    var data: DataResponse?
    var responseCode: Int?
    
    enum CodingKeys: String, CodingKey {
        case serviceName = "service_name"
        case message
        case globalError = "global_error"
        case error, data
        case responseCode = "response_code"
    }
}

struct DataResponse:Codable {
    var loginData: LoginData?
    var masterData: MasterData?
    var woOfTheDay: [WoOfTheDay]?
    var woLIB: [WoLIB]?
    var woDetail: [WoDetail]?
    var exerciseLIB: ExerciseDataClass?
    var blank: [JSONNull]?
    var status: Int?
    var homeData: HomeData?
    var trainingPlanList: [TrainingPlan]?
    var likeList: LikeList?
    var equipments: [Equipment]?
    var trainingPlanWeeks: [TrainingPlanWeek]?
    var trainingPlanWeekDetail: [TrainingPlanWeekDetail]?
    var skillList: [SkillList]?
    var skillWeeks: [SkillWeek]?
    var skillWeekDetail: [SkillWeekDetail]?
    var myProfile: MyProfile?
    var favourites: Fav?
    var favouriteExercise: FavouriteExercise?
    var favouriteTrainingPlan: FavouriteTrainingPlan?
    var favouriteWorkout: FavouriteWorkout?
    var favouriteSkills: FavouriteSkills?
    var completedWorkouts: FavouriteWorkout?
    
    enum CodingKeys: String, CodingKey {
        case loginData = "login_data"
        case masterData = "master_data"
        case woOfTheDay = "wo_of_the_day"
        case woLIB = "wo_lib"
        case woDetail = "wo_detail"
        case exerciseLIB = "exercise_lib"
        case blank, status
        case equipments
        case homeData = "home_data"
        case trainingPlanList = "tp_list"
        case likeList = "like_list"
        case trainingPlanWeeks = "tp_weeks"
        case trainingPlanWeekDetail = "tp_week_detail"
        case skillList = "skill_list"
        case skillWeeks = "skill_weeks"
        case skillWeekDetail = "skill_week_detail"
        case myProfile = "my_profile"
        case favourites = "fav"
        case favouriteExercise = "exercise"
        case favouriteWorkout = "wo"
        case favouriteSkills = "skills"
        case favouriteTrainingPlan = "tp"
        case completedWorkouts
    }
}


// MARK:- Workout of the Day
struct WoOfTheDay: Codable {
    var workoutID, workoutCategoryID, title: String?
    var woOfTheDayDescription, woImgURL: String?
    var workoutGoal, muscleGroups, status, addedDate: String?
    var likeCount, isWoOfTheDay, isLike: String?
    var workoutCategoryTitle:String?
    
    enum CodingKeys: String, CodingKey {
        case workoutID = "workout_id"
        case workoutCategoryID = "workout_category_id"
        case title
        case woOfTheDayDescription = "description"
        case workoutGoal = "workout_goal"
        case muscleGroups = "muscle_groups"
        case woImgURL = "wo_img_url"
        case status
        case addedDate = "added_date"
        case likeCount = "like_count"
        case isWoOfTheDay = "is_wo_of_the_day"
        case isLike = "is_like"
        case workoutCategoryTitle = "wo_category_title"
    }
}

struct WoLIB: Codable {
    var workoutCategoryID, title, status: String?
    var workouts: [Workouts]?
    
    enum CodingKeys: String, CodingKey {
        case workoutCategoryID = "workout_category_id"
        case title, status, workouts
    }
}

struct Workouts: Codable {
    var workoutID, workoutCategoryID, title, woDescription: String?
    var workoutGoal, muscleGroups, status, addedDate: String?
    var likeCount, isWoOfTheDay: String?
    var woImgURL: String?
    var woImg, type, isLike, workoutCategoryTitle: String?
    var woCompleteDate, isWoComplete, muscleGroupsTitle: String?
    
    enum CodingKeys: String, CodingKey {
        case workoutID = "workout_id"
        case workoutCategoryID = "workout_category_id"
        case title
        case woDescription = "description"
        case workoutGoal = "workout_goal"
        case muscleGroups = "muscle_groups"
        case muscleGroupsTitle = "muscle_groups_title"
        case status
        case addedDate = "added_date"
        case likeCount = "like_count"
        case isWoOfTheDay = "is_wo_of_the_day"
        case woImgURL = "wo_img_url"
        case woImg = "wo_img"
        case type
        case isLike = "is_like"
        case workoutCategoryTitle = "wo_category_title"
        case isWoComplete = "is_wo_complete"
        case woCompleteDate = "wo_complete_date"
    }
}
struct WoDetail: Codable {
    var roundTitle, noOfSets: String?
    var exercise: [Exercise]?
    
    enum CodingKeys: String, CodingKey {
        case roundTitle = "round_title"
        case noOfSets = "no_of_sets"
        case exercise
    }
}


struct Workout: Codable {
    var workoutID, workoutCategoryID, title: String?
    var workoutDescription: String?
    var workoutGoal, muscleGroups, status, addedDate: String?
    var likeCount: String?
    
    enum CodingKeys: String, CodingKey {
        case workoutID = "workout_id"
        case workoutCategoryID = "workout_category_id"
        case title
        case workoutDescription = "description"
        case workoutGoal = "workout_goal"
        case muscleGroups = "muscle_groups"
        case status
        case addedDate = "added_date"
        case likeCount = "like_count"
    }
}

struct LoginData: Codable {
    var sessionkey: String?
}

struct InformationResponseModel: Codable {
    var serviceName, message, globalError, error: String
    var data: [JSONAny]
    var responseCode: Int
    
    enum CodingKeys: String, CodingKey {
        case serviceName = "service_name"
        case message
        case globalError = "global_error"
        case error, data
        case responseCode = "response_code"
    }
}

// MARK:- Master Data
struct MasterData: Codable {
    var fitnessLevel: [FitnessLevel]
    var goals: [Goal]
    var muscleGroup: [MuscleGroup]
    
    enum CodingKeys: String, CodingKey {
        case fitnessLevel = "fitness_level"
        case goals
        case muscleGroup = "muscle_group"
    }
}

struct FitnessLevel: Codable {
    var fnLevelID, title, fitnessLevelDescription: String
    
    enum CodingKeys: String, CodingKey {
        case fnLevelID = "fn_level_id"
        case title
        case fitnessLevelDescription = "description"
    }
}

struct Goal: Codable {
    var goalID, title, description: String
    
    enum CodingKeys: String, CodingKey {
        case goalID = "goal_id"
        case title
        case description = "description"
    }
}

struct MuscleGroup: Codable {
    var muscleID, title, status: String
    
    enum CodingKeys: String, CodingKey {
        case muscleID = "muscle_id"
        case title, status
    }
}


// MARK: - UserProfile
struct UserProfile: Codable {
    var userID, userUniqueID: String
    var firstName, lastName: JSONAny?
    var userName, balance, referralCode, email: String
    var tagline, teamID, phoneNo, panVerified: JSONAny?
    var panNo: JSONAny?
    var status, dob: String
    var phoneVerfied: JSONAny?
    var image: String
    var currency: String
    var profileStatus, isProfileComplete: Int
    var requireOtp: Bool
    var phonecode, badge: JSONAny?
    
    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case userUniqueID = "user_unique_id"
        case firstName = "first_name"
        case lastName = "last_name"
        case userName = "user_name"
        case balance
        case referralCode = "referral_code"
        case email, tagline
        case teamID = "team_id"
        case phoneNo = "phone_no"
        case panVerified = "pan_verified"
        case panNo = "pan_no"
        case status, dob
        case phoneVerfied = "phone_verfied"
        case image, currency
        case profileStatus = "profile_status"
        case isProfileComplete = "is_profile_complete"
        case requireOtp = "require_otp"
        case phonecode, badge
    }
}

// MARK: - API Error Response Model
struct APIErrorResponseData: Codable {
    var responseCode: Int
    var error, serviceName, message, globalError: String
    var data: [JSONAny]
    
    enum CodingKeys: String, CodingKey {
        case responseCode = "response_code"
        case error
        case serviceName = "service_name"
        case message
        case globalError = "global_error"
        case data
    }
    
    static func from(data:Data) -> APIErrorResponseData? {
        return try? JSONDecoder().decode(APIErrorResponseData.self, from: data)
    }
}

struct OTPResponseModel: Codable {
    var serviceName, message, globalError,error: String
    var data: SessionResponseModel
    var responseCode: Int
    
    enum CodingKeys: String, CodingKey {
        case serviceName = "service_name"
        case message
        case globalError = "global_error"
        case error, data
        case responseCode = "response_code"
    }
}

// MARK: - Response Error
struct SessionResponseModel: Codable {
    var sessionkey: String?
}

// MARK: - All Exercise Libarary
struct ExerciseLibrary: Codable {
    var serviceName, message, globalError, error: String
    var data: ExerciseDataClass
    var responseCode: Int
    
    enum CodingKeys: String, CodingKey {
        case serviceName = "service_name"
        case message
        case globalError = "global_error"
        case error, data
        case responseCode = "response_code"
    }
}

struct ExerciseDataClass: Codable {
    var result: [Exercise]
    var total: String
}

// MARK: - Exercise Result
struct Exercise: Codable {
    var exerciseID, title, exerciseDescription, fnLevel: String?
    var muscleGroups, equipmentRequired, equipment: String?
    var videoURL: String?
    var mediaData: MediaData?
    var status, addedDate, resp, hold: String?
    var rest, workoutExerciseID, workoutDate, round: String?
    var noOfSets, isLike, isWoComplete, fitnessLevel: String?
    
    enum CodingKeys: String, CodingKey {
        case exerciseID = "exercise_id"
        case title
        case exerciseDescription = "description"
        case fnLevel = "fn_level"
        case muscleGroups = "muscle_groups"
        case equipmentRequired = "equipment_required"
        case equipment
        case videoURL = "video_url"
        case mediaData = "media_data"
        case status
        case addedDate = "added_date"
        case resp, hold, rest
        case workoutExerciseID = "workout_exercise_id"
        case workoutDate = "workout_date"
        case round
        case noOfSets = "no_of_sets"
        case isLike = "is_like"
        case isWoComplete = "is_wo_complete"
        case fitnessLevel = "fitness_level"
    }
}


// MARK: - MediaData
struct MediaData: Codable {
    var image: String?
    var title, videoID, mediaType: String?
    var duration: Int?
    var url: String?
    var thumbID: String?
    
    enum CodingKeys: String, CodingKey {
        case image, title
        case videoID = "video_id"
        case mediaType = "media_type"
        case duration, url
        case thumbID = "thumb_id"
    }
}

// MARK: - Home Dashboard Data
struct HomeData: Codable {
    var totalExercise, totalTrainingPlan, totalWorkout, totalSkills, dayWorkout, equipment: Int?
    
    enum CodingKeys: String, CodingKey {
        case totalExercise = "total_exercise"
        case totalTrainingPlan = "total_tp"
        case totalWorkout = "total_workout"
        case totalSkills = "total_skills"
        case equipment, dayWorkout
    }
}

// MARK: - Training Plans
struct TrainingPlan: Codable {
    var tpID, title, tpListDescription, fnLevel: String?
    var introVideoURL, status, addedDate: String?
    var tpImg, tpImgURL, isLike, fitnessLevel: String?
    var introMediaData: IntroMediaData?
    enum CodingKeys: String, CodingKey {
        case tpID = "tp_id"
        case title
        case tpListDescription = "description"
        case fnLevel = "fn_level"
        case introVideoURL = "intro_video_url"
        case introMediaData = "intro_media_data"
        case status
        case addedDate = "added_date"
        case tpImg = "tp_img"
        case tpImgURL = "tp_img_url"
        case isLike = "is_like"
        case fitnessLevel = "fitness_level"
    }
}

// MARK: - IntroMediaData
struct IntroMediaData: Codable {
    var image: String?
    var title, videoID, mediaType: String?
    var duration: Int?
    var url: String?
    var thumbID: String?
    
    enum CodingKeys: String, CodingKey {
        case image, title
        case videoID = "video_id"
        case mediaType = "media_type"
        case duration, url
        case thumbID = "thumb_id"
    }
}

// MARK: - LikeList
struct LikeList: Codable {
    var exercise: [LikeExercise]?
    var wo: [Workouts]?
    var tp: [TrainingPlan]?
}

struct LikeExercise: Codable {
    var exerciseID, title, exerciseDescription, fnLevel: String?
    var muscleGroups, equipmentRequired, equipment: String?
    var videoURL: String?
    var mediaData, status, addedDate, isLike: String?

    enum CodingKeys: String, CodingKey {
        case exerciseID = "exercise_id"
        case title
        case exerciseDescription = "description"
        case fnLevel = "fn_level"
        case muscleGroups = "muscle_groups"
        case equipmentRequired = "equipment_required"
        case equipment
        case videoURL = "video_url"
        case mediaData = "media_data"
        case status
        case addedDate = "added_date"
        case isLike = "is_like"
    }
}

// MARK: - Equipment
struct Equipment: Codable {
    var equipmentID, title, image: String?
    var link: String?
    var imageURL: String?
    
    enum CodingKeys: String, CodingKey {
        case equipmentID = "equipment_id"
        case title, image, link
        case imageURL = "image_url"
    }
}

// MARK: - Training Plan Week
struct TrainingPlanWeek: Codable {
    var tpWeekID, tpID, week, title: String?
    var status: String?
    
    enum CodingKeys: String, CodingKey {
        case tpWeekID = "tp_week_id"
        case tpID = "tp_id"
        case week, title, status
    }
}


// MARK: - Training Plan Week Detail
struct TrainingPlanWeekDetail: Codable {
    var tpWeeksWoID, tpWeekID, workoutID, tpID: String?
    var status, addedDate, title: String?
    var woImgURL: String?
    var woImg, workoutGoal, muscleGroups, fnLevelID: String?
    var fitnessLevel: String?

    enum CodingKeys: String, CodingKey {
        case tpWeeksWoID = "tp_weeks_wo_id"
        case tpWeekID = "tp_week_id"
        case workoutID = "workout_id"
        case tpID = "tp_id"
        case status
        case addedDate = "added_date"
        case title
        case woImgURL = "wo_img_url"
        case woImg = "wo_img"
        case workoutGoal = "workout_goal"
        case muscleGroups = "muscle_groups"
        case fnLevelID = "fn_level_id"
        case fitnessLevel = "fitness_level"
    }
}

// MARK: - SkillList
struct SkillList: Codable {
    var skillID, title, skillListDescription, fnLevel: String?
    var introVideoURL: String?
    var introMediaData: IntroMediaData?
    var status, addedDate, reqID, isLike: String?
    var fitnessLevel: String?

    enum CodingKeys: String, CodingKey {
        case skillID = "skill_id"
        case title
        case skillListDescription = "description"
        case fnLevel = "fn_level"
        case introVideoURL = "intro_video_url"
        case introMediaData = "intro_media_data"
        case status
        case addedDate = "added_date"
        case reqID = "req_id"
        case isLike = "is_like"
        case fitnessLevel = "fitness_level"
    }
}

// MARK: - SkillWeek
struct SkillWeek: Codable {
    var skillWeekID, skillID, week, title: String?
    var status: String?

    enum CodingKeys: String, CodingKey {
        case skillWeekID = "skill_week_id"
        case skillID = "skill_id"
        case week, title, status
    }
}

// MARK: - SkillWeekDetail
struct SkillWeekDetail: Codable {
    var skillWeeksWoID, skillWeekID, workoutID, skillID: String?
    var status, addedDate, title: String?
    var woImgURL: String?
    var woImg, workoutGoal, muscleGroups, fnLevelID: String?
    var fitnessLevel: String?

    enum CodingKeys: String, CodingKey {
        case skillWeeksWoID = "skill_weeks_wo_id"
        case skillWeekID = "skill_week_id"
        case workoutID = "workout_id"
        case skillID = "skill_id"
        case status
        case addedDate = "added_date"
        case title
        case woImgURL = "wo_img_url"
        case woImg = "wo_img"
        case workoutGoal = "workout_goal"
        case muscleGroups = "muscle_groups"
        case fnLevelID = "fn_level_id"
        case fitnessLevel = "fitness_level"
    }
}

// MARK: - MyProfile
struct MyProfile: Codable {
    var fitnessLevel, name, userID, userUniqueID: String?
    var email, userName, dob, password: String?
    var phoneNo, masterCountryID: String?
    var image: String?
    var status, statusReason, newPasswordKey, newPasswordRequested: String?
    var lastLogin, lastIP, addedDate, modifiedDate: String?
    var height, heightUnit, weight, weightUnit: String?
    var fnLevelID, goals, maxPullups, maxPushups: String?
    var maxSquats, maxDips, userGoals: String?

    enum CodingKeys: String, CodingKey {
        case fitnessLevel = "fitness_level"
        case name
        case userID = "user_id"
        case userUniqueID = "user_unique_id"
        case email
        case userName = "user_name"
        case dob, password
        case phoneNo = "phone_no"
        case masterCountryID = "master_country_id"
        case image, status
        case statusReason = "status_reason"
        case newPasswordKey = "new_password_key"
        case newPasswordRequested = "new_password_requested"
        case lastLogin = "last_login"
        case lastIP = "last_ip"
        case addedDate = "added_date"
        case modifiedDate = "modified_date"
        case height
        case heightUnit = "height_unit"
        case weight
        case weightUnit = "weight_unit"
        case fnLevelID = "fn_level_id"
        case goals
        case maxPullups = "max_pullups"
        case maxPushups = "max_pushups"
        case maxSquats = "max_squats"
        case maxDips = "max_dips"
        case userGoals = "User_goals"
    }
}

struct Fav: Codable {
    let wo: [Workouts]?
    let exercise: [Exercise]?
    let skills: [SkillList]?
    let tp: [TrainingPlan]?
}

struct FavouriteSkills: Codable {
    var skills: [SkillList]?
    var total: String?
    
    enum CodingKeys: String, CodingKey {
        case skills = "result"
        case total
    }
}

struct FavouriteWorkout: Codable {
    var workouts: [Workouts]?
    var total: String?
    
    enum CodingKeys: String, CodingKey {
        case workouts = "result"
        case total
    }
}

struct FavouriteTrainingPlan: Codable {
    var trainingPlan: [TrainingPlan]?
    var total: String?
    
    enum CodingKeys: String, CodingKey {
        case trainingPlan = "result"
        case total
    }
}

struct FavouriteExercise: Codable {
    var exercises: [Exercise]?
    var total: String?
    
    enum CodingKeys: String, CodingKey {
        case exercises = "result"
        case total
    }
}

struct CompletedWorkouts: Codable {
    var workouts: [Workouts]?
    let total: String?
}
