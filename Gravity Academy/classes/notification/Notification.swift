
//
//  Notification.swift
//  Gravity Academy
//
//  Created by Apple on 19/02/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import UserNotifications
import UIKit

import UserNotifications
import UIKit

class Notifications: NSObject,UNUserNotificationCenterDelegate {
   var registerCallback: ((Bool)->())?
   
   let api = UserService()
   override init() {
      super.init()
      UNUserNotificationCenter.current().delegate = self
   }
   
   func register() {
      UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {
         (granted, error) in
       
         self.registerCallback?(granted)
         guard granted else { return }
         self.notificationSettings()
      }
   }
   
   func notificationSettings() {
      UNUserNotificationCenter.current().getNotificationSettings { (settings) in
         guard settings.authorizationStatus == .authorized else { return }
         DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
         }
      }
   }
   
   func didRegister(with token:Data) {
      let tokenParts = token.map { data -> String in
         return String(format: "%02.2hhx", data) }
      let token = tokenParts.joined()
      print("Device Token: \(token)")
      api.registerPush(token: token)
   }
   
   func didFailed(with error:Error) {
       print("remote notifications", error)
   }
   
   func handleRemoteNotifications(from userInfo: [AnyHashable : Any]) {
      print("remote notifications")
      print(userInfo)
   }
   
   func handleRemoteNotifications(from launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
      if let notification = launchOptions?[.remoteNotification] as? [String: AnyObject] {
         let aps = notification["aps"] as! [String: AnyObject]
        print(aps)
      }
   }
}

extension Notifications {
   
   func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
      print("content", response.notification.request.content.userInfo)
    
      completionHandler()
   }
   
   func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
      
   }
   
   func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
     
   }
}
