
//
//  App.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 04/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import UIKit

class App {
    
    private var window: UIWindow?
    private var coordinator: Coordinator<Scenes>!
    private var homeCoordinator: HomeCoordinator!
    private var loginCoordinator: LoginCoordinator!
    
    
    init() {
        let router = Router()
        homeCoordinator = HomeCoordinator(router: router)
        loginCoordinator = LoginCoordinator(router: router)
    }
    
    func start(window: UIWindow) {
        self.window = window
        if UserStore.token != nil {
            homeCoordinator.start()
            window.rootViewController = homeCoordinator.toPresentable()
        } else {
            loginCoordinator.start()
            window.rootViewController = loginCoordinator.toPresentable()
        }
    }
}
