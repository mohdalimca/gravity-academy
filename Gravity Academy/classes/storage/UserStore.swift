//
//  UserStore.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 09/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
struct UserStore {
    private static let sessionkey = "sessionkey"
    private static let authkey = "api_auth_token"
    private static let idkey = "api_id_key"
    private static let appKey = "api_app_key"
    private static let pushTokenKey = "push_token"
    
    private static let fullOnboardedKey = "full_oboarded"
    private static let userIdKey = "user_id"
    private static let masterDataKey = "master_data"
    private static let usernameKey = "usernameKey"
    private static let userImageKey = "userImageKey"
    private static let genderKey = "gender_key"

    
    static var token: String? {
        return UserDefaults.standard.string(forKey: sessionkey)
    }
    
    static var app:String? {
        return UserDefaults.standard.string(forKey: appKey)
    }
    
    static var id:String? {
        return  UserDefaults.standard.string(forKey: idkey)
    }
    
    static var pushToken:String? {
        return UserDefaults.standard.string(forKey: pushTokenKey)
    }
    
    static var userID:String? {
        return UserDefaults.standard.string(forKey: userIdKey)
    }
    
    static var fullOnboarded:Bool? {
        return UserDefaults.standard.bool(forKey: fullOnboardedKey)
    }
    
    static var username:String? {
        return UserDefaults.standard.string(forKey: usernameKey)
    }
    
    static var userImage:String? {
        return UserDefaults.standard.string(forKey: userImageKey)
    }
    
    static func save(username:String) {
        UserDefaults.standard.set(id, forKey:usernameKey)
    }
    
    static func save(userImage:String) {
        UserDefaults.standard.set(id, forKey:userImageKey)
    }
    
    static func save(token:String) {
        UserDefaults.standard.set(token, forKey: sessionkey)
    }
    
    static func save(id:String) {
        UserDefaults.standard.set(id, forKey:idkey)
    }
        
    static func save(app:String) {
        UserDefaults.standard.set(app, forKey: appKey)
    }
    
    static func save(pushToken:String) {
        UserDefaults.standard.set(pushToken, forKey: pushTokenKey)
    }
    
    static func save(fullOnboarded:Bool) {
        UserDefaults.standard.set(fullOnboarded, forKey: fullOnboardedKey)
    }
    
    static func save(userID:String) {
        UserDefaults.standard.set(userID, forKey: userIdKey)
    }
    
    static var masterData: MasterData? {
        if let data =  UserDefaults.standard.data(forKey: masterDataKey) {
            return try! PropertyListDecoder().decode(MasterData.self, from: data)
        }
        return nil
    }
    
    static func save(masterData:MasterData) {
        if let data = try? PropertyListEncoder().encode(masterData) {
            UserDefaults.standard.set(data, forKey: masterDataKey)
        }
    }
    
    static func removeMaster() {
        UserDefaults.standard.removeObject(forKey: masterDataKey)
    }
    
    static func save(gender:String) {
        UserDefaults.standard.set(gender, forKey: genderKey)
    }
    
    static var gender:String? {
        return UserDefaults.standard.string(forKey: genderKey)
    }
}
