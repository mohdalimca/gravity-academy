//
//  RestAlert.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 19/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

protocol RestAlertDelegate: class {
    func didSelectButton()
}

class RestAlert: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    
    weak var delegate: RestAlertDelegate?
    var alertTitle:String?
    var alertMessage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
//        lblTitle.text = alertTitle
//        lblMessage.text = alertMessage
        btnDone.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
    }
    
    @objc func buttonPressed(sender: UIButton) {
        delegate?.didSelectButton()
        dismiss(animated: true, completion: nil)
    }
}

