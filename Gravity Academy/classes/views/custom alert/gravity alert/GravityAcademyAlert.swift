//
//  GravityAcademyAlert.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 17/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class GravityAcademyAlertData {
    static var isDouble: Bool = false
}

enum GravityAcademyAlertButtonAction: String {
    case ok
    case oksingle
}

protocol GravityAcademyAlertDelegate: class {
    func didSelect(action: GravityAcademyAlertButtonAction)
}

class GravityAcademyAlert: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOkSingle: UIButton!
    @IBOutlet weak var singleStack: UIStackView!
    @IBOutlet weak var doubleStack: UIStackView! {
        didSet {
            self.doubleStack.isHidden = true
        }
    }
    
    weak var delegate: GravityAcademyAlertDelegate?
    var isDouble: Bool!
    var alertTitle:String?
    var alertMessage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        
        lblTitle.text = alertTitle
        lblMessage.text = alertMessage
        [btnOK, btnCancel, btnOkSingle].forEach {
            $0?.layer.cornerRadius = 8
            $0?.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
        }
        
        if !GravityAcademyAlertData.isDouble {
            self.singleStack.isHidden = false
            self.doubleStack.isHidden = true
        } else {
            self.singleStack.isHidden = true
            self.doubleStack.isHidden = false
        }
    }
    
    @objc func buttonPressed(sender: UIButton) {
        switch sender {
        case btnOK:
            delegate?.didSelect(action: .ok)
        case btnOkSingle:
            delegate?.didSelect(action: .oksingle)
        default: break
        }
        dismiss(animated: true, completion: nil)
    }    
}
