//
//  AllExerciseProvider.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 04/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class AllExerciseServiceProvider: AllExerciseServiceProviderable {
    
    var delegate: AllExerciseServiceProviderDelegate?
    private let task = ExerciseTask()
    
    func exerciseLibrary() {
        task.exerciseLibrary(modeling: SuccessResponseModel.self) { [weak self] (resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .exerciseLibrary, with: resp, and: err)
            }
            guard let _ = resp else { return }
            self?.delegate?.completed(for: .exerciseLibrary, with: resp, and: err)
        }
    }
    
    func likeDislike(param:LikeDislikeParam) {
        task.exerciseLike(params: param, modeling: SuccessResponseModel.self) { [weak self] (resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .exerciseLikeDislike, with: resp, and: err)
            }
            guard let _ = resp else { return }
            self?.delegate?.completed(for: .exerciseLikeDislike, with: resp, and: err)
        }
    }
    
    func filter(param:ExerciseParams.Filter) {
        task.exerciseFilter(params: param, modeling: SuccessResponseModel.self) { [weak self] (resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .exerciseFilter, with: resp, and: err)
            }
            guard let _ = resp else { return }
            self?.delegate?.completed(for: .exerciseFilter, with: resp, and: err)
        }
    }

}
