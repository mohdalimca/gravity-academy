//
//  AllExerciseCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import  UIKit

final class AllExerciseCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: AllExerciseViewController = AllExerciseViewController.from(from: .exercise, with: .allExercise)

    override func start() {
        super.start()
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.delegateTable = self
        controller.router = self
    }
    
    private func startExercise(_ object: Exercise?) {
        let r = Router()
        let exercise = ExerciseCoordinator(router: r)
        add(exercise)
        exercise.delegate = self
        exercise.startWith(object!)
        router.present(exercise, animated: true)
    }
    
    private func startFilter() {
        let filter = FilterViewController()
        filter.router = self
        filter.transitioningDelegate = self
        filter.modalPresentationStyle = .custom
        filter.didSelectFilters = controller.didSelectFilters
        router.present(filter, animated: true) {
            filter.presentationController?.presentedView?.gestureRecognizers?[0].isEnabled = true
        }
//        router.present(filter, animated: true)
    }
    
    private func startcomplete() {
        let complete = CompleteWorkoutViewController()
        complete.router = self
        complete.transitioningDelegate = self
        complete.modalPresentationStyle = .custom
        router.present(complete, animated: true)
    }
}

extension AllExerciseCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .filter: startFilter()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension AllExerciseCoordinator: AllExerciseTableViewDelegate {
    func didSelectAtIndex(index: Int) {}
    
    func showDetailWith(object: Exercise) {
        startExercise(object)
    }
}

extension AllExerciseCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

extension AllExerciseCoordinator : UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return PresentationController(presentedViewController: presented, presenting: presenting)
    }
}

class PresentationController : UIPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        get {
            guard let theView = containerView else {
                return CGRect.zero
            }
            return CGRect(x: 0, y: safeAreaInsets.top, width: theView.bounds.width, height: theView.bounds.height - (safeAreaInsets.top + safeAreaInsets.bottom))
        }
    }
}
