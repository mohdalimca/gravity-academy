//
//  AllExerciseViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 04/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class AllExerciseViewModel {
    
    weak var view:AllExerciseViewRepresentable?
    var provider: AllExerciseServiceProviderable?
    var items: [Exercise] = []
    var fitnessLevel: String?
    var musclesID: String?
    var muscles: String?
    var equipment: String?

    
    init(provider: AllExerciseServiceProviderable) {
        provider.delegate = self
        self.provider = provider
    }
    
    func exerciseLibrary() {
        provider?.exerciseLibrary()
    }
    
    func exerciseFilter() {
        provider?.filter(param: ExerciseParams.Filter(equipment_required: equipment, fn_level_id: fitnessLevel, muscle_id: musclesID))
    }
    
    private func handleExercise(response: [Exercise], error:APIError?, action:AllExerciseAction) {
        DispatchQueue.main.async {
            if error != nil {
                guard let message = error?.responseData?.error else {
                    self.view?.onAction(.errorMessage(errorMessage))
                    return
                }
                self.view?.onAction(.errorMessage(message))
            } else {
                self.items = response
                self.view?.onAction(action)
            }
        }
    }
}

extension AllExerciseViewModel: AllExerciseServiceProviderDelegate {
    func completed<T>(for action: AllExerciseAction, with response: T?, and error: APIError?) {
        switch action {
        case .exerciseLibrary, .exerciseFilter:
            if let resp = response as? SuccessResponseModel {
                handleExercise(response: (resp.data?.exerciseLIB!.result)!, error: error, action:action)
            }
        default: break
        }
    }
}
