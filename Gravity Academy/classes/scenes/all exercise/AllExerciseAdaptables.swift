//
//  AllExerciseAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum AllExerciseAction {
    case requireField(_ text:String)
    case errorMessage(_ message:String)
    case successMessage(_ message:String)
    case exerciseLibrary
    case exerciseLikeDislike
    case exerciseFilter
}

enum AllExerciseHeaderButtonAction: String {
    case back
    case filter
    case search
}

protocol AllExerciseHeaderViewDelegate:class {
    func sendAction(action: AllExerciseHeaderButtonAction)
    func didRemoveFilter(action:Scenes)
}

protocol AllExerciseTableViewDelegate:class {
    func showDetailWith(object: Exercise)
    func didSelectAtIndex(index: Int)
}

protocol FilterViewControllerDelegate:class {
    func filterBasedOn(option:Bool)
}
protocol AllExerciseServiceProviderDelegate:class {
    func completed<T>(for action:AllExerciseAction, with response:T?, and error:APIError?)
}
protocol AllExerciseServiceProviderable:class {
    var delegate: AllExerciseServiceProviderDelegate? { get set }
    func exerciseLibrary()
    func likeDislike(param:LikeDislikeParam)
    func filter(param:ExerciseParams.Filter)
}
protocol AllExerciseViewRepresentable :class {
    func onAction(_ action:AllExerciseAction)
}
protocol AllExerciseTableViewCellDelegate:class {
    func didSelectAction(like: Bool)
}
