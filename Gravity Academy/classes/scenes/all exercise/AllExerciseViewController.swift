//
//  AllExerciseViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import NotificationBannerSwift

class AllExerciseViewController: UIViewController {
    
    @IBOutlet weak var tableView: AllExerciseTableView!
    @IBOutlet weak var header: AllExerciseHeaderView!
    weak var router: NextSceneDismisserPresenter?
    weak var delegateTable: AllExerciseTableViewDelegate?
    
    let viewModel = AllExerciseViewModel(provider: AllExerciseServiceProvider())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup() {
        tableView.delegateTable = delegateTable
        header.delegate = self
        viewModel.view = self
        tableView.configure(viewModel)
        startAnimation()
        viewModel.exerciseLibrary()
        NotificationCenter.default.addObserver(self, selector: #selector(likeDislikeUpdate(_:)), name: .handleExercisLikeDislike, object: nil)
    }
    
    @objc func likeDislikeUpdate(_ notification: Notification) {
        viewModel.exerciseLibrary()
    }
    
    func didSelectFilters(_ fitnessLevel:String?, _ muscleID:String?, _ muscles:String?, _ equipment:String?) {
        startAnimation()
        if fitnessLevel != nil {
            DispatchQueue.main.async {
                self.header.viewButton.isHidden = false
                self.header.viewFitness.isHidden = false
                self.header.lblFitness.text = fitnessLevelTitle(fitnessLevel!)
                
                if muscleID != nil {
                    self.header.viewMuscle.isHidden = false
                }
                if equipment != nil {
                    self.header.viewEquipment.isHidden = false
                }
            }
        }
        
        if muscleID != nil {
            DispatchQueue.main.async {
                self.header.viewButton.isHidden = false
                self.header.viewMuscle.isHidden = false
                self.header.lblMuscle.text = muscles
                
                if fitnessLevel != nil {
                    self.header.viewFitness.isHidden = false
                }
                if equipment != nil {
                    self.header.viewEquipment.isHidden = false
                }
            }
        }
        if equipment != nil {
            DispatchQueue.main.async {
                self.header.viewButton.isHidden = false
                self.header.viewEquipment.isHidden = false
                let title = (equipment == "1") ? "Equipment" : "No Equipment"
                self.header.lblEquipment.text = title
                
                if fitnessLevel != nil {
                    self.header.viewFitness.isHidden = false
                }
                if muscleID != nil {
                    self.header.viewMuscle.isHidden = false
                }
            }
        }
        
        viewModel.fitnessLevel = fitnessLevel
        viewModel.muscles = muscles
        viewModel.musclesID = muscleID
        viewModel.equipment = equipment
        viewModel.exerciseFilter()
    }
    
    private func library() {
        tableView.reloadData()
        stopAnimating()
    }
    
    private func presentFilter() {
        let filter = FilterViewController()
        filter.router = router
        //        filter.transitioningDelegate = self
        filter.modalPresentationStyle = .fullScreen
        if #available(iOS 13.0, *) {
            filter.isModalInPresentation = true
        } else {
            // Fallback on earlier versions
        }
        filter.didSelectFilters = didSelectFilters
        self.present(filter, animated: true) {
            filter.presentationController?.presentedView?.gestureRecognizers?[0].isEnabled = true
        }
    }
}

extension AllExerciseViewController: AllExerciseHeaderViewDelegate {
    func sendAction(action: AllExerciseHeaderButtonAction) {
        switch action {
        case .back: router?.dismiss(controller: .allExercise)
        case .search: router?.dismiss(controller: .allExercise)
        case .filter:
            router?.push(scene: .filter)
        }
    }
    
    func didRemoveFilter(action: Scenes) {
        startAnimation()
        switch action {
        case .fitness:
            viewModel.fitnessLevel = nil
            DispatchQueue.main.async {
                self.header.viewFitness.isHidden = true
                if self.viewModel.muscles == nil && self.viewModel.equipment == nil {
                    self.header.viewButton.isHidden = true
                }
            }
            
        case .muscle:
            viewModel.muscles = nil
            viewModel.musclesID = nil
            DispatchQueue.main.async {
                self.header.viewMuscle.isHidden = true
                if self.viewModel.fitnessLevel == nil && self.viewModel.equipment == nil {
                    self.header.viewButton.isHidden = true
                }
                
            }
            
        case .equipments:
            viewModel.equipment = nil
            DispatchQueue.main.async {
                self.header.viewEquipment.isHidden = true
                if self.viewModel.fitnessLevel == nil && self.viewModel.muscles == nil {
                    self.header.viewButton.isHidden = true
                }
            }
            
        default:
            break
        }
        viewModel.exerciseFilter()
    }
}


extension AllExerciseViewController: AllExerciseViewRepresentable {
    func onAction(_ action: AllExerciseAction) {
        switch action {
        case let .errorMessage(text):
            showBannerWith(text: text, style: .warning)
            stopAnimating()
        case .exerciseLibrary, .exerciseFilter:
            library()
        default:
            stopAnimating()
        }
    }
}
