//
//  AllExerciseHeaderView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit


class AllExerciseHeaderView: UIView {
    
    @IBOutlet weak var lblFitness: UILabel!
    @IBOutlet weak var lblMuscle: UILabel!
    @IBOutlet weak var lblEquipment: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var viewFitness: UIView!
    @IBOutlet weak var viewMuscle: UIView!
    @IBOutlet weak var viewEquipment: UIView!
    @IBOutlet weak var viewButton: UIView!
    
    weak var delegate: AllExerciseHeaderViewDelegate?
//    var didRemoveFilter: ((_ action:Scenes) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        [viewButton, viewFitness, viewMuscle, viewEquipment].forEach {
            $0?.isHidden = true
        }
        
        [btnBack, btnFilter, btnSearch].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }

    @objc func buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnBack: delegate?.sendAction(action: .back)
        case btnFilter: delegate?.sendAction(action: .filter)
        case btnSearch: delegate?.sendAction(action: .search)
        default: break
        }
    }
    
    @IBAction func cancelAction(_ sender:UIButton) {
        switch sender.tag {
        case 1:
            delegate?.didRemoveFilter(action: .fitness)
        case 2:
            delegate?.didRemoveFilter(action: .muscle)
        case 3:
            delegate?.didRemoveFilter(action: .equipments)
        default:
            break
        }
    }
}
