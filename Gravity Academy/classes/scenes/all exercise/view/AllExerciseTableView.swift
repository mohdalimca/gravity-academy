//
//  AllExerciseTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class AllExerciseTableView: UITableView {
    var viewModel: AllExerciseViewModel!
    weak var delegateTable: AllExerciseTableViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure(_ viewModel: AllExerciseViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
    }
}

extension AllExerciseTableView: UITableViewDelegate, UITableViewDataSource  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        let cell = tableView.dequeueReusable(indexPath) as AllExerciseTableViewCell
        cell.delegate = self
        cell.configure(with: viewModel.items[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegateTable?.showDetailWith(object: viewModel.items[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}

extension AllExerciseTableView: AllExerciseTableViewCellDelegate {
    func didSelectAction(like: Bool) {
        
    }
}
