//
//  AllExerciseTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class AllExerciseTableViewCell: UITableViewCell, Reusable {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imageExercise: UIImageView!
    @IBOutlet weak var imageLike: UIImageView!
    @IBOutlet weak var containerView: UIView!
    weak var delegate: AllExerciseTableViewCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        guard let item = content as? Exercise else { return }
        lblTitle.text = item.title
        imageExercise.downloadImageFrom(urlString: (item.mediaData?.image!)!, with: UIImage())
        btnLike.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        btnLike.isHidden = true
        if item.isLike == "1" {
            btnLike.isHidden = false
        }
    }
    
    private func setup() {
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        delegate?.didSelectAction(like: true)
    }
}
