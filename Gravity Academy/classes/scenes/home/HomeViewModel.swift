//
//  HomeViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 22/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
final class HomeViewModel {
    
    var apiName: AppAPI!
    var workoutOfDay: WoOfTheDay?
    var dashBoardData: HomeData?
    var equipments: [Equipment] = []
    var musclesGroup: [String] = []
    private var items: [RowSectionDisplayable] = []
    var didSelect:((_ item:RowSectionDisplayable) -> Void)?
    let provider: WorkoutsServiceProviderable
    weak var view: WorkoutsViewRepresentable?
    var isLike = false
    var imageArray: [String] = []
    
    var maleImageArray: [String] = ["workout-of-day-icon", "home-workouts-icon-male", "home-training-plan-icon-male", "home-skills-icon-male", "home-exercises-icon-male", "home-equipment-icon-male"]
    
    var femaleImageArray: [String] = ["workout-of-day-icon", "home-workouts-icon-female", "home-training-plan-icon-female", "home-skills-icon-female", "home-exercises-icon-female", "home-equipment-icon-female"]

    var titleArray: [String] = ["Workout of the Day", "Workout Archive", "Training Archive", "Skills Archive", "Exercise Archive", "Equipment Archive"]
    
    var count:Int {
        
        return items.count
    }
    
    init(provider: WorkoutsServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
        items = RowSectionDisplayableFunction.makeRowSectionDisplayable(resource: "Home")
    }
    
    func masterData() {
        provider.masterData()
    }
    
    func workoutsLibrary() {
        provider.workoutLibrary()
    }
    
    func workoutOfTheDay() {
        provider.workoutOfTheDay()
    }
    
    func likeAPI(param:LikeDislikeParam) {
        provider.likeDislike(param: param)
    }
    
    func dashboardData() {
        provider.dashBoardData()
    }
    
    func getEquipments() {
        provider.equipments()
    }
    
    func items(in section: Int) -> [Row] {
        return items[section].content
    }
    
    func item(at section: Int) -> RowSectionDisplayable {
        return items[section]
    }
    
    func item(for indexPath: IndexPath) -> Row {
        return items(in: indexPath.section)[indexPath.row]
    }
}

extension HomeViewModel: WorkoutsServiceProviderDelegate {
    func completed<T>(for action: WorkoutsAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .day:
                        self.workoutOfDay = resp.data?.woOfTheDay?.first
                        self.musclesGroup = self.workoutOfDay?.muscleGroups?.components(separatedBy: ",") ?? self.musclesGroup
                    case .likeDislike:
                        self.isLike = (resp.data?.status != 0 ) ? true : false
                    case .master:
                        UserStore.removeMaster()
                        UserStore.save(masterData: (resp.data?.masterData)!)
                    case .dashBoardData:
                        self.dashBoardData = resp.data?.homeData
                    case .equipments:
                        self.equipments = resp.data?.equipments ?? self.equipments
                    default: break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
