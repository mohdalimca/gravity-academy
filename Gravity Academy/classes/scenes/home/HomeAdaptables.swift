//
//  HomeAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

class HomeViewData {
    static var homeScene : Scenes = .workouts
}


///************* Workouts *x************///

enum HomeScreenImage:String {
    case dayWorkout = "workout-of-day-icon"
    case workouts = "home-workouts-icon"
    case trainingPlan = "home-training-plan-icon"
    case skill = "home-skills-icon"
    case exercise = "home-exercises-icon"
    case equipments = "home-equipment-icon"
}

enum WorkoutsSection: String {
    case detail
    case workout
    case program
    case technique
    case skill
    case equipment
    case none
}

protocol HomeCollectionCellDelegate: class {
    func didSelect()
}
protocol WorkoutCollectionViewDelegate: class {
    func didSelect(at index:Int, section:WorkoutsSection)
}

protocol HomeTableViewDelegate: class {
    func didSelect(at index: Int)
    func didShowDetailView(section: WorkoutsSection)
    func like(at index:Int, section:WorkoutsSection)
    func sendAction(action:Scenes, id:String)
}
protocol HomeViewControllerDelegate:class {
    func sendAction(action:Scenes)
}
protocol HomeHeaderCellDelegate:class {
    func didSelectViewAll(at index: Int)
}


///************* Profile & Settings *************///
protocol SettingsTableViewDelegate: class {
    func sendAction(action: Scenes, viewModel:RegistrationViewModel)
}
