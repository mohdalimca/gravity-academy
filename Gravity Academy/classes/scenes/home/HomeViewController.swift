//
//  HomeViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 06/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var homeTabBar: HomeTabBar!
    
    let button = UIButton.init(type: .custom)
    weak var delegateHome:HomeViewControllerDelegate?
    weak var delegate:ControllerDismisser?
    weak var router: NextSceneDismisserPresenter?
    let selectedImage = UIImage(named: "tab-timeline-selected")
    let unselectedImage = UIImage(named: "tab-timeline-unselected")

    let workouts: WorkoutsViewController = WorkoutsViewController.from(from: .home, with: .workouts)
    let favourite: FavouriteViewController = FavouriteViewController.from(from: .home, with: .favourite)
    let profile: UserProfileViewController = UserProfileViewController.from(from: .home, with: .profile)
    let timeline: TimelineViewController = TimelineViewController.from(from: .home, with: .timeline)
    let gravityAcademy: GravityAcademyViewController = GravityAcademyViewController.from(from: .gravityAcademy, with: .gravityAcademy)
    let nointernet: NoInternetViewController = NoInternetViewController.from(from: .common, with: .nointernet)

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showViews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        button.center = CGPoint(x: homeTabBar.center.x, y: homeTabBar.center.y - homeTabBar.bounds.height / 2 )
    }
    
    private func setup() {
        homeTabBar.delegate = self
        homeTabBar.selectedItem = homeTabBar.items![0] as UITabBarItem
        homeTabBar.barTintColor = UIColor.init(hex: tabBarColor)
        onStart()
        setCenterButton()
    }
    
    private func onStart() {
        workouts.router = router
        workouts.delegate = router
        favourite.router = router
        timeline.router = router
        profile.router = router
        gravityAcademy.router = router
        nointernet.router = router
    }
    

    private func setCenterButton() {
        button.setBackgroundImage(unselectedImage, for: .normal)
        button.tintColor = .clear
        button.frame = CGRect.init(x: 0, y: 0, width: 62, height: 62)
        button.addTarget(self, action: #selector(btnCenterAction(_:)), for: .touchUpInside)
        view.insertSubview(button, aboveSubview: homeTabBar)
    }
    
    @objc func btnCenterAction(_ sender: UIButton) {
        tabBar(homeTabBar, didSelect: homeTabBar.items![2] as UITabBarItem)
        homeTabBar.selectedItem = homeTabBar.items![2] as UITabBarItem
        button.setBackgroundImage(selectedImage, for: .normal)
        showViews()
    }
    
    private func showViews() {
        var vc = UIViewController()
        switch HomeViewData.homeScene {
        case .workouts:
//            nointernet.didTryAgain = login.didTryAgain
            vc = workouts
        case .favourite:
            vc = favourite
        case .timeline:
            vc = timeline
        case .profile:
            vc = profile
        case .gravityAcademy:
            vc = gravityAcademy
        default: break
        }
        
//        vc.willMove(toParent: self)
//        self.customView.addSubview(vc.view)
//        self.addChild(vc)
//        vc.didMove(toParent: self)
        
        self.addChild(vc)
        vc.view.frame = self.customView.bounds
        self.customView.addSubview(vc.view)
        vc.willMove(toParent: self)
    }
}

extension HomeViewController: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        button.setBackgroundImage(unselectedImage, for: .normal)
        switch item.tag {
        case 1: HomeViewData.homeScene = .workouts
        case 2: HomeViewData.homeScene = .favourite
        case 3:
            HomeViewData.homeScene = .timeline
            button.setBackgroundImage(selectedImage, for: .normal)
        case 4: HomeViewData.homeScene = .profile
        case 5: HomeViewData.homeScene = .gravityAcademy
        default: break
        }
        showViews()
    }
}

extension HomeViewController:HomeViewControllerDelegate {
    func sendAction(action: Scenes) {
        router?.present(scene: action)
    }
}

