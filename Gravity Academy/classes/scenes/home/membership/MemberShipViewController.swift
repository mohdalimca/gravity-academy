//
//  MemberShipViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 15/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class MemberShipViewController: UIViewController {
    
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblMonthAmount: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblYearAmount: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnMonthly: UIButton!
    @IBOutlet weak var btnYearly: UIButton!
    @IBOutlet weak var btnMonthActivate: GravityAcademyButton!
    @IBOutlet weak var btnYearActivate: GravityAcademyButton!
    @IBOutlet weak var imageMonth: UIImageView!
    @IBOutlet weak var imageYear: UIImageView!
    
    var selectedImage = UIImage(named: "membership-image-selected")
    var unselectedImage = UIImage(named: "membership-image-unselected")
    var yearSelected = UIImage(named: "membership-year-selected")
    weak var router: NextSceneDismisserPresenter?
    var isFromSetting: Bool?
    let browserURL = URL.init(string: "https://www.gravity-academy.de/cart?add-to-cart=2340")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup() {
//        lblDiscount.transform = CGAffineTransform(rotationAngle: -45)
        print(CGAffineTransform(rotationAngle: CGFloat(Double(-45) * .pi/180)))
        lblDiscount.transform = CGAffineTransform(rotationAngle: CGFloat(Double(-45) * .pi/180))
        [btnBack, btnMonthly, btnYearly, btnMonthActivate, btnYearActivate].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        setButtonWith(isMonth: false)
//        if isFromSetting == nil {
//            btnBack.setImage(nil, for: .normal)
//        }
    }
    
    private func setButtonWith(isMonth: Bool) {
        btnMonthly.isSelected = isMonth
        btnYearly.isSelected = !isMonth
        btnMonthActivate.isEnabled = isMonth
        btnYearActivate.isEnabled = !isMonth

        if btnMonthly.isSelected {
            imageMonth.image = selectedImage
            imageYear.image = unselectedImage
            btnYearActivate.backgroundColor = .white
            btnYearActivate.layer.borderColor = UIColor.gray.cgColor
            btnMonthActivate.backgroundColor = UIColor.init(hex: APP_THEME_ORANGE_COLOR)
            btnMonthActivate.layer.borderColor = UIColor.init(hex: APP_THEME_ORANGE_COLOR).cgColor
            
            DispatchQueue.main.async {
                self.btnYearActivate.titleLabel?.textColor = .black
                self.btnMonthActivate.titleLabel?.textColor = .white
            }
            
        } else {
            imageMonth.image = unselectedImage
            imageYear.image = yearSelected
            btnMonthActivate.backgroundColor = .white
            btnMonthActivate.layer.borderColor = UIColor.gray.cgColor
            btnYearActivate.backgroundColor = UIColor.init(hex: APP_THEME_ORANGE_COLOR)
            btnYearActivate.layer.borderColor = UIColor.init(hex: APP_THEME_ORANGE_COLOR).cgColor
            DispatchQueue.main.async {
                self.btnMonthActivate.titleLabel?.textColor = .black
                self.btnYearActivate.titleLabel?.textColor = .white
            }
        }
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnBack: router?.dismiss(controller: .membership)
        case btnMonthly: setButtonWith(isMonth: true)
        case btnYearly: setButtonWith(isMonth: false)
        case btnMonthActivate, btnYearActivate:
            router?.push(scene: .browser)
        default: break
        }
    }
}
