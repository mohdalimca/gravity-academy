//
//  MemberShipCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class MemberShipCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let membership: MemberShipViewController = MemberShipViewController.from(from: .home, with: .membership)

    var browser: BrowserCoordinator!

    override func start() {
        super.start()
        onStart()
        router.setRootModule(membership, hideBar: true)
    }
    
    private func onStart() {
        membership.router = self
    }
    
    private func startBrowser() {
        browser = BrowserCoordinator (router: Router())
        add(browser)
        browser.delegate = self
        browser.start(url: membership.browserURL)
        self.router.present(browser, animated: true)
    }
}

extension MemberShipCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .browser: startBrowser()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension MemberShipCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
