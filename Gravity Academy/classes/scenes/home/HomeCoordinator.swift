//
//  HomeCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import UIKit

final class HomeCoordinator: Coordinator<Scenes> {

    let homeTabController: HomeTabViewController = HomeTabViewController.from(from: .home, with: .homeTab)
    let controller: HomeViewController = HomeViewController.from(from: .home, with: .home)
    
    var browser: BrowserCoordinator!


    // Tab View Controllers
//    let homeWorkout: WorkoutsViewController = WorkoutsViewController.from(from: .home, with: .workouts)
//    let homeFavourite: FavouriteViewController = FavouriteViewController.from(from: .home, with: .favourite)
//    let homeProfile: UserProfileViewController = UserProfileViewController.from(from: .home, with: .profile)
//    let homeTimeline: TimelineViewController = TimelineViewController.from(from: .home, with: .timeline)
//    let homeGravityAcademy: GravityAcademyViewController = GravityAcademyViewController.from(from: .gravityAcademy, with: .gravityAcademy)

    let workoutList: WorkoutsListVC = WorkoutsListVC.from(from: .workouts, with: .workoutsList)
    let programs: ProgramsListVC = ProgramsListVC.from(from: .programs, with: .programsList)
    let userInfo: UserInfoViewController = UserInfoViewController.from(from: .register, with: .userInfo)
    let membership: MemberShipViewController = MemberShipViewController.from(from: .home, with: .membership)
    let password: ChangePasswordViewController = ChangePasswordViewController.from(from: .forgot, with: .changePassword)
    let nointernet: NoInternetViewController = NoInternetViewController.from(from: .common, with: .nointernet)
    
    
    var list:FavouritesListCoordinator!
    var programDetail: ProgramDetailCoordinator!
    var skillDetail: TechniqueDetailCoordinator!
    
    override func start() {
        super.start()
        homeTabController.router = self
        router.setRootModule(homeTabController, hideBar: true)
        onStart()
    }
    
    private func onStart() {
        controller.delegate = self
//        homeWorkout.router = self
//        homeFavourite.router = self
//        homeTimeline.router = self
//        homeProfile.router = self
//        homeGravityAcademy.router = self
        
        homeTabController.favourite.delegate = self
        homeTabController.timeline.delegate = self
        homeTabController.profile.delegate = self
    
        workoutList.router = self
        programs.router = self
        membership.router = self
        password.router = self
        controller.gravityAcademy.router = self
        controller.gravityAcademy.delegate = self
        nointernet.router = self
        controller.favourite.delegate = self
        //        nointernet.didTryAgain = home.didTryAgain
    }
    
    private func startWorkoutDetail(_ workouts: Workouts) {
        let r = Router()
        let detail = DetailCoordinator(router: r)
        add(detail)
        detail.delegate = self
        detail.start(detail: workouts, categoryTitle: "")
        router.present(detail, animated: true)
    }
    
    private func startActivity() {
        let r = Router()
        let program = ProgramCoordinator(router: r)
        add(program)
        program.start()
        program.delegate = self
        router.present(program, animated: true)
    }
    
    private func startTechnique() {
        let r = Router()
        let technique = TechniquesCoordinator(router: r)
        add(technique)
        technique.start()
        technique.delegate = self
        router.present(technique, animated: true)
    }
    
    private func startSettings() {
        let r = Router()
        let settings = SettingsCoordinator(router: r)
        add(settings)
        settings.delegate = self
        settings.start(homeTabController.profile.viewModel.myProfile)
        router.present(settings, animated: true)
    }
    
    private func dayWorkoutDetail() {
        let r = Router()
        let detail = DetailCoordinator(router: r)
        add(detail)
        detail.delegate = self
        detail.start(day: homeTabController.workouts.viewModel.workoutOfDay!, categoryTitle:"")
        router.present(detail, animated: true)
    }
    
    private func startWorkoutLibrary() {
        let r = Router()
        let library = WorkoutLibraryCoordinator(router: r)
        add(library)
        library.delegate = self
        let wod = WoOfTheDay(workoutID: "2", workoutCategoryID: "2", title: "Andy Wokrout", woOfTheDayDescription: "Walk and Run", workoutGoal: "Run Daily", muscleGroups: "whole body,Chest,Shoulders", status: "1", addedDate: "", likeCount: "0", isWoOfTheDay: "1", isLike: "0")
        library.start(with: homeTabController.workouts.viewModel.workoutOfDay ?? wod)
        router.present(library, animated: true)
    }
    
    private func startProgramLibrary() {
        let r = Router()
        let library = ProgramLibraryCoordinator(router: r)
        add(library)
        library.delegate = self
        library.start()
        router.present(library, animated: true)
    }
    
    private func startSkills() {
        let skills = TechniquesCoordinator(router: Router())
        add(skills)
        skills.delegate = self
        skills.start()
        router.present(skills, animated: true)
    }
    
    private func startAllExercise() {
        let r = Router()
        let all = AllExerciseCoordinator(router: r)
        add(all)
        all.delegate = self
        all.start()
        router.present(all, animated: true)
    }
    
    private func startEquipments() {
        let r = Router()
        let equipments = EquipmentsCorrdinator(router: r)
        add(equipments)
        equipments.delegate = self
        equipments.start()
        router.present(equipments, animated: true)
    }
    
    private func startCreate() {
        let r = Router()
        let create = CreateWorkoutCoordinator(router: r)
        add(create)
        create.delegate = self
        create.start()
        router.present(create, animated: true)
    }
    
    private func startAthlete() {
        let r = Router()
        let athlete = AthleteWorkoutCoordinator(router: r)
        add(athlete)
        athlete.delegate = self
        athlete.start()
        router.present(athlete, animated: true)
    }
    
    private func startMembership() {
        let r = Router()
        let member = MemberShipCoordinator(router: r)
        add(member)
        member.delegate = self
        member.start()
        router.present(member, animated: true)
    }
    
    private func startCourse(action: GravityAcademyViewAction) {
        let r = Router()
        let course = AcademyCourseCoordinator(router: r)
        add(course)
        course.delegate = self
        course.startWith(action: action)
        router.present(course, animated: true)
    }
    
    private func startSavedExercise() {
        let r = Router()
        let saved = SavedExeciseCoordinator(router: r)
        add(saved)
        saved.delegate = self
        saved.start()
        router.present(saved, animated: true)
    }
    
    private func startEbooKDetail() {
        let r = Router()
        let ebookDetail = EbookDetailCoordinator(router: r)
        add(ebookDetail)
        ebookDetail.delegate = self
        ebookDetail.start()
        router.present(ebookDetail, animated: true)
    }
    
    private func startAllFavouriteList() {
        list = FavouritesListCoordinator(router: Router())
        add(list)
        list.delegate = self
        list.start(homeTabController.favourite.viewModel.listScene)
        router.present(list, animated: true)
    }
    
    private func startProgramDetail() {
        let r = Router()
        programDetail = ProgramDetailCoordinator(router: r)
        add(programDetail)
        programDetail.delegate = self
        programDetail.start(trainingPlan:homeTabController.favourite.viewModel.selectedPlan)
        router.present(programDetail, animated: true)
    }
    
    private func startSkillDetail() {
        skillDetail = TechniqueDetailCoordinator(router: Router())
        add(skillDetail)
        skillDetail.delegate = self
        skillDetail.start(skill: homeTabController.favourite.viewModel.selectedSkill)
        router.present(skillDetail, animated: true)
    }
    
    private func startBrowser() {
        browser = BrowserCoordinator (router: Router())
        add(browser)
        browser.delegate = self
        browser.start(url: homeTabController.gravityAcademy.viewModel.browserURL)
        self.router.present(browser, animated: true)
    }
}

extension HomeCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .settings: startSettings()
        case .workoutLibrary: startWorkoutLibrary()
        case .workoutsList: router.push(workoutList, animated: true, completion: nil)
        case .programLibrary: startProgramLibrary()
        case .programsList: router.push(programs, animated: true, completion: nil)
        case .techniquesList: startSkills()
        case .allExercise: startAllExercise()
        case .equipments: startEquipments()
        case .dayWorkoutDetail: dayWorkoutDetail()
        case .activityDetail: startActivity()
//        case .workoutDetail: startWorkoutDetail()
        case .createWorkout: startCreate()
        case .athlete: startAthlete()
        case .membership: startMembership()
        case .savedExercise: startSavedExercise()
        case .favouritesList: startAllFavouriteList()
        case .programDetail: startProgramDetail()
        case .techniqueDetail: startSkillDetail()
        case .browser: startBrowser()
        case .nointernet:
            self.router.present(nointernet, animated: true)
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        switch controller {
        case .workoutsList, .programs, .dayWorkoutDetail:
            router.popModule(animated: true)
        default: router.dismissModule(animated: true, completion: nil)
        }
    }
}

extension HomeCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

extension HomeCoordinator: GravityAcademyTableViewDelegate {
    func didSelectAt(index: Int) {
        router.push(programs, animated: true, completion: nil)
    }
}
extension HomeCoordinator: GravityAcademyViewControllerDelegate {
    func sendAction(action: GravityAcademyViewAction) {
        //        startCourse(action: action)
        startEbooKDetail()
    }
}

extension HomeCoordinator: FavouriteViewControllerDelegate {
    func didOpenScene(_ scene: Scenes, _ selectedPlan: TrainingPlan?, _ selectedSkill: SkillList?, _ selectedWorkout: Workouts?) {
        switch scene {
        case .programDetail:
            homeTabController.favourite.viewModel.selectedPlan = selectedPlan
            startProgramDetail()
        case .techniqueDetail:
            homeTabController.favourite.viewModel.selectedSkill = selectedSkill
            startSkillDetail()
        case .workoutDetail:
            homeTabController.favourite.viewModel.selectedWorkout = selectedWorkout
            startWorkoutDetail(selectedWorkout!)
        default:
            break
        }
    }
}

extension HomeCoordinator: UserProfileViewControllerDelegate {
    func didOpenWorkout(workout: Workouts) {
        startWorkoutDetail(workout)
    }
}

extension HomeCoordinator: TimelineViewControllerDelegate {
    func didOpenWorkoutDetails(_ workout: Workouts) {
        startWorkoutDetail(workout)
    }
    
    func didOpenProgramDetails(_ workout: Workouts) {
        startWorkoutDetail(workout)
    }
    
    func didOpenSkillDetails(_ workout: Workouts) {
        startWorkoutDetail(workout)
    }
}

