
//
//  UserProfileViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 21/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class UserProfileViewModel {

    var likeList: LikeList?
    var myProfile:MyProfile!
    var finished:FavouriteWorkout?
    var selectedWorkout: Workouts!
    var completedWorkouts:[Workouts] = []
    weak var view: UserProfileViewRepresentable?
    var provider: UserProfileServiceProviderable
    private var items: [RowSectionDisplayable] = []
    var didSelect:((_ item:RowSectionDisplayable) -> Void)?
    var currentPageNo = 1
    
    init(provider:UserProfileServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    var count:Int {
        return 6
    }
    
    var completeWoCount: Int {
        return completedWorkouts.count
    }
    
    func profile() {
        provider.getProfile()
    }
    
    func compeletedWorkoutList() {
        provider.completeWorkout(param: WorkoutsParams.CompleteWokrout(page_no: String(currentPageNo)))
    }
}

extension UserProfileViewModel: UserProfileServiceProviderDelegate {
    func completed<T>(for action: UserProfileAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .profile:
                        self.myProfile = resp.data?.myProfile
                    case .completeWorkout:
                        self.finished = resp.data?.favouriteWorkout
                        if let workouts = resp.data?.favouriteWorkout?.workouts {
                            self.completedWorkouts = workouts
                        }
                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
