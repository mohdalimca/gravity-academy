//
//  UserProfileHeaderView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit


class UserProfileHeaderView: UIView {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblMembership: UILabel!
    @IBOutlet weak var lblFitnessLevel: UILabel!
    @IBOutlet weak var lblWorkout: UILabel!
    @IBOutlet weak var lblWorkoutCount: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var lblCompleteProfile: UILabel!
    @IBOutlet weak var btnEditProfile: GravityAcademyButton!
    @IBOutlet weak var btnCreateWorkout: GravityAcademyButton!
    @IBOutlet weak var btnEditNow: GravityAcademyButton!
    @IBOutlet weak var btnMuscles: GravityAcademyButton!
    @IBOutlet weak var btnCardio: GravityAcademyButton!
    @IBOutlet weak var btnStrength: GravityAcademyButton!
    @IBOutlet weak var btnTechnique: GravityAcademyButton!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var imagePremiumBatch: UIImageView!
    @IBOutlet weak var viewCircle: UIView!
    
    weak var delegateHeader:UserProfileHeaderDelegate?
    
    let shapeLayer = CAShapeLayer()
    let orange = UIColor.init(hex: "#FFE4DF")
    let gray = UIColor.init(hex: "#E6E6E6")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        [lblMembership, lblFitnessLevel].forEach {
            $0?.layer.cornerRadius = 4
            $0?.layer.masksToBounds = true
        }
        
        [btnEditProfile, btnEditNow, btnCreateWorkout, btnMuscles, btnCardio, btnStrength, btnTechnique].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        
        setCircle()
    }
    
    func populateDetails(_ profile:MyProfile) {
        imageProfile.downloadImageFrom(urlString: profile.image ?? "", with: shadowImage)
        lblName.text = profile.name
        lblFitnessLevel.text = profile.fitnessLevel
    }
    
    func updateWorkoutCount(_ total:Int) {
        lblWorkoutCount.text = "\(total)"
    }
    
    private func setCircle() {
        let trackLayer = CAShapeLayer()
        let circularPath = UIBezierPath(arcCenter: CGPoint(x: 25, y: 25), radius: 15, startAngle: -.pi/2, endAngle: -.pi/2 + .pi * 2, clockwise: true)
        
        trackLayer.path = circularPath.cgPath
        trackLayer.strokeColor = UIColor.init(hex: "#D67171").cgColor
        trackLayer.lineWidth = 4
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = .round
        viewCircle.layer.addSublayer(trackLayer)
        
        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 4
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = .round
        shapeLayer.strokeEnd = 0
        viewCircle.layer.addSublayer(shapeLayer)
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 0.8
        basicAnimation.duration = 0.5
        basicAnimation.fillMode = .forwards
        basicAnimation.isRemovedOnCompletion = false
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
    }
    
    private func updateButtons(_ sender: GravityAcademyButton) {
        switch sender {
        case btnMuscles:
            btnMuscles.backgroundColor = orange
            btnCardio.backgroundColor = gray
            btnStrength.backgroundColor = gray
            btnTechnique.backgroundColor = gray
        case btnCardio:
            btnMuscles.backgroundColor = gray
            btnCardio.backgroundColor = orange
            btnStrength.backgroundColor = gray
            btnTechnique.backgroundColor = gray
        case btnStrength:
            btnMuscles.backgroundColor = gray
            btnCardio.backgroundColor = gray
            btnStrength.backgroundColor = orange
            btnTechnique.backgroundColor = gray
        case btnTechnique:
            btnMuscles.backgroundColor = gray
            btnCardio.backgroundColor = gray
            btnStrength.backgroundColor = gray
            btnTechnique.backgroundColor = orange
        default: break
        }
    }
    
    @objc func buttonPressed(_ sender: GravityAcademyButton) {
        switch sender {
        case btnEditProfile: delegateHeader?.sendAction(action: .editProfile)
        case btnCreateWorkout: delegateHeader?.sendAction(action: .createWorkout)
        case btnEditNow: delegateHeader?.sendAction(action: .editNow)
        default: updateButtons(sender)
        }
    }
}
