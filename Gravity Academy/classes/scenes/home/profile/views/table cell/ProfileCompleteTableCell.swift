//
//  ProfileCompleteTableCell.swift
//  Gravity Academy
//
//  Created by Apple on 08/10/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import UIKit

class ProfileCompleteTableCell: UITableViewCell, Reusable {

    @IBOutlet weak var imageBG: UIImageView!
    @IBOutlet weak var imageLike: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    private func setup() {
        selectionStyle = .none
    }

    func configure<T>(with content: T) {
        guard let workout = content as? Workouts else { return }
        lblTitle.text = workout.title
        lblDate.text = workout.addedDate?.gravityDate()
        imageBG.downloadImageFrom(urlString: workout.woImgURL ?? "", with: shadowImage)
        imageLike.isHidden = true
        if workout.isLike == "1" {
            imageLike.isHidden = false
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
