//
//  CompleteWorkoutTableView.swift
//  Gravity Academy
//
//  Created by Apple on 08/10/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import UIKit

class CompleteWorkoutTableView: UITableView {
    
    var viewModel: UserProfileViewModel!
    var didSelectRowAtIndex: ((Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ viewModel: UserProfileViewModel){
        self.viewModel = viewModel
        setup()
    }
    
    func setup() {
        dataSource = self
        delegate = self
        tableFooterView = UIView.init(frame: .zero)
        reloadData()
    }
}

extension CompleteWorkoutTableView: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.completeWoCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusable(indexPath) as ProfileCompleteTableCell
        cell.configure(with: viewModel.completedWorkouts[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectRowAtIndex?(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}







