//
//  WorkoutTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 23/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class CustomWorkoutTableView: UITableView {
    
    var viewModel: UserProfileViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure(_ viewModel: UserProfileViewModel){
        self.viewModel = viewModel
        setup()
    }
    
    func setup() {
        dataSource = self
        delegate = self
        tableFooterView = UIView.init(frame: .zero)
        reloadData()
    }
}

extension CustomWorkoutTableView: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusable(indexPath) as CustomWorkoutCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}



