//
//  UserProfileViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController {
    
    @IBOutlet weak var tableView: CustomWorkoutTableView!
    @IBOutlet weak var completeTableView: CompleteWorkoutTableView!
    @IBOutlet weak var header: UserProfileHeaderView!
    @IBOutlet weak var viewNoData: UIView! {
        didSet {
            viewNoData.isHidden = true
        }
    }
    
    weak var delegate: UserProfileViewControllerDelegate?
    weak var router: NextSceneDismisserPresenter?
    let viewModel: UserProfileViewModel = UserProfileViewModel(provider: UserProfileServiceProvider())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        startAnimation()
        viewModel.compeletedWorkoutList()
    }
    
    private func setup() {
        viewModel.view = self
        viewModel.profile()
        header.delegateHeader = self
        tableView.configure(viewModel)
        completeTableView.configure(viewModel)
        completeTableView.didSelectRowAtIndex = didSelectRowAtIndex
    }
    
    private func populateUserDetails() {
        header.populateDetails(viewModel.myProfile)
        stopAnimating()
    }
    
    private func showTableView(_ hidden:Bool) {
        header.updateWorkoutCount(viewModel.completedWorkouts.count)
        completeTableView.reloadData()
        completeTableView.isHidden = !hidden
        viewNoData.isHidden = hidden
        stopAnimating()
    }
    
    private func reloadCompleteWorkouts() {
        if !viewModel.completedWorkouts.isEmpty {
            showTableView(true)
        } else {
            showTableView(false)
        }
    }
    
    private func didSelectRowAtIndex(_ index:Int) {
        viewModel.selectedWorkout = viewModel.completedWorkouts[index]
        delegate?.didOpenWorkout(workout: viewModel.completedWorkouts[index])
    }
}

extension UserProfileViewController: UserProfileHeaderDelegate {
    func sendAction(action: UserProfileHeaderButtonAction) {
        switch action {
        case .editProfile: router?.push(scene: .settings)
        case .createWorkout: router?.push(scene: .createWorkout)
        case .editNow: router?.push(scene: .settings)
        }
    }
}

extension UserProfileViewController:UserProfileViewRepresentable {
    func onAction(_ action: UserProfileAction) {
        switch action {
        case let .errorMessage(text):
            showBannerWith(text: text, style: .warning)
            stopAnimating()
        case .profile:
            populateUserDetails()
        case .completeWorkout:
            reloadCompleteWorkouts()
        default:
            stopAnimating()
        }
    }
}
