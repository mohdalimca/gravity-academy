//
//  UserProfileAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum UserProfileAction {
    case errorMessage(_ text:String)
    case profile
    case update
    case image
    case chooseImage
    case completeWorkout
    case completeProgram
    case completeSkill
    case nointernet
    case logout
    case refreshTableView(_ from: RefreshOption)
}

enum UserProfileHeaderButtonAction: String {
    case editProfile
    case createWorkout
    case editNow
}

protocol UserProfileHeaderDelegate:class {
    func sendAction(action: UserProfileHeaderButtonAction)
}

protocol UserProfileViewControllerDelegate:class {
    func didOpenWorkout(workout: Workouts)
}


protocol UserProfileViewRepresentable:class {
    func onAction(_ action:UserProfileAction)
}

protocol UserProfileServiceProviderDelegate:class {
    func completed<T>(for action:UserProfileAction, with response:T?, error:APIError?)
}

protocol UserProfileServiceProviderable:class {
    var delegate: UserProfileServiceProviderDelegate? { get set }
    func getProfile()
    func updateProfile()
    func profileImage()
    func completeWorkout(param: WorkoutsParams.CompleteWokrout)
    func compeletedProgram(param: ProgramsParams.CompleteProgram)
    func compeletedSkill(param: TechniquesParam.CompleteSkill)
    func logout(param: UserParams.Logout)
}
