
//
//  UserProfileProvider.swift
//  Gravity Academy
//
//  Created by Apple on 21/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class UserProfileServiceProvider: UserProfileServiceProviderable {
    var delegate: UserProfileServiceProviderDelegate?
    private let task = UserTask()
    
    func getProfile() {
        task.getProfile(responseModel: SuccessResponseModel.self) { (resp, err) in
            if err != nil {
                self.delegate?.completed(for: .profile, with: resp, error: err)
                return
            }
            self.delegate?.completed(for: .profile, with: resp, error: err)
        }
    }
    
    func logout(param: UserParams.Logout) {
        task.logout(param: param, responseModel: SuccessResponseModel.self) { (resp, err) in
            if err != nil {
                self.delegate?.completed(for: .logout, with: resp, error: err)
                return
            }
            self.delegate?.completed(for: .logout, with: resp, error: err)
        }
    }
    
    func updateProfile() {
        task.updateProfile(responseModel: SuccessResponseModel.self) { (resp, err) in
            if err != nil {
                self.delegate?.completed(for: .update, with: resp, error: err)
                return
            }
            self.delegate?.completed(for: .update, with: resp, error: err)
        }
    }
    
    func profileImage() {
        
    }
    
    func completeWorkout(param: WorkoutsParams.CompleteWokrout) {
        task.completeWorkout(param: param, responseModel: SuccessResponseModel.self) { (resp, err) in
            if err != nil {
                self.delegate?.completed(for: .completeWorkout, with: resp, error: err)
                return
            }
            self.delegate?.completed(for: .completeWorkout, with: resp, error: err)
        }
    }
    
    func compeletedProgram(param: ProgramsParams.CompleteProgram) {
        task.completeProgram(param: param, responseModel: SuccessResponseModel.self) { (resp, err) in
            if err != nil {
                self.delegate?.completed(for: .completeProgram, with: resp, error: err)
                return
            }
            self.delegate?.completed(for: .completeProgram, with: resp, error: err)
        }
    }
    
    func compeletedSkill(param: TechniquesParam.CompleteSkill) {
        task.completeSkill(param: param, responseModel: SuccessResponseModel.self) { (resp, err) in
            if err != nil {
                self.delegate?.completed(for: .completeSkill, with: resp, error: err)
                return
            }
            self.delegate?.completed(for: .completeSkill, with: resp, error: err)
        }
    }
}
