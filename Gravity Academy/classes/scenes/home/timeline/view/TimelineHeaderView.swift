//
//  TimelineHeaderView.swift
//  Gravity Academy
//
//  Created by Apple on 27/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class TimelineHeaderView: UIView {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblWorkoutCount: UILabel!
    @IBOutlet weak var lblAllActivity: UILabel!
    @IBOutlet weak var lblStatistics: UILabel!
    @IBOutlet weak var lblWorkout: UILabel!
    @IBOutlet weak var lblProgram: UILabel!
    @IBOutlet weak var lblSkills: UILabel!
    @IBOutlet weak var viewAllActivity: UIView!
    @IBOutlet weak var viewStatistics: UIView!
    @IBOutlet weak var viewWorkout: UIView!
    @IBOutlet weak var viewProgram: UIView!
    @IBOutlet weak var viewSkills: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnAllActivity: UIButton!
    @IBOutlet weak var btnStatistics: UIButton!
    @IBOutlet weak var btnWorkout: UIButton!
    @IBOutlet weak var btnProgram: UIButton!
    @IBOutlet weak var btnSkills: UIButton!
    
    @IBOutlet weak var imageBatch: UIImageView!
    @IBOutlet weak var imageProfile: UIImageView!
    
    weak var delegate:TimelineHeaderViewDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        [btnBack, btnAllActivity, btnStatistics, btnProgram, btnSkills, btnWorkout, btnStatistics].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        updateButtons(btnWorkout)
    }
    
    func populate(_ name:String, _ image:String) {
        lblName.text = name
        imageProfile.downloadImageFrom(urlString: image, with: UIImage())
    }
    
    private func updateButtons(_ sender: UIButton) {
        switch sender {
        case btnAllActivity:
            viewAllActivity.alpha = 1.0
            viewStatistics.alpha = 0.5
            viewWorkout.alpha = 0.5
            viewProgram.alpha = 0.5
            viewSkills.alpha = 0.5
        case btnStatistics:
            viewAllActivity.alpha = 0.5
            viewStatistics.alpha = 1.0
            viewWorkout.alpha = 0.5
            viewProgram.alpha = 0.5
            viewSkills.alpha = 0.5
        case btnWorkout:
            viewAllActivity.alpha = 0.5
            viewStatistics.alpha = 0.5
            viewWorkout.alpha = 1.0
            viewProgram.alpha = 0.5
            viewSkills.alpha = 0.5
        case btnProgram:
            viewAllActivity.alpha = 0.5
            viewStatistics.alpha = 0.5
            viewWorkout.alpha = 0.5
            viewProgram.alpha = 1.0
            viewSkills.alpha = 0.5
        case btnSkills:
            viewAllActivity.alpha = 0.5
            viewStatistics.alpha = 0.5
            viewWorkout.alpha = 0.5
            viewProgram.alpha = 0.5
            viewSkills.alpha = 1.0
        default: break
        }
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        updateButtons(sender)
        switch sender {
        case btnBack: delegate?.sendAction(action: .back)
        case btnAllActivity: delegate?.sendAction(action: .all)
        case btnStatistics: delegate?.sendAction(action: .statistics)
        case btnWorkout: delegate?.sendAction(action: .workout)
        case btnProgram: delegate?.sendAction(action: .program)
        case btnSkills: delegate?.sendAction(action: .skills)
        default: break
        }
    }
}
