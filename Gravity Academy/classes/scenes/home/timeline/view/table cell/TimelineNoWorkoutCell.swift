//
//  TimelineNoWorkoutCell.swift
//  Gravity Academy
//
//  Created by Apple on 27/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class TimelineNoWorkoutCell: UITableViewCell, Reusable {

    @IBOutlet weak var viewDash: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblNoWorkout: UILabel!
    @IBOutlet weak var btnStartWorkout: UIButton!
    
    weak var delegate: TimelineNoWorkoutCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        viewDash.createDashedLine(from: CGPoint.init(x: 0, y: 0), to: CGPoint.init(x: 0, y: viewDash.frame.size.height-10), color: .lightGray, strokeLength: 7, gapLength: 4, width: 0.5)
    }
    
    private func setup() {
        selectionStyle = .none
//        btnStartWorkout.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        delegate?.startWorkout()
    }
    
}
