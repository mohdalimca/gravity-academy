//
//  NoDataTableCell.swift
//  Gravity Academy
//
//  Created by Apple on 18/01/21.
//  Copyright © 2021 W3Surface. All rights reserved.
//

import UIKit

class NoDataTableCell: UITableViewCell, Reusable {

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func configure<T>(with content: T) {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
