//
//  TimelineProgrammCell.swift
//  Gravity Academy
//
//  Created by Apple on 28/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class TimelineProgrammCell: UITableViewCell, Reusable {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var lblFitnessLevel: UILabel!
    @IBOutlet weak var lblCompletedDate: UILabel!
    @IBOutlet weak var imageBG: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var viewDash: UIView!
    @IBOutlet weak var imageLike: UIImageView!
    
    weak var delegate: TechniquesListCellDelegate?
    var didLikeId: ((_ id:String, _ indexPath:IndexPath) -> Void)?
    var id: String!
    var indexPath: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    func configure<T>(with content: T) {
        viewDash.createDashedLine(from: CGPoint.init(x: 0, y: 0), to: CGPoint.init(x: 0, y: viewDash.frame.size.height-10), color: .lightGray, strokeLength: 7, gapLength: 4, width: 0.5)

        if let plan = content as? TrainingPlan {
            lblTitle.text = plan.title
            lblFitnessLevel.text = plan.fitnessLevel
            imageLike.isHidden = true
            if (plan.isLike == "1") {
                imageLike.isHidden = false
            }
            self.id = plan.tpID
            imageBG.downloadImageFrom(urlString: plan.tpImgURL ?? "", with: shadowImage)
            return
        }
    }
    
    private func setup() {
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
