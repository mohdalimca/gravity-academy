//
//  TimelineWorkoutCell.swift
//  Gravity Academy
//
//  Created by Apple on 28/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class TimelineWorkoutCell: UITableViewCell, Reusable {

    @IBOutlet weak var imageBG: UIImageView!
    @IBOutlet weak var imageLike: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddedDate: UILabel!
    @IBOutlet weak var lblCompletedDate: UILabel!
    @IBOutlet weak var viewDash: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    private func setup() {
        selectionStyle = .none
    }

    func configure<T>(with content: T) {
        guard let workout = content as? Workouts else { return }
        lblTitle.text = workout.title
        lblAddedDate.text = workout.addedDate?.gravityDate()
        lblCompletedDate.text = workout.woCompleteDate?.gravityDate()
        imageBG.downloadImageFrom(urlString: workout.woImgURL ?? "", with: shadowImage)
        imageLike.isHidden = true
        if workout.isLike == "1" {
            imageLike.isHidden = false
        }
        viewDash.createDashedLine(from: CGPoint.init(x: 0, y: 0), to: CGPoint.init(x: 0, y: viewDash.frame.size.height-10), color: .lightGray, strokeLength: 7, gapLength: 4, width: 0.5)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
