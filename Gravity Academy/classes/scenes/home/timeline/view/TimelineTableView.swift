//
//  TimelineTableView.swift
//  Gravity Academy
//
//  Created by Apple on 27/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class TimelineTableView: UITableView {
    
    var viewModel: TimelineViewModel!
    var spinner: UIActivityIndicatorView!
    lazy var refresh: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return control
    }()
    
    var didSelectAtIndex:((Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ viewModel: TimelineViewModel) {
        self.viewModel = viewModel
        self.setup()
    }
    
    func setup() {
        dataSource = self
        delegate = self
        reloadData()
        registerReusable(ListHeaderCell.self)
        refreshControl = refresh
//        addSpinner()
    }
    
    private func addSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: 44)
        tableFooterView = spinner
        tableFooterView?.isHidden = true
    }
    
    @objc func refresh(_ control:UIRefreshControl) {
        viewModel.view?.onAction(.refreshTableView(.top))
    }
    
    private func timelineNoWorkoutCell(_ indexPath:IndexPath) -> TimelineNoWorkoutCell {
        let cell = self.dequeueReusable(indexPath) as TimelineNoWorkoutCell
        cell.configure(with: 1)
        return cell
    }
    
    private func timelineWorkoutCell(_ indexPath:IndexPath) -> TimelineWorkoutCell {
        let cell = self.dequeueReusable(indexPath) as TimelineWorkoutCell
        cell.configure(with: viewModel.completedWorkouts[indexPath.row])
        return cell
    }
    
    private func timelineProgrammCell(_ indexPath:IndexPath) -> TimelineProgrammCell {
        let cell = self.dequeueReusable(indexPath) as TimelineProgrammCell
        cell.configure(with: viewModel.completedWorkouts[indexPath.row])
        return cell
    }
    
    private func timelineSkillCell(_ indexPath:IndexPath) -> TimelineSkillCell {
        let cell = self.dequeueReusable(indexPath) as TimelineSkillCell
        cell.configure(with: viewModel.completedWorkouts[indexPath.row])
        return cell
    }
    
    private func noDataTableCell(_ indexPath:IndexPath) -> NoDataTableCell {
        let cell = self.dequeueReusable(indexPath) as NoDataTableCell
        return cell
    }
}

extension TimelineTableView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch viewModel.buttonAction {
        case .workout:
            return timelineWorkoutCell(indexPath)
        case .program:
            return timelineProgrammCell(indexPath)
        case .skills:
            return timelineSkillCell(indexPath)
        default:
            return noDataTableCell(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectAtIndex?(indexPath.row)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.count < viewModel.totalCount && indexPath.row == viewModel.count - 1 && viewModel.totalCount > 10 {
//            spinner.startAnimating()
            tableFooterView?.isHidden = false
            viewModel.view?.onAction(.refreshTableView(.bottom))
        } else {
//            spinner.stopAnimating()
            tableFooterView?.isHidden = true
        }
    }
}



