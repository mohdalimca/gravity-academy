
//
//  TimelineViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 27/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class TimelineViewModel {
    
    weak var view: UserProfileViewRepresentable?
    var provider: UserProfileServiceProviderable
    var finished:FavouriteWorkout?
    var myProfile:MyProfile!
    var completedWorkouts:[Workouts] = []
    var currentPageNo = 1
    var totalCount = 0
    var buttonAction: TimelineHeaderButtonAction = .workout
    var refreshOption: RefreshOption = .top
    var selectedIndex:Int = 0
    
    init(provider:UserProfileServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    var count:Int {
        switch buttonAction {
        case .workout:
            return completedWorkouts.count
        case .program:
            return completedWorkouts.count
        case .skills:
            return completedWorkouts.count
        default:
            return 1
        }
    }
    
    func profile() {
        provider.getProfile()
    }
    
    func compeletedWorkoutList() {
        provider.completeWorkout(param: WorkoutsParams.CompleteWokrout(page_no: String(currentPageNo)))
    }
    
    func compeletedProgramList() {
        provider.completeWorkout(param: WorkoutsParams.CompleteWokrout(page_no: String(currentPageNo)))
    }
    
    func compeletedSkillList() {
        provider.completeWorkout(param: WorkoutsParams.CompleteWokrout(page_no: String(currentPageNo)))
    }
}

extension TimelineViewModel: UserProfileServiceProviderDelegate {
    func completed<T>(for action: UserProfileAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .profile:
                        self.myProfile = resp.data?.myProfile
                    case .completeWorkout:
                        self.finished = resp.data?.favouriteWorkout
                        self.totalCount = (resp.data?.favouriteWorkout?.total ?? "0").toInt
                        if let workouts = resp.data?.favouriteWorkout?.workouts {
                            if self.refreshOption == .top {
                                self.completedWorkouts.removeAll()
                            }
                            self.completedWorkouts.append(contentsOf: workouts)
                        }
                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}

