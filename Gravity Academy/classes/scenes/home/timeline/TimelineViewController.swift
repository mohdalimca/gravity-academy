//
//  TimelineViewController.swift
//  Gravity Academy
//
//  Created by Apple on 27/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class TimelineViewController: UIViewController {
    
    @IBOutlet weak var header: TimelineHeaderView!
    @IBOutlet weak var tableView: TimelineTableView!
    @IBOutlet weak var viewNoData: UIView! {
        didSet {
            viewNoData.isHidden = true
        }
    }
    
    weak var delegate: TimelineViewControllerDelegate?
    weak var router: NextSceneDismisserPresenter?
    let viewModel: TimelineViewModel = TimelineViewModel(provider: UserProfileServiceProvider())
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setup()
    }
    
    private func setup() {
        startAnimation()
        viewModel.profile()
        viewModel.compeletedWorkoutList()
        viewModel.view = self
        header.delegate = self
        tableView.configure(viewModel)
        tableView.didSelectAtIndex = didSelectAtIndex
    }
    
    private func didSelectAtIndex(_ index:Int) {
        viewModel.selectedIndex = index
        switch viewModel.buttonAction {
        case .all :
            break
        case .workout:
            delegate?.didOpenWorkoutDetails(viewModel.completedWorkouts[index])
//            router?.push(scene: .workoutDetail)
        case .program:
            delegate?.didOpenProgramDetails(viewModel.completedWorkouts[index])
//            router?.push(scene: .programDetail)
        case .skills:
            delegate?.didOpenSkillDetails(viewModel.completedWorkouts[index])
//            router?.push(scene: .techniqueDetail)
        case .statistics:
            break
        default:
            break
        }
    }
    
    private func populateUserDetails() {
        header.populate(viewModel.myProfile.name ?? "", viewModel.myProfile.image ?? "")
    }
    
    private func showTableView(_ hidden:Bool) {
        tableView.reloadData()
//        tableView.isHidden = !hidden
//        viewNoData.isHidden = hidden
        tableView.isHidden = false
        viewNoData.isHidden = true
        stopAnimating()
    }
    
    private func reloadCompleteWorkouts() {
        if !viewModel.completedWorkouts.isEmpty {
            header.lblWorkoutCount.text = "\(viewModel.completedWorkouts.count)"
            showTableView(true)
        } else {
            viewModel.buttonAction = .nodata
            header.lblWorkoutCount.text = "0"
            showTableView(false)
        }
    }
    
    private func reloadCompleteProgram() {
        if !viewModel.completedWorkouts.isEmpty {
            header.lblWorkoutCount.text = "\(viewModel.completedWorkouts.count)"
            showTableView(true)
        } else {
            viewModel.buttonAction = .nodata
            header.lblWorkoutCount.text = "0"
            showTableView(false)
        }
    }
    
    private func reloadCompleteSkill() {
        if !viewModel.completedWorkouts.isEmpty {
            header.lblWorkoutCount.text = "\(viewModel.completedWorkouts.count)"
            showTableView(true)
        } else {
            viewModel.buttonAction = .nodata
            header.lblWorkoutCount.text = "0"
            showTableView(false)
        }
    }
    
    private func refreshTable() {
        tableView.refresh.endRefreshing()
        viewModel.currentPageNo = 1
        sendAction(action: viewModel.buttonAction)
    }
    
    private func loadMore() {
        viewModel.currentPageNo += 1
        sendAction(action: viewModel.buttonAction)
    }
}

extension TimelineViewController: TimelineHeaderViewDelegate {
    
    func sendAction(action: TimelineHeaderButtonAction) {
        viewModel.buttonAction = action
        switch action {
        case .back: router?.dismiss(controller: .athlete)
        case .workout:
            startAnimation()
            viewModel.compeletedWorkoutList()
        case .program:
            startAnimation()
            viewModel.compeletedWorkoutList()
        case .skills:
            startAnimation()
            viewModel.compeletedWorkoutList()
        default: break
        }
    }
}

extension TimelineViewController:UserProfileViewRepresentable {
    func onAction(_ action: UserProfileAction) {
        switch action {
        case let .errorMessage(text):
            showBannerWith(text: text, style: .warning)
            stopAnimating()
        case .profile:
            populateUserDetails()
        case .completeWorkout:
            reloadCompleteWorkouts()
        case .completeProgram:
            reloadCompleteProgram()
        case .completeSkill:
            reloadCompleteSkill()
        case let .refreshTableView(option):
            viewModel.refreshOption = option
            (option == .top) ? refreshTable() : loadMore()
        default:
            stopAnimating()
        }
    }
}
