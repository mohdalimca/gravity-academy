//
//  TimelineAdaptables.swift
//  Gravity Academy
//
//  Created by Apple on 27/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum TimelineHeaderButtonAction: String {
    case all
    case back
    case skills
    case workout
    case program
    case statistics
    case nodata
}

protocol TimelineHeaderViewDelegate:class {
    func sendAction(action: TimelineHeaderButtonAction)
}
protocol TimelineNoWorkoutCellDelegate:class {
    func startWorkout()
}
protocol TimelineViewControllerDelegate:class {
    func didOpenWorkoutDetails(_ workout: Workouts)
    func didOpenProgramDetails(_ workout: Workouts)
    func didOpenSkillDetails(_ workout: Workouts)
}
