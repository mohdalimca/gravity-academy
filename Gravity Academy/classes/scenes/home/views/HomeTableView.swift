//
//  HomeTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class HomeTableView: UITableView {
    
    var viewModel: HomeViewModel!
    weak var delegateTable: HomeTableViewDelegate?
    var didLikeDislikeWorkoutLibrary:((_ param:LikeDislikeParam, _ indexPath:IndexPath) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ viewModel: HomeViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
        registerReusable(HomeHeaderCell.self)
        reloadData()
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        switch sender.name {
        case "0": delegateTable?.didSelect(at: 0)
        case "1": delegateTable?.didSelect(at: 1)
        case "2": delegateTable?.didSelect(at: 2)
        case "3": delegateTable?.didSelect(at: 3)
        default : delegateTable?.didSelect(at: -1)
        }
    }
    
    private func tableHeader(at section:Int, with tableView:UITableView) -> UIView {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeHeaderCell") as? HomeHeaderCell
        cell?.configure(with: viewModel.item(at: section))
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.handleTap(_:)))
        tap.name = "\(section)"
        tap.numberOfTapsRequired = 1
        cell!.btnViewAll.addGestureRecognizer(tap)
        cell!.btnViewAll.isHidden = true
        return cell?.contentView ?? UIView()
    }
    
    private func athleteTableHeader(at section:Int, with tableView:UITableView) -> UIView {
        
        let cell = tableView.dequeueReusable(IndexPath(row: 0, section: 0)) as AthleteTableViewCell
        cell.collectionView.delegateAthlete = self
        cell.configure(with: viewModel.item(at: section))
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.handleTap(_:)))
        tap.name = "-1"
        tap.numberOfTapsRequired = 1
        cell.btnViewAll.addGestureRecognizer(tap)
        return cell.contentView
    }
    
    private func setTitle(_ index:Int) -> String {
        switch index {
        case 1:
            return "\(viewModel.dashBoardData?.totalWorkout ?? 0) Workouts"
        case 2:
            return "\(viewModel.dashBoardData?.totalTrainingPlan ?? 0) Programms"
        case 3:
            return "\(viewModel.dashBoardData?.totalSkills ?? 0) Skills"
        case 4:
            return "\(viewModel.dashBoardData?.totalExercise ?? 0) Exercises"
        case 5:
            return "\(viewModel.dashBoardData?.equipment ?? 0) Equipments"
        default:
            return "\(viewModel.dashBoardData?.dayWorkout ?? 0)"
        }
    }
}

extension HomeTableView: UITableViewDelegate, UITableViewDataSource  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items(in: section).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.separatorStyle = .none
        switch viewModel.item(for: indexPath).scence {
        case .dayWorkout:
            let cell = tableView.dequeueReusable(indexPath) as DayWorkoutTableCell
            cell.imageBG.image = UIImage(named: viewModel.imageArray[indexPath.row])
            cell.lblTitle.text = viewModel.titleArray[indexPath.section]
            cell.didLikeDislikeWorkoutLibrary = didLikeDislikeWorkoutLibrary
            cell.configure(with: viewModel.workoutOfDay)
            return cell
        default:
            let cell = tableView.dequeueReusable(indexPath) as HomeTableViewCell
            cell.lblTitle.text = viewModel.titleArray[indexPath.section]
            cell.lblDate.text = self.setTitle(indexPath.section)
            cell.imageBG.image = UIImage(named: viewModel.imageArray[indexPath.section])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegateTable?.didSelect(at: indexPath.section)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section != 0 {
            return tableHeader(at: section, with: tableView)
        } else {
            return athleteTableHeader(at: section, with: tableView)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section != 0) ? 40.0 : 1.0 // 120.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}

extension HomeTableView: HomeHeaderCellDelegate {
    func didSelectViewAll(at index: Int) {
        delegateTable?.didSelect(at: index)
    }
}

extension HomeTableView: WorkoutCollectionViewDelegate {
    func didSelect(at index: Int, section: WorkoutsSection) {
        if section != .none {
            delegateTable?.didShowDetailView(section: section)
        } else {
            delegateTable?.didSelect(at: index)
        }
    }
}

extension HomeTableView: AthleteCollectionViewDelegate {
    func sendAction(action: Scenes, with id: String) {
    }
}

extension HomeTableView:DayWorkoutTableCellDelegate {
    func likeUnlikeButtonAction() {
        viewModel.apiName = .likeUnlike
        viewModel.likeAPI(param: LikeDislikeParam(type: .workout, type_id: viewModel.workoutOfDay?.workoutID))
    }
}
