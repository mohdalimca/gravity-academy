//
//  HomeTabBar.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class HomeTabBar: UITabBar {
    
    var centerButton: UIButton?

//    override init(frame: CGRect) {
//        self.setup()
//    }
//
//    required init?(coder: NSCoder) {
//        self.setup()
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup() {
        isTranslucent = false
        clipsToBounds = true
        tintColor = UIColor.init(hex: APP_THEME_ORANGE_COLOR)
        unselectedItemTintColor = .white
        itemPositioning = .centered
        setupItems()
    }
    
    func setupItems() {
        let workout = UITabBarItem()
        workout.title = "Workout"
        workout.image = UIImage(named: "tab-workout")
        workout.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -4)
        workout.tag = 1
        
        let favorite = UITabBarItem()
        favorite.title = "Favorite"
        favorite.image = UIImage(named: "tab-favourite")
        favorite.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -4)
        favorite.tag = 2
        
        let timeline = UITabBarItem()
        timeline.title = "Timeline"
        timeline.tag = 3
        
        let profile = UITabBarItem()
        profile.title = "Profile"
        profile.image = UIImage(named: "tab-profile")
        profile.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -4)
        profile.tag = 4
        
        let academy = UITabBarItem()
        academy.title = "Academy"
        academy.image = UIImage(named: "tab-gravity-academy")
        academy.tag = 5
        academy.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -4)
        
        setItems([workout, favorite, timeline, profile, academy], animated: true)
    }    
}
