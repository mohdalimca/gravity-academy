//
//  HomeTabViewController.swift
//  Gravity Academy
//
//  Created by Apple on 15/01/21.
//  Copyright © 2021 W3Surface. All rights reserved.
//

import UIKit

class HomeTabViewController: UITabBarController {
    
    let workouts: WorkoutsViewController = WorkoutsViewController.from(from: .home, with: .workouts)
    let favourite: FavouriteViewController = FavouriteViewController.from(from: .home, with: .favourite)
    let profile: UserProfileViewController = UserProfileViewController.from(from: .home, with: .profile)
    let timeline: TimelineViewController = TimelineViewController.from(from: .home, with: .timeline)
    let gravityAcademy: GravityAcademyViewController = GravityAcademyViewController.from(from: .gravityAcademy, with: .gravityAcademy)
    let nointernet: NoInternetViewController = NoInternetViewController.from(from: .common, with: .nointernet)
    
    weak var router: NextSceneDismisserPresenter?
    var centerButton = UIButton.init(type: .custom)
    
    let selectedImage = UIImage(named: "tab-timeline-selected")
    let unselectedImage = UIImage(named: "tab-timeline-unselected")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupItems()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupMiddleButton()
    }
    
    func setupItems() {
        tabBar.isTranslucent = false
        tabBar.clipsToBounds = true
        tabBar.tintColor = UIColor.init(hex: APP_THEME_ORANGE_COLOR)
        tabBar.unselectedItemTintColor = .white
        tabBar.itemPositioning = .centered
        
        let workout = UITabBarItem()
        workout.title = "Workout"
        workout.image = UIImage(named: "tab-workout")
        workout.tag = 1
        
        let favorite = UITabBarItem()
        favorite.title = "Favorite"
        favorite.image = UIImage(named: "tab-favourite")
        favorite.tag = 2
        
        let timeline = UITabBarItem()
        timeline.title = "Timeline"
        //        favorite.image = UIImage(named: "tab-timeline")
        timeline.tag = 3
        
        let profile = UITabBarItem()
        profile.title = "Profile"
        profile.image = UIImage(named: "tab-profile")
        profile.tag = 4
        
        let academy = UITabBarItem()
        academy.title = "Academy"
        academy.image = UIImage(named: "tab-gravity-academy")
        academy.tag = 5
        
        self.workouts.tabBarItem = workout
        self.workouts.router = router
        
        self.favourite.tabBarItem = favorite
        self.favourite.router = router
        
        self.timeline.tabBarItem = timeline
        self.timeline.router = router
        
        self.profile.tabBarItem = profile
        self.profile.router = router
        
        self.gravityAcademy.tabBarItem = academy
        self.gravityAcademy.router = router
        
        self.viewControllers = [self.workouts, self.favourite, self.timeline, self.profile, self.gravityAcademy]
        setViewControllers(self.viewControllers, animated: true)
        delegate = self
    }
    
    // TabBarButton – Setup Middle Button
    func setupMiddleButton() {
        centerButton = UIButton(frame: CGRect(x: 0, y: 0, width: 62, height: 62))
        var centerButtonFrame = centerButton.frame
//        centerButtonFrame.origin.x = self.view.bounds.width / 2 - centerButtonFrame.size.width / 2
//        centerButtonFrame.origin.y = self.view.bounds.height - (centerButtonFrame.height + ((tabBar.frame.size.height - 10) / 2))
        
        let xOrigin = self.view.bounds.width / 2 - centerButtonFrame.size.width / 2
        var yOrigin: CGFloat = 0.0
        if tabBar.frame.size.height == 49 {
            yOrigin = self.view.bounds.height - (centerButtonFrame.height + ((tabBar.frame.size.height - 10) / 2))
        } else {
            yOrigin = self.view.bounds.height - (centerButtonFrame.height + 49)
        }

        centerButtonFrame.origin.x = xOrigin
        centerButtonFrame.origin.y = yOrigin
        centerButton.frame = centerButtonFrame
        centerButton.layer.cornerRadius = centerButtonFrame.height/2
        self.view.addSubview(centerButton)
        
        centerButton.setImage(unselectedImage, for: .normal)
        centerButton.addTarget(self, action: #selector(centerButtonAction(sender:)), for: .touchUpInside)
        self.view.layoutIfNeeded()
    }
    
    // Menu Button Touch Action
    @objc func centerButtonAction(sender: UIButton) {
        self.selectedIndex = 2
        self.selectedViewController = self.timeline
        self.changeCenterButtonImage(selectedImage!)
    }
    
    private func changeCenterButtonImage(_ image:UIImage) {
        DispatchQueue.main.async {
            self.centerButton.setImage(image, for: .normal)
        }
    }
}

extension HomeTabViewController: UITabBarControllerDelegate {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag != 3 {
            self.changeCenterButtonImage(unselectedImage!)
        } else {
            self.changeCenterButtonImage(selectedImage!)
        }
        print(item.tag)
    }
}
