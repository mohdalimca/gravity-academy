//
//  FavouriteViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class FavouriteViewController: UIViewController {

    @IBOutlet weak var tableView: FavouriteTableView!
    let viewModel = FavouriteViewModel(provider: FavouriteServiceProvider())
    weak var router: NextSceneDismisserPresenter?
    weak var delegateTable: FavouriteTableViewDelegate?
    weak var delegate: FavouriteViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setup()
    }
    
    private func setup() {
        startAnimation()
        viewModel.view = self
        viewModel.favouritesData()
        addNotificationObserver()
        tableView.isHidden = true
        tableView.configure(viewModel)
        tableView.delegateTable = self
        tableView.didShowLibrary = didShowLibrary
        tableView.didSelectViewAll = didSelectViewAll
        tableView.didSelectAtIndexPath = didSelectAtIndexPath
    }
    
    private func addNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(likeDislikeUpdate(_:)), name: .handleExercisLikeDislike, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(likeDislikeUpdate(_:)), name: .handleWorkoutLikeDislike, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(likeDislikeUpdate(_:)), name: .handleTrainingLikeDislike, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(likeDislikeUpdate(_:)), name: .handleSkillsLikeDislike, object: nil)
    }
    
    private func reload() {
        tableView.reloadData()
        tableView.isHidden = false
        stopAnimating()
    }
    
    private func getFavourites() {
        tableView.refreshControl?.endRefreshing()
        viewModel.favouritesData()
    }
    
    func didTryAgain(_ api:AppAPI) {
        startAnimation()
        viewModel.favouritesData()
    }
    
    @objc func likeDislikeUpdate(_ notification: Notification) {
        viewModel.favouritesData()
    }

    // MARK: Closures
    private func didSelectViewAll(_ section:Int) {
        switch section {
        case 0:
            viewModel.listScene = .favouriteSkills
        case 1:
            viewModel.listScene = .favouriteTrainingPlan
        case 2:
            viewModel.listScene = .favouriteWorkout
        default:
            break
        }
        router?.push(scene: .favouritesList)
    }
    
    private func didSelectAtIndexPath(_ indexPath:IndexPath) {
        switch indexPath.section {
        case 0:
            viewModel.selectedSkill = viewModel.favourites?.skills![indexPath.row]
            delegate?.didOpenScene(.techniqueDetail, nil, viewModel.selectedSkill, nil)

        case 1:
            viewModel.selectedPlan = viewModel.favourites?.tp![indexPath.row]
            delegate?.didOpenScene(.programDetail, viewModel.selectedPlan, nil, nil)
            
        case 2:
            viewModel.selectedWorkout = viewModel.favourites?.wo![indexPath.row]
            delegate?.didOpenScene(.workoutDetail, nil, nil, viewModel.selectedWorkout)

        default:
            break
        }
    }
    
    private func didShowLibrary(_ section:Int) {
        if section != 3 { // show workout library
            router?.push(scene: .workoutLibrary)
        } else { // show exercise library
            router?.push(scene: .allExercise)
        }
    }
}

extension FavouriteViewController: FavouriteTableViewDelegate {
    func didSelectAt(row: Int) {
        router?.push(scene: .savedExercise)
    }
}

extension FavouriteViewController: FavouriteViewRepresentable {
    func onAction(_ action: FavouriteAction) {
        switch action {
        case let .errorMessage(text):
            showBannerWith(text: text, style: .warning)
            stopAnimating()
        case .favourites:
            reload()
        case .refreshTableView(.top):
            getFavourites()
        case .nointernet:
            router?.push(scene: .nointernet)
        default:
            stopAnimating()
        }
    }
}
