//
//  FavouritesListTableView.swift
//  Gravity Academy
//
//  Created by Apple on 12/09/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import UIKit

class FavouritesListTableView: UITableView {
    
    var viewModel: FavouritesListViewModel!
    var didSelectedAtIndex: ((Int) -> Void)?
    var spinner: UIActivityIndicatorView!
    lazy var refresh: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return control
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ viewModel: FavouritesListViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        showsVerticalScrollIndicator = false
        dataSource = self
        delegate = self
        estimatedRowHeight = 70.0
        rowHeight = UITableView.automaticDimension
        refreshControl = refresh
        addSpinner()
    }
    
    private func addSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: 44)
        tableFooterView = spinner
        tableFooterView?.isHidden = true
    }
    
    @objc func refresh(_ control:UIRefreshControl) {
        viewModel.view?.onAction(.refreshTableView(.top))
    }
}

extension FavouritesListTableView: UITableViewDelegate, UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        switch viewModel.favouriteScene {
        case .favouriteWorkout:
            let cell = tableView.dequeueReusable(indexPath) as WorkoutFavouriteTableCell
            cell.configure(with: viewModel.favouriteWorkout[indexPath.row])
            return cell
        case .favouriteTrainingPlan:
            let cell = tableView.dequeueReusable(indexPath) as ProgramTechniqueFavouriteCell
            cell.configure(with: viewModel.favouriteTrainingPlan[indexPath.row])
            return cell
        case .favouriteSkills:
            let cell = tableView.dequeueReusable(indexPath) as ProgramTechniqueFavouriteCell
            cell.configure(with: viewModel.favouriteSkills[indexPath.row])
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectedAtIndex?(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.count < viewModel.totalCount && indexPath.row == viewModel.count - 1 && viewModel.totalCount > 10 {
            spinner.startAnimating()
            tableFooterView?.isHidden = false
            viewModel.view?.onAction(.refreshTableView(.bottom))
        } else {
            spinner.stopAnimating()
            tableFooterView?.isHidden = true
        }
    }
    
    
}

extension FavouriteTableView: FavouriteCollectionViewDelegate {
    func sendAction(action: Scenes) {
    }
}



