//
//  ProgramTechniqueFavouriteCell.swift
//  
//
//  Created by Apple on 12/09/20.
//

import UIKit

class ProgramTechniqueFavouriteCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblFitnessLevel: UILabel!
    @IBOutlet weak var imageBG: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure<T>(with content: T) {
        if let plan = content as? TrainingPlan {
            lblTitle.text = plan.title
            lblFitnessLevel.text = plan.fitnessLevel
            imageBG.downloadImageFrom(urlString: plan.tpImgURL ?? "", with: shadowImage)
        }
        
        if let skill = content as? SkillList {
            lblTitle.text = skill.title
            lblFitnessLevel.text = skill.fitnessLevel
            imageBG.downloadImageFrom(urlString: skill.introMediaData?.image ?? "", with: shadowImage)
        }
    }
}
