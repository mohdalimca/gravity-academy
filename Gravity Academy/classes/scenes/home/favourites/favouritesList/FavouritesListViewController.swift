//
//  FavouritesListViewController.swift
//  Gravity Academy
//
//  Created by Apple on 12/09/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import UIKit

class FavouritesListViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tableView: FavouritesListTableView!
    let viewModel = FavouritesListViewModel(provider: FavouriteServiceProvider())
    weak var router: NextSceneDismisserPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setup()
    }

    private func setup() {
        setTitle()
        startAnimation()
        viewModel.view = self
        tableView.configure(viewModel)
        tableView.didSelectedAtIndex = didSelectedAtIndex
        viewModel.favouriteList()
        [btnBack].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    private func setTitle() {
        switch viewModel.favouriteScene {
        case .favouriteTrainingPlan:
            lblTitle.text = "Liked Techniques"
        case .favouriteSkills:
            lblTitle.text = "Liked Training Plans"
        case .favouriteWorkout:
            lblTitle.text = "Liked Workouts"
        default:
            break
        }

    }
    
    @objc func buttonPressed(_ btn:UIButton) {
        router?.dismiss(controller: .favouritesList)
    }
    
    private func reload() {
        tableView.reloadData()
        stopAnimating()
    }
        
    private func didSelectedAtIndex(_ index:Int) {
        switch viewModel.favouriteScene {
        case .favouriteTrainingPlan:
            viewModel.selectedPlan = viewModel.favouriteTrainingPlan[index]
            router?.push(scene: .programDetail)
        case .favouriteSkills:
            viewModel.selectedSkill = viewModel.favouriteSkills[index]
            router?.push(scene: .techniqueDetail)
        case .favouriteWorkout:
            viewModel.selectedWorkout = viewModel.favouriteWorkout[index]
            router?.push(scene: .detail)
        default:
            break
        }
    }
    
    private func refreshTable() {
        tableView.refresh.endRefreshing()
        viewModel.currentPageNo = 1
        viewModel.favouriteList()
    }
    
    private func loadMore() {
        viewModel.currentPageNo += 1
        viewModel.favouriteList()
    }
}

extension FavouritesListViewController: FavouriteViewRepresentable {
    func onAction(_ action: FavouriteAction) {
        switch action {
        case let .errorMessage(text):
            showBannerWith(text: text, style: .warning)
            stopAnimating()
        case .favourites:
            reload()
        case let .refreshTableView(option):
            viewModel.refreshOption = option
            (option == .top) ? refreshTable() : loadMore()
        default:
            stopAnimating()
        }
    }
}

