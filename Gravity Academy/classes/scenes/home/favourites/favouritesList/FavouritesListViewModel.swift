//
//  FavouritesListViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 12/09/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class FavouritesListViewModel {

    weak var view: FavouriteViewRepresentable?
    var provider: FavouriteServiceProviderable
    var favouriteWorkout = [Workouts]()
    var favouriteSkills =  [SkillList]()
    var favouriteTrainingPlan = [TrainingPlan]()
    var selectedPlan: TrainingPlan!
    var selectedSkill: SkillList!
    var selectedWorkout: Workouts!
    var favouriteExercise = [Exercise]()
    var favouriteScene:Scenes!
    var currentPageNo = 1
    var totalCount = 0
    var refreshOption: RefreshOption = .top
    
    init(provider:FavouriteServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    var count:Int {
        switch favouriteScene {
        case .favouriteWorkout:
            return self.favouriteWorkout.count
        case .favouriteExercise:
            return self.favouriteExercise.count
        case .favouriteSkills:
            return self.favouriteSkills.count
        default:
            return self.favouriteTrainingPlan.count
        }
    }
    
    func favouriteList() {
        provider.favouriteList(param: FavouriteList(page_no: String(currentPageNo), page_size: "10"), scene: favouriteScene)
    }
}

extension FavouritesListViewModel: FavouriteServiceProviderDelegate {
    func completed<T>(for action: FavouriteAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch self.favouriteScene {
                    case .favouriteWorkout:
                        if self.refreshOption == .top {
                            self.favouriteWorkout.removeAll()
                        }
                        self.favouriteWorkout.append(contentsOf: resp.data?.favouriteWorkout?.workouts ?? [])
                        self.totalCount = resp.data?.favouriteWorkout?.total?.toInt ?? self.totalCount
                        
                    case .favouriteTrainingPlan:
                        if self.refreshOption == .top {
                            self.favouriteTrainingPlan.removeAll()
                        }
                        self.favouriteTrainingPlan.append(contentsOf: resp.data?.favouriteTrainingPlan?.trainingPlan ?? [])
                        self.totalCount = resp.data?.favouriteTrainingPlan?.total?.toInt ?? self.totalCount

                    case .favouriteSkills:
                        if self.refreshOption == .top {
                            self.favouriteSkills.removeAll()
                        }
                        self.favouriteSkills.append(contentsOf: resp.data?.favouriteSkills?.skills ?? [])
                        self.totalCount = resp.data?.favouriteSkills?.total?.toInt ?? self.totalCount

                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
