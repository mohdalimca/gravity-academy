//
//  FavouritesListCoordinator.swift
//  Gravity Academy
//
//  Created by Apple on 12/09/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class FavouritesListCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: FavouritesListViewController = FavouritesListViewController.from(from: .home, with: .favouritesList)
    
    var workoutDetail: DetailCoordinator!
    var programDetail: ProgramDetailCoordinator!
    var skillDetail: TechniqueDetailCoordinator!
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    func start(_ scene:Scenes) {
        super.start()
        onStart()
        controller.viewModel.favouriteScene = scene
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.router = self
    }
    
    private func startWorkoutDetail() {
        workoutDetail = DetailCoordinator(router: Router())
        add(workoutDetail)
        workoutDetail.delegate = self
        workoutDetail.start(detail: controller.viewModel.selectedWorkout, categoryTitle: "Test title")
        router.present(workoutDetail, animated: true)
    }
    
    private func startProgramDetail() {
        let r = Router()
        programDetail = ProgramDetailCoordinator(router: r)
        add(programDetail)
        programDetail.delegate = self
        programDetail.start(trainingPlan:controller.viewModel.selectedPlan)
        router.present(programDetail, animated: true)
    }
    
    private func startSkillDetail() {
        skillDetail = TechniqueDetailCoordinator(router: Router())
        add(skillDetail)
        skillDetail.delegate = self
        skillDetail.start(skill: controller.viewModel.selectedSkill)
        router.present(skillDetail, animated: true)
    }
}

extension FavouritesListCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .detail: startWorkoutDetail()
        case .programDetail: startProgramDetail()
        case .techniqueDetail: startSkillDetail()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension FavouritesListCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
