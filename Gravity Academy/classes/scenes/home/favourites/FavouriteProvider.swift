//
//  FavouriteProvider.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class FavouriteServiceProvider: FavouriteServiceProviderable {
    var delegate: FavouriteServiceProviderDelegate?
    private let task = WorkoutsTask()
    
    func favourites() {
        task.favourites { (resp, err) in
            if err != nil {
                self.delegate?.completed(for: .favourites, with: resp, error: err)
                return
            }
            self.delegate?.completed(for: .favourites, with: resp, error: nil)
        }
    }
    
    func favouriteList(param:FavouriteList, scene:Scenes) {
        task.favouriteList(param: param, scene: scene) { (resp, err) in
            if err != nil {
                self.delegate?.completed(for: .favourites, with: resp, error: err)
                return
            }
            self.delegate?.completed(for: .favourites, with: resp, error: nil)
        }
    }
    
    func likeDislike(param: LikeDislikeParam) {}
}
