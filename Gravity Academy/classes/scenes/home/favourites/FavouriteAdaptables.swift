//
//  FavouriteAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum RefreshOption:String {
    case top
    case bottom
}

enum FavouriteAction {
    case errorMessage(_ text:String)
    case login
    case favourites
    case detail
    case likeDislike
    case nointernet
    case favouriteWorkout
    case favouriteTrainingPlan
    case favouriteSkills
    case favouriteExercise
    case refreshTableView(_ from: RefreshOption)
}

protocol FavouriteViewControllerDelegate:class {
    func didOpenScene(_ scene:Scenes, _ selectedPlan: TrainingPlan?, _ selectedSkill: SkillList?, _ selectedWorkout: Workouts?)
}

protocol FavouriteTableViewDelegate:class {
    func didSelectAt(row: Int)
}

protocol FavouriteCollectionViewDelegate:class {
    func sendAction(action: Scenes)
}

protocol FavouriteCollectionViewCellDelegate:class {
    func didSelectAt(item: Int)
}

protocol FavouriteViewRepresentable:class {
    func onAction(_ action:FavouriteAction)
}

protocol FavouriteServiceProviderDelegate:class {
    func completed<T>(for action:FavouriteAction, with response:T?, error:APIError?)
}

protocol FavouriteServiceProviderable:class {
    var delegate: FavouriteServiceProviderDelegate? { get set }
    func favourites()
    func likeDislike(param:LikeDislikeParam)
    func favouriteList (param:FavouriteList, scene:Scenes)
}
