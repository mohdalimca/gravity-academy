//
//  FavouriteTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 31/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class FavouriteTableViewCell: UITableViewCell, Reusable {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var collectionView: FavouriteCollectionView!
    var didSelectViewAll: ((Int) -> Void)?
    var didSelectAtIndexPath: ((IndexPath) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    func configure<T>(with content: T) {
        btnViewAll.isHidden = true
        collectionView.didSelectAtIndexPath = didSelectAtIndexPath
        if let skills = content as? [SkillList] {
            if skills.count > 3 {
                btnViewAll.isHidden = false
            }
            collectionView.configure(with: skills, type: .skill)
        }

        if let programs = content as? [TrainingPlan] {
            if programs.count > 3 {
                btnViewAll.isHidden = false
            }
            collectionView.configure(with: programs, type: .program)
        }

        if let workouts = content as? [Workouts] {
            if workouts.count > 3 {
                btnViewAll.isHidden = false
            }
            collectionView.configure(with: workouts, type: .workout)
        }

        if let exercises = content as? [Exercise] {
            collectionView.configure(with: exercises, type: .exercise)
        }
    }
    
    private func setup() {
        selectionStyle = .none
        btnViewAll.addTarget(self, action: #selector(viewAllAction(_:)), for: .touchUpInside)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func viewAllAction(_ sender:UIButton) {
        didSelectViewAll?(sender.tag)
    }
}
