//
//  FavouriteExerciseTableCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 31/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class FavouriteExerciseTableCell: UITableViewCell, Reusable {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNoContentTitle: UILabel!
    @IBOutlet weak var lblSavedExercise: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var btnShowLibrary: UIButton!
    @IBOutlet weak var stackViewNoContent: UIStackView!
    @IBOutlet weak var viewExerciseImage: UIView!
    var tableSection: Int!
    
    var didShowLibrary: ((Int) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func configure<T>(with content: T) {
        guard let item = content as? RowSectionDisplayable else { return }
        lblTitle.text = item.title.capitalized
        updateTextForNoContent()
    }
    
    private func updateTextForNoContent() {
        viewExerciseImage.isHidden = true
        switch tableSection {
        case 0:
            lblNoContentTitle.text = "Skills that you like will appear here"
            btnShowLibrary.isHidden = true
        case 1:
            lblNoContentTitle.text = "Training plans that you like will appear here"
            btnShowLibrary.isHidden = true
        case 2:
            lblNoContentTitle.text = "Workout that you like will appear here"
            btnShowLibrary.setTitle("See workouts library", for: .normal)
            btnShowLibrary.isHidden = false
        default:
            lblNoContentTitle.text = "You dont have any saved exercise yet"
            btnShowLibrary.setTitle("See exercises library", for: .normal)
            btnShowLibrary.isHidden = false
        }
    }
    
    private func setup() {
        selectionStyle = .none
        [btnShowLibrary].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    @objc func buttonPressed(_ sender:UIButton) {
        didShowLibrary?(sender.tag)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
