//
//  FavouriteTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class FavouriteTableView: UITableView {
    
    var viewModel: FavouriteViewModel!
    var didSelectViewAll: ((Int) -> Void)?
    var didSelectAtIndexPath: ((IndexPath) -> Void)?
    weak var delegateTable: FavouriteTableViewDelegate?
    var didShowLibrary: ((Int) -> Void)?
    
    var spinner: UIActivityIndicatorView!
    lazy var refresh: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return control
    }()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addSpinner()
    }
    
    func configure(_ viewModel: FavouriteViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
    }
    
    private func addSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: 44)
        tableFooterView = spinner
        tableFooterView?.isHidden = true
    }
    
    @objc func refresh(_ control:UIRefreshControl) {
        viewModel.view?.onAction(.refreshTableView(.top))
    }
}

extension FavouriteTableView: UITableViewDelegate, UITableViewDataSource  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items(in: section).count
    }
    
    private func showFavouriteTableCell(_ indexPath:IndexPath) -> FavouriteExerciseTableCell {
        let cell = self.dequeueReusable(indexPath) as FavouriteExerciseTableCell
        cell.viewExerciseImage.isHidden = true
        cell.lblTitle.isHidden = false
        cell.stackViewNoContent.isHidden = false
        cell.configure(with: viewModel.item(at: indexPath.section))
        cell.tableSection = indexPath.section
        cell.btnViewAll.tag = indexPath.section
        cell.btnShowLibrary.tag = indexPath.section
        cell.didShowLibrary = didShowLibrary
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        let cell = tableView.dequeueReusable(indexPath) as FavouriteTableViewCell
        cell.lblTitle.text = viewModel.item(at: indexPath.section).title.capitalized
        cell.collectionView.tag = indexPath.section
        cell.didSelectViewAll = didSelectViewAll
        cell.didSelectAtIndexPath = didSelectAtIndexPath
        cell.btnViewAll.tag = indexPath.section
        cell.configure(with: viewModel)
        
        switch indexPath.section {
        case 0:
            if let skills = viewModel.favourites?.skills , !skills.isEmpty {
                cell.configure(with: skills)
            } else {
                return showFavouriteTableCell(indexPath)
            }
            
        case 1:
            if let tp = viewModel.favourites?.tp , !tp.isEmpty {
                cell.configure(with: tp)
            } else {
                return showFavouriteTableCell(indexPath)
            }
            
        case 2:
            if let wo = viewModel.favourites?.wo , !wo.isEmpty {
                cell.configure(with: wo)
            } else {
                return showFavouriteTableCell(indexPath)
            }

        default:
            if let exercise = viewModel.favourites?.exercise , !exercise.isEmpty {
                let cell = tableView.dequeueReusable(indexPath) as FavouriteExerciseTableCell
                cell.viewExerciseImage.isHidden = false
                cell.lblTitle.isHidden = true
                cell.stackViewNoContent.isHidden = true
                return cell
            } else {
                return showFavouriteTableCell(indexPath)
            }
        }
        return cell
    }
    
    private func setImageAccordingToGender() -> UIImage {
        if UserStore.gender == "male" {
            return UIImage(named: "home-exercises-icon-male")!
        } else {
            return UIImage(named: "home-exercises-icon-female")!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 3 {
            if let exercise = viewModel.favourites?.exercise , !exercise.isEmpty {
                delegateTable?.didSelectAt(row: indexPath.row)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}


