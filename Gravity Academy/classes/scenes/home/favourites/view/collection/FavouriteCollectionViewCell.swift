//
//  FavouriteCollectionViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 31/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class FavouriteCollectionViewCell: UICollectionViewCell, Reusable {
    
    @IBOutlet weak var imageBG: UIImageView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblFitness: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    weak var delegate: FavouriteCollectionViewCellDelegate?
    var typeItem:FavouriteTypeItem!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure<T>(with content: T, type:FavouriteTypeItem ) {
        self.typeItem = type
        switch type {
        case .skill:
            if let skill = content as? SkillList {
                lblFitness.text = skill.fitnessLevel
                lblCategory.isHidden = true
                self.updateValues(title: skill.title, category: skill.fitnessLevel, imageURL: skill.introMediaData?.image)
            }
        case .program:
            if let program = content as? TrainingPlan {
                self.updateValues(title: program.title, category: program.fitnessLevel, imageURL: program.tpImgURL)
            }
        case .workout:
            if let workout = content as? Workouts {
                self.updateValues(title: workout.title, category: workout.workoutCategoryTitle, imageURL: workout.woImgURL)
            }
        case .exercise:
            break
        }
    }
    
    func configure<T>(with content: T) {}
    
    private func updateValues(title:String?, category:String?, imageURL:String?) {
        lblTitle.text = title
        lblCategory.text = category
        imageBG.downloadImageFrom(urlString: imageURL ?? "", with: shadowImage)
    }
}
