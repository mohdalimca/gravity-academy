//
//  FavouriteWorkoutCollectionCell.swift
//  Gravity Academy
//
//  Created by Apple on 17/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import UIKit

class FavouriteWorkoutCollectionCell: UICollectionViewCell, Reusable {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblMuscleGroup1: UILabel!
    @IBOutlet weak var lblMuscleGroup2: UILabel!
    @IBOutlet weak var lblMuscleGroup3: UILabel!
    @IBOutlet weak var viewMuscleGroup1: UIView!
    @IBOutlet weak var viewMuscleGroup2: UIView!
    @IBOutlet weak var viewMuscleGroup3: UIView!
    
    @IBOutlet weak var imageBG: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure<T>(with content: T) {
        if let workout = content as? Workouts {
            self.setMuscles(workout.muscleGroups ?? "")
            self.updateValues(date: workout.addedDate, category: workout.workoutCategoryTitle, imageURL: workout.woImgURL)
            self.lblDate.text = workout.addedDate?.gravityDate()
        }
    }
    
    private func setMuscles(_ muscles:String) {
        let musclesIds = muscles.components(separatedBy: ",")
        var groups = [MuscleGroup]()
        if UserStore.masterData?.muscleGroup != nil {
            for id in musclesIds {
                let filter = UserStore.masterData!.muscleGroup.filter {
                    $0.muscleID == id
                }
                groups.append(contentsOf: filter)
            }
        }
        switch musclesIds.count {
        case 0:
            viewMuscleGroup1.isHidden = true
            viewMuscleGroup2.isHidden = true
            viewMuscleGroup3.isHidden = true
        case 1:
            lblMuscleGroup1.text = groups[0].title
            viewMuscleGroup2.isHidden = true
            viewMuscleGroup3.isHidden = true
        case 2:
            lblMuscleGroup1.text = groups[0].title
            lblMuscleGroup2.text = groups[1].title
            viewMuscleGroup3.isHidden = true
        default:
            lblMuscleGroup1.text = groups[0].title
            lblMuscleGroup2.text = groups[1].title
            lblMuscleGroup3.text = groups[2].title
            viewMuscleGroup1.isHidden = false
            viewMuscleGroup2.isHidden = false
            viewMuscleGroup3.isHidden = false
        }
    }
    
    private func updateValues(date:String?, category:String?, imageURL:String?) {
        lblDate.text = date
        lblCategory.text = category
        imageBG.downloadImageFrom(urlString: imageURL ?? "", with: shadowImage)
    }
}
