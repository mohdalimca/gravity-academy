//
//  FavouriteCollectionView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 31/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

enum FavouriteTypeItem: String {
    case skill
    case program
    case workout
    case exercise
}

class FavouriteCollectionView: UICollectionView, Reusable {
    var viewModel: FavouriteViewModel!
    weak var delegateCollection: FavouriteCollectionViewDelegate?
    let layout = UICollectionViewFlowLayout()
    var skills = [SkillList]()
    var programs = [TrainingPlan]()
    var workouts = [Workouts]()
    var typeItem:FavouriteTypeItem!
    var didSelectAtIndexPath: ((IndexPath) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {}
    
    func configure<T>(with content: T, type:FavouriteTypeItem ) {
        self.typeItem = type
        switch type {
        case .skill:
            if let skills = content as? [SkillList] {
                self.skills = skills
            }
        case .program:
            if let programs = content as? [TrainingPlan] {
                self.programs = programs
            }
        case .workout:
            if let workouts = content as? [Workouts] {
                self.workouts = workouts
            }
        case .exercise:
            break
        }
        reloadData()
    }
    
    
    //    func configure(_ viewModel:FavouriteViewModel) {
    //        self.viewModel = viewModel
    //        setup()
    //    }
    
    private func setup() {
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 10
        isPagingEnabled = false
        collectionViewLayout = layout
        delegate = self
        dataSource = self
        reloadData()
    }
}

extension FavouriteCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch typeItem {
        case .skill:
            return (skills.count > 3) ? 3 : skills.count 
        case .program:
            return (programs.count > 3) ? 3: programs.count
        case .workout:
            return (workouts.count > 3) ? 3: workouts.count
        case .exercise, .none:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusable(indexPath) as FavouriteCollectionViewCell
        cell.delegate = self
        switch typeItem {
        case .skill:
            cell.configure(with: skills[indexPath.item], type: .skill)
            return cell
        case .program:
            cell.configure(with: programs[indexPath.item], type: .program)
            return cell
        case .workout:
            let cell = dequeueReusable(indexPath) as FavouriteWorkoutCollectionCell
            cell.configure(with: workouts[indexPath.item])
            return cell
        case .exercise, .none:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath)
        print(collectionView.tag)
        let index = IndexPath(item: indexPath.item, section: collectionView.tag)
        didSelectAtIndexPath?(index)
//        delegateCollection?.sendAction(action: .exercise)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width - 50, height: collectionView.bounds.height)
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let pageWidth: Float = Float(self.bounds.width - 50) // width + space
        
        let currentOffset = Float(scrollView.contentOffset.x)
        let targetOffset = Float(targetContentOffset.pointee.x)
        var newTargetOffset: Float = 0
        
        if targetOffset > currentOffset {
            newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth
        } else {
            newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth
        }
        
        if newTargetOffset < 0 {
            newTargetOffset = 0
        } else if CGFloat(newTargetOffset) > scrollView.contentSize.width {
            newTargetOffset = Float(scrollView.contentSize.width)
        }
        
        targetContentOffset.pointee.x = CGFloat(currentOffset)
        scrollView.setContentOffset(CGPoint(x: CGFloat(newTargetOffset), y: 0), animated: true)
        
        var index = Int(newTargetOffset / pageWidth)
        
        if index == 0 {
            // If first index
            var cell = self.cellForItem(at: IndexPath(item: index, section: 0))
            
            UIView.animate(withDuration: 0.2, animations: {
                cell?.transform = .identity
            })
            cell = self.cellForItem(at: IndexPath(item: index + 1, section: 0))
            UIView.animate(withDuration: 0.2, animations: {
                cell?.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            })
        } else {
            var cell = self.cellForItem(at: IndexPath(item: index, section: 0))
            UIView.animate(withDuration: 0.2, animations: {
                cell?.transform = .identity
            })
            
            index -= 1 // left
            cell = self.cellForItem(at: IndexPath(item: index, section: 0))
            UIView.animate(withDuration: 0.2, animations: {
                cell?.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            })
            
            index += 1
            index += 1 // right
            cell = self.cellForItem(at: IndexPath(item: index, section: 0))
            UIView.animate(withDuration: 0.2, animations: {
                cell?.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            })
        }
    }
}

extension FavouriteCollectionView: FavouriteCollectionViewCellDelegate {
    func didSelectAt(item: Int) {
        
    }
}


