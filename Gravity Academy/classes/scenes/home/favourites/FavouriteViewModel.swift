//
//  FavouriteViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 30/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
final class FavouriteViewModel {

    var favourites: Fav?
    var listScene:Scenes!
    var selectedPlan: TrainingPlan!
    var selectedSkill: SkillList!
    var selectedWorkout: Workouts!
    weak var view: FavouriteViewRepresentable?
    var provider: FavouriteServiceProviderable
    private var items: [RowSectionDisplayable] = []
    var didSelect:((_ item:RowSectionDisplayable) -> Void)?
    var apiName: AppAPI!

    
    init(provider:FavouriteServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
        items = RowSectionDisplayableFunction.makeRowSectionDisplayable(resource: "Favourite")
    }
    
    var count:Int {
        return items.count
    }
    
    func items(in section: Int) -> [Row] {
        return items[section].content
    }
    
    func item(at section: Int) -> RowSectionDisplayable {
        return items[section]
    }
    
    func item(for indexPath: IndexPath) -> Row {
        return items(in: indexPath.section)[indexPath.row]
    }
    
    func favouritesData() {
        apiName = .favouritesData
        provider.favourites()
    }
}


extension FavouriteViewModel: FavouriteServiceProviderDelegate {
    
    private func handleAPIError(apiError: APIError?) {
        var strError = ""
        guard let error = apiError else { return }
        switch error.errorCode {
        case .network:
            strError = "Check your intenet connection"
            self.view?.onAction(.nointernet)
        case .server:
            strError = "No connection with server"
            self.view?.onAction(.errorMessage(strError))
        case .authorize:
            strError = "Session has been expired."
            self.view?.onAction(.login)
        case .parsing:
            strError = "Data is not in proper format, parsing issue."
            self.view?.onAction(.errorMessage(strError))
        default:
            self.view?.onAction(.errorMessage(errorMessage))
        }
    }
    
    func completed<T>(for action: FavouriteAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.handleAPIError(apiError: error)
//                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .favourites:
                        self.favourites = resp.data?.favourites
                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
