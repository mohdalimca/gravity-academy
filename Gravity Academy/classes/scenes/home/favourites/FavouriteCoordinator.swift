//
//  FavouriteCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class FavouriteCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let favourite: FavouriteViewController = FavouriteViewController.from(from: .favourite, with: .favourite)
    let nointernet: NoInternetViewController = NoInternetViewController.from(from: .common, with: .nointernet)

    
    var programDetail: ProgramDetailCoordinator!
    var skillDetail: TechniqueDetailCoordinator!
    var workoutDetail: DetailCoordinator!
    var allExercise: AllExerciseCoordinator!
    var workoutLibrary: WorkoutLibraryCoordinator!
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(favourite, hideBar: true)
    }
    
    private func onStart() {
        favourite.router = self
        favourite.delegateTable = self
        nointernet.router = self
        nointernet.didTryAgain = favourite.didTryAgain
    }
    
    private func startDetail() {
        let r = Router()
        let detail = DetailCoordinator(router: r)
        add(detail)
        detail.delegate = self
        detail.start()
        router.present(detail, animated: true)
    }
    
    private func startSavedExercise() {
        let r = Router()
        let saved = SavedExeciseCoordinator(router: r)
        add(saved)
        saved.delegate = self
        saved.start()
        router.present(saved, animated: true)
    }
    
    private func startProgramDetail() {
        let r = Router()
        programDetail = ProgramDetailCoordinator(router: r)
        add(programDetail)
        programDetail.delegate = self
        programDetail.start(trainingPlan:favourite.viewModel.selectedPlan)
        router.present(programDetail, animated: true)
    }
    
    private func startSkillDetail() {
        skillDetail = TechniqueDetailCoordinator(router: Router())
        add(skillDetail)
        skillDetail.delegate = self
        skillDetail.start(skill: favourite.viewModel.selectedSkill)
        router.present(skillDetail, animated: true)
    }
    
    private func startWorkoutDetail() {
        workoutDetail = DetailCoordinator(router: Router())
        add(workoutDetail)
        workoutDetail.delegate = self
        workoutDetail.start(detail: favourite.viewModel.selectedWorkout, categoryTitle: favourite.viewModel.selectedWorkout.workoutCategoryTitle ?? "")
        router.present(workoutDetail, animated: true)
    }
    
    private func startWorkoutLibrary() {
        workoutLibrary = WorkoutLibraryCoordinator(router: Router())
        add(workoutLibrary)
        workoutLibrary.delegate = self
        let wod = WoOfTheDay(workoutID: "2", workoutCategoryID: "2", title: "Andy Wokrout", woOfTheDayDescription: "Walk and Run", workoutGoal: "Run Daily", muscleGroups: "whole body,Chest,Shoulders", status: "1", addedDate: "", likeCount: "0", isWoOfTheDay: "1", isLike: "0")
        workoutLibrary.start(with: wod)
        router.present(workoutLibrary, animated: true)
    }
    
    private func startAllExercise() {
        allExercise = AllExerciseCoordinator(router: Router())
        add(allExercise)
        allExercise.delegate = self
        allExercise.start()
        router.present(allExercise, animated: true)
    }
}

extension FavouriteCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .programDetail: startProgramDetail()
        case .techniqueDetail: startSkillDetail()
        case .workoutDetail: startWorkoutDetail()
        case .workoutLibrary: startWorkoutLibrary()
        case .allExercise: startAllExercise()
        case .nointernet:
            nointernet.apiName = favourite.viewModel.apiName
            self.router.present(nointernet, animated: true)
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension FavouriteCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

extension FavouriteCoordinator: FavouriteTableViewDelegate {
    func didSelectAt(row: Int) {
        startSavedExercise()
    }
}
