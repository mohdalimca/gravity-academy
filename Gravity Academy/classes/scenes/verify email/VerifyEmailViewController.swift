//
//  VerifyEmailViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 09/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class VerifyEmailViewController: UIViewController {

    var router: NextSceneDismisserPresenter?
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnChangeEmail: GravityAcademyButton!
    @IBOutlet weak var btnConfirmEmail: GravityAcademyButton!
    @IBOutlet weak var btnGenerateOTP: GravityAcademyButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        [btnChangeEmail, btnConfirmEmail, btnGenerateOTP].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    @objc func buttonPressed(_ sender: GravityAcademyButton) {
        switch sender {
        case btnChangeEmail: router?.dismiss(controller: .verifyEmail)
        case btnGenerateOTP: break // api to generate otp
        case btnConfirmEmail: router?.push(scene: .verifyOTP)
        default: break
        }
    }
}
