//
//  VerifyEmailCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 10/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class VerifyEmailCoordinator: Coordinator<Scenes> {
    
//    let verifyEmail: VerifyEmailViewController = VerifyEmailViewController.from(from: .register, with: .verifyEmail)
    let controller: VerifyOTPViewController = VerifyOTPViewController.from(from: .register, with: .verifyOTP)
    
    override func start() {
        super.start()
        router.setRootModule(controller, hideBar: true)
        onStart()
    }
    private func onStart() {
//        verifyEmail.router = self
        controller.router = self
    }
}

extension VerifyEmailCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {
        switch scene {
//        case .verifyEmail: router.present(verifyEmail, animated: true)
        case .verifyOTP: router.present(controller, animated: true)
        default: break
        }
    }
    
    func push(scene: Scenes) {
        
    }
    
    func dismiss(controller: Scenes) {
        router.dismissModule(animated: true, completion: nil)
    }
}
