//
//  FilterViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 22/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

enum FilterType: String {
    case all
    case workout
}

class FilterViewController: UIViewController {
    
    @IBOutlet weak var tableView: FilterTableView!
    @IBOutlet weak var stackFitness: UIStackView!
    @IBOutlet weak var stackMuscles: UIStackView!
    @IBOutlet weak var stackEquipment: UIStackView!
    
    @IBOutlet weak var btnFitnessAll: GravityAcademyButton!
    @IBOutlet weak var btnFitnessBeginner: GravityAcademyButton!
    @IBOutlet weak var btnFitnessIntermediate: GravityAcademyButton!
    @IBOutlet weak var btnFitnessAdvanced: GravityAcademyButton!
    
    @IBOutlet weak var btnWholeBody: GravityAcademyButton!
    @IBOutlet weak var btnGoalBack: GravityAcademyButton!
    @IBOutlet weak var btnGoalBiceps: GravityAcademyButton!
    @IBOutlet weak var btnGoalChest: GravityAcademyButton!
    @IBOutlet weak var btnGoalTriceps: GravityAcademyButton!
    @IBOutlet weak var btnGoalShoulders: GravityAcademyButton!
    @IBOutlet weak var btnGoalAbs: GravityAcademyButton!
    @IBOutlet weak var btnGoalLegs: GravityAcademyButton!
    
    @IBOutlet weak var btnNoEquipment: GravityAcademyButton!
    @IBOutlet weak var btnEquipment: GravityAcademyButton!
    @IBOutlet weak var btnEquiBars: GravityAcademyButton!
    @IBOutlet weak var btnEquiHome: GravityAcademyButton!
    
    @IBOutlet weak var containerView: GravityAcademyView!
    @IBOutlet weak var imageBG: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var fitnessLevel: String?
    var musclesID: String?
    var muscles: String?
    var equipment: String?
    var filterType: FilterType = .all
    
    weak var router: NextSceneDismisserPresenter?
    weak var delegate: FilterViewControllerDelegate?
    var didSelectFilters: ((_ fitnessLevel:String?, _ muscleID:String?, _ muscles:String?, _ equipment:String?) -> Void)?
    
    init() {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        setButtons()
        setStackView()
        view.backgroundColor = .clear
        imageBG.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        gestureRecognizer.delegate = self
        containerView.addGestureRecognizer(gestureRecognizer)
        btnFitnessAll.backgroundColor = UIColor.init(hex: filterSelected)
        navigationController?.presentationController?.presentedView?.gestureRecognizers?.forEach {
            $0.delegate = self
        }
    }
    
    private func setStackView() {
        if filterType != .all {
            lblTitle.text = "Filter workouts"
            stackFitness.isHidden = true
            stackEquipment.isHidden = true
        }
    }
    
    private func setButtons() {
        setGoalsButtons()
        setFitnessButtons()
        setEquipmentsButtons()
    }
    
    private func setFitnessButtons() {
        [btnFitnessAll, btnFitnessBeginner, btnFitnessIntermediate, btnFitnessAdvanced].forEach {
            $0?.addTarget(self, action: #selector(fitnessButtonPressed(_:)), for: .touchUpInside)
        }
        btnFitnessAll.backgroundColor = UIColor.init(hex: filterSelected)
        btnFitnessAll.setTitleColor(.white, for: .normal)
    }
    
    private func setGoalsButtons() {
        [btnWholeBody, btnGoalAbs, btnGoalBack, btnGoalLegs, btnGoalChest, btnGoalBiceps, btnGoalShoulders, btnGoalTriceps].forEach {
            $0?.addTarget(self, action: #selector(goalButtonPressed(_:)), for: .touchUpInside)
        }
    }
    
    private func setEquipmentsButtons() {
        [btnNoEquipment, btnEquipment, btnEquiBars, btnEquiHome].forEach {
            $0?.addTarget(self, action: #selector(equipmentButtonPressed(_:)), for: .touchUpInside)
        }
    }
    
    @IBAction func dismissAction(_ sender:UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func fitnessButtonPressed(_ sender: GravityAcademyButton) {
        switch sender {
        case btnFitnessAll:
            fitnessLevel = ""
            btnFitnessAll.backgroundColor = UIColor.init(hex: filterSelected)
            btnFitnessBeginner.backgroundColor = UIColor.init(hex: filterUnselected)
            btnFitnessIntermediate.backgroundColor = UIColor.init(hex: filterUnselected)
            btnFitnessAdvanced.backgroundColor = UIColor.init(hex: filterUnselected)
            btnFitnessAll.setTitleColor(.white, for: .normal)
            btnFitnessBeginner.setTitleColor(.black, for: .normal)
            btnFitnessIntermediate.setTitleColor(.black, for: .normal)
            btnFitnessAdvanced.setTitleColor(.black, for: .normal)
            
        case btnFitnessBeginner:
            fitnessLevel = "1"
            btnFitnessAll.backgroundColor = UIColor.init(hex: filterUnselected)
            btnFitnessBeginner.backgroundColor = UIColor.init(hex: filterSelected)
            btnFitnessIntermediate.backgroundColor = UIColor.init(hex: filterUnselected)
            btnFitnessAdvanced.backgroundColor = UIColor.init(hex: filterUnselected)
            btnFitnessAll.setTitleColor(.black, for: .normal)
            btnFitnessBeginner.setTitleColor(.white, for: .normal)
            btnFitnessIntermediate.setTitleColor(.black, for: .normal)
            btnFitnessAdvanced.setTitleColor(.black, for: .normal)
            
        case btnFitnessIntermediate:
            fitnessLevel = "2"
            btnFitnessAll.backgroundColor = UIColor.init(hex: filterUnselected)
            btnFitnessBeginner.backgroundColor = UIColor.init(hex: filterUnselected)
            btnFitnessIntermediate.backgroundColor = UIColor.init(hex: filterSelected)
            btnFitnessAdvanced.backgroundColor = UIColor.init(hex: filterUnselected)
            btnFitnessAll.setTitleColor(.black, for: .normal)
            btnFitnessBeginner.setTitleColor(.black, for: .normal)
            btnFitnessIntermediate.setTitleColor(.white, for: .normal)
            btnFitnessAdvanced.setTitleColor(.black, for: .normal)
            
        case btnFitnessAdvanced:
            fitnessLevel = "3"
            btnFitnessAll.backgroundColor = UIColor.init(hex: filterUnselected)
            btnFitnessBeginner.backgroundColor = UIColor.init(hex: filterUnselected)
            btnFitnessIntermediate.backgroundColor = UIColor.init(hex: filterUnselected)
            btnFitnessAdvanced.backgroundColor = UIColor.init(hex: filterSelected)
            btnFitnessAll.setTitleColor(.black, for: .normal)
            btnFitnessBeginner.setTitleColor(.black, for: .normal)
            btnFitnessIntermediate.setTitleColor(.black, for: .normal)
            btnFitnessAdvanced.setTitleColor(.white, for: .normal)
            
        default: break
        }
    }
    
    @objc func goalButtonPressed(_ sender: GravityAcademyButton) {
        switch sender {
        case btnWholeBody:
            muscles = "Whole Body"
            musclesID = String(btnWholeBody.tag)
            btnWholeBody.backgroundColor = UIColor.init(hex: filterSelected)
            btnGoalBack.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBiceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalChest.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalTriceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalShoulders.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalAbs.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalLegs.backgroundColor = UIColor.init(hex: filterUnselected)
            
            btnWholeBody.setTitleColor(.white, for: .normal)
            btnGoalBack.setTitleColor(.black, for: .normal)
            btnGoalBiceps.setTitleColor(.black, for: .normal)
            btnGoalChest.setTitleColor(.black, for: .normal)
            btnGoalTriceps.setTitleColor(.black, for: .normal)
            btnGoalShoulders.setTitleColor(.black, for: .normal)
            btnGoalAbs.setTitleColor(.black, for: .normal)
            btnGoalLegs.setTitleColor(.black, for: .normal)
            
            
        case btnGoalBack:
            muscles = "Back"
            musclesID = String(btnGoalBack.tag)
            btnWholeBody.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBack.backgroundColor = UIColor.init(hex: filterSelected)
            btnGoalBiceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalChest.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalTriceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalShoulders.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalAbs.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalLegs.backgroundColor = UIColor.init(hex: filterUnselected)
            
            btnWholeBody.setTitleColor(.black, for: .normal)
            btnGoalBack.setTitleColor(.white, for: .normal)
            btnGoalBiceps.setTitleColor(.black, for: .normal)
            btnGoalChest.setTitleColor(.black, for: .normal)
            btnGoalTriceps.setTitleColor(.black, for: .normal)
            btnGoalShoulders.setTitleColor(.black, for: .normal)
            btnGoalAbs.setTitleColor(.black, for: .normal)
            btnGoalLegs.setTitleColor(.black, for: .normal)
            
        case btnGoalBiceps:
            muscles = "Biceps"
            musclesID = String(btnGoalBiceps.tag)
            btnWholeBody.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBack.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBiceps.backgroundColor = UIColor.init(hex: filterSelected)
            btnGoalChest.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalTriceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalShoulders.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalAbs.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalLegs.backgroundColor = UIColor.init(hex: filterUnselected)
            
            btnWholeBody.setTitleColor(.black, for: .normal)
            btnGoalBack.setTitleColor(.black, for: .normal)
            btnGoalBiceps.setTitleColor(.white, for: .normal)
            btnGoalChest.setTitleColor(.black, for: .normal)
            btnGoalTriceps.setTitleColor(.black, for: .normal)
            btnGoalShoulders.setTitleColor(.black, for: .normal)
            btnGoalAbs.setTitleColor(.black, for: .normal)
            btnGoalLegs.setTitleColor(.black, for: .normal)
            
        case btnGoalChest:
            muscles = "Chest"
            musclesID = String(btnGoalChest.tag)
            btnWholeBody.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBack.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBiceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalChest.backgroundColor = UIColor.init(hex: filterSelected)
            btnGoalTriceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalShoulders.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalAbs.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalLegs.backgroundColor = UIColor.init(hex: filterUnselected)
            
            btnWholeBody.setTitleColor(.black, for: .normal)
            btnGoalBack.setTitleColor(.black, for: .normal)
            btnGoalBiceps.setTitleColor(.black, for: .normal)
            btnGoalChest.setTitleColor(.white, for: .normal)
            btnGoalTriceps.setTitleColor(.black, for: .normal)
            btnGoalShoulders.setTitleColor(.black, for: .normal)
            btnGoalAbs.setTitleColor(.black, for: .normal)
            btnGoalLegs.setTitleColor(.black, for: .normal)
            
        case btnGoalTriceps:
            muscles = "Triceps"
            musclesID = String(btnGoalTriceps.tag)
            btnWholeBody.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBack.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBiceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalChest.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalTriceps.backgroundColor = UIColor.init(hex: filterSelected)
            btnGoalShoulders.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalAbs.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalLegs.backgroundColor = UIColor.init(hex: filterUnselected)
            
            btnWholeBody.setTitleColor(.black, for: .normal)
            btnGoalBack.setTitleColor(.black, for: .normal)
            btnGoalBiceps.setTitleColor(.black, for: .normal)
            btnGoalChest.setTitleColor(.black, for: .normal)
            btnGoalTriceps.setTitleColor(.white, for: .normal)
            btnGoalShoulders.setTitleColor(.black, for: .normal)
            btnGoalAbs.setTitleColor(.black, for: .normal)
            btnGoalLegs.setTitleColor(.black, for: .normal)
            
        case btnGoalShoulders:
            muscles = "Shoulders"
            musclesID = String(btnGoalShoulders.tag)
            btnWholeBody.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBack.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBiceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalChest.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalTriceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalShoulders.backgroundColor = UIColor.init(hex: filterSelected)
            btnGoalAbs.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalLegs.backgroundColor = UIColor.init(hex: filterUnselected)
            
            btnWholeBody.setTitleColor(.black, for: .normal)
            btnGoalBack.setTitleColor(.black, for: .normal)
            btnGoalBiceps.setTitleColor(.black, for: .normal)
            btnGoalChest.setTitleColor(.black, for: .normal)
            btnGoalTriceps.setTitleColor(.black, for: .normal)
            btnGoalShoulders.setTitleColor(.white, for: .normal)
            btnGoalAbs.setTitleColor(.black, for: .normal)
            btnGoalLegs.setTitleColor(.black, for: .normal)
            
        case btnGoalAbs:
            muscles = "Abs"
            musclesID = String(btnGoalAbs.tag)
            btnWholeBody.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBack.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBiceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalChest.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalTriceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalShoulders.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalAbs.backgroundColor = UIColor.init(hex: filterSelected)
            btnGoalLegs.backgroundColor = UIColor.init(hex: filterUnselected)
            
            btnWholeBody.setTitleColor(.black, for: .normal)
            btnGoalBack.setTitleColor(.black, for: .normal)
            btnGoalBiceps.setTitleColor(.black, for: .normal)
            btnGoalChest.setTitleColor(.black, for: .normal)
            btnGoalTriceps.setTitleColor(.black, for: .normal)
            btnGoalShoulders.setTitleColor(.black, for: .normal)
            btnGoalAbs.setTitleColor(.white, for: .normal)
            btnGoalLegs.setTitleColor(.black, for: .normal)
            
        case btnGoalLegs:
            muscles = "Legs"
            musclesID = String(btnGoalLegs.tag)
            btnWholeBody.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBack.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalBiceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalChest.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalTriceps.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalShoulders.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalAbs.backgroundColor = UIColor.init(hex: filterUnselected)
            btnGoalLegs.backgroundColor = UIColor.init(hex: filterSelected)
            
            btnWholeBody.setTitleColor(.black, for: .normal)
            btnGoalBack.setTitleColor(.black, for: .normal)
            btnGoalBiceps.setTitleColor(.black, for: .normal)
            btnGoalChest.setTitleColor(.black, for: .normal)
            btnGoalTriceps.setTitleColor(.black, for: .normal)
            btnGoalShoulders.setTitleColor(.black, for: .normal)
            btnGoalAbs.setTitleColor(.black, for: .normal)
            btnGoalLegs.setTitleColor(.white, for: .normal)
            
        default: break
        }
    }
    
    @objc func equipmentButtonPressed(_ sender: GravityAcademyButton) {
        switch sender {
        case btnNoEquipment:
            equipment = "0"
            btnNoEquipment.backgroundColor = UIColor.init(hex: filterSelected)
            btnEquipment.backgroundColor = UIColor.init(hex: filterUnselected)
            btnEquiBars.backgroundColor = UIColor.init(hex: filterUnselected)
            btnEquiHome.backgroundColor = UIColor.init(hex: filterUnselected)
            btnNoEquipment.setTitleColor(.white, for: .normal)
            btnEquipment.setTitleColor(.black, for: .normal)
            btnEquiBars.setTitleColor(.black, for: .normal)
            btnEquiHome.setTitleColor(.black, for: .normal)
            
        case btnEquipment:
            equipment = "1"
            btnNoEquipment.backgroundColor = UIColor.init(hex: filterUnselected)
            btnEquipment.backgroundColor = UIColor.init(hex: filterSelected)
            btnEquiBars.backgroundColor = UIColor.init(hex: filterUnselected)
            btnEquiHome.backgroundColor = UIColor.init(hex: filterUnselected)
            btnNoEquipment.setTitleColor(.black, for: .normal)
            btnEquipment.setTitleColor(.white, for: .normal)
            btnEquiBars.setTitleColor(.black, for: .normal)
            btnEquiHome.setTitleColor(.black, for: .normal)
            
        case btnEquiBars:
            btnNoEquipment.backgroundColor = UIColor.init(hex: filterUnselected)
            btnEquipment.backgroundColor = UIColor.init(hex: filterUnselected)
            btnEquiBars.backgroundColor = UIColor.init(hex: filterSelected)
            btnEquiHome.backgroundColor = UIColor.init(hex: filterUnselected)
            btnNoEquipment.setTitleColor(.black, for: .normal)
            btnEquipment.setTitleColor(.black, for: .normal)
            btnEquiBars.setTitleColor(.white, for: .normal)
            btnEquiHome.setTitleColor(.black, for: .normal)
            
        case btnEquiHome:
            btnNoEquipment.backgroundColor = UIColor.init(hex: filterUnselected)
            btnEquipment.backgroundColor = UIColor.init(hex: filterUnselected)
            btnEquiBars.backgroundColor = UIColor.init(hex: filterUnselected)
            btnEquiHome.backgroundColor = UIColor.init(hex: filterSelected)
            btnNoEquipment.setTitleColor(.black, for: .normal)
            btnEquipment.setTitleColor(.black, for: .normal)
            btnEquiBars.setTitleColor(.black, for: .normal)
            btnEquiHome.setTitleColor(.white, for: .normal)
            
        default: break
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @objc func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: view?.window)
        var initialTouchPoint = CGPoint.zero
        
        switch sender.state {
        case .began:
            initialTouchPoint = touchPoint
        case .changed:
            if touchPoint.y > initialTouchPoint.y {
                view.frame.origin.y = touchPoint.y - initialTouchPoint.y
            }
        case .ended, .cancelled:
            if touchPoint.y - initialTouchPoint.y > 300 {
                dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        default: break
        }
    }
    
    @IBAction func btnApplyAction(_ sender: UIButton) {
        if filterType == .workout && self.muscles == nil  {
            showBannerWith(text: "Select any muscle group", style: .danger)
            return
        }
        self.didSelectFilters?(self.fitnessLevel, self.musclesID, self.muscles, self.equipment)
        dismiss(animated: true, completion: nil)
    }
}

extension FilterViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer === UIPanGestureRecognizer.self && otherGestureRecognizer === UISwipeGestureRecognizer.self {
            return true
        }
        return false
    }
}
