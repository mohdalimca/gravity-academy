//
//  FilterCollectionView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 22/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class FilterCollectionView: UICollectionView {
    
    let filterArray = ["MAK", "TEST", "CHECK", "NEW"]
    var selectedIndex = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        reloadData()
    }
}

extension FilterCollectionView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusable(indexPath) as FilterCollectionViewCell
        cell.lblTitle.text = filterArray[indexPath.item]
        if selectedIndex == indexPath.item {
            cell.lblTitle.backgroundColor = .black
            cell.lblTitle.textColor = .white
        } else {
            cell.lblTitle.backgroundColor = .white
            cell.lblTitle.textColor = .black
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedIndex != indexPath.item {
            selectedIndex = indexPath.item
            reloadData()
        }
    }
}

