//
//  FilterCollectionViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 22/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class FilterCollectionViewCell: UICollectionViewCell, Reusable {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        
    }
    
    private func setup() {
        lblTitle.layer.cornerRadius = 8
    }
    
}
