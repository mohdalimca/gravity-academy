//
//  FilterTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 22/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class FilterTableView: UITableView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup() {
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
        dataSource = self
        delegate = self
        reloadData()
    }
}

extension FilterTableView: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusable(indexPath) as FilterTableViewCell
        cell.backgroundColor = .blue
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

