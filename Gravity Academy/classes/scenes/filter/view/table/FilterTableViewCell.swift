//
//  FilterTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 22/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collection: FilterCollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) { }
    
    func setup() {
        collection.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
