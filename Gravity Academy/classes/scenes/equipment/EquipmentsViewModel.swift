//
//  EquipmentsViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 04/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class EquipmentsViewModel {
    
    var equipments: [Equipment] = []
    let provider: WorkoutsServiceProviderable
    weak var view: WorkoutsViewRepresentable?
    var browserURL: URL!
    
    init(provider:WorkoutsServiceProvider) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    var count: Int {
        return equipments.count
    }
    
    func getEquipments() {
        provider.equipments()
    }
}

extension EquipmentsViewModel: WorkoutsServiceProviderDelegate {
    func completed<T>(for action: WorkoutsAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    self.equipments = resp.data?.equipments ?? self.equipments
                    self.view?.onAction(.equipments)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
