//
//  EquipmentsTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 24/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class EquipmentsTableView: UITableView {
    
    var viewModel: EquipmentsViewModel!
    var didSelectEquipment: ((_ index:Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure(_ viewModel: EquipmentsViewModel) {
        self.viewModel = viewModel
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
    }
}

extension EquipmentsTableView: UITableViewDelegate, UITableViewDataSource  {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.separatorStyle = .none
        let cell = tableView.dequeueReusable(indexPath) as EquipmentsTableViewCell
        cell.configure(with: viewModel.equipments[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectEquipment?(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}

