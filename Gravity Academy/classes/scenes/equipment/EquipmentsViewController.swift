//
//  EquipmentsViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 24/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class EquipmentsViewController: UIViewController {

    @IBOutlet weak var tableView: EquipmentsTableView!
    @IBOutlet weak var btnBack: UIButton!
    
    weak var router: NextSceneDismisserPresenter?
    let viewModel = EquipmentsViewModel(provider: WorkoutsServiceProvider())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        viewModel.view = self
        tableView.configure(viewModel)
        tableView.didSelectEquipment = didSelectEquipment
        startAnimation()
        viewModel.getEquipments()
        btnBack.addTarget(self, action: #selector(btnBackAction(_:)), for: .touchUpInside)
    }
    
    private func didSelectEquipment(_ index:Int) {
        let url = URL(string: viewModel.equipments[index].link ?? "")
        guard let equipmentURL = url else {
            showBannerWith(text: "No equipment link available", style: .danger)
            return
        }
        viewModel.browserURL = equipmentURL
        router?.push(scene: .browser)
//        if UIApplication.shared.canOpenURL(equipmentURL) {
//            UIApplication.shared.open(equipmentURL, options: [:], completionHandler: nil)
//        }
    }

    @objc func btnBackAction(_ sender: UIButton) {
        router?.dismiss(controller: .equipments)
    }
    
    private func reload() {
        tableView.reloadData()
        stopAnimating()
    }
}

extension EquipmentsViewController: WorkoutsViewRepresentable {
    func onAction(_ action: WorkoutsAction) {
        switch action {
        case let .errorMessage(msg):
            stopAnimating()
            showBannerWith(text: msg, style: .danger)
        case .equipments:
            reload()
        default:
            stopAnimating()
        }
    }
}
