//
//  EquipmentsTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 24/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class EquipmentsTableViewCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageEquipment: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configure<T>(with content: T) {
        guard let equiment = content as? Equipment else { return }
        lblTitle.text = equiment.title
        imageEquipment.downloadImageFrom(urlString: equiment.imageURL ?? "", with: shadowImage)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
