
//
//  EquipmentsCorrdinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 24/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class EquipmentsCorrdinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    
    let equipments: EquipmentsViewController = EquipmentsViewController.from(from: .common, with: .equipments)

    var browser: BrowserCoordinator!
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(equipments, hideBar: true)
    }
    
    private func onStart() {
        equipments.router = self
    }
    
    private func startBrowser() {
        browser = BrowserCoordinator (router: Router())
        add(browser)
        browser.delegate = self
        browser.start(url: equipments.viewModel.browserURL)
        self.router.present(browser, animated: true)
    }
}

extension EquipmentsCorrdinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .browser: startBrowser()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        router.dismissModule(animated: true, completion: nil)
    }
}


extension EquipmentsCorrdinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

