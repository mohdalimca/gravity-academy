//
//  RegistrationCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 06/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class RegistrationCoordinator: Coordinator<Scenes> {
    
    let register: RegistrationViewController = RegistrationViewController.from(from: .register, with: .register)
    let verifyEmail: VerifyEmailViewController = VerifyEmailViewController.from(from: .register, with: .verifyEmail)
    let verifyOTP: VerifyOTPViewController = VerifyOTPViewController.from(from: .register, with: .verifyOTP)
    let login: LoginViewController = LoginViewController.from(from: .login, with: .login)
    let nointernet: NoInternetViewController = NoInternetViewController.from(from: .common, with: .nointernet)

    override func start() {
        super.start()
        register.router = self
        router.setRootModule(register, hideBar: true)
        onStart()
    }
    private func onStart() {
        verifyEmail.router = self
        verifyOTP.router = self
        nointernet.router = self
        nointernet.didTryAgain = register.didTryAgain
    }
    
    private func emailModule() {
        let router = Router()
        let email = VerifyEmailCoordinator(router: router)
        add(email)
        email.start()
        self.router.present(verifyEmail, animated: true)
    }
    
    private func otpModule() {
        let router = Router()
        let email = VerifyEmailCoordinator(router: router)
        add(email)
        email.start()
        self.router.present(verifyOTP, animated: true)
    }
    
    private func loginModule() {
        let router = Router()
        let login = LoginCoordinator(router: router)
        add(login)
        login.start()
        self.router.present(login, animated: true)
    }
}

extension RegistrationCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {
        switch scene {
        case .login: loginModule()
        case .verifyEmail: emailModule()
        case .home: loginModule()
        case .verifyOTP: router.push(verifyOTP, animated: true, completion: nil)
        default: break
        }
    }
    
    func push(scene: Scenes) {
        switch scene {
        case .nointernet:
            nointernet.apiName = register.viewModel.apiName
            self.router.present(nointernet, animated: true)
        default:
            break
        }
    }
    
    func dismiss(controller: Scenes) {
        switch controller {
        case .verifyOTP: router.popModule(animated: true)
        default: router.dismissModule(animated: true, completion: nil)
        }
    }
}

class RegistrationData {
    static var currentStep: Int = 0
}
