//
//  SignupAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 06/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum RegistrationHeaderButtonAction: String {
    case back
}

protocol RegistrationHeaderViewDelegate: class {
    func didSelect(action: RegistrationHeaderButtonAction)
}

protocol RegistrationViewControllerDelegate {
    func didSelect()
}
