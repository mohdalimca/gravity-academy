//
//  RegistrationViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import NVActivityIndicatorView

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var viewHeader: RegistrationHeaderView!
    @IBOutlet weak var btnNext: UIButton!
    //    @IBOutlet weak var lblTerms: UILabel!
    
    var router: NextSceneDismisserPresenter?
    var controller: RegistrationPagerVC!
//    var didTryAgain:(() -> Void)?
    let viewModel = RegistrationViewModel(provider: OnboardingServiceProvider())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateProgressView(0)
        super.viewDidAppear(animated)
    }
    
    private func setup() {
        viewHeader.delegate = self
        viewModel.view = self
        viewModel.apiName = .masterData
        viewModel.masterData()
    }
    
    func didTryAgain(_ api:AppAPI) {
        switch api {
        case .masterData:
            viewModel.masterData()
        default: break
        }
        router?.dismiss(controller: .nointernet)
    }
    
    private func updateProgressView(_ index: Int) {
        let percent = Float(index + 1) / 7.0
        DispatchQueue.main.async {
            if index != 6 {  self.viewHeader.progressView.isHidden = false
            } else {  self.viewHeader.progressView.isHidden = true  }
            self.viewHeader.progressView.setProgress(percent, animated: true)
        }
    }
    
    private func clearViewModel() {
        RegistrationViewModel.gender = ""
        RegistrationViewModel.height = ""
        RegistrationViewModel.weight = ""
        RegistrationViewModel.fn_level_id = ""
        RegistrationViewModel.max_pushups = ""
        RegistrationViewModel.max_pullups = ""
        RegistrationViewModel.max_squats = ""
        RegistrationViewModel.max_dips = ""
        RegistrationViewModel.user_name = ""
        RegistrationViewModel.name = ""
        RegistrationViewModel.email = ""
        RegistrationViewModel.password = ""
        RegistrationViewModel.goals.removeAll()
    }
    
    private func setLabel(_ isHidden: Bool) {
        UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            //            self.lblTerms.isHidden = isHidden
        }, completion: nil)
    }
    
    private func validateGender() {
        if RegistrationViewModel.gender == "" {
            showBannerWith(text: "select gender", style: .danger)
        } else {
            nextScreen()
        }
    }
    
    private func validateFitnessLevel() {
        if RegistrationViewModel.fn_level_id == "" {
            showBannerWith(text: "select fitness level", style: .danger)
        } else {
            nextScreen()
        }
    }
    
    private func validateGoals() {
        if RegistrationViewModel.goals.count == 0 {
            showBannerWith(text: "select goals", style: .danger)
        } else {
            nextScreen()
        }
    }
    
    private func validatePerformance() {
        if RegistrationViewModel.max_pushups == "" {
            showBannerWith(text: "select pushups", style: .danger)
        } else if RegistrationViewModel.max_pullups == "" {
            showBannerWith(text: "select pullups", style: .danger)
        } else if RegistrationViewModel.max_squats == "" {
            showBannerWith(text: "select squats", style: .danger)
        } else if RegistrationViewModel.max_dips == "" {
            showBannerWith(text: "select dips", style: .danger)
        } else {
            nextScreen()
        }
    }
    
    private func validateUserInfo() {
        startAnimation()
        viewModel.onAction(action: .inputComplete(.register), for: .register)
    }
    
    private func nextScreen() {
        self.controller.next()
        updateProgressView(controller.viewModel.currentIndex)
    }
    
    private func validateAction() {
        switch controller.viewModel.currentIndex {
        case 0: validateGender()
        case 3: validateFitnessLevel()
        case 4: validateGoals()
        case 5: validatePerformance()
        case 6: validateUserInfo()
        default: nextScreen()
        }
    }
    
    @IBAction func btnNextAction(_ sender: UIButton ) {
        validateAction()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "registrationPageController" {
            if let viewController1 = segue.destination as? RegistrationPagerVC {
                self.controller = viewController1
                self.controller.viewModel = self.viewModel
            }
        }
    }
}

extension RegistrationViewController: RegistrationHeaderViewDelegate {
    func didSelect(action: RegistrationHeaderButtonAction) {
        if self.controller.viewModel.currentIndex != 0 {
            updateProgressView(controller.viewModel.currentIndex - 1)
            self.controller.previous()
            setLabel(true)
        } else {
            clearViewModel()
            router?.dismiss(controller: .register)
        }
    }
}

extension RegistrationViewController: OnboardingViewRepresentable {
    func onAction(_ action: OnboardingAction) {
        switch action {
        case .noInternet:
            router?.push(scene: .nointernet)
        case let .errorMessage(text), let .requireFields(text):
            showBannerWith(text: text, style: .danger)
            stopAnimating()
        case .userRegistered:
            stopAnimating()
            router?.present(scene: .verifyOTP)
        default: break
        }
    }
}

