//
//  RegistrationPagerVC.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit


class RegistrationPagerVC: UIPageViewController {
    
    var onFinalScreen: (() -> Void)?
    var viewModel: RegistrationViewModel!
    
    let gender: GenderViewController = GenderViewController.from(from: .register, with: .gender)
    let height: HeightViewController = HeightViewController.from(from: .register, with: .height)
    let weight: WeightViewController = WeightViewController.from(from: .register, with: .weight)
    let fitness: FitnessLevelVC = FitnessLevelVC.from(from: .register, with: .fitness)
    let goals: GoalsViewController = GoalsViewController.from(from: .register, with: .goals)
    let performance: PerformanceViewController = PerformanceViewController.from(from: .register, with: .performance)
    let userInfo: UserInfoViewController = UserInfoViewController.from(from: .register, with: .userInfo)

    var pages = [UIViewController]()
    
    override func viewDidLoad() {
        viewModel.view = self
        setViewModel()
        pages.append(contentsOf: [gender, height, weight, fitness, goals, performance, userInfo])
        super.viewDidLoad()
        setControllers()
        delegate = self
        dataSource = self
    }
    
    private func setViewModel() {
        fitness.viewModel = viewModel
        goals.viewModel = viewModel
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func setControllers() {
        let first = pages[0]
        setViewControllers([first], direction: .forward, animated: true, completion: nil)
    }
    
    func next() {
        guard viewModel.currentIndex + 1 <= pages.count - 1 else { return }
        setViewControllers([pages[viewModel.currentIndex + 1]], direction: .forward, animated: true, completion: nil)
        viewModel.currentIndex += 1
        viewModel.onIndexChanged?(viewModel.currentIndex)
    }
    
    func previous() {
        guard viewModel.currentIndex - 1 >= 0 else { return }
        setViewControllers([pages[viewModel.currentIndex - 1]], direction: .reverse, animated: true, completion: nil)
        viewModel.currentIndex -= 1
        viewModel.onIndexChanged?(viewModel.currentIndex)
    }
}

extension RegistrationPagerVC: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = pages.firstIndex(of: viewController ) else { return nil }
        viewModel.onIndexChanged?(index)
        viewModel.currentIndex = index
        let preview = index - 1
        if preview < 0 { return nil }
        return pages[preview]
    }
    
    func pageViewController(_: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = pages.firstIndex(of: viewController) else { return nil }
        viewModel.onIndexChanged?(index)
        viewModel.currentIndex = index
//        let next = index + 1
//        if next > pages.count - 1 { return nil }
        return nil
        /*return pages[next]*/
    }
}

extension RegistrationPagerVC: OnboardingViewRepresentable {
    func onAction(_ action: OnboardingAction) {
        switch action {
        case let .errorMessage(text): showBannerWith(text: text, style: .warning)
        default: break
        }
    }
}

