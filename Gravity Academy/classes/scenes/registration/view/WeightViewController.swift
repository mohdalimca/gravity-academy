//
//  WeightViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 08/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class WeightViewController: UIViewController {
    
    @IBOutlet weak var weightPicker: UIPickerView!
    @IBOutlet weak var toggleControl: Toggle!
    
    var weightKG: [Int] = []
    var weightLBS: [Int] = []
    var isLBS: Bool = false
    weak var router: NextSceneDismisserPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RegistrationViewModel.previeousValue = RegistrationViewModel.weight
        RegistrationViewModel.unit = RegistrationViewModel.weight_unit
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.layoutIfNeeded()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("weight === \(RegistrationViewModel.weight)")
        print("weight Unit === \(RegistrationViewModel.weight_unit)")
    }
    
    private func setup() {
        weightPicker.delegate = self
        weightPicker.dataSource = self
        weightPicker.showsSelectionIndicator = false
        for i in 40...150 {
            weightKG.append(i)
        }
        for i in 88...330 {
            weightLBS.append(i)
        }
        setToggle()
        RegistrationViewModel.weight = "\(weightPicker.selectedRow(inComponent: 0))"
        RegistrationViewModel.weight_unit = weightUnitKG
    }
    
    private func setToggle() {
        toggleControl.layer.cornerRadius = 20.0
        toggleControl.layer.masksToBounds = true
        toggleControl.addTarget(self, action: #selector(toggleAction(_:)), for: .valueChanged)
        toggleControl.setSelected(index: 0)
    }
    
    @objc func toggleAction(_ toggle: Toggle) {
        isLBS = (toggleControl.indexPos == 0) ? false : true
        weightPicker.reloadAllComponents()
    }
    
}

extension WeightViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return !isLBS ? weightKG.count :  weightLBS.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return !isLBS ? "\(weightKG[row])" : "\(weightLBS[row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 60
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        RegistrationViewModel.weight = !isLBS ? "\(weightKG[row])" : "\(weightLBS[row])"
        RegistrationViewModel.weight_unit = !isLBS ? weightUnitKG : weightUnitLBS
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = view as? UILabel ?? UILabel()
        label.font = UIFont(name: Roboto_Bold, size: 20)
        label.textColor = .white
        label.textAlignment = .center
        label.text = !isLBS ? "\(weightKG[row])" : "\(weightLBS[row])"
        RegistrationViewModel.weight = label.text!
        RegistrationViewModel.weight_unit = !isLBS ? weightUnitKG : weightUnitLBS
        return label
    }
}


