//
//  VerifyOTPViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 09/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import SVPinView
import NotificationBannerSwift
import NVActivityIndicatorView

class VerifyOTPViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnSubmit: GravityAcademyButton!
    @IBOutlet weak var btnBack: GravityAcademyButton!
    @IBOutlet weak var pinView:SVPinView!
    
    var email: String!
    var router: NextSceneDismisserPresenter?
    let viewModel = RegistrationViewModel(provider: OnboardingServiceProvider())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        viewModel.view = self
        [btnBack, btnSubmit].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        email = (RegistrationViewModel.email != "") ? RegistrationViewModel.email : LoginViewModel.email
        configurePinView()
    }
    
    private func configurePinView() {
        pinView.style = .box
        pinView.becomeFirstResponderAtIndex = 0
        pinView.font = UIFont.systemFont(ofSize: 26)
        pinView.keyboardType = .phonePad
        pinView.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 40))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        
        pinView.didFinishCallback = didFinishEnteringPin(pin:)
        pinView.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
        }
    }
    
    func didFinishEnteringPin(pin:String) {
        startAnimation()
        viewModel.validateOTP(email: email, otp: pin)
    }
    
    private func validateOTP() {
        let pin = pinView.getPin()
        guard !pin.isEmpty else {
            showBannerWith(text: "OTP entry incomplete", style: .danger)
            return
        }
        startAnimation()
        viewModel.validateOTP(email: email, otp: pinView.getPin())
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    @objc func buttonPressed(_ sender: GravityAcademyButton) {
        if sender == btnSubmit {
            validateOTP()
        } else {
            router?.dismiss(controller: .verifyOTP)
        }
    }
}


extension VerifyOTPViewController: OnboardingViewRepresentable {
    func onAction(_ action: OnboardingAction) {
        switch action {
        case let .errorMessage(text), let .requireFields(text):
            showBannerWith(text: text, style: .danger)
            stopAnimating()
        case .validateOTP:
            stopAnimating()
            router?.present(scene: .home)
        default: break
        }
    }
}
