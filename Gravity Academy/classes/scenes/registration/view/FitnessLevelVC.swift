//
//  FitnessLevelVC.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class FitnessLevelVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleLevelOne: UILabel!
    @IBOutlet weak var lblTitleLevelTwo: UILabel!
    @IBOutlet weak var lblTitleLevelThree: UILabel!
    @IBOutlet weak var lblTitleLevelFour: UILabel!
    @IBOutlet weak var lblDescriptionLevelOne: UILabel!
    @IBOutlet weak var lblDescriptionLevelTwo: UILabel!
    @IBOutlet weak var lblDescriptionLevelThree: UILabel!
    @IBOutlet weak var lblDescriptionLevelFour: UILabel!
    @IBOutlet weak var btnLevelOne: UIButton!
    @IBOutlet weak var btnLevelTwo: UIButton!
    @IBOutlet weak var btnLevelThree: UIButton!
    @IBOutlet weak var btnLevelFour: UIButton!
    @IBOutlet weak var viewLevelOne: GravityAcademyView!
    @IBOutlet weak var viewLevelTwo: GravityAcademyView!
    @IBOutlet weak var viewLevelThree: GravityAcademyView!
    @IBOutlet weak var viewLevelFour: GravityAcademyView!
    
    var viewModel: RegistrationViewModel!
    weak var router: NextSceneDismisserPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RegistrationViewModel.previeousValue = RegistrationViewModel.fn_level_id
        setup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("fitness === \(RegistrationViewModel.fn_level_id)")
    }

    private func setup() {
        [btnLevelOne, btnLevelTwo, btnLevelThree, btnLevelFour].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        lblTitleLevelOne.text = (viewModel.fitnessLevels[0]).title
        lblTitleLevelTwo.text = (viewModel.fitnessLevels[1]).title
        lblTitleLevelThree.text = (viewModel.fitnessLevels[2]).title
        lblTitleLevelFour.text = (viewModel.fitnessLevels[3]).title
        lblDescriptionLevelOne.text = (viewModel.fitnessLevels[0]).fitnessLevelDescription
        lblDescriptionLevelTwo.text = (viewModel.fitnessLevels[1]).fitnessLevelDescription
        lblDescriptionLevelThree.text = (viewModel.fitnessLevels[2]).fitnessLevelDescription
        lblDescriptionLevelFour.text = (viewModel.fitnessLevels[3]).fitnessLevelDescription
    }
    
    private func setLevelOneColor(color:UIColor) {
        lblTitleLevelOne.textColor = color
        lblDescriptionLevelOne.textColor = color
        viewLevelOne.layer.borderColor = color.cgColor
    }
    
    private func setLevelTwoColor(color:UIColor) {
        lblTitleLevelTwo.textColor = color
        lblDescriptionLevelTwo.textColor = color
        viewLevelTwo.layer.borderColor = color.cgColor
    }
    
    private func setLevelThreeColor(color:UIColor) {
        lblTitleLevelThree.textColor = color
        lblDescriptionLevelThree.textColor = color
        viewLevelThree.layer.borderColor = color.cgColor
    }
    
    private func setLevelFourColor(color:UIColor) {
        lblTitleLevelFour.textColor = color
        lblDescriptionLevelFour.textColor = color
        viewLevelFour.layer.borderColor = color.cgColor
    }
    
    private func updateViewOnLevelOne() {
        setLevelOneColor(color: UIColor.init(hex: APP_THEME_ORANGE_COLOR))
        setLevelTwoColor(color: .white)
        setLevelThreeColor(color: .white)
        setLevelFourColor(color: .white)
        RegistrationViewModel.fn_level_id = (viewModel.fitnessLevels[0]).fnLevelID
        viewLevelOne.layer.borderWidth = updateBorderWith
        viewLevelTwo.layer.borderWidth = normalBorderWith
        viewLevelThree.layer.borderWidth = normalBorderWith
        viewLevelFour.layer.borderWidth = normalBorderWith
    }
    
    private func updateViewOnLevelTwo() {
        setLevelOneColor(color: .white)
        setLevelTwoColor(color: UIColor.init(hex: APP_THEME_ORANGE_COLOR))
        setLevelThreeColor(color: .white)
        setLevelFourColor(color: .white)
        RegistrationViewModel.fn_level_id = (viewModel.fitnessLevels[1]).fnLevelID
        viewLevelOne.layer.borderWidth = normalBorderWith
        viewLevelTwo.layer.borderWidth = updateBorderWith
        viewLevelThree.layer.borderWidth = normalBorderWith
        viewLevelFour.layer.borderWidth = normalBorderWith
    }
    
    private func updateViewOnLevelThree() {
        setLevelOneColor(color: .white)
        setLevelTwoColor(color: .white)
        setLevelThreeColor(color: UIColor.init(hex: APP_THEME_ORANGE_COLOR))
        setLevelFourColor(color: .white)
        RegistrationViewModel.fn_level_id = (viewModel.fitnessLevels[2]).fnLevelID
        viewLevelOne.layer.borderWidth = normalBorderWith
        viewLevelTwo.layer.borderWidth = normalBorderWith
        viewLevelThree.layer.borderWidth = updateBorderWith
        viewLevelFour.layer.borderWidth = normalBorderWith
    }
    
    private func updateViewOnLevelFour() {
        setLevelOneColor(color: .white)
        setLevelTwoColor(color: .white)
        setLevelThreeColor(color: .white)
        setLevelFourColor(color: UIColor.init(hex: APP_THEME_ORANGE_COLOR))
        RegistrationViewModel.fn_level_id = (viewModel.fitnessLevels[3]).fnLevelID
        viewLevelOne.layer.borderWidth = normalBorderWith
        viewLevelTwo.layer.borderWidth = normalBorderWith
        viewLevelThree.layer.borderWidth = normalBorderWith
        viewLevelFour.layer.borderWidth = updateBorderWith
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnLevelOne: updateViewOnLevelOne()
        case btnLevelTwo: updateViewOnLevelTwo()
        case btnLevelThree: updateViewOnLevelThree()
        case btnLevelFour: updateViewOnLevelFour()
        default: break
        }
    }
}
