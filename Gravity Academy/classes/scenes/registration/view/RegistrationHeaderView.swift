//
//  RegistrationHeaderView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 06/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class RegistrationHeaderView: UIView {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    
    weak var delegate: RegistrationHeaderViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        btnBack.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        progressView.layer.cornerRadius = 3
        progressView.layer.masksToBounds = true
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        delegate?.didSelect(action: .back)
    }
}
