//
//  HeightViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 08/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class HeightViewController: UIViewController {
    
    @IBOutlet weak var heightPicker: UIPickerView!
    @IBOutlet weak var toggleControl: Toggle!
    
    var heightCM: [Int] = []
    var heightInch: [Int] = []
    var isInch: Bool = false
    weak var router: NextSceneDismisserPresenter?
    var getHeightUpdate: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RegistrationViewModel.previeousValue = RegistrationViewModel.height
        RegistrationViewModel.unit = RegistrationViewModel.height_unit
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.layoutIfNeeded()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("height === \(RegistrationViewModel.height)")
        print("height Unit=== \(RegistrationViewModel.height_unit)")
    }
    
    private func setup() {
        heightPicker.delegate = self
        heightPicker.dataSource = self
        heightPicker.showsSelectionIndicator = false
        for i in 132...228 {
            heightCM.append(i)
        }
        for i in 50...90 {
            heightInch.append(i)
        }
        setToggle()
        RegistrationViewModel.height = "\(heightPicker.selectedRow(inComponent: 0))"
        RegistrationViewModel.height_unit = heightUnitCM
    }
    
    private func setToggle() {
        toggleControl.layer.cornerRadius = 20.0
        toggleControl.layer.masksToBounds = true
        toggleControl.addTarget(self, action: #selector(toggleAction(_:)), for: .valueChanged)
        toggleControl.setSelected(index: 0)
    }
    
    @objc func toggleAction(_ toggle: Toggle) {
        isInch = (toggleControl.indexPos == 0) ? false : true
        heightPicker.reloadAllComponents()
    }
}

extension HeightViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return !isInch ? heightCM.count :  heightInch.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return !isInch ? "\(heightCM[row])" : "\(heightInch[row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 60
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        RegistrationViewModel.height = !isInch ? "\(heightCM[row])" : "\(heightInch[row])"
        RegistrationViewModel.height_unit = !isInch ? heightUnitCM : heightUnitInch
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = view as? UILabel ?? UILabel()
        label.font = UIFont(name: Roboto_Bold, size: 20)
        label.textColor = .white
        label.textAlignment = .center
        label.text = !isInch ? "\(heightCM[row])" : "\(heightInch[row])"
        RegistrationViewModel.height = label.text!
        RegistrationViewModel.height_unit = !isInch ? heightUnitCM : heightUnitInch
        return label
    }
}

