//
//  GenderViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 08/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class GenderViewController: UIViewController {
    
    @IBOutlet weak var btnMale: GravityAcademyButton!
    @IBOutlet weak var btnFemale: GravityAcademyButton!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    
    var maleSelected = UIImage(named: "male-selected")
    var maleUnselected = UIImage(named: "male-unselected")
    var femaleSelected = UIImage(named: "female-selected")
    var femaleUnselected = UIImage(named: "female-unselected")
    var didUpdateGender: ((_ gender:String) -> Void)?
    
    weak var router: NextSceneDismisserPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if RegistrationViewModel.gender != "" {
            RegistrationViewModel.previeousValue = RegistrationViewModel.gender
            (RegistrationViewModel.gender == "1") ? buttonPressed(btnMale) : buttonPressed(btnFemale)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("gender === \(RegistrationViewModel.gender)")
    }
    
    private func setup() {
        [btnMale, btnFemale].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    @objc func buttonPressed(_ sender: GravityAcademyButton) {
        if sender == btnMale {
            RegistrationViewModel.gender = male //APP_THEME_ORANGE_COLOR
            btnMale.layer.borderColor = UIColor.init(hex: APP_THEME_ORANGE_COLOR).cgColor
            btnMale.setImage(maleSelected, for: .normal)
            lblMale.textColor = UIColor.init(hex: APP_THEME_ORANGE_COLOR)
            btnFemale.layer.borderColor = UIColor.white.cgColor
            btnFemale.setImage(femaleUnselected, for: .normal)
            lblFemale.textColor = .white
        } else {
            RegistrationViewModel.gender = female
            btnFemale.layer.borderColor = UIColor.init(hex: APP_THEME_ORANGE_COLOR).cgColor
            btnFemale.setImage(femaleSelected, for: .normal)
            lblFemale.textColor = UIColor.init(hex: APP_THEME_ORANGE_COLOR)
            btnMale.layer.borderColor = UIColor.white.cgColor
            btnMale.setImage(maleUnselected, for: .normal)
            lblMale.textColor = .white
        }
        didUpdateGender?(RegistrationViewModel.gender)
    }
}
