//
//  PerformanceViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 08/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class PerformanceViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblPushups: UILabel!
    @IBOutlet weak var lblPullups: UILabel!
    @IBOutlet weak var lblSquats: UILabel!
    @IBOutlet weak var lblDips: UILabel!
    @IBOutlet weak var txtPushups: UITextField!
    @IBOutlet weak var txtPullups: UITextField!
    @IBOutlet weak var txtSquats: UITextField!
    @IBOutlet weak var txtDips: UITextField!
    
    var viewModel: RegistrationViewModel!
    weak var router: NextSceneDismisserPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("pushup === \(RegistrationViewModel.max_pushups)")
        print("pullup === \(RegistrationViewModel.max_pullups)")
        print("squats === \(RegistrationViewModel.max_squats)")
        print("dips === \(RegistrationViewModel.max_dips)")
    }
    
    private func setup() {
        self.setTextOnAppear()
        [txtPushups, txtPullups, txtSquats, txtDips].forEach {
            $0?.delegate = self
            $0?.addTarget(self, action: #selector(textDidChanged(textField:)), for: .editingChanged)
        }
    }
    
    private func setTextOnAppear() {
        RegistrationViewModel.previeousDips = RegistrationViewModel.max_dips
        RegistrationViewModel.previeousPullup = RegistrationViewModel.max_pullups
        RegistrationViewModel.previeousPushup = RegistrationViewModel.max_pushups
        RegistrationViewModel.previeousSquats = RegistrationViewModel.max_squats
        
        txtPushups.text = RegistrationViewModel.max_pushups
        txtPullups.text = RegistrationViewModel.max_pullups
        txtSquats.text = RegistrationViewModel.max_squats
        txtDips.text = RegistrationViewModel.max_dips
    }
    
    @objc func textDidChanged(textField: UITextField) {
        if (textField == txtPushups) {
            RegistrationViewModel.max_pushups = textField.text ?? ""
        } else if (textField == txtPullups) {
            RegistrationViewModel.max_pullups = textField.text ?? ""
        } else if (textField == txtSquats) {
            RegistrationViewModel.max_squats = textField.text ?? ""
        } else{
            RegistrationViewModel.max_dips = textField.text ?? ""
        }
    }
}

extension PerformanceViewController:UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField == txtPushups) {
            RegistrationViewModel.max_pushups = textField.text ?? ""
        } else if (textField == txtPullups) {
            RegistrationViewModel.max_pullups = textField.text ?? ""
        } else if (textField == txtSquats) {
            RegistrationViewModel.max_squats = textField.text ?? ""
        } else{
            RegistrationViewModel.max_dips = textField.text ?? ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return range.location < 3
    }
}

