//
//  UserInfoViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 08/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController {
    
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnEye: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("username === \(RegistrationViewModel.user_name)")
        print("email === \(RegistrationViewModel.email)")
        print("password === \(RegistrationViewModel.password)")
    }
    
    private func setup() {
        txtUsername.delegate = self
        txtEmail.delegate = self
        txtPassword.delegate = self
        [txtUsername, txtName, txtEmail, txtPassword].forEach {
            $0?.delegate = self
            $0?.addTarget(self, action: #selector(textDidChanged(textField:)), for: .editingChanged)
        }
        btnEye.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
    }
    
    @objc func textDidChanged(textField: UITextField) {
        if (textField == txtUsername) {
            RegistrationViewModel.user_name = textField.text ?? ""
        } else if (textField == txtName) {
            RegistrationViewModel.name = textField.text ?? ""
        } else if (textField == txtEmail) {
            RegistrationViewModel.email = textField.text ?? ""
        } else {
            RegistrationViewModel.password = textField.text ?? ""
        }
    }
    
    @objc func buttonPressed( sender: UIButton ) {
        txtPassword.isSecureTextEntry = (!txtPassword.isSecureTextEntry) ? true : false
    }
}

extension UserInfoViewController:UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField == txtUsername) {
            RegistrationViewModel.user_name = textField.text ?? ""
        } else if (textField == txtName) {
            RegistrationViewModel.name = textField.text ?? ""
        } else if (textField == txtEmail) {
            RegistrationViewModel.email = textField.text ?? ""
        } else {
            RegistrationViewModel.password = textField.text ?? ""
        }
    }
}

