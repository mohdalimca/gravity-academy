//
//  GoalsViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 08/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class GoalsViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitleGoalOne: UILabel!
    @IBOutlet weak var lblTitleGoalTwo: UILabel!
    @IBOutlet weak var lblTitleGoalThree: UILabel!
    @IBOutlet weak var lblTitleGoalFour: UILabel!
    @IBOutlet weak var lblDescriptionGoalOne: UILabel!
    @IBOutlet weak var lblDescriptionGoalTwo: UILabel!
    @IBOutlet weak var lblDescriptionGoalThree: UILabel!
    @IBOutlet weak var lblDescriptionGoalFour: UILabel!
    @IBOutlet weak var btnGoalOne: UIButton!
    @IBOutlet weak var btnGoalTwo: UIButton!
    @IBOutlet weak var btnGoalThree: UIButton!
    @IBOutlet weak var btnGoalFour: UIButton!
    @IBOutlet weak var viewGoalOne: GravityAcademyView!
    @IBOutlet weak var viewGoalTwo: GravityAcademyView!
    @IBOutlet weak var viewGoalThree: GravityAcademyView!
    @IBOutlet weak var viewGoalFour: GravityAcademyView!
    
    var viewModel: RegistrationViewModel!
    weak var router: NextSceneDismisserPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RegistrationViewModel.previeousGoals = RegistrationViewModel.goals
        setup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("goals === \(RegistrationViewModel.goals)")
    }
    
    private func setup() {
        [btnGoalOne, btnGoalTwo, btnGoalThree, btnGoalFour].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        lblTitleGoalOne.text = (viewModel.goals[0]).title
        lblTitleGoalTwo.text = (viewModel.goals[1]).title
        lblTitleGoalThree.text = (viewModel.goals[2]).title
        lblTitleGoalFour.text = (viewModel.goals[3]).title
        lblDescriptionGoalOne.text = (viewModel.goals[0]).description
        lblDescriptionGoalTwo.text = (viewModel.goals[1]).description
        lblDescriptionGoalThree.text = (viewModel.goals[2]).description
        lblDescriptionGoalFour.text = (viewModel.goals[3]).description
    }
    
    private func setGoalOneColor(color:UIColor) {
        lblTitleGoalOne.textColor = color
        lblDescriptionGoalOne.textColor = color
        viewGoalOne.layer.borderColor = color.cgColor
    }
    
    private func setGoalTwoColor(color:UIColor) {
        lblTitleGoalTwo.textColor = color
        lblDescriptionGoalTwo.textColor = color
        viewGoalTwo.layer.borderColor = color.cgColor
    }
    
    private func setGoalThreeColor(color:UIColor) {
        lblTitleGoalThree.textColor = color
        lblDescriptionGoalThree.textColor = color
        viewGoalThree.layer.borderColor = color.cgColor
    }
    
    private func setGoalFourColor(color:UIColor) {
        lblTitleGoalFour.textColor = color
        lblDescriptionGoalFour.textColor = color
        viewGoalFour.layer.borderColor = color.cgColor
    }
    
    private func selectView(_ element:String) {
        switch element {
        case elementOne:
            setGoalOneColor(color: UIColor.init(hex: APP_THEME_ORANGE_COLOR))
            viewGoalOne.layer.borderWidth = updateBorderWith
        case elementTwo:
            setGoalTwoColor(color: UIColor.init(hex: APP_THEME_ORANGE_COLOR))
            viewGoalTwo.layer.borderWidth = updateBorderWith
        case elementThree:
            setGoalThreeColor(color: UIColor.init(hex: APP_THEME_ORANGE_COLOR))
            viewGoalThree.layer.borderWidth = updateBorderWith
        case elementFour:
            setGoalFourColor(color: UIColor.init(hex: APP_THEME_ORANGE_COLOR))
            viewGoalFour.layer.borderWidth = updateBorderWith
        default: break
        }
    }
    
    private func deSelectView(_ element:String) {
        switch element {
        case elementOne:
            setGoalOneColor(color: .white)
            viewGoalOne.layer.borderWidth = normalBorderWith
        case elementTwo:
            setGoalTwoColor(color: .white)
            viewGoalTwo.layer.borderWidth = normalBorderWith
        case elementThree:
            setGoalThreeColor(color: .white)
            viewGoalThree.layer.borderWidth = normalBorderWith
        case elementFour:
            setGoalFourColor(color: .white)
            viewGoalFour.layer.borderWidth = normalBorderWith
        default: break
        }
    }
    
    private func addElement(_ element:String) {
        if !RegistrationViewModel.goals.contains(element) {
            RegistrationViewModel.goals.append(element)
            selectView(element)
        } else {
            RegistrationViewModel.goals.removeAll { (item) -> Bool in
                deSelectView(element)
                return item == element
            }
        }
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnGoalOne: addElement((viewModel.goals[0]).goalID)
        case btnGoalTwo: addElement((viewModel.goals[1]).goalID)
        case btnGoalThree: addElement((viewModel.goals[2]).goalID)
        case btnGoalFour: addElement((viewModel.goals[3]).goalID)
        default: break
        }
    }
}
