//
//  GenderViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import UIKit

final class RegistrationViewModel: InputViewDelegate {
    
    static var gender = ""
    static var height = ""
    static var height_unit = ""
    static var weight = ""
    static var weight_unit = ""
    static var fn_level_id = ""
    static var goals = [String]()
    static var max_pullups = ""
    static var max_pushups = ""
    static var max_squats = ""
    static var max_dips = ""
    static var user_name = ""
    static var name = ""
    static var email = ""
    static var password = ""
    static var country = ""
    static var state = ""
    static var city = ""
    static var bio = ""
    static var previeousValue = ""
    static var previeousGoals = [String]()
    static var unit = ""
    static var previeousPushup = ""
    static var previeousPullup = ""
    static var previeousSquats = ""
    static var previeousDips = ""
    
    var apiName: AppAPI!
    var onIndexChanged: ((Int) -> Void)?
    var currentIndex: Int = 0
    var goals = [Goal]()
    var fitnessLevels = [FitnessLevel]()
    
    let provider:OnboardingServiceProviderable
    weak var view:OnboardingViewRepresentable?
    private let onboarding = OnboardingViewModel(provider: OnboardingServiceProvider())
    
    init(provider:OnboardingServiceProviderable) {
        self.provider = provider
        provider.delegate = self
    }
    
    func masterData() {
        provider.masterData()
    }
    
    func validateOTP(email: String, otp: String) {
        provider.validateOTP(email: email, otp: otp)
    }
    
    func onAction(action: OnboardingAction, for screen: OnboardingScreenType) {
        switch action {
        case let .editingDidEnd(field, input):
            setField(field: field, input: input)
        case .inputComplete(.register): validate()
        default: break
        }
    }
    
    private func setField(field:String, input:String) {
        if field == "Email"{
            RegistrationViewModel.email = input
        }
        if field == "Password" {
            RegistrationViewModel.password = input
        }
    }
    
    private func validate() {
        if RegistrationViewModel.name == "" {
            view?.onAction(.requireFields("name is required"))
            return
        }
        
        RegistrationViewModel.user_name = RegistrationViewModel.name
//        if RegistrationViewModel.user_name == "" {
//            view?.onAction(.requireFields("username is required"))
//            return
//        }

        if RegistrationViewModel.email == "" {
            view?.onAction(.requireFields("email is required"))
            return
        }
        if !onboarding.validEmail(email: RegistrationViewModel.email) {
            view?.onAction(.requireFields("invalid email"))
            return
        }
        if RegistrationViewModel.password == "" {
            view?.onAction(.requireFields("Password field is required"))
            return
        }
        
        if RegistrationViewModel.password.count < 8 {
            view?.onAction(.requireFields("password must be at least 8 characters"))
            return
        }
        
        provider.register(param: UserParams.Register(gender: RegistrationViewModel.gender, height: RegistrationViewModel.height, height_unit: RegistrationViewModel.height_unit, weight: RegistrationViewModel.weight, weight_unit: RegistrationViewModel.weight_unit, fn_level_id: RegistrationViewModel.fn_level_id, goals: RegistrationViewModel.goals, max_pullups: RegistrationViewModel.max_pushups, max_pushups: RegistrationViewModel.max_pullups, max_squats: RegistrationViewModel.max_squats, max_dips: RegistrationViewModel.max_dips, user_name: RegistrationViewModel.user_name, email: RegistrationViewModel.email, name: RegistrationViewModel.name, password: RegistrationViewModel.password))
    }
    
    // MARK: API Response
    private func masterAPI(with response: SuccessResponseModel?, with error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                guard let message = error?.responseData?.error else {
                    if error?.errorCode == .some(.network) {
                        self.view?.onAction(.noInternet)
                    } else {
                        self.view?.onAction(.errorMessage(errorMessage))
                    }
                    return
                }
                self.view?.onAction(.errorMessage(message))
            } else {
                self.fitnessLevels = response?.data?.masterData?.fitnessLevel ?? self.fitnessLevels
                self.goals = response?.data?.masterData?.goals ?? self.goals
                self.view?.onAction(.master)
            }
        }
    }
    
    private func registerAPI(with action: OnboardingAction, with error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                guard let message = error?.responseData?.error else {
                    if error?.errorCode == .some(.network) {
                        self.view?.onAction(.noInternet)
                    } else {
                        self.view?.onAction(.errorMessage(errorMessage))
                    }
                    return
                }
                self.view?.onAction(.errorMessage(message))
            } else {
                self.view?.onAction(action)
            }
        }
    }
    
    private func otpAPI(with action: OnboardingAction, response:OTPResponseModel?, with error: APIError?) {
        if error != nil {
            DispatchQueue.main.async {
                guard let message = error?.responseData?.error else {
                    if error?.errorCode == .some(.network) {
                        self.view?.onAction(.noInternet)
                    } else {
                        self.view?.onAction(.errorMessage(errorMessage))
                    }
                    return
                }
                self.view?.onAction(.errorMessage(message))
            }
        } else {
            DispatchQueue.main.async {
                UserStore.save(token: (response?.data.sessionkey)!)
                self.view?.onAction(action)
            }
        }
    }
}

extension RegistrationViewModel:OnboardingServiceProviderDelegate {
    func completed<T>(for action: OnboardingAction, with response: T?, with error: APIError?) {
        switch action {
        case .master: masterAPI(with: (response as? SuccessResponseModel ?? nil), with: error)
        case .userRegistered: registerAPI(with: .userRegistered, with: error)
        case .validateOTP: otpAPI(with: .validateOTP, response: (response as? OTPResponseModel ?? nil), with: error)
        default: break
        }
    }
}


