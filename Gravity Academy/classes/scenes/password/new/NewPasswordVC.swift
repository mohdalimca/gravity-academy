//
//  NewPasswordVC.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 02/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import NotificationBannerSwift

class NewPasswordVC: UIViewController {
    
    @IBOutlet weak var newMainView: NewMainView!
    weak var router: NextSceneDismisser?
    weak var delegate: NewPasswordViewControllerDelegate?
    private let viewModel = ForgotViewModel(provider: OnboardingServiceProvider())
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        viewModel.view = self
        newMainView.delegate = self
        self.view.setGesture()
    }
    
    private func newPasswordRequest() {
        startAnimation()
        viewModel.onAction(action: .inputComplete(.new), for: .new)
    }
}

extension NewPasswordVC: NewMainViewDelegate{
    func didSelect(action: ForgotButtonAction) {
        switch action {
        case .new: newPasswordRequest()
        case .back: router?.dismiss(controller: .newPassword)
        case .login: router?.push(scene: .newPassword)
        default: break
        }
    }
}


extension NewPasswordVC: OnboardingViewRepresentable {
    func onAction(_ action: OnboardingAction) {
        switch action {
        case let .errorMessage(text), let .requireFields(text):
            showBannerWith(text: text, style: .danger)
            stopAnimating()
        case .new:
            stopAnimating()
            router?.dismiss(controller: .newPassword)
        default: break
        }
    }
}
