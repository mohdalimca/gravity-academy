//
//  NewMainView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 03/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import UIKit

class NewMainView: UIView {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnNew: UIButton!
    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    weak var delegate: NewMainViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        txtPassword.delegate = self
        [ btnBack, btnEye, btnLogin, btnNew].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
        }
        [txtPassword, txtOTP].forEach {
            $0?.addTarget(self, action: #selector(didChangeText(_:)), for: .editingChanged)
        }
    }
    
    @objc func buttonPressed( sender: UIButton ) {
        switch sender {
        case btnEye: setText()
        case btnBack: delegate?.didSelect(action: .back)
        case btnLogin: delegate?.didSelect(action: .login)
        case btnNew: delegate?.didSelect(action: .new)
        default: break
        }
    }
    
    @objc func didChangeText(_ textField: UITextField) {
        if textField == txtPassword {
            ForgotViewModel.password = textField.text ?? ""
        } else {
            ForgotViewModel.otp = textField.text ?? ""
        }
    }
    
    private func setText() {
        txtPassword.isSecureTextEntry = (!txtPassword.isSecureTextEntry) ? true : false
    }
}

extension NewMainView: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtPassword {
            ForgotViewModel.password = textField.text ?? ""
        } else {
            ForgotViewModel.otp = textField.text ?? ""
        }
    }
}

