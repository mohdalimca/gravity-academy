//
//  ChangePasswordViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 12/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnNew: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var txtEmail: UITextField!

    weak var router: NextSceneDismisser?
    private let viewModel = ForgotViewModel(provider: OnboardingServiceProvider())

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
//        txtEmail.delegate = self
        [ btnBack].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
        }
    }
    
    @objc func buttonPressed( sender: UIButton ) {
        switch sender {
        case btnBack: router?.dismiss(controller: .changePassword)
        case btnLogin: break
        case btnNew: break
        default: break
        }
    }
}

//extension ChangePasswordViewController: ChangePasswordViewDelegate {
//    func didSelect(action: ForgotButtonAction) {
//        switch action {
//        case .new: viewModel.onAction(action: .inputComplete(.new), for: .new)
//        case .back: router?.dismiss(controller: .changePassword)
//        case .login: router?.push(scene: .newPassword)
//        default: break
//        }
//    }
//}


extension ChangePasswordViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        ForgotViewModel.email = textField.text ?? ""
    }
}


