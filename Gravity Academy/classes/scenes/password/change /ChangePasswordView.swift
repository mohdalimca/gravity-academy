//
//  ChangePasswordView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 12/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//


import Foundation
import UIKit

class ChangePasswordView: UIView {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnNew: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    
    weak var delegate: ChangePasswordViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        txtEmail.delegate = self
        [ btnBack, btnLogin, btnNew].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
        }
    }
    
    @objc func buttonPressed( sender: UIButton ) {
        switch sender {
        case btnBack: delegate?.didSelect(action: .back)
        case btnLogin: delegate?.didSelect(action: .login)
        case btnNew: delegate?.didSelect(action: .new)
        default: break
        }
    }
}

extension ChangePasswordView: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        ForgotViewModel.email = textField.text ?? ""
    }
}


