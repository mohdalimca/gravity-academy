//
//  ForgotAdaptable.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 02/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum ForgotButtonAction: String {
    case back
    case reset
    case new
    case login
}

protocol ForgotMainViewDelegate: class {
    func didSelect(action: ForgotButtonAction)
}

protocol ForgotViewControllerDelegate: class {
    func onAction(action: Scenes)
}

protocol NewMainViewDelegate: class {
    func didSelect(action: ForgotButtonAction)
}

protocol NewPasswordViewControllerDelegate: class {
    func onAction(action: Scenes)
}
protocol ChangePasswordViewDelegate: class {
    func didSelect(action: ForgotButtonAction)
}
