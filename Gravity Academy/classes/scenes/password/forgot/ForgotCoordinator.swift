//
//  ForgotCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 02/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

import Foundation

final class ForgotCoordinator: Coordinator<Scenes> {
    
    weak var delegate:CoordinatorDimisser?
    
    let forgot: ForgotPasswordVC = ForgotPasswordVC.from(from: .forgot, with: .forgot)
    let new: NewPasswordVC = NewPasswordVC.from(from: .forgot, with: .newPassword)
    
    override func start() {
        super.start()
        router.setRootModule(forgot, hideBar: true)
        onStart()
    }
    private func onStart() {
        forgot.router = self
        new.router = self
    }
}

extension ForgotCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {
        router.present(new, animated: true)
    }
    
    func push(scene: Scenes) {}
    
    func dismiss(controller: Scenes) {
        router.dismissModule(animated: true, completion: nil)
    }
}
