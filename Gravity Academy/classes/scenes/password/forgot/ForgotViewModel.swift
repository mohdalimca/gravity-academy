//
//  ForgotViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 11/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class ForgotViewModel:InputViewDelegate {
    weak var view:OnboardingViewRepresentable?
    static var email = ""
    static var password = ""
    static var otp = ""
    var message: String = ""
    let provider:OnboardingServiceProviderable
    let onboarding = OnboardingViewModel(provider: OnboardingServiceProvider())
    
    func onAction(action: OnboardingAction, for screen: OnboardingScreenType) {
        switch action {
        case let .editingDidEnd(field, input): setField(field: field, input: input)
        case .inputComplete(_): validate(on: screen)
        default:break
        }
    }
    
    init(provider:OnboardingServiceProviderable) {
        self.provider = provider
        provider.delegate = self
    }
    
    private func setField(field:String, input:String) {
        if field == "Email"{
            LoginViewModel.email = input
        }
    }
    
    private func validate(on screen:OnboardingScreenType) {
        switch screen {
        case .forgot: forgotEmailValidate()
        case .new: forgotPasswordValidate()
        default: break
        }
    }
    
    private func forgotEmailValidate() {
        if ForgotViewModel.email == "" {
            view?.onAction(.requireFields("email is required"))
            return
        }
        if !onboarding.validEmail(email: ForgotViewModel.email) {
            view?.onAction(.requireFields("invalid email"))
            return
        }
        provider.forgotPassword(email: ForgotViewModel.email)
    }
    
    private func forgotPasswordValidate() {
        
        if ForgotViewModel.otp == "" {
            view?.onAction(.requireFields("otp is required"))
            return
        }
        
        if ForgotViewModel.password == "" {
            view?.onAction(.requireFields("password is required"))
            return
        }
        
        if ForgotViewModel.password.count <= 7 {
            view?.onAction(.requireFields("password length should be 8 or more"))
            return
        }
        provider.newPassword(password: ForgotViewModel.password, otp: ForgotViewModel.otp)
    }
    
    private func handleForgot() {
        
    }
    
    private func handleForgot(with response: InformationResponseModel?, with error: APIError?) {
        if error != nil {
            DispatchQueue.main.async {
                guard let message = error?.responseData?.error else {
                    self.view?.onAction(.errorMessage(errorMessage))
                    return
                }
                self.view?.onAction(.errorMessage(message))
            }
        } else {
            DispatchQueue.main.async {
                guard let message = (response)?.message else { return }
                self.message = message
                self.view?.onAction(.forgot)
            }
        }
    }
    
    private func newPassword(with response: InformationResponseModel?, with error: APIError?) {
        if error != nil {
            DispatchQueue.main.async {
                guard let message = error?.responseData?.error else {
                    self.view?.onAction(.errorMessage(errorMessage))
                    return
                }
                self.view?.onAction(.errorMessage(message))
            }
        } else {
            DispatchQueue.main.async {
                guard let message = (response)?.message else { return }
                self.message = message
                self.view?.onAction(.forgot)
            }
        }
    }
}

extension ForgotViewModel:OnboardingServiceProviderDelegate {
    
    func completed<T>(for action: OnboardingAction, with response: T?, with error: APIError?) {
        if error != nil {
            DispatchQueue.main.async {
                guard let message = error?.responseData?.error else {
                    self.view?.onAction(.errorMessage(errorMessage))
                    return
                }
                self.view?.onAction(.errorMessage(message))
            }
        } else {
            DispatchQueue.main.async {
                guard let message = (response as? InformationResponseModel)?.message else { return }
                self.message = message
                self.view?.onAction(action)
            }
        }
    }
}
