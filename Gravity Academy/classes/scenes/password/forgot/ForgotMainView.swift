//
//  ForgotMainView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 02/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ForgotMainView: UIView {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    weak var delegate: ForgotMainViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        txtEmail.addTarget(self, action: #selector(onOnchange(sender:)), for: .editingChanged)
        txtEmail.addTarget(self, action: #selector(onDidEnd(sender:)), for: .editingDidEnd)
        txtEmail.addTarget(self, action: #selector(editingDidEndOnExit(sender:)), for: .editingDidEndOnExit)
        [ btnBack, btnLogin, btnReset].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
        }
    }
    
    @objc func editingDidEndOnExit(sender: UITextField) {
        endEditing(true)
        setText(sender)
    }
    @objc func onDidEnd(sender:UITextField) {
        endEditing(true)
        setText(sender)
    }
    @objc func onOnchange(sender:UITextField){
        setText(sender)
    }
    
    private func setText(_ sender:UITextField) {
        ForgotViewModel.email = sender.text!
    }
    
    @objc func buttonPressed( sender: UIButton ) {
        switch sender {
            case btnBack: delegate?.didSelect(action: .back)
            case btnLogin: delegate?.didSelect(action: .login)
            case btnReset: delegate?.didSelect(action: .reset)
            default: break
        }
    }
}

extension ForgotMainView: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        ForgotViewModel.email = textField.text ?? ""
    }
}

