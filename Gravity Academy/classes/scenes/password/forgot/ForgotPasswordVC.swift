//
//  ForgotPasswordVC.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 02/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import NotificationBannerSwift

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var forgotMainView: ForgotMainView!
    weak var router: NextSceneDismisserPresenter?
    weak var delegate: ForgotViewControllerDelegate?
    private let viewModel = ForgotViewModel(provider: OnboardingServiceProvider())
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        forgotMainView.delegate = self
        viewModel.view = self
    }
    
    private func forgotSuccess() {
        stopAnimating()
        showBannerWith(text: viewModel.message, style: .success)
        router?.present(scene: .newPassword)
    }
}

extension ForgotPasswordVC: ForgotMainViewDelegate {
    func didSelect(action: ForgotButtonAction) {
        switch action {
        case .reset:
        startAnimation()
        viewModel.onAction(action: .inputComplete(.forgot), for: .forgot)
        case .back: router?.dismiss(controller: .forgot)
        case .login: router?.push(scene: .newPassword)
        default: break
        }
    }
}

extension ForgotPasswordVC: OnboardingViewRepresentable {
    func onAction(_ action: OnboardingAction) {
        switch action {
        case let .errorMessage(text), let .requireFields(text):
            showBannerWith(text: text, style: .danger)
            stopAnimating()
        case .forgot: forgotSuccess()
        default: break
        }
    }
}
