
//
//  EditViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 14/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {
    
    @IBOutlet weak var editView: UIView!
    var viewModel: RegistrationViewModel!
    
    weak var router: NextSceneDismisserPresenter?
    var scene: Scenes!
    var vc = UIViewController()
    var currentValue:String?
    
    let gender: GenderViewController = GenderViewController.from(from: .register, with: .gender)
    let height: HeightViewController = HeightViewController.from(from: .register, with: .height)
    let weight: WeightViewController = WeightViewController.from(from: .register, with: .weight)
    let fitness: FitnessLevelVC = FitnessLevelVC.from(from: .register, with: .fitness)
    let goals: GoalsViewController = GoalsViewController.from(from: .register, with: .goals)
    let performance: PerformanceViewController = PerformanceViewController.from(from: .register, with: .performance)
    
    var didUpdateFitnessProfile: (() -> Void)?
    var didUpdateHeight: ((_ gender:String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        for view in editView.subviews {
            view.removeFromSuperview()
        }
    }
    
    private func genderView() {
        gender.router = router
        gender.didUpdateGender = getGenderUpdate
        vc = gender
        vc.view.tag = 1
    }
    
    private func heightView() {
        height.router = router
        vc = height
        vc.view.tag = 2
    }
    
    private func weightView() {
        weight.router = router
        vc = weight
    }
    
    private func fitnessView() {
        fitness.router = router
        fitness.viewModel = viewModel
        vc = fitness
    }
    
    private func goalsView() {
        goals.router = router
        goals.viewModel = viewModel
        vc = goals
    }
    
    private func performanceView() {
        performance.router = router
        performance.viewModel = viewModel
        vc = performance
    }
    
    private func setup() {
        switch scene {
        case .gender?: genderView()
        case .height?: heightView()
        case .weight?: weightView()
        case .fitness?: fitnessView()
        case .goals?: goalsView()
        case .maxDips?, .maxPullups?, .maxPushups?, .maxSquats:
            performanceView()
        default: break
        }
        self.addChild(vc)
        vc.view.frame = self.editView.bounds
        self.editView.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    private func resetToPreviousValue() {
        switch scene {
        case .gender?:
            RegistrationViewModel.gender = RegistrationViewModel.previeousValue
        case .height?:
            RegistrationViewModel.height = RegistrationViewModel.previeousValue
            RegistrationViewModel.height_unit = RegistrationViewModel.unit
        case .weight?:
            RegistrationViewModel.weight = RegistrationViewModel.previeousValue
            RegistrationViewModel.weight_unit = RegistrationViewModel.unit
        case .fitness?:
            RegistrationViewModel.fn_level_id = RegistrationViewModel.previeousValue
        case .goals?:
            let goals = RegistrationViewModel.previeousValue.components(separatedBy: ",")
            RegistrationViewModel.goals = goals
        case .maxDips?:
            RegistrationViewModel.max_dips = RegistrationViewModel.previeousDips
        case .maxPullups?:
            RegistrationViewModel.max_pullups = RegistrationViewModel.previeousPullup
        case .maxPushups?:
            RegistrationViewModel.max_pushups = RegistrationViewModel.previeousPushup
        case .maxSquats:
            RegistrationViewModel.max_squats = RegistrationViewModel.previeousSquats
        default: break
        }
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        resetToPreviousValue()
        router?.dismiss(controller: scene)
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        didUpdateFitnessProfile?()
        router?.dismiss(controller: scene)
    }
    
    func getGenderUpdate(_ gender:String) {
        print(gender)
    }
    
    func getHeightUpdate() {
    }
}
