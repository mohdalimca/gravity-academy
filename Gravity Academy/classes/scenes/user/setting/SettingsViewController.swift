//
//  SettingsViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 19/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import MessageUI
import NotificationBannerSwift
import NVActivityIndicatorView

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    
    @IBOutlet weak var tableView: SettingsTableView!
    var imagePicker: ImagePicker!
    weak var delegate:ControllerDismisser?
    weak var delegateTable:SettingsTableViewDelegate?
    weak var router: NextSceneDismisserPresenter?
    let viewModel = SettingsViewModel(provider: UserProfileServiceProvider())
    let registrationViewModel = RegistrationViewModel(provider: OnboardingServiceProvider())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        startAnimation()
        viewModel.getProfile()
        viewModel.view = self
        registrationViewModel.view = self
        registrationViewModel.masterData()
        tableView.delegateTable = delegateTable
        tableView.configure(viewModel)
        tableView.didSelectScene = didSelectScene
        imagePicker = ImagePicker(presentationController: self, delegate: viewModel)
        [btnBack, btnCamera, btnSave].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    @objc func buttonPressed(_ sender:UIButton) {
        switch sender {
        case btnBack:
            router?.dismiss(controller: .settings)
        case btnSave:
            startAnimation()
            viewModel.updateProfile()
        case btnCamera:
            imagePicker.present(from: sender)
        default:
            break
        }
    }
    
    private func populateInfo() {
        imageProfile.makeCircular()
        imageProfile.downloadImageFrom(urlString: viewModel.myProfile.image ?? "", with: shadowImage)
        lblName.text = viewModel.myProfile.name
        
        //        viewModel.gender = viewModel.myProfile.
        viewModel.height = viewModel.myProfile.height ?? ""
        viewModel.height_unit = viewModel.myProfile.heightUnit ?? ""
        viewModel.weight = viewModel.myProfile.weight ?? ""
        viewModel.weight_unit = viewModel.myProfile.weightUnit ?? ""
        viewModel.fn_level_id = viewModel.myProfile.fnLevelID ?? ""
        viewModel.goals = viewModel.myProfile.userGoals ?? ""
        viewModel.max_pullups = viewModel.myProfile.maxPullups ?? ""
        viewModel.max_pushups = viewModel.myProfile.maxPushups ?? ""
        viewModel.max_squats = viewModel.myProfile.maxSquats ?? ""
        viewModel.max_dips = viewModel.myProfile.maxDips ?? ""
        viewModel.user_name = viewModel.myProfile.userName ?? ""
        viewModel.name = viewModel.myProfile.name ?? ""
        viewModel.email = viewModel.myProfile.email ?? ""
        
        RegistrationViewModel.email = viewModel.myProfile.email ?? ""
        RegistrationViewModel.name = viewModel.myProfile.name ?? ""
        RegistrationViewModel.height = viewModel.myProfile.height ?? ""
        RegistrationViewModel.height_unit = viewModel.myProfile.heightUnit ?? ""
        RegistrationViewModel.weight = viewModel.myProfile.weight ?? ""
        RegistrationViewModel.weight_unit = viewModel.myProfile.weightUnit ?? ""
        RegistrationViewModel.fn_level_id = viewModel.myProfile.fnLevelID ?? ""
        RegistrationViewModel.goals = (viewModel.myProfile.goals?.components(separatedBy: ","))!
        RegistrationViewModel.max_pullups = viewModel.myProfile.maxPullups ?? ""
        RegistrationViewModel.max_pushups = viewModel.myProfile.maxPushups ?? ""
        RegistrationViewModel.max_squats = viewModel.myProfile.maxSquats ?? ""
        RegistrationViewModel.max_dips = viewModel.myProfile.maxDips ?? ""
        RegistrationViewModel.user_name = viewModel.myProfile.userName ?? ""
        RegistrationViewModel.gender = (viewModel.gender == "1") ? "1" : "2"
        tableView.reloadData()
        stopAnimating()
    }
    
    private func didSelectScene(_ scene:Scenes) {
        if scene != .contactus {
            router?.push(scene: scene)
        } else {
            sendEmail()
        }
    }
    
    func didUpdateFitnessProfile() {
        tableView.reloadSections(IndexSet(integer: 1), with: .fade)
    }
    
    func didUpdateHeight() {
        viewModel.gender = RegistrationViewModel.height
    }
    
    private func sendEmail() {
        let recipientEmail = "test@email.com"
        let subject = "Multi client email support"
        let body = ""

        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([recipientEmail])
            present(mail, animated: true)
        } else if let emailUrl = createEmailUrl(to: recipientEmail, subject: subject, body: body) {
            UIApplication.shared.open(emailUrl)
        } else {
            showBannerWith(text: "Can not send emails.", style: .info)
        }
    }
    
    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        return defaultUrl
    }
}

extension SettingsViewController: GravityAcademyAlertDelegate {
    func didSelect(action: GravityAcademyAlertButtonAction) {
        switch action {
        case .ok:
            startAnimation()
            viewModel.logout()
        default: break
        }
    }
}

extension SettingsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .cancelled:
            print("Mail cancelled")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        case .failed:
            print("Mail sent failure: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        controller.dismiss(animated: true)
    }
}

extension SettingsViewController: OnboardingViewRepresentable {
    func onAction(_ action: OnboardingAction) {
        switch action {
        case .master:
            break
        default:
            break
        }
    }
}

extension SettingsViewController: UserProfileViewRepresentable {
    func onAction(_ action: UserProfileAction) {
        switch action {
        case let .errorMessage(text):
            showBannerWith(text: text, style: .warning)
            stopAnimating()
        case .profile:
            populateInfo()
        case .chooseImage:
            if viewModel.selectedProfile != nil {
                imageProfile.image = viewModel.selectedProfile
            }
        case .logout:
            stopAnimating()
            router?.push(scene: .login)
        default:
            stopAnimating()
        }
    }
}
