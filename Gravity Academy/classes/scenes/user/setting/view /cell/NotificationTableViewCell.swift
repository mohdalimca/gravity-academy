//
//  NotificationTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 13/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell, Reusable {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var toggle: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        selectionStyle = .none
        toggle.set(width: 25, height: 13)
    }
    
    func configure<T>(with content: T) {
        guard let content = content as? Row else { return }
        lblTitle.text = content.name
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
