//
//  SettingHeaderCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class SettingHeaderCell: UITableViewCell, Reusable {
  
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configure<T>(with content: T) {
        guard let item = content as? RowSectionDisplayable else { return }
        title.text = item.title
    }
}
