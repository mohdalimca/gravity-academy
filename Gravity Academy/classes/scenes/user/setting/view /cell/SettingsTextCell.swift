//
//  SettingsTextCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class SettingsTextCell: UITableViewCell, Reusable {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtValue: UITextField!
    var scene:Scenes!
    var onChange: ((_ text:String, _ scene:Scenes) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    func configure<T>(with content: T) {
        selectionStyle = .none
        guard let content = content as? Row else { return }
        lblTitle.text = content.name
    }
    
    private func setup() {
        selectionStyle = .none
        txtValue.delegate = self
        txtValue.addTarget(self, action: #selector(onDidChange(textField:)), for: .editingChanged)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func onDidChange(textField:UITextField) {
        if let value = textField.text {
            onChange?(value, scene)
        }
    }
}

extension SettingsTextCell:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch scene {
        case .username, .email, .name, .country, .state, .city, .bio:
            textField.becomeFirstResponder()
        default:
            break
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch scene {
        case .username, .email, .name, .country, .state, .city, .bio:
            textField.becomeFirstResponder()
            return true
        default:
            return false
        }
    }
}
