//
//  SettingsTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class SettingsTableView: UITableView {
    
    var viewModel: SettingsViewModel!
    var registerViewModel: RegistrationViewModel!
    weak var delegateTable: SettingsTableViewDelegate?
    var didSelectScene: ((_ scene:Scenes) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ viewModel: SettingsViewModel) {
        self.viewModel = viewModel
        //        self.registerViewModel = registerViewModel
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 44
        rowHeight = UITableView.automaticDimension
        registerReusable(SettingHeaderCell.self)
        reloadData()
    }
    
    private func isLastRow(for indexPath: IndexPath) -> Bool{
        let rows = self.numberOfRows(inSection: indexPath.section)
        return (indexPath.row == rows - 1) ? true : false
    }
    
    private func setUserInputs(_ text:String, _ scene:Scenes) {
        switch scene {
        case .username:
            RegistrationViewModel.user_name = text
        case .email:
            RegistrationViewModel.email = text
        case .name:
            RegistrationViewModel.name = text
        case .country:
            RegistrationViewModel.country = text
        case .state:
            RegistrationViewModel.state = text
        case .city:
            RegistrationViewModel.city = text
        case .bio:
            RegistrationViewModel.bio = text
        default:
            break
        }
    }
    
    private func updateTextAndModel(_ cell:SettingsTextCell, _ scene:Scenes) {
        switch scene {
        case .username:
            cell.txtValue.text = RegistrationViewModel.user_name
        case .email:
            cell.txtValue.text = RegistrationViewModel.email
        case .name:
            cell.txtValue.text = RegistrationViewModel.name
        case .country:
            cell.txtValue.text = RegistrationViewModel.country
        case .state:
            cell.txtValue.text = RegistrationViewModel.state
        case .city:
            cell.txtValue.text = RegistrationViewModel.city
        case .bio:
            cell.txtValue.text = RegistrationViewModel.bio
        case .gender:
            cell.txtValue.text = (RegistrationViewModel.gender == "1") ? "Male" : "Female"
        case .height:
            let unit = (RegistrationViewModel.height_unit == "1") ? "centimeters" : "inchs"
            cell.txtValue.text = RegistrationViewModel.height + " " + unit
        case .weight:
            let unit = (RegistrationViewModel.weight_unit == "1") ? "kilograms" : "pounds"
            cell.txtValue.text = RegistrationViewModel.weight + " " + unit
        case .fitness:
            cell.txtValue.text = fitnessLevelTitle(RegistrationViewModel.fn_level_id)
        case .goals:
            cell.txtValue.text = viewModel.goals
        case .maxPushups:
            cell.txtValue.text = RegistrationViewModel.max_pushups
        case .maxPullups:
            cell.txtValue.text = RegistrationViewModel.max_pullups
        case .maxSquats:
            cell.txtValue.text = RegistrationViewModel.max_squats
        case .maxDips:
            cell.txtValue.text = RegistrationViewModel.max_dips
        default:
            break
        }
    }
}

extension SettingsTableView: UITableViewDelegate, UITableViewDataSource  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items(in: section).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusable(indexPath) as SettingsTextCell
        cell.scene = viewModel.item(for: indexPath).scence
        cell.configure(with: viewModel.item(for: indexPath))
        cell.txtValue.isHidden = false
        cell.onChange = { [unowned self ] (text, scene) in
            self.setUserInputs(text, scene)
        }
        
        switch indexPath.section {
        case 0: // profile
            cell.lblTitle.alpha = 0.5
            cell.accessoryType = .none
            cell.txtValue.isUserInteractionEnabled = true
        case 1: // fitness
            cell.lblTitle.alpha = 0.5
            cell.accessoryType = .none
            cell.txtValue.isUserInteractionEnabled = false
        case 2:
            if viewModel.item(for: indexPath).scence == .notification {
                let cell = tableView.dequeueReusable(indexPath) as NotificationTableViewCell
                cell.configure(with: viewModel.item(at: indexPath.section))
                return cell
            } else if viewModel.item(for: indexPath).scence == .delete {
                cell.lblTitle.textColor = .red
            }
            cell.lblTitle.alpha = 1
            cell.txtValue.isHidden = true
            cell.accessoryType = .disclosureIndicator
        default: // app
            cell.lblTitle.alpha = 1
            cell.txtValue.isHidden = true
            switch viewModel.item(for: indexPath).scence {
            case .logout:
                cell.accessoryType = .none
            case .version:
                cell.accessoryType = .none
                if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                    cell.lblTitle.text = "Version \(appVersion)"
                }

            default:
                cell.accessoryType = .disclosureIndicator
            }
        }
        
        self.updateTextAndModel(cell, viewModel.item(for: indexPath).scence)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rows = tableView.numberOfRows(inSection: indexPath.section)
        if indexPath.row == rows - 1 { /// LAST ROW
            cell.backgroundColor = .white
            tableView.separatorStyle = .none
        } else {
            cell.backgroundColor = .white
            tableView.separatorStyle = .singleLine
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectScene?(viewModel.item(for: indexPath).scence)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingHeaderCell") as? SettingHeaderCell
        cell?.configure(with: viewModel.item(at: section))
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}

