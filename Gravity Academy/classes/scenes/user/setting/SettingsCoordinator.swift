//
//  SettingsCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 19/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//


import Foundation
import UIKit
import MessageUI

final class SettingsCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: SettingsViewController = SettingsViewController.from(from: .settings, with: .settings)
    let edit: EditViewController = EditViewController.from(from: .settings, with: .edit)
    let userInfo: UserInfoViewController = UserInfoViewController.from(from: .register, with: .userInfo)
    let password: ChangePasswordViewController = ChangePasswordViewController.from(from: .forgot, with: .changePassword)
    let terms: TermsAndConditionVC = TermsAndConditionVC.from(from: .settings, with: .terms)
    let aboutus: AboutUsVC = AboutUsVC.from(from: .settings, with: .aboutus)
    
    var membership: MemberShipCoordinator!
    var login: LoginCoordinator!
    
    override func start() {
        super.start()
    }
    
    func start(_ myProfile:MyProfile) {
        super.start()
        controller.viewModel.myProfile = myProfile
        router.setRootModule(controller, hideBar: true)
        onStart()
    }
    
    private func onStart() {
        controller.delegateTable = self
        controller.router = self
        edit.router = self
        password.router = self
        terms.router = self
        aboutus.router = self
    }
    
    private func showAlert() {
        GravityAcademyAlertData.isDouble = true
        controller.gravityAlert(controller: controller)
    }
    
    private func sendMail() {
        if MFMailComposeViewController.canSendMail() {
            let mail  = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["check@mail.com"])
            mail.setMessageBody("Gravity Academy", isHTML: true)
            router.present(mail, animated: true)
        }
    }
    
    private func navigateToEditView(scene:Scenes) {
        edit.scene = scene
        edit.viewModel = controller.registrationViewModel
        edit.didUpdateFitnessProfile = controller.didUpdateFitnessProfile
        router.present(edit, animated: true)
    }
    
    private func startMembership() {
        membership = MemberShipCoordinator(router: Router())
        add(membership)
        membership.delegate = self
        membership.start()
        self.router.present(membership, animated: true)
    }
    
    private func startLogin() {
        login = LoginCoordinator(router: Router())
        add(login)
        login.delegate = self
        login.start()
        self.router.present(login, animated: true)
    }
}

extension SettingsCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .gender, .height, .weight, .fitness, .goals, .maxDips, .maxSquats, .maxPullups, .maxPushups:
            navigateToEditView(scene: scene)
        case .membership: startMembership()
        case .changePassword: router.present(password, animated: true)
        case .terms: router.present(terms, animated: true)
        case .contactus: sendMail()
        case .aboutus: router.present(aboutus, animated: true)
        //        case .support: router.present(performance, animated: true)
        case .delete, .logout: showAlert()
        case .login: startLogin()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        router.dismissModule(animated: true, completion: nil)
    }
}

extension SettingsCoordinator: SettingsTableViewDelegate {
    
    func sendAction(action: Scenes, viewModel: RegistrationViewModel) {
        switch action {
        case .gender, .height, .weight, .fitness, .goals, .performance:
            edit.scene = action
            edit.viewModel = viewModel
            router.present(edit, animated: true)
        case .membership: router.present(membership, animated: true)
        case .changePassword: router.present(password, animated: true)
        case .terms: router.present(terms, animated: true)
        case .contactus: sendMail()
        case .aboutus: router.present(aboutus, animated: true)
        //        case .support: router.present(performance, animated: true)
        case .delete, .logout: showAlert()
        default: break
        }
    }
}

extension SettingsCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

extension SettingsCoordinator: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case MFMailComposeResult.cancelled: print("cancelled")
        case MFMailComposeResult.failed: print("failed")
        case MFMailComposeResult.saved: print("saved")
        case MFMailComposeResult.sent: print("sent")
        default: break
        }
        router.dismissModule(animated: true, completion: nil)
    }
}
