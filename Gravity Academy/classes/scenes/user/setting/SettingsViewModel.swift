//
//  SettingsViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import UIKit

final class SettingsViewModel {
    
    var myProfile:MyProfile!
    weak var view: UserProfileViewRepresentable?
    var provider: UserProfileServiceProviderable
    private var items: [RowSectionDisplayable] = []
    var didSelect:((_ item:RowSectionDisplayable) -> Void)?
    var selectedProfile: UIImage?
    
    var gender = ""
    var height = ""
    var height_unit = ""
    var weight = ""
    var weight_unit = ""
    var fn_level_id = ""
    var goals = ""
    var max_pullups = ""
    var max_pushups = ""
    var max_squats = ""
    var max_dips = ""
    var user_name = ""
    var name = ""
    var email = ""
    var password = ""
    var country = ""
    var state = ""
    var city = ""
    var bio = ""
    
    init(provider:UserProfileServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
        items = RowSectionDisplayableFunction.makeRowSectionDisplayable(resource: "Settings")
    }
    
    var count:Int {
        return items.count
    }
    
    func items(in section: Int) -> [Row] {
        return items[section].content
    }
    
    func item(at section: Int) -> RowSectionDisplayable {
//        return section <= 3 ? items[section] : items[0]
        return items[section]
    }
    
    func item(for indexPath: IndexPath) -> Row {
        return items(in: indexPath.section)[indexPath.row]
    }
    
    func getProfile() {
        provider.getProfile()
    }
    
    func updateProfile() {
        provider.updateProfile()
    }
    
    func logout() {
        provider.logout(param: UserParams.Logout(sessionkey: UserStore.token ?? ""))
    }
}

extension SettingsViewModel:ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        guard let img = image else { return }
        self.selectedProfile = img
        view?.onAction(.chooseImage)
    }
}

extension SettingsViewModel: UserProfileServiceProviderDelegate {
    func completed<T>(for action: UserProfileAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .profile:
                        self.myProfile = resp.data?.myProfile
                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}

