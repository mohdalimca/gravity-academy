//
//  NoInternetViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 16/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import NVActivityIndicatorView

class NoInternetViewController: UIViewController {

    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnTryAgain: UIButton!
    var router: NextSceneDismisserPresenter?
    var didTryAgain:((_ api: AppAPI) -> Void)?
    var apiName: AppAPI!
    
    override func viewDidLoad() {
        setup()
    }
    
    private func setup() {
        super.viewDidLoad()
        [btnCancel, btnTryAgain].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    @objc func buttonPressed(_ sender:UIButton) {
        if sender == btnCancel {
            self.dismiss(animated: true, completion: nil)
        } else {
            if !Reachability.isConnectedToNetwork() {
                showBannerWith(text: noInternet, style: .danger)
                return
            } else {
                didTryAgain?(apiName)
            }
        }
    }
}
