//
//  BrowserCoordinator.swift
//  Clustry
//
//  Created by cis on 17/08/20.
//  Copyright © 2020 cis. All rights reserved.
//

import Foundation
final class BrowserCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: BrowserViewController = BrowserViewController.from(from: .common, with: .browser)
    
    override func start() {
        super.start()
        controller.router = self
        router.setRootModule(controller, hideBar: true)
    }
    
    func start(url:URL) {
        super.start()
        controller.router = self
        controller.browserURL = url
        router.setRootModule(controller, hideBar: true)
    }
}

extension BrowserCoordinator: NextSceneDismisser {
    
    func push(scene: Scenes) {
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension BrowserCoordinator: CoordinatorDimisser {
    
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
