//
//  ProMemberViewController.swift
//  Gravity Academy
//
//  Created by Apple on 17/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ProMemberViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    weak var router: NextSceneDismisser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        router?.dismiss(controller: .proMember)
    }
}
