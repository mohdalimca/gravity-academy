//
//  ActivityDetailHeaderView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 16/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ActivityDetailHeaderView: UIView {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnMore: UIButton!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var cnsViewHeight: NSLayoutConstraint!
    weak var delegateHeader: ActivityDetailHeaderViewDelegate?

    var isMore = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        [btnBack, btnLike, btnMore].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }

    private func animateView() {
        if isMore { // up and down status, header size 40
            UIView.animate(withDuration: 0.4) {
                self.cnsViewHeight.constant = 375
                self.layoutIfNeeded()
            }
            isMore = false // close
        }else{
            isMore = true // open
            UIView.animate(withDuration: 0.4) {
                self.cnsViewHeight.constant = 275
                self.layoutIfNeeded()
            }
        }
        self.endEditing(true)
    }
    
     @objc func  buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnBack: delegateHeader?.didSelect(action: .back)
        case btnLike: delegateHeader?.didSelect(action: .like)
        case btnMore: animateView()
        default: break
        }
    }
}
