//
//  ActivityDetailTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ActivityDetailTableViewCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var lblPart: UILabel!
    @IBOutlet weak var lblPartTile: UILabel!
    @IBOutlet weak var lblSession: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var imageCheck: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configure<T>(with content: T) {
        if let week = content as? TrainingPlanWeek {
            lblPart.text = "Week \(week.week!)"
            lblSession.text = week.status
            lblPartTile.text = week.title
//            containerView.addShadowToView()
            return
            //        containerView.addShadowAround(shadowSize: 10, color: .gray)
        }
        
        if let week = content as? SkillWeek {
            lblPart.text = "Week \(week.week!)"
            lblSession.text = week.status
            lblPartTile.text = week.title
            containerView.layer.cornerRadius = 10
//            containerView.addShadowToView()
            return
            //        containerView.addShadowAround(shadowSize: 10, color: .gray)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
