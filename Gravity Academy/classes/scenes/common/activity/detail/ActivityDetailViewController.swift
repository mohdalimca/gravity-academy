//
//  ActivityDetailViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ActivityDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: ActivityDetailTableView!
    @IBOutlet weak var header: ActivityDetailHeaderView!
    weak var delegate:ControllerDismisser?
    weak var router: NextSceneDismisserPresenter?
    weak var delegateTable: ActivityDetailTableViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        header.delegateHeader = self
        tableView.reloadData()
        tableView.delegateTable = delegateTable
        //        tableView.configure(viewModel)
    }
}

extension ActivityDetailViewController: ActivityDetailHeaderViewDelegate {
    func didSelect(action: activityDetailAction) {
        if action == .back {
            delegate?.dismiss(controller: .activityDetail)
        }
    }
}
