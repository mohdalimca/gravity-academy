
//
//  ExerciseAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum ExerciseHeaderButtonAction: String {
    case back
    case like
    case start
    case playPause
}

enum ExerciseAction {
    case incomplet(_ text:String)
    case errorMessage(_ text:String)
    case requiredField(_ text:String)
    case list
}

protocol ExerciseHeaderViewDelegate:class {
    func sendAction(action: ExerciseHeaderButtonAction)
}
protocol ExerciseViewRepresentable:class {
    func onAction(action: ExerciseAction)
}
protocol ExerciseServiceProviderDelegate:class{
    func completed<T>(for action:ExerciseAction, with response:T?, and error:APIError?)
}
protocol ExerciseServiceProviderable:class {
    var delegate: ExerciseServiceProviderDelegate? { get set }
    func exerciseLibrary()
    func likeDislike(param:LikeDislikeParam)
}
