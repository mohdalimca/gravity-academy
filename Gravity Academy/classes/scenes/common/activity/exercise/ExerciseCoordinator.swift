//
//  ExerciseCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class ExerciseCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: ExerciseViewController = ExerciseViewController.from(from: .common, with: .exercise)
    let timer: TimerViewController = TimerViewController.from(from: .exercise, with: .timer)

    override func start() {
        super.start()
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    func startWith(_ exercise: Exercise, _ indexPath:IndexPath, _ workoutsDetail:[WoDetail], _ all:[Exercise]) {
        super.start()
        onStart()
        controller.viewModel.detail = exercise
        controller.viewModel.allExercise = all
        controller.viewModel.indexPath = indexPath
        router.setRootModule(controller, hideBar: true)
    }
    
    func startWith(_ exercise: Exercise, hideNextButton:Bool = true) {
        super.start()
        onStart()
        controller.viewModel.detail = exercise
        controller.viewModel.hideNextButton = hideNextButton
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.router = self
        timer.router = self
    }
}

extension ExerciseCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        router.present(timer, animated: true)
    }
    
    func dismiss(controller: Scenes) {
        switch controller {
        case .timer:
            router.dismissModule(animated: true, completion: nil)
        default:
            delegate?.dismiss(coordinator: self)
        }
    }
}
