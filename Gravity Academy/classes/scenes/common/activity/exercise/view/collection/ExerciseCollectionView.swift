//
//  ExerciseCollectionView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ExerciseCollectionView: UICollectionView {
    var viewModel: ExerciseViewModel!
    let layout = UICollectionViewFlowLayout()
    var index: Int?
    
    var items: [String] = []
    var fitness: [String] = []
    var muscles: [String] = []
    var equipments: [String] = []
    let labelFont = UIFont(name: AppFont.Medium.rawValue, size: 9.0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(viewModel:ExerciseViewModel, index: Int) {
        self.index = index
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        sectionItems()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 10
        collectionViewLayout = layout
        delegate = self
        dataSource = self
        reloadData()
    }
    private func sectionItems() {
        switch index {
        case 0:
            items = (viewModel.detail.fitnessLevel?.components(separatedBy: ","))!
        case 1:
            items = (viewModel.detail.muscleGroups?.components(separatedBy: ","))!
        case 2:
            items = viewModel.detail.equipment?.components(separatedBy: ",") ?? [NOEQUIPMENT]
        default:
            break
        }
    }
}

extension ExerciseCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusable(indexPath) as ExerciseCollectionViewCell
        cell.configure(with: items[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath)
        print(collectionView.tag)
    }
    
    private func getSizeOfText(_ text:String , _ width:CGFloat, _ height:CGFloat) -> CGSize {
        let lbl = UILabel()
        lbl.text = text
        lbl.textAlignment = .center
        lbl.sizeToFit()
        
        return CGSize(width: lbl.frame.size.width + width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let lbl = UILabel()
//        lbl.text = items[indexPath.item]
//        let size = items[indexPath.item].getSizeOfText(15, 25)
//        lbl.textAlignment = .center
//        lbl.sizeToFit()
//        layout.itemSize = items[indexPath.item].getSizeOfText(20, 25)
//        return layout.itemSize
//        let cc = items[indexPath.item]
        
//        let size = items[indexPath.item].size(OfFont: labelFont!)
//        return CGSize(width: size.width + 40, height: 25)
        
        let lbl = UILabel()
        lbl.text = items[indexPath.item]
        lbl.textAlignment = .center
        lbl.sizeToFit()
        layout.itemSize = CGSize(width: lbl.frame.size.width + 30, height: 25)
        return layout.itemSize
    }
}

