//
//  ExerciseCollectionViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ExerciseCollectionViewCell: UICollectionViewCell, Reusable {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure<T>(with content: T) {
        guard let title = content as? String else { return }
        lblTitle.text = title.uppercased()
    }
    
    private func setup() {
        lblTitle.text = "KEIN EQUIPMENT"
    }
    
}
