//
//  ExerciseTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ExerciseTableViewCell: UITableViewCell, Reusable {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var exerciseCollection: ExerciseCollectionView!
    var viewModel: ExerciseViewModel!
    var index: Int!
    
    override func awakeFromNib() {
        selectionStyle = .none
        super.awakeFromNib()
    }

    func configure<T>(with content: T) {
        guard let viewModel = content as? ExerciseViewModel else { return }
        self.viewModel = viewModel
        exerciseCollection.configure(viewModel: viewModel, index: index)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setup() {
        exerciseCollection.configure(viewModel: viewModel, index: index)
    }
}
