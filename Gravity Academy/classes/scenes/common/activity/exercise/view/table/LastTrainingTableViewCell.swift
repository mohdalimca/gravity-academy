//
//  LastTrainingTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class LastTrainingTableViewCell: UITableViewCell, Reusable {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblWDH: UILabel!
    @IBOutlet weak var lblWDHCount: UILabel!
    @IBOutlet weak var lblSatze: UILabel!
    @IBOutlet weak var lblSatzeCount: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblWeightCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    func configure<T>(with content: T) {
        
    }
    
    private func setup() {
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
