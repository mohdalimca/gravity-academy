//
//  ButtonTableViewCell.swift
//  Gravity Academy
//
//  Created by Apple on 30/06/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import UIKit

class ButtonTableViewCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var btnNext:UIButton!
    var didSelectNext: (() -> Void)?
    var indexPath: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        guard let index = content as? IndexPath else { return }
        self.indexPath = index
    }
    
    private func setup() {
        selectionStyle = .none
        btnNext.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
    }
    
    @objc func buttonPressed(_ btn:UIButton) {
        didSelectNext?()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
