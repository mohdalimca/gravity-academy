//
//  ExerciseTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ExerciseTableView: UITableView {
    
    var viewModel: ExerciseViewModel!
    var didSelectNext: (() -> Void)?
    var titleArray = ["Fitness Level", "Muscle Group", "Equipment Necessary"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure(_ viewModel: ExerciseViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
        registerReusable(StartHeaderCell.self)
    }
}

extension ExerciseTableView: UITableViewDelegate, UITableViewDataSource  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        if indexPath.row != 3 {
            let cell = tableView.dequeueReusable(indexPath) as ExerciseTableViewCell
            cell.index = indexPath.item
            cell.lblTitle.text = titleArray[indexPath.row]
            cell.configure(with: viewModel)
            return cell
        } else {
            if !viewModel.hideNextButton {
                let cell = tableView.dequeueReusable(indexPath) as ButtonTableViewCell
                cell.didSelectNext = didSelectNext
                cell.btnNext.isHidden = false
                if viewModel.allExercise != nil && viewModel.currentIndex == viewModel.allExercise.count - 1 {
                    cell.btnNext.isHidden = true
                }
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}




