//
//  ExerciseHeaderView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import HCVimeoVideoExtractor

class ExerciseHeaderView: UIView {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var toggle: UISwitch!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var imageExercise: UIImageView!

    weak var delegate: ExerciseHeaderViewDelegate?
    var viewModel: ExerciseViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        toggle.set(width: 25, height: 13)
        btnLike.layer.cornerRadius = btnLike.bounds.height / 2
        [btnBack, btnLike, btnStart, btnPlayPause].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    func populateDetail(_ viewModel: ExerciseViewModel) {
        self.viewModel = viewModel
        let detail = viewModel.detail!
        imageExercise.downloadImageFrom(urlString: (detail.mediaData?.image!)!, with: placeHolderImage)
        lblName.text = detail.title
//        if detail.resp != nil && detail.resp != "0" {
        if detail.resp != nil {
            lblDescription.text = "\(detail.resp ?? "0") reps ● rest \(detail.rest ?? "0") seconds"
        } else {
            lblDescription.text = "\(detail.hold ?? "0") hold ● rest \(detail.rest ?? "0") seconds"
        }
        btnLike.isSelected = (detail.isLike == "1") ? true : false
        getDataFromVimeoLink(detail.videoURL ?? "")
    }
    
    private func getDataFromVimeoLink(_ videoURL:String) {
        
        if let url = URL(string: videoURL) {
            HCVimeoVideoExtractor.fetchVideoURLFrom(url: url, completion: { ( video:HCVimeoVideo?, error:Error?) -> Void in
            
                if let err = error {
                    print("Error = \(err.localizedDescription)")
                    DispatchQueue.main.async() {
                        self.viewModel.view?.onAction(.errorMessage("Vimeo link is not public"))
                    }
                    return
                }
                
                guard let vid = video else {
                    self.viewModel.view?.onAction(.errorMessage("Invalid video object"))
                    return
                }
                
                print("Title = \(vid.title), url = \(vid.videoURL), thumbnail = \(vid.thumbnailURL)")
            
                DispatchQueue.main.async() {
                    self.viewModel.videoURL = vid.videoURL[.Quality540p]
                }
            })
        }
    }
    
    func updateLikeDislikeButton(_ detail: Exercise) {
        DispatchQueue.main.async {
            self.btnLike.isSelected = (detail.isLike != "0") ? true : false
        }
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnBack: delegate?.sendAction(action: .back)
        case btnLike: delegate?.sendAction(action: .like)
        case btnPlayPause: delegate?.sendAction(action: .playPause)
        case btnStart: delegate?.sendAction(action: .start)
        default: break
        }
    }
}
