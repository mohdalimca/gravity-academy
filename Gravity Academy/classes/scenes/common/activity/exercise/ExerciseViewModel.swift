//
//  ExerciseViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 31/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class ExerciseViewModel {

    var items: [Exercise] = []
    weak var view:AllExerciseViewRepresentable?
    var provider: AllExerciseServiceProviderable?
    var detail: Exercise!
    var likeStatus: Bool = false
    var hideNextButton: Bool = false
    var indexPath: IndexPath!
    var currentIndex:Int!
    var allExercise: [Exercise]!
    var videoURL:URL?
    
    init(provider: AllExerciseServiceProviderable) {
        provider.delegate = self
        self.provider = provider
    }
    
    func likeDislikeAction() {
        self.provider?.likeDislike(param: LikeDislikeParam(type: .exercise, type_id: detail.exerciseID))
    }
}

extension ExerciseViewModel: AllExerciseServiceProviderDelegate {
    func completed<T>(for action: AllExerciseAction, with response: T?, and error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view!.onAction(.errorMessage(errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .exerciseLikeDislike:
                        self.likeStatus = (resp.data?.status == 1) ? true : false
                    default:
                        break
                    }
                    self.view?.onAction(action)
                }
            }
        }
    }
}

