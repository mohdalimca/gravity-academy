//
//  ExerciseViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ExerciseViewController: UIViewController {
    
    @IBOutlet weak var tableView: ExerciseTableView!
    @IBOutlet weak var header: ExerciseHeaderView!
    
    weak var router:NextSceneDismisserPresenter?
    var allViewModel: AllExerciseViewModel!
    var viewModel = ExerciseViewModel(provider: AllExerciseServiceProvider())
    var handleLike: ((_ isLike:Bool, _ indexPath:IndexPath) -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        viewModel.view = self
        header.delegate = self
        if viewModel.allExercise != nil {
            viewModel.currentIndex = viewModel.allExercise.firstIndex(where: {
                $0.exerciseID == viewModel.detail.exerciseID
            })
        } else {
            
        }
        header.populateDetail(viewModel)
        tableView.didSelectNext = didSelectNext
        tableView.configure(viewModel)
    }
    
    private func reloadAfterLikeDislikeAction() {
        viewModel.detail.isLike = (viewModel.likeStatus) ? "1" : "0"
        handleLike?(viewModel.likeStatus, viewModel.indexPath)
        header.updateLikeDislikeButton(viewModel.detail)
        NotificationCenter.default.post(name: .handleExercisLikeDislike, object: viewModel.likeStatus)
    }
    
    private func presentPlayer() {
        if let _ = viewModel.videoURL {
            self.playVideo(viewModel.videoURL!)
        } else {
            showBannerWith(text: "No video available for the exercise", style: .info)
        }
    }
    
    private func didSelectNext() {
        //        viewModel.currentIndex = viewModel.allExercise.firstIndex(where: {
        //            $0.exerciseID == viewModel.detail.exerciseID
        //        })
        
        viewModel.currentIndex = viewModel.currentIndex + 1
        if viewModel.currentIndex < viewModel.allExercise.count {
            viewModel.detail = viewModel.allExercise[viewModel.currentIndex]
        }
        header.populateDetail(viewModel)
        tableView.reloadData()
    }
}

extension ExerciseViewController: ExerciseHeaderViewDelegate {
    func sendAction(action: ExerciseHeaderButtonAction) {
        switch action {
        case .back: router?.dismiss(controller: .exercise)
        case .like: viewModel.likeDislikeAction()
        case .start: router?.push(scene: .timer)
        case .playPause: presentPlayer()
        }
    }
}


extension ExerciseViewController: AllExerciseViewRepresentable {
    func onAction(_ action: AllExerciseAction) {
        switch action {
        case let .errorMessage(text):
            showBannerWith(text: text, style: .warning)
            stopAnimating()
        case .exerciseLikeDislike:
            reloadAfterLikeDislikeAction()
        default:
            stopAnimating()
        }
    }
}
