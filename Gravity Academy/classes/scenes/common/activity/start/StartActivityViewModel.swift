//
//  StartActivityViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 16/04/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class StartActivityViewModel {
    
    var workoutId: String?
    var workout: Workouts?
    var workoutOfTheDay: WoOfTheDay?
    var fitnessLevel: String!
    var workoutCategory:String!
    var trainingPlan: TrainingPlanWeekDetail?
    var skillDetail: SkillWeekDetail!
    var workoutDetail = [WoDetail]()
    let provider: WorkoutsServiceProviderable
    weak var view: WorkoutsViewRepresentable?
    var selectedIndexpath: IndexPath!
    var isLike = false
    var totalExercise = 0
    var allEexercise = [Exercise]()
    
    
    init(provider: WorkoutsServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    func workoutDetail(_ param: WorkoutsParams.WorkoutsDetail) {
        self.provider.workoutDetail(param: param)
    }
    
    func likeAPI(param:LikeDislikeParam) {
        provider.likeDislike(param: param)
    }
}

extension StartActivityViewModel: WorkoutsServiceProviderDelegate {
    func completed<T>(for action: WorkoutsAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .likeDislike:
                        self.isLike = (resp.data?.status != 0 ) ? true : false
                    case .detail:
                        self.workoutDetail = resp.data?.woDetail ?? self.workoutDetail
                    default: break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
