
//
//  StartActivityCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import UIKit

final class StartActivityCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: StartActivityViewController = StartActivityViewController.from(from: .common, with: .startActivity)
    var exercise: ExerciseCoordinator!
    var home: HomeCoordinator!
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    func start(with workout:Workouts?, woOfDay:WoOfTheDay?, level:String!, workoutCategory:String) {
        super.start()
        onStart()
        controller.viewModel.fitnessLevel = level
        controller.viewModel.workout = workout
        controller.viewModel.workoutOfTheDay = woOfDay
        controller.viewModel.workoutCategory = workoutCategory
        router.setRootModule(controller, hideBar: true)
    }
    
    func start(trainingPlan: TrainingPlanWeekDetail) {
        super.start()
        onStart()
        controller.viewModel.trainingPlan = trainingPlan
        controller.viewModel.fitnessLevel = trainingPlan.fnLevelID
        controller.viewModel.workoutCategory = trainingPlan.workoutGoal
        router.setRootModule(controller, hideBar: true)
    }
    
    func start(skillDetail: SkillWeekDetail) {
        super.start()
        onStart()
        controller.viewModel.skillDetail = skillDetail
        controller.viewModel.fitnessLevel = skillDetail.fnLevelID
        controller.viewModel.workoutCategory = skillDetail.workoutGoal
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.delegateTable = self
        controller.router = self
    }
    
    private func startExercise(_ indexPath: IndexPath) {
        exercise = ExerciseCoordinator(router: Router())
        add(exercise)
        exercise.delegate = self
        exercise.controller.handleLike = controller.handleLike
        let result = ((controller.viewModel.workoutDetail[indexPath.section]).exercise![indexPath.row])
        exercise.startWith(result, indexPath, controller.viewModel.workoutDetail, controller.viewModel.allEexercise)
        router.present(exercise, animated: true)
    }
    
    private func allExercise() {
        let r = Router()
        let all = AllExerciseCoordinator(router: r)
        add(all)
        all.delegate = self
        all.start()
        router.present(all, animated: true)
    }
    
    private func startcomplete() {
        let complete = CompleteWorkoutViewController()
        complete.router = self
        complete.transitioningDelegate = self
        complete.modalPresentationStyle = .custom
        complete.viewModel.workoutId = controller.viewModel.workoutId
        router.present(complete, animated: true)
    }
    
    private func startHome() {
        home = HomeCoordinator(router: Router())
        add(home)
        home.start()
        router.present(home, animated: true)
    }
}


extension StartActivityCoordinator : UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return PresentationController(presentedViewController: presented, presenting: presenting)
    }
}

extension StartActivityCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .completeWorkout: startcomplete()
        case .allExercise: allExercise()
        case .home: startHome()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension StartActivityCoordinator: ActivityDetailTableViewDelegate {
    func didSelectAt(index: Int) {
        //        router.present(session, animated: true)
    }
}

extension StartActivityCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

extension StartActivityCoordinator: StartActivityTableViewDelegate {
    func didSelectAt(indexPath: IndexPath) {
        print("indexpath is \(indexPath)")
        startExercise(indexPath)
    }
}

