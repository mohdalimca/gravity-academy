//
//  StartActivityHeaderView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit


class StartActivityHeaderView: UIView {
    
    @IBOutlet weak var lblWorkoutGoal: UILabel!
    @IBOutlet weak var lblFitnessLevel: UILabel!
    @IBOutlet weak var lblWorkoutCategory: UILabel!
    @IBOutlet weak var lblMuscles: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnStartWorkout: UIButton!
    @IBOutlet weak var btnPlayPause: UIButton! {
        didSet {
            btnPlayPause.isHidden = true
        }
    }
    @IBOutlet weak var imageBG: UIImageView!
    @IBOutlet weak var collection: MusclesCollectionView!
    
    var isStart = false
    var timer: Timer!
    var countdown: Int = 1
    weak var delegateHeader:StartActivityHeaderViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        btnLike.layer.cornerRadius = btnLike.bounds.height / 2
        [btnBack, btnLike, btnStartWorkout, btnPlayPause].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    private func fitlessLevel(_ level:String) -> String {
        var title = ""
        if level == "1" {
            title = "Beginner"
        } else if level == "1" {
            title = "Intermediate"
        } else if level == "1" {
            title = "Advanced"
        }
        return title
    }
    
    func populateDetails(_ workout:Workouts?, _ dayWorkout:WoOfTheDay?, _ trainingPlan:TrainingPlanWeekDetail?, _ level:String, _ category:String?) {
        lblFitnessLevel.text = fitnessLevelTitle(level)
        lblWorkoutCategory.text = dayWorkout?.workoutGoal ?? "Test Category"
//        lblWorkoutCategory.text = category.capitalized
        
        if workout != nil { // Workouts
            btnLike.isSelected = (workout?.isLike == "1") ? true : false
            lblWorkoutGoal.text = workout?.workoutGoal
            imageBG.downloadImageFrom(urlString: workout?.woImgURL ?? "", with: shadowImage)
            collection.configure(musclesGroup: (workout?.muscleGroups?.components(separatedBy: ","))!, showDetailCell: true)

        } else if dayWorkout != nil { // WoOfTheDay
            btnLike.isSelected = (dayWorkout?.isLike == "1") ? true : false
            lblWorkoutGoal.text = dayWorkout?.workoutGoal
            imageBG.downloadImageFrom(urlString: dayWorkout?.woImgURL ?? "", with: shadowImage)
            collection.configure(musclesGroup: (dayWorkout?.muscleGroups?.components(separatedBy: ","))!, showDetailCell: true)
        } else { // TrainingPlanWeekDetail
//            btnLike.isSelected = (trainingPlan?.isLike == "1") ? true : false
            lblWorkoutGoal.text = trainingPlan?.workoutGoal
            lblWorkoutCategory.text = dayWorkout?.workoutGoal ?? "Test Category"
            imageBG.downloadImageFrom(urlString: trainingPlan?.woImgURL ?? "", with: shadowImage)

            collection.configure(musclesGroup: (trainingPlan?.muscleGroups?.components(separatedBy: ","))!, showDetailCell: true)
        }
    }
    
    func updateLikeButton(_ isLike: Bool) {
        btnLike.isSelected = (isLike) ? true : false
    }
    
    @objc func updateTime() {
        if countdown < 1000 {
            let seconds = countdown % 60
            let minutes = (countdown / 60) % 60
            let hours = countdown / 3600
            let strSecond = seconds > 9 ? String(seconds) : "0" + String(seconds)
            let strMinute = minutes > 9 ? String(minutes) : "0" + String(minutes)
            let strHours = hours > 9 ? String(hours) : "0" + String(hours)
            let buttonText = "\(strHours):\(strMinute):\(strSecond)"
            btnStartWorkout.setTitle(buttonText, for: .normal)
            countdown += 1
        } else {
            countdown = 1
            timer.invalidate()
            btnPlayPause.setImage(workoutStartIcon, for: .normal)
        }
    }
    
    private func startAction() {
        isStart = true
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        btnPlayPause.isHidden = false
    }
    
    private func playPauseAction() {
        if !isStart {
            isStart = true
            btnPlayPause.setImage(workoutPauseIcon, for: .normal)
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        } else {
            btnPlayPause.setImage(workoutStartIcon, for: .normal)
            timer.invalidate()
            isStart = false
        }
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnBack: delegateHeader?.sendAction(action: .back)
        case btnLike: delegateHeader?.sendAction(action: .like)
        case btnStartWorkout: startAction()
        case btnPlayPause: playPauseAction()
        default: break
        }
    }
}
