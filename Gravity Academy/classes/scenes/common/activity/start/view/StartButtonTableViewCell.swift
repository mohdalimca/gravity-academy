//
//  StartButtonTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class StartButtonTableViewCell: UITableViewCell, Reusable {

    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnPlayPause: UIButton! {
        didSet{
            btnPlayPause.isHidden = true
        }
    }
    
    weak var delegate: StartButtonTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure<T>(with content: T) {
        
    }
    
    private func setup() {
        [btnStart, btnPlayPause].forEach {
            $0?.layer.cornerRadius = 6.0
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        if sender == btnStart {
            delegate?.didSelect(action: .start)
        } else {
            delegate?.didSelect(action: .playPause)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
