//
//  StartHeaderCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class StartHeaderCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var lblActivity: UILabel!
    @IBOutlet weak var lblTimes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //setup()
    }
    
    func configure<T>(with content: T) {
        guard let detail = content as? WoDetail else { return }
        lblActivity.text = detail.roundTitle
        lblTimes.text = detail.noOfSets! + " Times"
    }
    
    private func setup() {
        lblActivity.text = ""
        lblTimes.text = ""
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
