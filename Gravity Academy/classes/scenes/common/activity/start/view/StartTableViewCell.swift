//
//  StartTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class StartTableViewCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var lblTitile: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var imageActivity: UIImageView!
    @IBOutlet weak var imageLike: UIImageView! {
        didSet {
            imageLike.isHidden = true
        }
    }
    let workoutImage = UIImage(named: "appLogo")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        selectionStyle = .none
        imageActivity.layer.cornerRadius = 10
        imageActivity.layer.masksToBounds = true
    }
    
    func configure<T>(with content: T) {
        guard let exercise = content as? Exercise else { return }
        lblTitile.text = exercise.title
        lblSubtitle.text = exercise.exerciseDescription
        if exercise.resp != "" && exercise.resp != "0" {
            lblSubtitle.text = "\(exercise.resp!) reps ● rest \(exercise.rest!) seconds"
        } else {
            lblSubtitle.text = "\(exercise.hold!) reps each ● rest \(exercise.rest!) seconds"
        }
        imageActivity.downloadImageFrom(urlString: exercise.mediaData?.image ?? "", with: workoutImage!)
        self.imageLike.isHidden = (exercise.isLike == "0") ? true : false
//        DispatchQueue.main.async {
//
//        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
