//
//  startActivityAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


enum StartActivityHeaderButton: String {
    case back
    case like
    case start
    case playPause
}

protocol StartActivityHeaderViewDelegate:class {
    func sendAction(action:StartActivityHeaderButton)
}
