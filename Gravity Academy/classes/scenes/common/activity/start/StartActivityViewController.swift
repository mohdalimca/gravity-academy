//
//  StartActivityViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class StartActivityViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var tableView: StartActivityTableView!
    @IBOutlet weak var header: StartActivityHeaderView!
    @IBOutlet weak var btnFinish: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var imageHeader: UIImageView!
    
    
    weak var router:NextSceneDismisserPresenter?
    weak var delegateTable: StartActivityTableViewDelegate?
    let viewModel = StartActivityViewModel(provider: WorkoutsServiceProvider())
    var handleLike: ((_ isLike:Bool, _ indexPath:IndexPath) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup() {
        self.startAnimation()
        self.setViewModel()
        self.setTableHeaderView()
        self.setButtons()
    }
    
    private func setViewModel() {
        viewModel.view = self
        if viewModel.workout != nil { // Workout
            viewModel.workoutId = viewModel.workout?.workoutID!
        } else if viewModel.workoutOfTheDay != nil { // Workout of Day
            viewModel.workoutId = viewModel.workoutOfTheDay?.workoutID!
        } else { // Traing Plan or Program
            viewModel.workoutId = viewModel.trainingPlan?.workoutID!
        }
        viewModel.workoutDetail(WorkoutsParams.WorkoutsDetail(workout_id: viewModel.workoutId, fn_level_id: viewModel.fitnessLevel))
        self.handleLike = handleLikeAction
    }
    
    func fitnessLevelTitle(_ level:String) -> String {
        switch level {
        case "1":
            return Fitness.beginner.rawValue
        case "2":
            return Fitness.intermediate.rawValue
        case "3":
            return Fitness.advanced.rawValue
        default:
            return ""
        }
    }
    
    private func setTableHeaderView() {
        tableView.configure(viewModel)
        tableView.delegateTable = delegateTable
        tableView.didUpdateViewAlpha = didUpdateViewAlpha
        header.delegateHeader = self
        header.populateDetails(viewModel.workout, viewModel.workoutOfTheDay, viewModel.trainingPlan, viewModel.fitnessLevel, viewModel.workoutCategory)
        viewHeader.alpha = 0
        imageHeader.alpha = 0
    }
    
    private func setButtons() {
        [btnBack, btnFinish].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(likeDislikeUpdate(_:)), name: .handleExercisLikeDislike, object: nil)
    }
    
    @objc func likeDislikeUpdate(_ notification: Notification) {
        //        if let isLike = notification.object as? Bool {
        //            handleLike(isLike)
    }
    
    private func handleLikeAction(_ isLike:Bool, _ indexPath:IndexPath) {
        viewModel.totalExercise = 0
        viewModel.allEexercise.removeAll()
        viewModel.workoutDetail(WorkoutsParams.WorkoutsDetail(workout_id: viewModel.workoutId, fn_level_id: viewModel.fitnessLevel))
    }
    
    
    private func didUpdateViewAlpha(_ alpha: CGFloat) {
        DispatchQueue.main.async {
            self.viewHeader.alpha = alpha
            self.imageHeader.alpha = alpha
        }
    }
    
    
    @objc func buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnBack:
            router?.dismiss(controller: .startActivity)
        case btnFinish:
            router?.push(scene: .completeWorkout)
        default:
            break
        }
    }
    
    private func reloadAfterLikeDislikeAction() {
        NotificationCenter.default.post(name: .handleWorkoutLikeDislike, object: viewModel.isLike)
        NotificationCenter.default.post(name: .reloadWorkoutLibrary, object: viewModel.isLike)
        header.updateLikeButton(viewModel.isLike)
    }
    
    private func reloadTable() {
        for temp in viewModel.workoutDetail {
            if let items = temp.exercise {
                viewModel.totalExercise += items.count
                viewModel.allEexercise.append(contentsOf: items)
            }
        }
        stopAnimating()
        tableView.reloadData()
    }
}

extension StartActivityViewController: StartActivityHeaderViewDelegate {
    func sendAction(action: StartActivityHeaderButton) {
        switch action {
        case .back:
            router?.dismiss(controller: .startActivity)
        case .like:
            viewModel.likeAPI(param: LikeDislikeParam(type: .workout, type_id: viewModel.workoutId))
        default:
            break
            
        }
    }
}

extension StartActivityViewController: WorkoutsViewRepresentable {
    func onAction(_ action: WorkoutsAction) {
        switch action {
        case let .errorMessage(text):
            showBannerWith(text: text, style: .danger)
            stopAnimating()
        case .detail:
            reloadTable()
        case .likeDislike:
            reloadAfterLikeDislikeAction()
        default:
            stopAnimating()
        }
    }
}
