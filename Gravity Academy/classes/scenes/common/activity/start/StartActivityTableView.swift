//
//  StartActivityTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class StartActivityTableView: UITableView {
    
    var viewModel: StartActivityViewModel!
    weak var delegateTable: StartActivityTableViewDelegate?
    var didUpdateViewAlpha: ((_ alpha: CGFloat) -> Void)?
    var headerViewHeight: CGFloat = 44
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure(_ viewModel: StartActivityViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
        registerReusable(StartHeaderCell.self)
    }
}

extension StartActivityTableView: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.workoutDetail.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (viewModel.workoutDetail[section]).exercise?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        let cell = tableView.dequeueReusable(indexPath) as StartTableViewCell
        cell.configure(with: (viewModel.workoutDetail[indexPath.section]).exercise![indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectedIndexpath = indexPath
        delegateTable?.didSelectAt(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StartHeaderCell") as? StartHeaderCell
        cell?.configure(with: viewModel.workoutDetail[section])
        return cell?.contentView ?? UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self {
            if(scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y <= headerViewHeight) {
                let percent = (scrollView.contentOffset.y / headerViewHeight)
                didUpdateViewAlpha?(percent)
            } else if (scrollView.contentOffset.y > headerViewHeight){
                didUpdateViewAlpha?(1.0)
            } else if (scrollView.contentOffset.y < 0) {
                didUpdateViewAlpha?(0.0)
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (scrollView.contentOffset.y > headerViewHeight){
            didUpdateViewAlpha?(1.0)
        } else if (scrollView.contentOffset.y < 0) {
            didUpdateViewAlpha?(0.0)
        }
    }
}
