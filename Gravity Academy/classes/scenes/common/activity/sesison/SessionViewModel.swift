//
//  SessionViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 15/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class SessionViewModel {
    
    var id:String!
    var weekDetails = [TrainingPlanWeekDetail]()
    weak var view: ProgramsViewRepresentable?
    var provider: ProgramsServiceProviderable
    var selectedIndexPath: IndexPath!
    var likeStatus: Bool = false
    
    init(provider:ProgramsServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    var count:Int {
        return weekDetails.count
    }
    
    func list() {
        provider.programWeekData(param: ProgramsParams.WeekData(tp_week_id: id))
    }
    
    func likeDislike() {
        provider.likeDislike(param: LikeDislikeParam(type: .trainingPlan, type_id: id))
    }
}

extension SessionViewModel: ProgramsServiceProviderDelegate {
    func completed<T>(for action: ProgramsAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .likeDislike:
                        self.likeStatus = (resp.data?.status == 1) ? true : false
                    case .weekData:
                        self.weekDetails = resp.data?.trainingPlanWeekDetail ?? self.weekDetails
                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}

