//
//  SessionViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 17/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class SessionViewController: UIViewController {
    
    @IBOutlet weak var tableView: SessionTableView!
    
    weak var delegateTable: SessionTableViewDelegate?
    weak var router: NextSceneDismisserPresenter?
    let viewModel = SessionViewModel(provider: ProgramsServiceProvider())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        viewModel.view = self
        startAnimation()
        viewModel.list()
        tableView.delegateTable = delegateTable
        tableView.configure(viewModel)
    }
    
    @IBAction func btnBackAction(_ swnder: UIButton) {
        router?.dismiss(controller: .session)
    }
    
    private func reload() {
        tableView.reloadData()
        stopAnimating()
    }
}

extension SessionViewController:RestAlertDelegate {
    func didSelectButton() {
        print("api call")
        router?.push(scene: .startActivity)
    }
}

extension SessionViewController: ProgramsViewRepresentable {
    func onAction(_ action: ProgramsAction) {
        switch action {
        case let .errorMessage(msg):
            stopAnimating()
            showBannerWith(text: msg, style: .danger)
        case .weekData:
            reload()
        default:
            stopAnimating()
        }
    }
}
