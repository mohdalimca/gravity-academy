//
//  SessionTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class SessionTableViewCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var lblCounter: UILabel!
    @IBOutlet weak var lblWorkout: UILabel!
    @IBOutlet weak var lblMuscle: UILabel!
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configure<T>(with content: T) {
//        guard let details = content as? TrainingPlanWeekDetail else { return }
        if let details = content as? TrainingPlanWeekDetail {
            lblWorkout.text = "Workout"
            lblMuscle.text = details.title
            containerView.addShadowToView()
            return
        }
        
        if let details = content as? SkillWeekDetail {
            lblWorkout.text = "Workout"
            lblMuscle.text = details.title
            containerView.addShadowToView()
            return
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
