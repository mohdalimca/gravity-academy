//
//  SessionCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 20/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class SessionCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: SessionViewController = SessionViewController.from(from: .common, with: .session)
    var startActivity: StartActivityCoordinator!
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    func start(id:String) {
        controller.viewModel.id = id
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.delegateTable = self
        controller.router = self
    }
    
    private func startActivities(_ index:Int) {
        startActivity = StartActivityCoordinator(router: Router())
        add(startActivity)
        startActivity.delegate = self
        startActivity.start(trainingPlan: controller.viewModel.weekDetails[index])
        router.present(startActivity, animated: true)
    }
}

extension SessionCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension SessionCoordinator: ActivityDetailTableViewDelegate {
    func didSelectAt(index: Int) {
        router.present(controller, animated: true)
    }
}

extension SessionCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
extension SessionCoordinator: SessionTableViewDelegate {
    func didSelectAt(row: Int) {
        print("index is \(row)")
        if row == 0 {
            controller.restAlert(controller: controller)
        } else {
            startActivities(row)
        }
    }
}
