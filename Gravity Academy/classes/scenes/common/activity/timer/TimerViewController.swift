//
//  TimerViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 24/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController {
    
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblReset: UILabel!
    @IBOutlet weak var lblStart: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnPlayPause: UIButton!
    
    var timer: Timer!
    var countdown: Int = 1
    var isStart: Bool = false
    weak var router:NextSceneDismisserPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if timer != nil {
            timer.invalidate()
            countdown = 1
            lblTimer.text = "00:00:00"
        }
    }
    
    private func setup() {
        [btnPlayPause, btnReset, btnCancel].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    private func resetAction() {
        timer.invalidate()
        lblTimer.text = "00:00:00"
        btnPlayPause.setImage(startTimerIcon, for: .normal)
        countdown = 1
        isStart = false
    }
    
    private func startAction() {
        if !isStart {
            isStart = true
            btnPlayPause.setImage(pauseTimerIcon, for: .normal)
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        } else {
            btnPlayPause.setImage(startTimerIcon, for: .normal)
            timer.invalidate()
            isStart = false
        }
    }
    
    @objc func updateTime() {
        if countdown < 1000 {
            let seconds = countdown % 60
            let minutes = (countdown / 60) % 60
            let hours = countdown / 3600
            let strSecond = seconds > 9 ? String(seconds) : "0" + String(seconds)
            let strMinute = minutes > 9 ? String(minutes) : "0" + String(minutes)
            let strHours = hours > 9 ? String(hours) : "0" + String(hours)
            lblTimer.text = "\(strHours):\(strMinute):\(strSecond)"
            countdown += 1
        } else {
            countdown = 1
            timer.invalidate()
            btnPlayPause.setImage(startTimerIcon, for: .normal)
        }
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnCancel: router?.dismiss(controller: .timer)
        case btnReset: resetAction()
        case btnPlayPause: startAction()
        default: break
        }
    }
}
