//
//  LoginAdaptable.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum LandingButtonAction: String {
    case create
    case signin
}

protocol LandingMainViewDelegate: class {
    func didSelect(action: LandingButtonAction)
}

protocol LandingViewControllerDelegate: class {
    func onAction(action: Scenes)
}
