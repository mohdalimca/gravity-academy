//
//  LandingCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import UIKit

final class LandingCoordinator: Coordinator<Scenes> {
    
    let landing: LandingVC = LandingVC.from(storyboard: .landing)
    let login: LoginViewController = LoginViewController.from(from: .login, with: .login)
    let register: RegistrationViewController = RegistrationViewController.from(from: .register, with: .register)
    
    override func start() {
        super.start()
        router.setRootModule(landing, hideBar: true)
        onStart()
    }
    
    private func onStart() {
        landing.delegate = self
    }
    
    private func loginModule() {
        let router = Router()
        let login = LoginCoordinator(router: router)
        add(login)
        login.start()
        self.router.present(login, animated: true)
    }
    
    private func registerModule() {
        let router = Router()
        let register = RegistrationCoordinator(router: router)
        add(register)
        register.start()
        self.router.present(register, animated: true)
    }
    
    private func show(scene: Scenes) {
        switch scene {
        case .login: loginModule(); break
        case .register: registerModule(); break
        default: break
        }        
    }
}


extension LandingCoordinator: LandingViewControllerDelegate{
    func onAction(action: Scenes) {
        show(scene: action)
    }
}
