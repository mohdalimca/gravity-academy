//
//  LandingMainView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class LandingMainView: UIView {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCreate: UIButton!
    @IBOutlet weak var btnSignIn: UIButton!
    
    weak var delegate: LandingMainViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setLabelAndButton()
    }
    
    private func setLabelAndButton() {
        [ btnSignIn, btnCreate ].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
        }
    }
    
    @objc func buttonPressed( sender: UIButton ) {
        if sender == btnCreate {
            delegate?.didSelect(action: .create)
        } else {
            delegate?.didSelect(action: .signin)
        }
    }
}
