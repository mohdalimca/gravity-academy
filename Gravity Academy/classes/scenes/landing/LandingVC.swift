//
//  LandingVC.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class LandingVC: UIViewController {

    @IBOutlet weak var landingMainView: LandingMainView!
    @IBOutlet weak var viewVideo: UIView!
    
    var router: NextSceneDismisserPresenter?
    weak var delegate: LandingViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setupVideoView() {
        
    }
    
    private func setup() {
        landingMainView.delegate = self
        setupVideoView()
    }
}

extension LandingVC: LandingMainViewDelegate {
    func didSelect(action: LandingButtonAction) {
        if action == .create {
            delegate?.onAction(action: .register)
        } else {
            delegate?.onAction(action: .login)
        }
    }
}
