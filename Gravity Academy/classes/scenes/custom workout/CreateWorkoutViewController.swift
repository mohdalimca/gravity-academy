
//
//  CreateWorkoutViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class CreateWorkoutViewController: UIViewController {
    
    //@IBOutlet weak var tableView: CreateWorkoutTableView!
    @IBOutlet weak var header: CreateWorkoutHeaderView!
    
    weak var router: NextSceneDismisserPresenter?
    let viewModel = SettingsViewModel(provider: UserProfileServiceProvider())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
//        tableView.configure()
        header.delegateHeader = self
    }
}

extension CreateWorkoutViewController: CreateWorkoutHeaderDelegate {
    func sendAction(action: CreateWorkoutHeaderButtonAction) {
        switch action {
        case .back: router?.dismiss(controller: .createWorkout)
        case .showAll: router?.push(scene: .workoutLibrary)
        case .saveName: router?.push(scene: .createWorkout)
        case .saveCategory: router?.push(scene: .settings)
        case .muscles: router?.push(scene: .settings)
        case .cardio: router?.push(scene: .createWorkout)
        case .strength: router?.push(scene: .settings)
        case .technique: router?.push(scene: .settings)
        }
    }
}

