//
//  CreateWorkoutHeaderView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class CreateWorkoutHeaderView: UIView {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblMembership: UILabel!
    @IBOutlet weak var lblFitnessLevel: UILabel!
    @IBOutlet weak var lblWorkout: UILabel!
    @IBOutlet weak var lblWorkoutCount: UILabel!
    @IBOutlet weak var lblWorkoutName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblRounds: UILabel!
    @IBOutlet weak var lblSets: UILabel!
    @IBOutlet weak var btnBack: GravityAcademyButton!
    @IBOutlet weak var btnShowAll: GravityAcademyButton!
    @IBOutlet weak var btnSaveName: GravityAcademyButton!
    @IBOutlet weak var btnSaveCategory: GravityAcademyButton!
    @IBOutlet weak var btnMuscles: GravityAcademyButton!
    @IBOutlet weak var btnCardio: GravityAcademyButton!
    @IBOutlet weak var btnStrength: GravityAcademyButton!
    @IBOutlet weak var btnTechnique: GravityAcademyButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtRounds: UITextField!
    @IBOutlet weak var txtSets: UITextField!
    @IBOutlet weak var imageProfile: UIImageView!
    
    weak var delegateHeader:CreateWorkoutHeaderDelegate?
    
    let shapeLayer = CAShapeLayer()
    let orange = UIColor.init(hex: "#FFE4DF")
    let gray = UIColor.init(hex: "#E6E6E6")

    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        [lblMembership, lblFitnessLevel].forEach {
            $0?.layer.cornerRadius = 4
            $0?.layer.masksToBounds = true
        }
        
        [btnBack, btnShowAll, btnSaveName, btnMuscles, btnCardio, btnStrength, btnTechnique, btnSaveCategory, btnSaveName].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        
    }
    
    private func updateButtons(_ sender: GravityAcademyButton) {
        switch sender {
        case btnMuscles:
            btnMuscles.backgroundColor = orange
            btnCardio.backgroundColor = gray
            btnStrength.backgroundColor = gray
            btnTechnique.backgroundColor = gray
        case btnCardio:
            btnMuscles.backgroundColor = gray
            btnCardio.backgroundColor = orange
            btnStrength.backgroundColor = gray
            btnTechnique.backgroundColor = gray
        case btnStrength:
            btnMuscles.backgroundColor = gray
            btnCardio.backgroundColor = gray
            btnStrength.backgroundColor = orange
            btnTechnique.backgroundColor = gray
        case btnTechnique:
            btnMuscles.backgroundColor = gray
            btnCardio.backgroundColor = gray
            btnStrength.backgroundColor = gray
            btnTechnique.backgroundColor = orange
        default: break
        }
    }
    
    @objc func buttonPressed(_ sender: GravityAcademyButton) {
        updateButtons(sender)
        switch sender {
        case btnBack: delegateHeader?.sendAction(action: .back)
        case btnShowAll: delegateHeader?.sendAction(action: .showAll)
        case btnSaveName: delegateHeader?.sendAction(action: .saveName)
        case btnSaveCategory: delegateHeader?.sendAction(action: .saveCategory)
        case btnMuscles: delegateHeader?.sendAction(action: .muscles)
        case btnCardio: delegateHeader?.sendAction(action: .cardio)
        case btnStrength: delegateHeader?.sendAction(action: .strength)
        case btnTechnique: delegateHeader?.sendAction(action: .technique)
        default: break
        }
    }
}
