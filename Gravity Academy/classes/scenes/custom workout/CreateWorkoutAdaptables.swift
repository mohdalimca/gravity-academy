//
//  CreateWorkoutAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


enum CreateWorkoutHeaderButtonAction: String {
    case back
    case showAll
    case saveName
    case muscles
    case cardio
    case strength
    case technique
    case saveCategory
}

protocol CreateWorkoutHeaderDelegate:class {
    func sendAction(action: CreateWorkoutHeaderButtonAction)
}
