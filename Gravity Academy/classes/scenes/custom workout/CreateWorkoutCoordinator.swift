//
//  CreateWorkoutCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 27/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class CreateWorkoutCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let create: CreateWorkoutViewController = CreateWorkoutViewController.from(from: .workouts, with: .createWorkout)
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(create, hideBar: true)
    }
    
    private func onStart() {
        create.router = self
    }
    
    private func startExercise() {
        let r = Router()
        let exercise = ExerciseCoordinator(router: r)
        add(exercise)
        exercise.delegate = self
        exercise.start()
        router.present(exercise, animated: true)
        
    }
    
    private func startWorkoutLibrary() {
        let r = Router()
        let library = WorkoutLibraryCoordinator(router: r)
        add(library)
        library.delegate = self
        library.start()
        router.present(library, animated: true)
    }
}

extension CreateWorkoutCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .workoutLibrary: startWorkoutLibrary()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension CreateWorkoutCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
