//
//  ProgramAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 17/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum activityDetailAction {
    case back
    case like
    case more
}

enum startActivityButtonAction: String {
    case start
    case playPause
}

protocol ActivityDetailHeaderViewDelegate: class {
    func didSelect(action: activityDetailAction)
}

protocol ActivityDetailTableViewDelegate: class {
    func didSelectAt(index: Int)
}

protocol SessionTableViewDelegate:class {
    func didSelectAt(row: Int)
}

protocol StartActivityTableViewDelegate:class {
    func didSelectAt(indexPath: IndexPath)
}

protocol StartButtonTableViewCellDelegate:class {
    func didSelect(action:startActivityButtonAction)
}

