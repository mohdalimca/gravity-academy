//
//  ProgramsListVC.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ProgramsListVC: UIViewController {
    
    @IBOutlet weak var tableView: ProgramsTableView!
    weak var router: NextSceneDismisserPresenter?
//    let viewModel = TechniquesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        tableView.configure()
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        router?.dismiss(controller: .techniquesList)
    }
}
