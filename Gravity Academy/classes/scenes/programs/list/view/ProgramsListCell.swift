//
//  ProgramsListCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ProgramsListCell: UITableViewCell, Reusable {

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configure<T>(with content: T) {
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
