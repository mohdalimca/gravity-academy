//
//  ProgramCorrdinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 16/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class ProgramCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?

    let activityDetail: ActivityDetailViewController = ActivityDetailViewController.from(from: .common, with: .activityDetail)

    override func start() {
        super.start()
        onStart()
        router.setRootModule(activityDetail, hideBar: true)
    }
    
    private func onStart() {
        activityDetail.delegate = self
        activityDetail.router = self
        activityDetail.delegateTable = self
    }
    
    private func session() {
        let r = Router()
        let session = SessionCoordinator(router: r)
        add(session)
        session.delegate = self
        session.start()
        router.present(session, animated: true)
    }
}

extension ProgramCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {}
    
    func dismiss(controller: Scenes) {
        router.dismissModule(animated: true, completion: nil)
    }
}

extension ProgramCoordinator: ActivityDetailTableViewDelegate {
    func didSelectAt(index: Int) {
        session()
    }
}

extension ProgramCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

