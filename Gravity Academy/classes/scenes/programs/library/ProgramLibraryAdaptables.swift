//
//  ProgramLibraryAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 26/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum ProgramsAction {
    case errorMessage(_ text:String)
    case library
    case detail
    case likeDislike
    case master
    case weekList
    case weekData
    case nointernet
}

protocol ProgramLibraryTableViewDelegate: class {
    func didSelect(at: Int)
}

protocol ProgramsViewRepresentable:class {
    func onAction(_ action: ProgramsAction)
}

protocol ProgramsServiceProviderDelegate:class {
    func completed<T>(for action:ProgramsAction, with response:T?, error:APIError?)
}

protocol ProgramsServiceProviderable: class {
    var delegate: ProgramsServiceProviderDelegate? { get set }
    func list(param:ProgramsParams.FitnessLevel)
    func likeDislike(param:LikeDislikeParam)
    func weekList(param: ProgramsParams.Weeks)
    func programWeekData(param: ProgramsParams.WeekData)
}
