//
//  ProgramLibraryViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 26/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class ProgramLibraryViewModel {
    
    var id:String!
    var list = [TrainingPlan]()
    var newList = [TrainingPlan]()
    var beginnerList = [TrainingPlan]()
    var intermediateList = [TrainingPlan]()
    var advancedList = [TrainingPlan]()
    weak var view: ProgramsViewRepresentable?
    var provider: ProgramsServiceProviderable
    var sections = [String]()
    var selectedIndexPath: IndexPath!
    var likeStatus: Bool = false
    var selectedPlan: TrainingPlan!
    
    init(provider:ProgramsServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    var count:Int {
        return 3
    }
    
    func list(level: String) {
        provider.list(param: ProgramsParams.FitnessLevel(fn_level_id: level))
    }
    
    func likeDislike() {
        provider.likeDislike(param: LikeDislikeParam(type: .trainingPlan, type_id: id))
    }
}


extension ProgramLibraryViewModel: ProgramsServiceProviderDelegate {
    func completed<T>(for action: ProgramsAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .library:
                        self.list = resp.data?.trainingPlanList ?? self.list
                    case .likeDislike:
                        self.likeStatus = (resp.data?.status == 1) ? true : false
                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
