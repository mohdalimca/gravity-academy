//
//  ProgramsTableCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ProgramsTableCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var programCollection: ProgramsCollectionView!
    let viewModel = HomeViewModel(provider: WorkoutsServiceProvider())
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setup() {
        selectionStyle = .none
        programCollection.configure(viewModel: viewModel)
    }
}
