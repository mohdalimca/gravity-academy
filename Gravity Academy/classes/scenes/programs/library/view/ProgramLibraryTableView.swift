//
//  ProgramLibraryTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 26/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ProgramLibraryTableView: UITableView {
    
    var viewModel: ProgramLibraryViewModel!
    weak var delegateTable: ProgramLibraryTableViewDelegate?
    var didLikeId: ((_ id:String, _ indexPath:IndexPath) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ viewModel: ProgramLibraryViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
        registerReusable(ListHeaderCell.self)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        switch sender.name {
        case "0": delegateTable?.didSelect(at: 0)
        case "1": delegateTable?.didSelect(at: 1)
        case "2": delegateTable?.didSelect(at: 2)
        case "3": delegateTable?.didSelect(at: 3)
        default : delegateTable?.didSelect(at: -1)
        }
    }
    
    private func tableHeader(at section:Int, with tableView:UITableView) -> UIView {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeHeaderCell") as? HomeHeaderCell
//        cell?.configure(with: viewModel.item(at: section))
//        let tap = UITapGestureRecognizer(target: self, action:#selector(self.handleTap(_:)))
//        tap.name = "\(section)"
//        tap.numberOfTapsRequired = 1
//        cell!.btnViewAll.addGestureRecognizer(tap)
        return cell?.contentView ?? UIView()
    }
    
    private func athleteTableHeader(at section:Int, with tableView:UITableView) -> UIView {
        
        let cell = tableView.dequeueReusable(IndexPath(row: 0, section: 0)) as AthleteTableViewCell
//        cell.configure(with: viewModel.item(at: section))
//        let tap = UITapGestureRecognizer(target: self, action:#selector(self.handleTap(_:)))
//        tap.name = "-1"
//        tap.numberOfTapsRequired = 1
//        cell.btnViewAll.addGestureRecognizer(tap)
        return cell.contentView
    }
}

extension ProgramLibraryTableView: UITableViewDelegate, UITableViewDataSource  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (viewModel.list.filter{ $0.fitnessLevel == viewModel.sections[section]}).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusable(indexPath) as TechniquesListCell
        cell.didLikeId = didLikeId
        let plans = viewModel.list.filter{ $0.fitnessLevel == viewModel.sections[indexPath.section]}
        cell.indexPath = indexPath
        cell.configure(with: plans[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListHeaderCell") as? ListHeaderCell
        cell?.lblTitle.text = viewModel.sections[section]
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let plans = viewModel.list.filter{ $0.fitnessLevel == (viewModel.sections[indexPath.section])}
        viewModel.selectedPlan = plans[indexPath.row]
        delegateTable?.didSelect(at: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}

extension ProgramLibraryTableView: HomeHeaderCellDelegate {
    func didSelectViewAll(at index: Int) {
        delegateTable?.didSelect(at: index)
    }
}

extension ProgramLibraryTableView: WorkoutCollectionViewDelegate {
    func didSelect(at index: Int, section: WorkoutsSection) {
        delegateTable?.didSelect(at: index)
    }
}

extension ProgramLibraryTableView: TechniquesListCellDelegate {
    func likeAction() {

    }
}
