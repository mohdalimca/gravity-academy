//
//  ProgramLibraryViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 26/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class ProgramLibraryViewController: UIViewController {
    
    @IBOutlet weak var tableView: ProgramLibraryTableView!
    @IBOutlet weak var btnBack: GravityAcademyButton!
    @IBOutlet weak var btnIntro: GravityAcademyButton!
    
    weak var delegate:ControllerDismisser?
    weak var router: NextSceneDismisserPresenter?
    let viewModel: ProgramLibraryViewModel = ProgramLibraryViewModel(provider: ProgramsServiceProvider())
    
    weak var delegateTable: ProgramLibraryTableViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    
    private func setup() {
        startAnimation()
        viewModel.view = self
        tableView.delegateTable = delegateTable
        tableView.configure(viewModel)
        tableView.didLikeId = didLikeId
        viewModel.list(level: "")
        [btnBack, btnIntro].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTrainingProgramList), name: .handleTrainingLikeDislike, object: nil)
    }
    
    @objc func reloadTrainingProgramList(notification:Notification) {
        viewModel.list(level: "")
    }

    @objc func buttonPressed(_ sender: GravityAcademyButton) {
        if sender == btnBack {
            router?.dismiss(controller: .programLibrary)
        } else {
            print("pay video")
        }
    }
    
    private func didLikeId(_ id:String, _ indexPath:IndexPath) {
        viewModel.id = id
        viewModel.selectedIndexPath = indexPath
        viewModel.likeDislike()
    }
    
    private func reload() {
        var main = [TrainingPlan]()
        var filter = viewModel.list.filter { $0.fnLevel == "1"}
        main.append(contentsOf: filter)
        filter = viewModel.list.filter { $0.fnLevel == "2"}
        main.append(contentsOf: filter)
        filter = viewModel.list.filter { $0.fnLevel == "3"}
        main.append(contentsOf: filter)
        filter = viewModel.list.filter { $0.fnLevel == "4"}
        main.append(contentsOf: filter)
        
        for temp in main {
            if !viewModel.sections.contains(temp.fitnessLevel!) {
                viewModel.sections.append(temp.fitnessLevel!)
            }
        }
        viewModel.list.removeAll()
        viewModel.list = main
        tableView.reloadData()
        stopAnimating()
        
//        viewModel.newList = viewModel.list.filter { $0.fnLevel == "0"}
//        viewModel.beginnerList = viewModel.list.filter { $0.fnLevel == "1"}
//        viewModel.intermediateList = viewModel.list.filter { $0.fnLevel == "2"}
//        viewModel.advancedList = viewModel.list.filter { $0.fnLevel == "3"}
    }
    
    private func reloadAfterLikeDislikeAction() {
        for i in 0..<viewModel.list.count {
            var program = viewModel.list[i]
            if program.tpID == viewModel.id {
                program.isLike = (viewModel.likeStatus) ? "1" : "0"
                viewModel.list[i] = program
                tableView.reloadSections(IndexSet(integer: viewModel.selectedIndexPath.section), with: .none)
                NotificationCenter.default.post(name: .handleTrainingLikeDislike, object: nil)
                return
            }
        }
    }
}

extension ProgramLibraryViewController: ProgramsViewRepresentable {
    func onAction(_ action: ProgramsAction) {
        switch action {
        case let .errorMessage(msg):
            stopAnimating()
            showBannerWith(text: msg, style: .danger)
        case .library:
            reload()
        case .likeDislike:
            reloadAfterLikeDislikeAction()
        default:
            stopAnimating()
        }
    }
}

