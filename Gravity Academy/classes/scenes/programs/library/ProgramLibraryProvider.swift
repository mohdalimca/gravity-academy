//
//  ProgramLibraryProvider.swift
//  Gravity Academy
//
//  Created by Apple on 24/06/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class ProgramsServiceProvider: ProgramsServiceProviderable {
    
    let task = ProgramsTask()
    var delegate: ProgramsServiceProviderDelegate?
    
    func list(param: ProgramsParams.FitnessLevel) {
        task.list(modeling: SuccessResponseModel.self) { [weak self](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .library, with: resp, error: err)
                return
            }
            self?.delegate?.completed(for: .library, with: resp, error: nil)
        }
    }
    
    func likeDislike(param:LikeDislikeParam) {
        task.likeDislike(param: param) { [weak self](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .likeDislike, with: resp, error: err)
                return
            }
            self?.delegate?.completed(for: .likeDislike, with: resp, error: nil)
        }
    }
    
    func weekList(param: ProgramsParams.Weeks) {
        task.weekList(param: param) { [weak self](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .weekList, with: resp, error: err)
                return
            }
            self?.delegate?.completed(for: .weekList, with: resp, error: nil)
        }
    }
    
    func programWeekData(param: ProgramsParams.WeekData) {
        task.programWeekData(param: param) { [weak self](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .weekData, with: resp, error: err)
                return
            }
            self?.delegate?.completed(for: .weekData, with: resp, error: nil)
        }
    }
}
