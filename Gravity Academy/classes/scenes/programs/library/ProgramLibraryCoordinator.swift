//
//  ProgramLibraryCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 26/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class ProgramLibraryCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: ProgramLibraryViewController = ProgramLibraryViewController.from(from: .programs, with: .programLibrary)
    var detail:ProgramDetailCoordinator!
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.router = self
        controller.delegateTable = self
    }
    
    private func startDetail() {
        let r = Router()
        detail = ProgramDetailCoordinator(router: r)
        add(detail)
        detail.delegate = self
        detail.start(trainingPlan:controller.viewModel.selectedPlan)
        router.present(detail, animated: true)
    }
    
    private func startActivityDetail() {
        let r = Router()
        let program = ProgramCoordinator(router: r)
        add(program)
        program.delegate = self
        program.start()
        router.present(program, animated: true)
    }
}

extension ProgramLibraryCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension ProgramLibraryCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

extension ProgramLibraryCoordinator: ProgramLibraryTableViewDelegate {
    func didSelect(at: Int) {
        startDetail()
    }
}
