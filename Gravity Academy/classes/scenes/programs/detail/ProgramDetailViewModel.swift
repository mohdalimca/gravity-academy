//
//  ProgramDetailViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 30/06/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class ProgramDetailViewModel {
    
    var detailId:String!
    var selectedProgram: TrainingPlan!
    var weeksList = [TrainingPlanWeek]()
    var intermediateList = [TrainingPlan]()
    var advancedList = [TrainingPlan]()
    weak var view: ProgramsViewRepresentable?
    var provider: ProgramsServiceProviderable
    var sections = [String]()
    var selectedIndexPath: IndexPath!
    var likeStatus: Bool = false
    
    init(provider:ProgramsServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    var count:Int {
        return weeksList.count
    }
    
    func list() {
        provider.weekList(param: ProgramsParams.Weeks(tp_id: selectedProgram.tpID))
    }
    
    func likeDislike() {
        provider.likeDislike(param: LikeDislikeParam(type: .trainingPlan, type_id: selectedProgram.tpID))
    }
}

extension ProgramDetailViewModel: ProgramsServiceProviderDelegate {
    func completed<T>(for action: ProgramsAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .library:
                        break
                    case .likeDislike:
                        self.likeStatus = (resp.data?.status == 1) ? true : false
                    case .weekList:
                        self.weeksList = resp.data?.trainingPlanWeeks ?? self.weeksList 
                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
