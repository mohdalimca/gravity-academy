//
//  ProgramDetailTableView.swift
//  Gravity Academy
//
//  Created by Apple on 30/06/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import UIKit

class ProgramDetailTableView: UITableView {
    
    var viewModel: StartActivityViewModel!
    weak var delegateTable: StartActivityTableViewDelegate?
    var didUpdateViewAlpha: ((_ alpha: CGFloat) -> Void)?
    var headerViewHeight: CGFloat = 44
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure(_ viewModel: StartActivityViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
        registerReusable(StartHeaderCell.self)
    }
}

extension ProgramDetailTableView: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath)
        return cell

//        let cell = tableView.dequeueReusable(indexPath) as StartTableViewCell
//        cell.configure(with: (viewModel.workoutDetail[indexPath.section]).exercise![indexPath.row])
//        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        viewModel.selectedIndexpath = indexPath
//        delegateTable?.didSelectAt(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self {
            if(scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y <= headerViewHeight) {
                let percent = (scrollView.contentOffset.y / headerViewHeight)
                didUpdateViewAlpha?(percent)
            } else if (scrollView.contentOffset.y > headerViewHeight){
                didUpdateViewAlpha?(1.0)
            } else if (scrollView.contentOffset.y < 0) {
                didUpdateViewAlpha?(0.0)
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (scrollView.contentOffset.y > headerViewHeight){
            didUpdateViewAlpha?(1.0)
        } else if (scrollView.contentOffset.y < 0) {
            didUpdateViewAlpha?(0.0)
        }
    }
}
