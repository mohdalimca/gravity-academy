//
//  ProgramDetailCoordinator.swift
//  Gravity Academy
//
//  Created by Apple on 30/06/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation
import UIKit

final class ProgramDetailCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: ProgramDetailViewController = ProgramDetailViewController.from(from: .common, with: .programDetail)
    var exercise: ExerciseCoordinator!
    var home: HomeCoordinator!
    var session: SessionCoordinator!
    
    override func start() {
        super.start()
    }
    
    func start(trainingPlan:TrainingPlan) {
        onStart()
        controller.viewModel.selectedProgram = trainingPlan
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.router = self
    }
        
    private func allExercise() {
        let r = Router()
        let all = AllExerciseCoordinator(router: r)
        add(all)
        all.delegate = self
        all.start()
        router.present(all, animated: true)
    }
    
    private func startSession() {
        session = SessionCoordinator(router: Router())
        add(session)
        session.delegate = self
        session.start(id: controller.viewModel.detailId)
        router.present(session, animated: true)
    }
}

extension ProgramDetailCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .session:
            startSession()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension ProgramDetailCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
