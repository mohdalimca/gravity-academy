
//
//  TechniqueWorkoutViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 17/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class TechniqueWorkoutViewModel {
    
    var id:String!
    var weekDetails = [SkillWeekDetail]()
    var selectedIndex: Int!
    
    weak var view: TechniquesViewRepresentable?
    var provider: TechniquesServiceProviderable
    
    init(provider:TechniquesServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    var count:Int {
        return weekDetails.count
    }
    
    func list() {
        provider.skillWeeekDetail(param: TechniquesParam.SkillWeekDetail(skill_week_id: id))
    }
}

extension TechniqueWorkoutViewModel: TechniquesServiceProviderDelegate {
    
    func completed<T>(for action: TechniquesAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .weekData:
                        self.weekDetails = resp.data?.skillWeekDetail ?? self.weekDetails
                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
