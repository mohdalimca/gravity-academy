//
//  TechniqueWorkoutTable.swift
//  Gravity Academy
//
//  Created by Apple on 17/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import UIKit

class TechniqueWorkoutTable: UITableView {
    
    var viewModel: TechniqueWorkoutViewModel!
    var didSelectAtIndex : ((_ index:Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure(_ viewModel: TechniqueWorkoutViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
    }
}

extension TechniqueWorkoutTable: UITableViewDelegate, UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        let cell = tableView.dequeueReusable(indexPath) as SessionTableViewCell
        cell.lblCounter.text = "\(indexPath.row + 1)"
        cell.configure(with: viewModel.weekDetails[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectAtIndex?(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}


