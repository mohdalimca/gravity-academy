//
//  TechniqueWorkoutVC.swift
//  Gravity Academy
//
//  Created by Apple on 17/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import UIKit

class TechniqueWorkoutVC: UIViewController {
    
    @IBOutlet weak var tableView: TechniqueWorkoutTable!
    
    weak var delegateTable: SessionTableViewDelegate?
    weak var router: NextSceneDismisserPresenter?
    let viewModel: TechniqueWorkoutViewModel = TechniqueWorkoutViewModel(provider: TechniquesServiceProvider())

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        viewModel.view = self
        startAnimation()
        viewModel.list()
        tableView.didSelectAtIndex = didSelectAtIndex
        tableView.configure(viewModel)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        router?.dismiss(controller: .session)
    }
    
    private func didSelectAtIndex(_ index:Int) {
        viewModel.selectedIndex = index
        router?.push(scene: .startActivity)
    }
    
    private func reload() {
        tableView.reloadData()
        stopAnimating()
    }
}

extension TechniqueWorkoutVC: TechniquesViewRepresentable {
    func onAction(_ action: TechniquesAction) {
        switch action {
        case let .errorMessage(msg):
            stopAnimating()
            showBannerWith(text: msg, style: .danger)
        case .weekData:
            reload()
        default:
            stopAnimating()
        }
    }
}
