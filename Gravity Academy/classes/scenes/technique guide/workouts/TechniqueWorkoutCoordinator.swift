//
//  TechniqueWorkoutCoordinator.swift
//  Gravity Academy
//
//  Created by Apple on 17/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class TechniqueWorkoutCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: TechniqueWorkoutVC = TechniqueWorkoutVC.from(from: .common, with: .techniqueWorkouts)
    var startActivity: StartActivityCoordinator!
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    func start(id:String) {
        controller.viewModel.id = id
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.router = self
    }
    
    private func startActivities() {
        startActivity = StartActivityCoordinator(router: Router())
        add(startActivity)
        startActivity.delegate = self
        startActivity.start(skillDetail: controller.viewModel.weekDetails[controller.viewModel.selectedIndex])
        router.present(startActivity, animated: true)
    }
}

extension TechniqueWorkoutCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) { }
    
    func push(scene: Scenes) {
        switch scene {
        case .startActivity:
            startActivities()
        default:
            break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension TechniqueWorkoutCoordinator: ActivityDetailTableViewDelegate {
    func didSelectAt(index: Int) {
        router.present(controller, animated: true)
    }
}

extension TechniqueWorkoutCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
