//
//  TechniquesListVC.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class TechniquesListVC: UIViewController {
    
    @IBOutlet weak var tableView: TechniquesTableView!
    @IBOutlet weak var btnBack: UIButton!
    weak var router: NextSceneDismisserPresenter?
    let viewModel = TechniquesViewModel(provider: TechniquesServiceProvider())
    weak var delegateTable: TechniquesTableViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        startAnimation()
        viewModel.view = self
        viewModel.skillList()
        tableView.configure(viewModel)
        tableView.delegateTable = delegateTable
        tableView.didLikeId = didLikeId
        btnBack.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTechniqueList), name: .handleSkillsLikeDislike, object: nil)
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        router?.dismiss(controller: .techniquesList)
    }
    
    @objc func reloadTechniqueList(notification:Notification) {
        viewModel.skillList()
    }
    
    private func didLikeId(_ id:String, _ indexPath:IndexPath) {
        viewModel.id = id
        viewModel.selectedIndexPath = indexPath
        viewModel.likeDislike()
    }
    
    private func reload() {
        var main = [SkillList]()
        var filter = viewModel.list.filter { $0.fnLevel == "1"}
        main.append(contentsOf: filter)
        filter = viewModel.list.filter { $0.fnLevel == "2"}
        main.append(contentsOf: filter)
        filter = viewModel.list.filter { $0.fnLevel == "3"}
        main.append(contentsOf: filter)
        filter = viewModel.list.filter { $0.fnLevel == "4"}
        main.append(contentsOf: filter)
        
        for temp in main {
            if !viewModel.sections.contains(temp.fitnessLevel!) {
                viewModel.sections.append(temp.fitnessLevel!)
            }
        }
        viewModel.list.removeAll()
        viewModel.list = main
        tableView.reloadData()
        stopAnimating()
    }
    
    private func reloadAfterLikeDislikeAction() {
        for i in 0..<viewModel.list.count {
            var skill = viewModel.list[i]
            if skill.skillID == viewModel.id {
                skill.isLike = (viewModel.likeStatus) ? "1" : "0"
                viewModel.list[i] = skill
                tableView.reloadSections(IndexSet(integer: viewModel.selectedIndexPath.section), with: .none)
                NotificationCenter.default.post(name: .handleSkillsLikeDislike, object: nil)

                return
            }
        }
    }
}

extension TechniquesListVC: TechniquesViewRepresentable {
    func onAction(_ action: TechniquesAction) {
        switch action {
        case let .errorMessage(text):
            showBannerWith(text: text, style: .warning)
            stopAnimating()
        case .list:
            reload()
        case .likeDislike:
            reloadAfterLikeDislikeAction()
        default:
            stopAnimating()
        }
    }
}
