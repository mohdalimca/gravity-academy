//
//  File.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum TechniquesAction {
    case errorMessage(_ text:String)
    case list
    case detail
    case likeDislike
    case weekList
    case weekData
    case nointernet
}

protocol TechniquesTableViewDelegate:class {
    func didSelectAt(row: Int)
}

protocol TechniquesListCellDelegate:class {
    func likeAction()
}

protocol TechniquesViewRepresentable:class {
    func onAction(_ action:TechniquesAction)
}

protocol TechniquesServiceProviderDelegate:class {
    func completed<T>(for action:TechniquesAction, with response:T?, error:APIError?)
}

protocol TechniquesServiceProviderable:class {
    var delegate: TechniquesServiceProviderDelegate? { get set }
    func list()
    func likeDislike(param:LikeDislikeParam)
    func skillWeeks(param:TechniquesParam.SkillWeeks)
    func skillWeeekDetail(param:TechniquesParam.SkillWeekDetail)
}
