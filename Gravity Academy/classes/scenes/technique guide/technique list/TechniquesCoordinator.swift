//
//  TechniquesCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 18/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import UIKit


final class TechniquesCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: TechniquesListVC = TechniquesListVC.from(from: .techniqueGuide, with: .techniquesList)
//    let detail: DetailViewController = DetailViewController.from(from: .workouts, with: .detail)
    
    var detail: TechniqueDetailCoordinator!
    
    override func start() {
        super.start()
        router.setRootModule(controller, hideBar: true)
        onStart()
    }
    
    private func onStart() {
        controller.router = self
        controller.delegateTable = self
    }
    
    private func startActivityDetail(_ index:Int) {
        detail = TechniqueDetailCoordinator(router: Router())
        add(detail)
        detail.delegate = self
        detail.start(skill: controller.viewModel.selectedSkill)
        router.present(detail, animated: true)
    }
}

extension TechniquesCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {
    }

    func push(scene: Scenes) {
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension TechniquesCoordinator: TechniquesTableViewDelegate {
    func didSelectAt(row: Int) {
        startActivityDetail(row)
    }
}

extension TechniquesCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
