//
//  TechniquesTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class TechniquesTableView: UITableView {
    
    weak var delegateTable: TechniquesTableViewDelegate?
    var viewModel:TechniquesViewModel!
    var didLikeId: ((_ id:String, _ indexPath:IndexPath) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func configure(_ viewModel:TechniquesViewModel) {
        self.viewModel = viewModel
        self.reloadData()
    }
    
    private func setup() {
        dataSource = self
        delegate = self
        registerReusable(ListHeaderCell.self)
    }
}

extension TechniquesTableView: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (viewModel.list.filter{ $0.fitnessLevel == viewModel.sections[section]}).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusable(indexPath) as TechniquesListCell
        cell.delegate = self
        let skill = viewModel.list.filter{ $0.fitnessLevel == viewModel.sections[indexPath.section]}
        cell.indexPath = indexPath
        cell.didLikeId = didLikeId
        cell.configure(with: skill[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListHeaderCell") as? ListHeaderCell
        cell?.lblTitle.text = viewModel.sections[section]
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let skill = viewModel.list.filter{ $0.fitnessLevel == (viewModel.sections[indexPath.section])}
        viewModel.selectedSkill = skill[indexPath.row]
        delegateTable?.didSelectAt(row: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}

extension TechniquesTableView: TechniquesListCellDelegate {
    func likeAction() {
        delegateTable?.didSelectAt(row: 0)
    }
}
