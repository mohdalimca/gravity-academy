//
//  TechniquesListCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class TechniquesListCell: UITableViewCell, Reusable {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var lblFitnessLevel: UILabel!
    @IBOutlet weak var imageBG: UIImageView!
    @IBOutlet weak var btnLike: GravityAcademyButton!
    @IBOutlet weak var progressView: UIProgressView!
    
    weak var delegate: TechniquesListCellDelegate?
    var didLikeId: ((_ id:String, _ indexPath:IndexPath) -> Void)?
    var id: String!
    var indexPath: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    func configure<T>(with content: T) {
        if let plan = content as? TrainingPlan {
            lblTitle.text = plan.title
            lblFitnessLevel.text = plan.fitnessLevel
            btnLike.isSelected = (plan.isLike == "0") ? false : true
            self.id = plan.tpID
            imageBG.downloadImageFrom(urlString: plan.tpImgURL ?? "", with: shadowImage)
            return
        }
        
        if let skill = content as? SkillList {
            lblTitle.text = skill.title
            lblFitnessLevel.text = skill.fitnessLevel
            btnLike.isSelected = (skill.isLike == "0") ? false : true
            self.id = skill.skillID
            imageBG.downloadImageFrom(urlString: skill.introMediaData?.image ?? "", with: shadowImage)
            return
        }
    }
    
    private func setup() {
        selectionStyle = .none
        btnLike.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func buttonPressed(_ sender: GravityAcademyButton) {
        didLikeId?(self.id, self.indexPath)
    }
}
