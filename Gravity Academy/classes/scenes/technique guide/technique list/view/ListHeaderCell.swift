//
//  ListHeaderCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/10/19.
//  Copyright © 2019 by W3Surface. All rights reserved.
//

import UIKit

class ListHeaderCell: UITableViewCell, Reusable {

    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.text = "Test"
    }

    func configure<T>(with content: T) {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
