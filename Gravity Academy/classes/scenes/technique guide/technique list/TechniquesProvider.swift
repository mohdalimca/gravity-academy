//
//  TechniquesProvider.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class TechniquesServiceProvider: TechniquesServiceProviderable {
    
    var delegate: TechniquesServiceProviderDelegate?
    private let task = TechniquesTask()
    
    func list() {
        task.list { (resp, err) in
            if err != nil {
                self.delegate?.completed(for: .list, with: resp, error: err)
                return
            }
            self.delegate?.completed(for: .list, with: resp, error: nil)
        }
    }
    
    func likeDislike(param:LikeDislikeParam) {
        task.likeDislike(param: param) { (resp, err) in
            if err != nil {
                self.delegate?.completed(for: .likeDislike, with: resp, error: err)
                return
            }
            self.delegate?.completed(for: .likeDislike, with: resp, error: nil)
        }
    }
    
    func skillWeeks(param:TechniquesParam.SkillWeeks) {
        task.skillWeeks(param: param) { (resp, err) in
            if err != nil {
                self.delegate?.completed(for: .weekList, with: resp, error: err)
                return
            }
            self.delegate?.completed(for: .weekList, with: resp, error: nil)
        }
    }
    
    func skillWeeekDetail(param:TechniquesParam.SkillWeekDetail) {
        task.skillWeeekDetail(param: param) { (resp, err) in
            if err != nil {
                self.delegate?.completed(for: .weekData, with: resp, error: err)
                return
            }
            self.delegate?.completed(for: .weekData, with: resp, error: nil)
        }
    }
}
