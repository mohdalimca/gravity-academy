//
//  TechniquesViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class TechniquesViewModel {
    
    var id:String!
    var list = [SkillList]()
    var sections = [String]()
    weak var view: TechniquesViewRepresentable?
    var provider: TechniquesServiceProviderable
    var selectedIndexPath: IndexPath!
    var likeStatus: Bool = false
    var selectedSkill: SkillList!
    
    init(provider:TechniquesServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }

    func skillList() {
        provider.list()
    }
    
    func likeDislike() {
        provider.likeDislike(param: LikeDislikeParam(type: .skills, type_id: id))
    }
}

extension TechniquesViewModel: TechniquesServiceProviderDelegate {
    func completed<T>(for action: TechniquesAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .list:
                        self.list = resp.data?.skillList ?? self.list
                    case .likeDislike:
                        self.likeStatus = (resp.data?.status == 1) ? true : false
                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
