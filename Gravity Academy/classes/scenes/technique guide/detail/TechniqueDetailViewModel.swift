//
//  TechniqueDetailViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 16/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class TechniqueDetailViewModel {
    
    var detailId:String!
    var selectedSkill: SkillList!
    var weeksList = [SkillWeek]()
    var selectedIndexPath: IndexPath!
    var likeStatus: Bool = false
    
    weak var view: TechniquesViewRepresentable?
    var provider: TechniquesServiceProviderable
    
    init(provider:TechniquesServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    var count:Int {
        return weeksList.count
    }
    
    func list() {
        provider.skillWeeks(param: TechniquesParam.SkillWeeks(skill_id: selectedSkill.skillID))
    }
    
    func likeDislike() {
        provider.likeDislike(param: LikeDislikeParam(type: .skills, type_id: selectedSkill.skillID))
    }
}

extension TechniqueDetailViewModel: TechniquesServiceProviderDelegate {
    
    func completed<T>(for action: TechniquesAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .likeDislike:
                        self.likeStatus = (resp.data?.status == 1) ? true : false
                    case .weekList:
                        self.weeksList = resp.data?.skillWeeks ?? self.weeksList
                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
