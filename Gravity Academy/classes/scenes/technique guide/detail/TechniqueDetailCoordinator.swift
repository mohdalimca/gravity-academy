//
//  TechniqueDetailCoordinator.swift
//  Gravity Academy
//
//  Created by Apple on 16/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation
import UIKit

final class TechniqueDetailCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: TechniqueDetailViewController = TechniqueDetailViewController.from(from: .common, with: .techniqueDetail)

    var session: SessionCoordinator!
    
    override func start() {
        super.start()
    }
    
    func start(skill:SkillList) {
        onStart()
        controller.viewModel.selectedSkill = skill
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.router = self
    }
        
    private func startSession() {
        session = SessionCoordinator(router: Router())
        add(session)
        session.delegate = self
        session.start(id: controller.viewModel.detailId)
        router.present(session, animated: true)
    }
}

extension TechniqueDetailCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .session:
            startSession()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension TechniqueDetailCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

