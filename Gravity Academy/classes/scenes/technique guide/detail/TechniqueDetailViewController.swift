//
//  TechniqueDetailViewController.swift
//  Gravity Academy
//
//  Created by Apple on 16/07/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import UIKit

class TechniqueDetailViewController: UIViewController {

    @IBOutlet var lblNavigationTitle: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
    
    @IBOutlet var topView: UIView!
    @IBOutlet var navigationView: UIView!
    
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var btnExpandCollpse: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageTechnique: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet var topViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var heightViewTopConstraint: NSLayoutConstraint!
    
    
    var headerViewHeight: CGFloat = 60
    var oldContentOffset = CGPoint.zero
    var isExpand = false
    var fixedHeight = 320
    var textHeight: CGFloat = 0
    weak var router: NextSceneDismisserPresenter?
    let viewModel: TechniqueDetailViewModel = TechniqueDetailViewModel(provider: TechniquesServiceProvider())

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        startAnimation()
        viewModel.view = self
        viewModel.list()
        populateTechniqueDetail()
        tableView.separatorStyle = .none
        lblDescription.textAlignment = .left
        lblDescription.sizeToFit()
        textHeight = lblDescription.frame.size.height + 40
        lblDescription.numberOfLines = 1
        topView.backgroundColor = .black
        navigationView.alpha = 0
        [btnBack, btnExpandCollpse, btnLike].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    private func populateTechniqueDetail() {
        lblTitle.text = viewModel.selectedSkill.title
        lblDescription.text = viewModel.selectedSkill.skillListDescription
        btnLike.isSelected = (viewModel.selectedSkill.isLike == "1") ? true : false
        imageTechnique.downloadImageFrom(urlString: viewModel.selectedSkill.introMediaData?.image ?? "", with: shadowImage)
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnBack:
            router?.dismiss(controller: .techniqueDetail)
        case btnLike:
            viewModel.likeDislike()
        case btnExpandCollpse:
            expandCollapseAction()
        default:
            break
        }
    }

    private func expandCollapseAction() {
        if !isExpand {
            UIView.animate(withDuration: 0.6) {
                self.isExpand = true
                self.heightViewTopConstraint.constant += self.textHeight
                self.lblDescription.numberOfLines = 0
                self.btnExpandCollpse.transform = CGAffineTransform(rotationAngle: .pi)
                self.view.layoutIfNeeded()
            }
        } else {
            UIView.animate(withDuration: 0.6) {
                self.isExpand = false
                self.heightViewTopConstraint.constant -= self.textHeight
                self.btnExpandCollpse.transform = CGAffineTransform.identity
                self.view.layoutIfNeeded()
            }
        }
    }
    
    private func didUpdateViewAlpha(_ alpha: CGFloat) {
        DispatchQueue.main.async {
            self.view.backgroundColor = (alpha == 1) ? .black :.white
            self.navigationView.alpha = alpha
        }
    }

    private func updateOnLikeDislike() {
        NotificationCenter.default.post(name: .handleSkillsLikeDislike, object: viewModel.selectedSkill)
        btnLike.isSelected = viewModel.likeStatus
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //ScrollView's contentOffset differences with previous contentOffset
        let contentOffset =  scrollView.contentOffset.y - oldContentOffset.y
      
        if(scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y <= headerViewHeight) {
            let percent = (scrollView.contentOffset.y / headerViewHeight)
            didUpdateViewAlpha(percent)
        } else if (scrollView.contentOffset.y > headerViewHeight){
            didUpdateViewAlpha(1.0)
        } else if (scrollView.contentOffset.y < 0) {
            didUpdateViewAlpha(0.0)
        }
        
        // Scrolls UP - we compress the top view
        if contentOffset > 0 && scrollView.contentOffset.y > 0 {
            if ( topViewTopConstraint.constant > -(self.heightViewTopConstraint.constant - 60) ) {
                topViewTopConstraint.constant -= contentOffset
                scrollView.contentOffset.y -= contentOffset
                topView.backgroundColor = .black
            }  else {
                topView.alpha = 1.0
                topView.backgroundColor = .black
            }
        }
        
        // Scrolls Down - we expand the top view
        if contentOffset < 0 && scrollView.contentOffset.y < 0 {
            if (topViewTopConstraint.constant < 0) {
                if topViewTopConstraint.constant - contentOffset > 0 {
                    topViewTopConstraint.constant = 0
                } else {
                    topViewTopConstraint.constant -= contentOffset
                }
                scrollView.contentOffset.y -= contentOffset
            }
        } else {
            topView.backgroundColor = .black
        }
        oldContentOffset = scrollView.contentOffset
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (scrollView.contentOffset.y > headerViewHeight){
            didUpdateViewAlpha(1.0)
        } else if (scrollView.contentOffset.y < 0) {
            didUpdateViewAlpha(0.0)
        }
    }
    
    private func reload() {
        tableView.reloadData()
        stopAnimating()
    }
}

extension TechniqueDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    //cells count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.weeksList.count
    }
    
    // cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusable(indexPath) as ActivityDetailTableViewCell
        cell.configure(with: viewModel.weeksList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.detailId = viewModel.weeksList[indexPath.row].skillWeekID
        router?.push(scene: .session)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init(frame: CGRect.zero)
    }
}


extension TechniqueDetailViewController:TechniquesViewRepresentable {
    
    func onAction(_ action: TechniquesAction) {
        switch action {
        case let .errorMessage(msg):
            stopAnimating()
            showBannerWith(text: msg, style: .danger)
        case .likeDislike:
            updateOnLikeDislike()
        case .weekList:
            reload()
        default:
            stopAnimating()
        }
    }
}
