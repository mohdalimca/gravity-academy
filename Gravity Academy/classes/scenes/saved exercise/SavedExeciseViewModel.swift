
//
//  SavedExeciseViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 31/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class SavedExeciseViewModel {

    weak var view: FavouriteViewRepresentable?
    var provider: FavouriteServiceProviderable
    var favouriteExercise = [Exercise]()
    var currentPageNo = 1
    var totalCount = 0
    var refreshOption: RefreshOption = .top
    var apiName: AppAPI!
    
    init(provider:FavouriteServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    var count:Int {
        return favouriteExercise.count
    }
    
    func favouriteList() {
        provider.favouriteList(param: FavouriteList(page_no: String(currentPageNo), page_size: "10"), scene: .favouriteExercise)
    }
}

extension SavedExeciseViewModel: FavouriteServiceProviderDelegate {
    func completed<T>(for action: FavouriteAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    if self.refreshOption == .top {
                        self.favouriteExercise.removeAll()
                    }
                    self.favouriteExercise.append(contentsOf: resp.data?.favouriteExercise?.exercises ?? [])
                    self.totalCount = resp.data?.favouriteExercise?.total?.toInt ?? self.totalCount

                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}

