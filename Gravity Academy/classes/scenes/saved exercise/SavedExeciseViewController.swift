//
//  SavedExeciseViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 31/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class SavedExeciseViewController: UIViewController {
    
    @IBOutlet weak var tableView: SavedExeciseTableView!
    
    let viewModel = SavedExeciseViewModel(provider: FavouriteServiceProvider())
    weak var router: NextSceneDismisserPresenter?
    weak var delegateTable: SavedExeciseTableViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup() {
        startAnimation()
        viewModel.view = self
        viewModel.favouriteList()
        tableView.configure(viewModel)
        tableView.delegateTable = delegateTable
    }
    
    private func reload() {
        tableView.reloadData()
        stopAnimating()
    }
    
    func didTryAgain(_ api:AppAPI) {
        startAnimation()
        viewModel.favouriteList()
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        router?.dismiss(controller: .savedExercise)
    }
    
    private func refreshTable() {
        tableView.refresh.endRefreshing()
        viewModel.currentPageNo = 1
        viewModel.favouriteList()
    }
    
    private func loadMore() {
        viewModel.currentPageNo += 1
        viewModel.favouriteList()
    }
}

extension SavedExeciseViewController: FavouriteViewRepresentable {
    func onAction(_ action: FavouriteAction) {
        switch action {
        case let .errorMessage(text):
            showBannerWith(text: text, style: .warning)
            stopAnimating()
        case .favourites:
            reload()
        case let .refreshTableView(option):
            viewModel.refreshOption = option
            (option == .top) ? refreshTable() : loadMore()
        default:
            stopAnimating()
        }
    }
}
