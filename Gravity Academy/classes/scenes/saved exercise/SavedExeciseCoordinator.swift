
//
//  SavedExeciseCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 31/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class SavedExeciseCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: SavedExeciseViewController = SavedExeciseViewController.from(from: .exercise, with: .savedExercise)
    let nointernet: NoInternetViewController = NoInternetViewController.from(from: .common, with: .nointernet)

    var exercise: ExerciseCoordinator!
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.router = self
        controller.delegateTable = self
        nointernet.router = self
        nointernet.didTryAgain = controller.didTryAgain
    }
    
    private func startDetail() {
        let r = Router()
        let detail = DetailCoordinator(router: r)
        add(detail)
        detail.delegate = self
        detail.start()
        router.present(detail, animated: true)
    }
    
    private func startExercise(_ index: Int) {
        exercise = ExerciseCoordinator(router: Router())
        add(exercise)
        exercise.delegate = self
//        exercise.controller.handleLike = controller.handleLike
        exercise.startWith(controller.viewModel.favouriteExercise[index])
        router.present(exercise, animated: true)
    }
}

extension SavedExeciseCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .nointernet:
            nointernet.apiName = controller.viewModel.apiName
            self.router.present(nointernet, animated: true)
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension SavedExeciseCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

extension SavedExeciseCoordinator: SavedExeciseTableViewDelegate {
    func didSelectAT(row: Int) {
        startExercise(row)
    }
}
