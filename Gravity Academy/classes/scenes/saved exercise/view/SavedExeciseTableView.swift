//
//  SavedExeciseTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 31/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class SavedExeciseTableView: UITableView {

    var viewModel: SavedExeciseViewModel! = nil
    weak var delegateTable: SavedExeciseTableViewDelegate?
    var spinner: UIActivityIndicatorView!
    lazy var refresh: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return control
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ viewModel: SavedExeciseViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        showsVerticalScrollIndicator = false
        dataSource = self
        delegate = self
        estimatedRowHeight = 70.0
        rowHeight = UITableView.automaticDimension
        refreshControl = refresh
        addSpinner()
    }
    
    private func addSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: 44)
        tableFooterView = spinner
        tableFooterView?.isHidden = true
    }

    @objc func refresh(_ control:UIRefreshControl) {
        viewModel.view?.onAction(.refreshTableView(.top))
    }
}

extension SavedExeciseTableView: UITableViewDelegate, UITableViewDataSource  {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        let cell = tableView.dequeueReusable(indexPath) as AllExerciseTableViewCell
        cell.backgroundColor = .clear
        cell.containerView.backgroundColor =  .clear
        cell.configure(with: viewModel.favouriteExercise[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegateTable?.didSelectAT(row: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.count < viewModel.totalCount && indexPath.row == viewModel.count - 1 && viewModel.totalCount > 10 {
            spinner.startAnimating()
            tableFooterView?.isHidden = false
            viewModel.view?.onAction(.refreshTableView(.bottom))
        } else {
            spinner.stopAnimating()
            tableFooterView?.isHidden = true
        }
    }
}



