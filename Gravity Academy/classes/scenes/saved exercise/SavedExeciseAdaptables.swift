//
//  SavedExeciseAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 31/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

protocol SavedExeciseTableViewDelegate:class {
    func didSelectAT(row : Int)
}
