//
//  EbookHeaderView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class EbookHeaderView: UIView {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtSearchField: UITextField!
    
    weak var delegate:EbookHeaderViewDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        [btnBack, btnSearch].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    
    
    @objc func buttonPressed(_ sender: UIButton) {
        if sender == btnBack {
            delegate?.sendAction(action: .back)
        } else {
            delegate?.sendAction(action: .search)
        }
    }
}
