//
//  EbookCollectionView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class EbookCollectionView: UICollectionView {
    
    let filterArray = ["MAK", "TEST", "CHECK", "NEW"]
    var selectedIndex = -1
    var viewModel: HomeViewModel!
    var visibleIndexPath: IndexPath? = nil
    var cellWidth = 276
    var cellSpacing = 10
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure(_ viewModel: HomeViewModel) {
        self.viewModel = viewModel
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        reloadData()
    }
}

extension EbookCollectionView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusable(indexPath) as EbookCollectionViewCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
  
    }
    
  
    
    func collectionView(_ collectionView: UICollectionView,
                                 willDisplay cell: UICollectionViewCell,
                                 forItemAt indexPath: IndexPath) {
        
//        cell.alpha = 0
//        UIView.animate(withDuration: 0.8) {
//            cell.alpha = 1
//        }
        
        
        if let visibleIndexPath = self.visibleIndexPath {
            
            // This conditional makes sure you only animate cells from the bottom and not the top, your choice to remove.
            
            cell.contentView.alpha = 0.3
            
            cell.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
            
            // Simple Animation
            UIView.animate(withDuration: 0.5) {
                cell.contentView.alpha = 1
                cell.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
            }
            
            if indexPath.row > visibleIndexPath.row {
                
               
            }
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        
//    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = self.contentOffset
        visibleRect.size = self.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        if let visibleIndexPath = self.indexPathForItem(at: visiblePoint) {
            self.visibleIndexPath = visibleIndexPath
        }
    }
    
    private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = cellWidth * filterArray.count
        let totalSpacingWidth = cellSpacing * (filterArray.count - 1)
        
        let leftInset = (self.bounds.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
}


