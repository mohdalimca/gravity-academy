//
//  EbookAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum EbookHeaderViewButtonAction: String {
    case back
    case search
}

protocol EbookHeaderViewDelegate:class {
    func sendAction(action: EbookHeaderViewButtonAction)
}
