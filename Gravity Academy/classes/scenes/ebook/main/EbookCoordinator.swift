
//
//  EbookCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class EbookCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: EbookViewController = EbookViewController.from(from: .ebook, with: .ebook)
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.router = self
    }
    
    private func startExercise() {
        let r = Router()
        let exercise = ExerciseCoordinator(router: r)
        add(exercise)
        exercise.delegate = self
        exercise.start()
        router.present(exercise, animated: true)
        
    }
    
    private func startCourse(action: GravityAcademyViewAction) {
        let r = Router()
        let course = AcademyCourseCoordinator(router: r)
        add(course)
        course.delegate = self
        course.startWith(action: action)
        router.present(course, animated: true)
    }
}

extension EbookCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {}
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension EbookCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
