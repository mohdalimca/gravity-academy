//
//  EbookViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class EbookViewController: UIViewController {

    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblEbookTitle: UILabel!
    @IBOutlet weak var header: EbookHeaderView!
    @IBOutlet weak var collection: EbookCollectionView!
    
    weak var router: NextSceneDismisserPresenter?
    let viewModel = HomeViewModel(provider: WorkoutsServiceProvider())

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        header.delegate = self
        collection.configure(viewModel)
    }
}

extension EbookViewController: EbookHeaderViewDelegate {
    func sendAction(action: EbookHeaderViewButtonAction) {
        if action == .back {
            router?.dismiss(controller: .ebook)
        } else {
            
        }
    }
}
