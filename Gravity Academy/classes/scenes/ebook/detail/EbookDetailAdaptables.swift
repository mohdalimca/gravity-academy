//
//  EbookDetailAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

//EbookDetailHeaderView
enum EbookDetailHeaderViewButtonAction: String {
    case back
    case buy
    case activate
    case list
}

protocol EbookDetailHeaderViewDelegate:class {
    func sendAction(action: EbookDetailHeaderViewButtonAction)
}

protocol EbookDetailCollectionViewDelegate:class {
    func didSelect(at index:Int)
}
