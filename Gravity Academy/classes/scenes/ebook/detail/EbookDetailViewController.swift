//
//  EbookDetailViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class EbookDetailViewController: UIViewController {
    
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblAuthorName: UILabel!
    @IBOutlet weak var lblEbookTitle: UILabel!
    @IBOutlet weak var lblMoreEbooks: UILabel!
    @IBOutlet weak var txtEbookDescription: UITextView!
    @IBOutlet weak var viewEbookDetail: UIView!
    @IBOutlet weak var header: EbookDetailHeaderView!
    @IBOutlet weak var collection: EbookDetailCollectionView!
    
    weak var router: NextSceneDismisserPresenter?
    weak var delegateCollection: EbookDetailCollectionViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        header.delegate = self
        viewEbookDetail.addShadowAround(shadowSize: 10, color: .gray)
        collection.delegateCollection = delegateCollection
    }
    
    private func buyAction() {
        if let url = URL(string: ebookPurchaseURL), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}

extension EbookDetailViewController: EbookDetailHeaderViewDelegate {
    func sendAction(action: EbookDetailHeaderViewButtonAction) {
        switch action {
        case .activate:
            router?.push(scene: .activate)
        case .back:
            router?.dismiss(controller: .ebookDetail)
        case .buy:
            buyAction()
        case .list:
            router?.dismiss(controller: .ebookDetail)
        }
    }
}

