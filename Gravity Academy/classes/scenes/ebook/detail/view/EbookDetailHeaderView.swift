//
//  EbookDetailHeaderView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class EbookDetailHeaderView: UIView {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnBuyNow: UIButton!
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var btnActivateNow: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imageEbook: UIImageView!
    
    weak var delegate:EbookDetailHeaderViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        [btnBack, btnBuyNow, btnActivateNow, btnList].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnBack: delegate?.sendAction(action: .back)
        case btnBuyNow: delegate?.sendAction(action: .buy)
        case btnActivateNow: delegate?.sendAction(action: .activate)
        case btnList: delegate?.sendAction(action: .list)
        default: break
        }
    }
}
