//
//  EbookDetailCollectionCell.swift
//  Gravity Academy
//
//  Created by Apple on 25/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class EbookDetailCollectionCell: UICollectionViewCell, Reusable {
    
    @IBOutlet weak var imageEbook: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        
    }
    
    private func setup() {
        
    }
}
