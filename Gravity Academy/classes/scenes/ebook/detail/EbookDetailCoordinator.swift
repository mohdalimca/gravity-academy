//
//  EbookDetailCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class EbookDetailCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: EbookDetailViewController = EbookDetailViewController.from(from: .ebook, with: .ebookDetail)
    let activate: ActivateEbookViewController = ActivateEbookViewController.from(from: .ebook, with: .activate)
    let ebookList: EbookViewController = EbookViewController.from(from: .ebook, with: .ebook)

    override func start() {
        super.start()
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.router = self
        controller.delegateCollection = self
        activate.router = self
        ebookList.router = self
        
    }
    
    private func startExercise() {
        let r = Router()
        let exercise = ExerciseCoordinator(router: r)
        add(exercise)
        exercise.delegate = self
        exercise.start()
        router.present(exercise, animated: true)
        
    }
    
    private func startCourse(action: GravityAcademyViewAction) {
        let r = Router()
        let course = AcademyCourseCoordinator(router: r)
        add(course)
        course.delegate = self
        course.startWith(action: action)
        router.present(course, animated: true)
    }
}

extension EbookDetailCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .activate:
            router.present(activate, animated: true)
        case .ebook:
            router.present(ebookList, animated: true)
        default:
            break
        }
    }
    
    func dismiss(controller: Scenes) {
        switch controller {
        case .ebook, .activate:
            router.dismissModule(animated: true, completion: nil)
        default:
            delegate?.dismiss(coordinator: self)
        }
    }
}

extension EbookDetailCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

extension EbookDetailCoordinator: EbookDetailCollectionViewDelegate {
    func didSelect(at index: Int) {
        router.present(ebookList, animated: true)
    }
}
