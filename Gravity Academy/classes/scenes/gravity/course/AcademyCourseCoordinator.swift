//
//  AcademyCourseCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class AcademyCourseCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let course: AcademyCourseViewController = AcademyCourseViewController.from(from: .gravityAcademy, with: .academyCourse)
    
    
    func startWith(action: GravityAcademyViewAction) {
        super.start()
        onStart()
        course.action = action
        router.setRootModule(course, hideBar: true)
    }
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(course, hideBar: true)
    }
    
    private func onStart() {
        course.delegateTable = self
        course.router = self
    }
    
    private func startAllExercise() {
        let r = Router()
        let all = AllExerciseCoordinator(router: r)
        add(all)
        all.delegate = self
        all.start()
        router.present(all, animated: true)
    }
    
    private func startSession() {
        let r = Router()
        let session = SessionCoordinator(router: r)
        add(session)
        session.delegate = self
        session.start()
        router.present(session, animated: true)
    }
    
    private func startActivity() {
        let r = Router()
        let program = ProgramCoordinator(router: r)
        add(program)
        program.start()
        program.delegate = self
        router.present(program, animated: true)
    }
}

extension AcademyCourseCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {}
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension AcademyCourseCoordinator: AcademyCourseTableViewDelegate {
    func sendAction(action: Scenes) {
        switch action {
        case .allExercise: startAllExercise()
        case .session: startSession()
        case .activityDetail: startActivity()
        default: break
        }
    }
}

extension AcademyCourseCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
