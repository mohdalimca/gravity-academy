//
//  AcademyCourseTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class AcademyCourseTableView: UITableView {
    var viewModel: AcademyCourseViewModel!
    var action: GravityAcademyViewAction!
    weak var delegateTable: AcademyCourseTableViewDelegate?
    var courseImage = "course-image-"
    var courseTitle = "course-title-"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ viewModel: AcademyCourseViewModel, _ action:GravityAcademyViewAction) {
        self.viewModel = viewModel
        self.action = action
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
    }
}

extension AcademyCourseTableView: UITableViewDelegate, UITableViewDataSource  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items(in: section).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.separatorStyle = .none
        switch viewModel.item(for: indexPath).scence {
        case .dayWorkout:
            let cell = tableView.dequeueReusable(indexPath) as DayWorkoutTableCell
            cell.imageBG.image = UIImage(named: "workout-of-day-icon")
            cell.configure(with: viewModel.item(at: indexPath.section))
            return cell
        default:
            let cell = tableView.dequeueReusable(indexPath) as AcademyCourseTableCell
            if indexPath.section == 1 {
                switch action {
                case .handstand?:
                    cell.imageCourse.image = UIImage(named: courseImage + "\(indexPath.section - 1)")
                case .pushups?:
                    let image = "ebook-course-image-1"
                    cell.imageCourse.image = UIImage(named: image)
                case .planche?:
                    let image = "ebook-course-image-2"
                    cell.imageCourse.image = UIImage(named: image)
                case .none: break
                }
            } else {
                cell.imageCourse.image = UIImage(named: courseImage + "\(indexPath.section - 1)")
            }
            cell.imageCourseTitle.image = UIImage(named: courseTitle + "\(indexPath.section - 1)")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0: delegateTable?.sendAction(action: .activityDetail)
        case 1,2: delegateTable?.sendAction(action: .session)
        default: delegateTable?.sendAction(action: .allExercise)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}
