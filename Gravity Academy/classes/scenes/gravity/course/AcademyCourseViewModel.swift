
//
//  AcademyCourseViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class AcademyCourseViewModel {
    
    private var items: [RowSectionDisplayable] = []
    
    var didSelect:((_ item:RowSectionDisplayable) -> Void)?
    
    var count:Int {
        return items.count
    }
    
    init() {
        items = RowSectionDisplayableFunction.makeRowSectionDisplayable(resource: "AcademyCourse")
    }
    
    func items(in section: Int) -> [Row] {
        return items[section].content
    }
    
    func item(at section: Int) -> RowSectionDisplayable {
        return items[section]
    }
    
    func item(for indexPath: IndexPath) -> Row {
        return items(in: indexPath.section)[indexPath.row]
    }
}
