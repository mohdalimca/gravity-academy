//
//  AcademyCourseAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum GravityAcademyViewAction: String {
    case handstand
    case pushups
    case planche
}

protocol AcademyCourseTableViewDelegate:class {
    func sendAction(action: Scenes)
}

protocol GravityAcademyViewControllerDelegate: class {
    func sendAction(action: GravityAcademyViewAction)
}
