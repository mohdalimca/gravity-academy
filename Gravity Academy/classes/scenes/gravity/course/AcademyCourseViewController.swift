//
//  AcademyCourseViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class AcademyCourseViewController: UIViewController {

    @IBOutlet weak var tableView: AcademyCourseTableView!
    @IBOutlet weak var btnBack: UIButton!
    
    let viewModel = AcademyCourseViewModel()
    var action: GravityAcademyViewAction!
    weak var delegateTable: AcademyCourseTableViewDelegate?
    weak var router: NextSceneDismisserPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        tableView.delegateTable = delegateTable
        tableView.configure(viewModel, action)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        router?.dismiss(controller: .academyCourse)
    }
}
