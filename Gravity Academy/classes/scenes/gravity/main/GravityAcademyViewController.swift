//
//  GravityAcademyViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class GravityAcademyViewController: UIViewController {

    @IBOutlet weak var tableView: GravityAcademyTableView!
    @IBOutlet weak var imageGravityAcademy: UIImageView!
    @IBOutlet weak var lblAcademyApp: UILabel!
    @IBOutlet weak var lblCourse: UILabel!

    weak var router: NextSceneDismisserPresenter?
    let viewModel = GravityAcademyViewModel()
    weak var delegateTable: GravityAcademyTableViewDelegate?
    weak var delegate: GravityAcademyViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        tableView.delegateTable = self
        tableView.configure(viewModel)
    }
    
    @IBAction func btnGravityAcademyAction(_ sender: UIButton) {
        router?.push(scene: .membership)
    }
}

extension GravityAcademyViewController: GravityAcademyTableViewDelegate {
    func didSelectAt(index: Int) {
        guard let url = URL(string: viewModel.courseLink[index]) else {
            print("invalid url")
            return
        }
        viewModel.browserURL = url
        router?.push(scene: .browser)
        
//        switch index {
//        case 0: delegate?.sendAction(action: .handstand)
//        case 1: delegate?.sendAction(action: .pushups)
//        case 2: delegate?.sendAction(action: .planche)
//        default: break
//        }
    }
}
