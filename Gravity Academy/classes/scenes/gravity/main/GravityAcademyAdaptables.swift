//
//  GravityAcademyAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

protocol GravityAcademyTableViewDelegate:class {
    func didSelectAt(index: Int)
}
