//
//  GravityAcademyViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class GravityAcademyViewModel {
    
    private var items: [RowSectionDisplayable] = []
    var apiName: AppAPI!
    var browserURL: URL!
    var didSelect:((_ item:RowSectionDisplayable) -> Void)?
    
    var count:Int {
        return items.count
    }
    
    var courseImage: [String] = [
        "ebook-course-image-0",
        "ebook-course-image-1",
        "ebook-course-image-2"
    ]
    
    var courseTitle: [String] = [
        "ebook-course-title-0",
        "ebook-course-title-1",
        "ebook-course-title-2"
    ]
    
    var courseLink: [String] = [
        "https://www.gravity-academy.de/handstand-anleitung",
        "https://www.gravity-academy.de/liegestuetze-anleitung",
        "https://www.gravity-academy.de/planche-anleitung"
    ]
    
    init() {
        items = RowSectionDisplayableFunction.makeRowSectionDisplayable(resource: "Home")
    }
    
    func items(in section: Int) -> [Row] {
        return items[section].content
    }
    
    func item(at section: Int) -> RowSectionDisplayable {
        return items[section]
    }
    
    func item(for indexPath: IndexPath) -> Row {
        return items(in: indexPath.section)[indexPath.row]
    }
}
