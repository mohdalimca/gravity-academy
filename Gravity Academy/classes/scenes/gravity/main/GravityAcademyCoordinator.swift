//
//  GravityAcademyCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class GravityAcademyCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let gravityAcademy: GravityAcademyViewController = GravityAcademyViewController.from(from: .gravityAcademy, with: .gravityAcademy)
    let nointernet: NoInternetViewController = NoInternetViewController.from(from: .common, with: .nointernet)

    var browser: BrowserCoordinator!
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(gravityAcademy, hideBar: true)
    }
    
    private func onStart() {
        gravityAcademy.router = self
        gravityAcademy.delegate = self
        nointernet.router = self
//        nointernet.didTryAgain = controller.didTryAgain
    }
    
    private func startExercise() {
        let r = Router()
        let exercise = ExerciseCoordinator(router: r)
        add(exercise)
        exercise.delegate = self
        exercise.start()
        router.present(exercise, animated: true)
        
    }
    
    private func startCourse(action: GravityAcademyViewAction) {
        let r = Router()
        let course = AcademyCourseCoordinator(router: r)
        add(course)
        course.delegate = self
        course.startWith(action: action)
        router.present(course, animated: true)
    }
    
    private func startEbooKDetail() {
        let r = Router()
        let ebookDetail = EbookDetailCoordinator(router: r)
        add(ebookDetail)
        ebookDetail.delegate = self
        ebookDetail.start()
        router.present(ebookDetail, animated: true)
    }
    
    private func startBrowser() {
        browser = BrowserCoordinator (router: Router())
        add(browser)
        browser.delegate = self
        browser.start(url: gravityAcademy.viewModel.browserURL)
        self.router.present(browser, animated: true)
    }
}

extension GravityAcademyCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .browser: startBrowser()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension GravityAcademyCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

extension GravityAcademyCoordinator: GravityAcademyViewControllerDelegate {
    func sendAction(action: GravityAcademyViewAction) {
        startCourse(action: action)
    }
}
