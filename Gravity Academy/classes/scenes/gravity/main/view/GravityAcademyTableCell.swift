//
//  GravityAcademyTableCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class GravityAcademyTableCell: UITableViewCell, Reusable {

    @IBOutlet weak var imageCourse: UIImageView!
    @IBOutlet weak var imageCourseTitle: UIImageView!
    @IBOutlet weak var imageCourseStatus: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        selectionStyle = .none
    }

    func configure<T>(with content: T) {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
