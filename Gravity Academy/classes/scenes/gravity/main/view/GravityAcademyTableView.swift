//
//  GravityAcademyTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 29/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class GravityAcademyTableView: UITableView {
    
    var viewModel: GravityAcademyViewModel!
    weak var delegateTable: GravityAcademyTableViewDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        dataSource = self
        delegate = self
    }
    
    func configure(_ viewModel: GravityAcademyViewModel) {
        self.viewModel = viewModel
        reloadData()
    }
    
    func configure() {
        dataSource = self
        delegate = self
        reloadData()
        registerReusable(ListHeaderCell.self)
    }
}

extension GravityAcademyTableView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.courseImage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusable(indexPath) as GravityAcademyTableCell
        cell.imageCourse.image = UIImage(named: viewModel.courseImage[indexPath.row])
//        cell.imageCourse.image = cell.imageCourse.image?.noir()
        cell.imageCourseTitle.image = UIImage(named: viewModel.courseTitle[indexPath.row])
//        cell.imageCourseTitle.image = cell.imageCourseTitle.image?.noir()
        return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegateTable?.didSelectAt(index: indexPath.row)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}
