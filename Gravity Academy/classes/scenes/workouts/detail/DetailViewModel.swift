//
//  DetailViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 24/02/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation
final class DetailViewModel {
    
    var workout: Workouts!
    var workoutCategory: String!
    var workoutOfDay: WoOfTheDay?
    var fitnessLevel: String = "1"
    var musclesGroup: [String] = []
    let provider: WorkoutsServiceProviderable
    weak var view: WorkoutsViewRepresentable?
    var likeStatus: Bool = false
    
    
    init(provider: WorkoutsServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    func likeAPI(param:LikeDislikeParam) {
        provider.likeDislike(param: param)
    }
}

extension DetailViewModel: WorkoutsServiceProviderDelegate {
    func completed<T>(for action: WorkoutsAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .likeDislike:
                        self.likeStatus = (resp.data?.status == 1) ? true : false
                    default: break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
