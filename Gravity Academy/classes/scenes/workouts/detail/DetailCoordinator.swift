//
//  DetailCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 24/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class DetailCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let detail: DetailViewController = DetailViewController.from(from: .workouts, with: .detail)
    var activity: StartActivityCoordinator!
    
    func start(detail workout: Workouts, categoryTitle:String) {
        onStart()
        detail.viewModel.workout = workout
        detail.viewModel.workoutCategory = categoryTitle
        router.setRootModule(detail, hideBar: true)
    }

    func start(day workout: WoOfTheDay, categoryTitle:String) {
        super.start()
        onStart()
        detail.viewModel.workoutOfDay = workout
        detail.viewModel.workoutCategory = categoryTitle
        router.setRootModule(detail, hideBar: true)
    }
    
    private func onStart() {
        detail.router = self
    }
    
    private func startActivity() {
        let r = Router()
        activity = StartActivityCoordinator(router: r)
        add(activity)
        activity.delegate = self
        activity.start(with: detail.viewModel.workout, woOfDay: detail.viewModel.workoutOfDay, level:detail.viewModel.fitnessLevel, workoutCategory:detail.viewModel.workoutCategory)
        router.present(activity, animated: true)
    }
}

extension DetailCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .startActivity: startActivity()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension DetailCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
