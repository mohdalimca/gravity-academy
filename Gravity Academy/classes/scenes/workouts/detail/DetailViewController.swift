//
//  DetailViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 19/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

enum Fitness: String {
    case beginner = "Beginner"
    case intermediate = "Intermediate"
    case advanced = "Advanced"
}

class DetailViewController: UIViewController {
    
    @IBOutlet weak var lblWorkoutGoal: UILabel!
    @IBOutlet weak var lblFitnessLevel: UILabel!
    @IBOutlet weak var lblWorkoutCategory: UILabel!
    @IBOutlet weak var lblMuscleGroup: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var gifImage: UIImageView!
    @IBOutlet weak var toggleControl: Toggle!
    @IBOutlet weak var collection: MusclesCollectionView!
    
    weak var router: NextSceneDismisserPresenter?
    var viewModel = DetailViewModel(provider: WorkoutsServiceProvider())
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        viewModel.view = self
        viewModel.fitnessLevel = "1"
        setToggle()
        [btnBack, btnLike, btnNext].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        populateDetail()
//        addRepeatingPulse()
        gifImage.image = UIImage.gif(name: "ripple_gif")
        
        NotificationCenter.default.addObserver(self, selector: #selector(likeDislikeUpdate(_:)), name: .handleWorkoutLikeDislike, object: nil)
    }
    
    private func addRepeatingPulse() {
        btnNext.backgroundColor = .orange
        self.btnNext.layer.removePulses()
        let _ = self.btnNext.layer.addPulse { pulse in
            pulse.lineWidth = 0.0
            pulse.borderColors = []
            pulse.borderColors = [
                UIColor(r: 0, g: 0, b:0).cgColor
            ]
            pulse.transformBefore = CATransform3DMakeScale(1, 1, 0.3)
            pulse.duration = 1.5
            pulse.repeatDelay = 0.0
            pulse.repeatCount = Int.max
            pulse.backgroundColors = self.colorsWithHalfOpacity(pulse.borderColors)
        }
    }
    
    func colorsWithHalfOpacity(_ colors: [CGColor]) -> [CGColor] {
        return colors.map({ $0.copy(alpha: $0.alpha * 0.5)! })
    }

    
    @objc func likeDislikeUpdate(_ notification: Notification) {
        if let isLike = notification.object as? Bool {
            btnLike.isSelected = isLike ? true : false
        }
    }

    private func populateDetail() {
        if viewModel.workoutOfDay == nil {
            lblWorkoutGoal.text = viewModel.workout.workoutGoal
            lblMuscleGroup.text = viewModel.workout.muscleGroups
            bgImage.downloadImageFrom(urlString: viewModel.workout?.woImgURL ?? "", with: shadowImage)
            viewModel.musclesGroup = viewModel.workout.muscleGroups?.components(separatedBy: ",") ?? viewModel.musclesGroup
            var tempMuscles : [String] = []
            for muscle in viewModel.musclesGroup {
                UserStore.masterData?.muscleGroup.forEach {
                    if $0.muscleID == muscle {
                        tempMuscles.append($0.title)
                    }
                }
            }
            
            if !tempMuscles.isEmpty {
                viewModel.musclesGroup.removeAll()
                viewModel.musclesGroup.append(contentsOf: tempMuscles)
            }
            
            lblWorkoutCategory.text = viewModel.workout.workoutCategoryTitle
            btnLike.isSelected = (viewModel.workout.isLike != "0") ? true : false
        } else {
            lblWorkoutGoal.text = viewModel.workoutOfDay?.workoutGoal
            lblMuscleGroup.text = viewModel.workoutOfDay?.muscleGroups
            bgImage.downloadImageFrom(urlString: viewModel.workoutOfDay?.woImgURL ?? "", with: shadowImage)
            viewModel.musclesGroup = viewModel.workoutOfDay?.muscleGroups?.components(separatedBy: ",") ?? viewModel.musclesGroup
            lblWorkoutCategory.text = viewModel.workoutOfDay?.workoutCategoryTitle
            btnLike.isSelected = (viewModel.workoutOfDay?.isLike != "0") ? true : false
        }
        
        
        collection.configure(musclesGroup: viewModel.musclesGroup, showDetailCell: true)
    }
    
    
    private func setToggle() {
        toggleControl.layer.cornerRadius = 15.0
        toggleControl.fontSize = 12
        toggleControl.fontName = "Roboto-Regular"
        toggleControl.layer.masksToBounds = true
        toggleControl.addTarget(self, action: #selector(toggleAction(_:)), for: .valueChanged)
        toggleControl.setSelected(index: 0)
    }
    
    @objc func toggleAction(_ toggle: Toggle) {
        if toggle.indexPos == 0 {
            viewModel.fitnessLevel = "1"
            lblFitnessLevel.text = Fitness.beginner.rawValue
        } else if toggle.indexPos == 1 {
            viewModel.fitnessLevel = "2"
            lblFitnessLevel.text = Fitness.intermediate.rawValue
        } else {
            viewModel.fitnessLevel = "3"
            lblFitnessLevel.text = Fitness.advanced.rawValue
        }
    }
    
    // MARK:- Button Action
    @objc func buttonPressed(_ btn:UIButton) {
        switch btn {
        case btnBack:
            router?.dismiss(controller: .detail)
        case btnLike:
            if viewModel.workoutOfDay != nil {
                viewModel.likeAPI(param: LikeDislikeParam(type: .workout, type_id: viewModel.workoutOfDay?.workoutID))
            } else {
                viewModel.likeAPI(param: LikeDislikeParam(type: .workout, type_id: viewModel.workout.workoutID))
            }
            
        case btnNext:
            router?.push(scene: .startActivity)
        default:
            break
        }
    }
    
    func handleLike() {
        btnLike.isSelected = (viewModel.likeStatus) ? true : false
        NotificationCenter.default.post(name: .handleWorkoutLikeDislike, object: viewModel.likeStatus)
    }
}

extension DetailViewController: WorkoutsViewRepresentable {
    func onAction(_ action: WorkoutsAction) {
        switch action {
        case .likeDislike: handleLike()
        default: break
        }
    }
    
}
