//
//  WorkoutsProvider.swift
//  Gravity Academy
//
//  Created by Apple on 08/12/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class WorkoutsServiceProvider: WorkoutsServiceProviderable {

    var delegate: WorkoutsServiceProviderDelegate?
    let task = WorkoutsTask()
    
    func masterData() {
        task.masterData(modeling: SuccessResponseModel.self) { [weak self] (resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .master, with: resp, error: err)
            }
            guard let _ = resp else { return }
            self?.delegate?.completed(for: .master, with: resp, error: nil)
        }
    }
    
    
    func workoutLibrary() {
        task.workoutLibrary(modeling: SuccessResponseModel.self) { [weak self](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .library, with: resp, error: err)
                return
            }
            self?.delegate?.completed(for: .library, with: resp, error: nil)
        }
    }
    
    func workoutDetail(param: WorkoutsParams.WorkoutsDetail) {
        task.workoutDetail(param: param, modeling: SuccessResponseModel.self) { [weak self](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .detail, with: resp, error: err)
                return
            }
            self?.delegate?.completed(for: .detail, with: resp, error: nil)
        }
    }
    
    func workoutOfTheDay() {
        task.workoutOfTheDay(modeling: SuccessResponseModel.self) { [weak self](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .day, with: resp, error: err)
                return
            }
            self?.delegate?.completed(for: .day, with: resp, error: nil)
        }
    }
    
    func likeDislike(param:LikeDislikeParam) {
        task.likeDislike(param: param) { [weak self](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .likeDislike, with: resp, error: err)
                return
            }
            self?.delegate?.completed(for: .likeDislike, with: resp, error: nil)
        }
    }
    
    func dashBoardData() {
        task.dashBoardData(modeling: SuccessResponseModel.self) { [weak self](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .dashBoardData, with: resp, error: err)
                return
            }
            self?.delegate?.completed(for: .dashBoardData, with: resp, error: nil)
        }
    }
    
    func equipments() {
        task.equipments(modeling: SuccessResponseModel.self) { [weak self](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .equipments, with: resp, error: err)
                return
            }
            self?.delegate?.completed(for: .equipments, with: resp, error: nil)
        }
    }
    
    func completeWorkout(param:WorkoutsParams.Complete) {
        task.completeWorkout(param: param) { [weak self](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .complete, with: resp, error: err)
                return
            }
            self?.delegate?.completed(for: .complete, with: resp, error: nil)
        }
    }
}
