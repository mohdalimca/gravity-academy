//
//  WorkoutLibraryCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 26/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class WorkoutLibraryCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: WorkoutLibraryViewController = WorkoutLibraryViewController.from(from: .workouts, with: .workoutLibrary)
    var detail: DetailCoordinator!
    var category: WorkoutCategoryCoordinator!
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    func start(with workoutOfDay: WoOfTheDay) {
        super.start()
        onStart()
        controller.viewModel.workoutOfDay = workoutOfDay
        router.setRootModule(controller, hideBar: true)
    }
    
    private func onStart() {
        controller.router = self
        controller.delegateTable = self
    }
    
    private func startWorkoutDayDetail() {
        detail = DetailCoordinator(router: Router())
        add(detail)
        detail.delegate = self
        detail.start(day: controller.viewModel.workoutOfDay!, categoryTitle: controller.viewModel.workoutCategory)
        router.present(detail, animated: true)
    }
    
    private func startDetail() {
        let r = Router()
        detail = DetailCoordinator(router: r)
        add(detail)
        detail.delegate = self
        detail.start(detail: controller.viewModel.workout!, categoryTitle: controller.viewModel.workoutCategory)
        router.present(detail, animated: true)
    }
    
    private func startCategory(index:Int) {
        let router = Router()
        category = WorkoutCategoryCoordinator(router: router)
        add(category)
        category.delegate = self
        category.start(id: (controller.viewModel.libraryWorkouts[index]).workoutCategoryID ?? "", title: controller.viewModel.libraryWorkouts[index].title ?? "")
        self.router.present(category, animated: true)
    }
}

extension WorkoutLibraryCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .detail:
            startDetail()
        case .dayWorkoutDetail:
            startWorkoutDayDetail()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension WorkoutLibraryCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

extension WorkoutLibraryCoordinator: WorkoutLibraryTableViewDelegate {
    func didSelect(at: Int) {
        if at == 0 { // detail
            startDetail()
        } else {
            startCategory(index: (at - 1))
        }
    }
}
