//
//  WorkoutLibraryAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 26/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

protocol WorkoutLibraryTableViewDelegate: class {
    func didSelect(at: Int)
}
