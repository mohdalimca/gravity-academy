//
//  WorkoutLibraryViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 26/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class WorkoutLibraryViewModel {
    var workoutOfDay: WoOfTheDay?
    var workoutOfTheDay: Workouts!
    private var items: [RowSectionDisplayable] = []
    var didSelect:((_ item:RowSectionDisplayable) -> Void)?
    let provider: WorkoutsServiceProviderable
    weak var view: WorkoutsViewRepresentable?
    var libraryWorkouts: [WoLIB] = []
    var sections:Int = 0
    var workout:Workouts?
    var workoutCategory:String!
    var likeDislikeIndexPath : IndexPath!
    var likeStatus: Bool = false
    
    var count:Int {
        if !libraryWorkouts.isEmpty {
            return libraryWorkouts.count + 1 //for day workout cell
        }
        return libraryWorkouts.count
    }
    
    init(provider: WorkoutsServiceProviderable) {
        items = RowSectionDisplayableFunction.makeRowSectionDisplayable(resource: "WorkoutLibrary")
        self.provider = provider
        self.provider.delegate = self
    }
    
    func workoutLibrary() {
        provider.workoutLibrary()
    }
    
    func workoutLikeDislike(_ param: LikeDislikeParam) {
        provider.likeDislike(param: param)
    }
    
    func items(in section: Int) -> [Row] {
        return items[section].content
    }
    
    func item(at section: Int) -> RowSectionDisplayable {
        return items[section]
    }
    
    func item(for indexPath: IndexPath) -> Row {
        return items(in: indexPath.section)[indexPath.row]
    }
    
    private func rearrangeLibraryWorkouts() {
        
        for i in 0..<libraryWorkouts.count - 1 {
            let library = libraryWorkouts[i]
            if library.workouts?.count == 1 && library.workouts?.first?.isWoOfTheDay == "1" {
                print("array result is \(library)")
                print("array result index is \(i)")
                self.workoutOfTheDay = library.workouts?.first!
                break
            } else {
                if (library.workouts?.count ?? 0) != 0 {
                    for j in 0..<library.workouts!.count - 1 {
                        let wokrouts = library.workouts![j]
                        if wokrouts.isWoOfTheDay == "1" {
                            print("array result is \(library)")
                            print("array result index is \(j)")
                            self.workoutOfTheDay = wokrouts
                            break
                        }
                    }
                } else {
                    libraryWorkouts.remove(at: i)
                }
            }
        }
    }
}

extension WorkoutLibraryViewModel: WorkoutsServiceProviderDelegate {
    func completed<T>(for action: WorkoutsAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage(errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .library:
                        if let library = resp.data?.woLIB {
                            self.libraryWorkouts = library
                            self.rearrangeLibraryWorkouts()
                        }
                    case .likeDislike:
                        self.likeStatus = (resp.data?.status == 1) ? true : false
                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
