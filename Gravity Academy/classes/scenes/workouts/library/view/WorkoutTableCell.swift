//
//  WorkoutTableCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 23/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class WorkoutTableCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var workoutCollection: WorkoutCollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        guard let workoutArray = content as? [Workouts] else { return }
        workoutCollection.configure(workoutArray)
    }
    
    func configure(viewModel:WorkoutLibraryViewModel, array:[Workouts]) {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setup() {
        selectionStyle = .none
//        workoutCollection.configure(viewModel: viewModel)
    }
}
