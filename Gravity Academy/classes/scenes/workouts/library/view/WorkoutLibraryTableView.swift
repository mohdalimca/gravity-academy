//
//  WorkoutLibraryTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 26/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class WorkoutLibraryTableView: UITableView {
    
    var viewModel: WorkoutLibraryViewModel!
    var homeViewModel: HomeViewModel!
    var musculeGrop:[String]!
    var didLikeDislikeWorkoutLibrary:((_ param:LikeDislikeParam, _ indexPath:IndexPath) -> Void)?
    weak var delegateTable: WorkoutLibraryTableViewDelegate?
    var presentDetailController: ((_ workout:Workouts, _ indexPath:IndexPath) -> Void)?
    var presentDetailControllerDayWorkout: ((_ indexPath:IndexPath) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ viewModel: WorkoutLibraryViewModel) {
          self.viewModel = viewModel
          setup()
      }
    func configure(_ viewModel: WorkoutLibraryViewModel, homeViewModel: HomeViewModel) {
        self.viewModel = viewModel
        self.homeViewModel = homeViewModel
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
        registerReusable(HomeHeaderCell.self)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        switch sender.name {
        case "0": delegateTable?.didSelect(at: 0)
        case "1": delegateTable?.didSelect(at: 1)
        case "2": delegateTable?.didSelect(at: 2)
        case "3": delegateTable?.didSelect(at: 3)
        default : delegateTable?.didSelect(at: -1)
        }
    }
    
    @objc func didTapHeaderButton(_ sender:UIButton) {
        delegateTable?.didSelect(at: sender.tag)
    }
    
    private func tableHeader(at section:Int, with tableView:UITableView) -> UIView {
        if viewModel.libraryWorkouts.count > 0 && section > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeHeaderCell") as? HomeHeaderCell
            cell?.configure(with: (viewModel.libraryWorkouts[section - 1]).title ?? "")
            cell?.btnViewAll.tag = section
            cell?.btnViewAll.addTarget(self, action: #selector(didTapHeaderButton(_:)), for: .touchUpInside)
            cell?.btnViewAll.isHidden = true
            if ((viewModel.libraryWorkouts[section - 1]).workouts?.count ?? 0) > 3 {
                cell?.btnViewAll.isHidden = false
            }
//            let tap = UITapGestureRecognizer(target: self, action:#selector(self.handleTap(_:)))
//            tap.name = "\(section)"
//            tap.numberOfTapsRequired = 1
//            cell!.btnViewAll.addGestureRecognizer(tap)
            return cell?.contentView ?? UIView()
        } else {
            return UIView()
        }
    }
}

extension WorkoutLibraryTableView: UITableViewDelegate, UITableViewDataSource  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.separatorStyle = .none
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusable(indexPath) as DayWorkoutTableCell
            cell.btnLike.tag = indexPath.section
            cell.didLikeDislikeWorkoutLibrary = didLikeDislikeWorkoutLibrary
            cell.configure(with: viewModel.workoutOfDay)
            return cell
        } else {
            let cell = tableView.dequeueReusable(indexPath) as WorkoutTableCell
            cell.workoutCollection.didLikeDislikeWorkoutLibrary = didLikeDislikeWorkoutLibrary
            cell.workoutCollection.tag = indexPath.section
            cell.workoutCollection.presentDetailController = presentDetailController
            cell.configure(with: viewModel.libraryWorkouts[indexPath.section - 1].workouts)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presentDetailControllerDayWorkout?(indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tableHeader(at: section, with: tableView)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section != 0) ? 40 : 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}

extension WorkoutLibraryTableView: HomeHeaderCellDelegate {
    func didSelectViewAll(at index: Int) {
        delegateTable?.didSelect(at: index)
    }
}

extension WorkoutLibraryTableView: WorkoutCollectionViewDelegate {
    func didSelect(at index: Int, section: WorkoutsSection) {
        delegateTable?.didSelect(at: index)
    }
}

extension WorkoutLibraryTableView:DayWorkoutTableCellDelegate {
    func likeUnlikeButtonAction() {
        viewModel.workoutLikeDislike(LikeDislikeParam(type: .workout, type_id: viewModel.workoutOfDay?.workoutID))
    }
}
