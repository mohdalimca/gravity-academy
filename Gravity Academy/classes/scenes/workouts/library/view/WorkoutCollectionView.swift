//
//  WorkoutCollectionView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 23/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class WorkoutCollectionView: UICollectionView {
    var viewModel: WorkoutLibraryViewModel!
    weak var delegateCollection: WorkoutCollectionViewDelegate?
    var didLikeDislikeWorkoutLibrary:((_ param:LikeDislikeParam, _ indexPath:IndexPath) -> Void)?
    var arrWrokouts: [Workouts] = []
    var dict: [Int: [Workouts]] = [:]
    var array: [[Int: [Workouts]]] = []
    let layout = UICollectionViewFlowLayout()
    var isDone: Bool?
    var presentDetailController: ((_ workout:Workouts, _ indexPath:IndexPath) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(viewModel:WorkoutLibraryViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    
    func configure(_ array:[Workouts]) {
        self.arrWrokouts = array
        setup()
    }
    
    private func setup() {
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 20
        isPagingEnabled = false
        collectionViewLayout = layout
        delegate = self
        dataSource = self
        reloadData()
    }
    
    private func setDataSource() {
        if isDone == nil {
            let library = viewModel.libraryWorkouts
            for i in 0..<library.count {
                if library[i].workouts?.count != 0 {
                    dict[i] = library[i].workouts
                } else {
                    dict[i] = library[0].workouts
                }
                array.append(dict)
                dict = [:]
            }
            isDone = true
            print("array count ===== \(array.count)")
            print("main array ===== \(array)")
        }
    }
}

extension WorkoutCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (arrWrokouts.count < 3) ? arrWrokouts.count : 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusable(indexPath) as LibraryCollectionCell
        cell.section = self.tag
        cell.btnLike.tag = indexPath.item
        cell.didLikeDislikeWorkoutLibrary = didLikeDislikeWorkoutLibrary
        cell.configure(with: arrWrokouts[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath)
        print(collectionView.tag)
        presentDetailController?(arrWrokouts[indexPath.item], IndexPath(row: indexPath.item, section: collectionView.tag))
//        delegateCollection?.didSelect(at: indexPath.row, section: .workout)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width - 50, height: collectionView.bounds.height)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let pageWidth: Float = Float(self.bounds.width - 50) // width + space
        
        let currentOffset = Float(scrollView.contentOffset.x)
        let targetOffset = Float(targetContentOffset.pointee.x)
        var newTargetOffset: Float = 0
        
        if targetOffset > currentOffset {
            newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth
        } else {
            newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth
        }
        
        if newTargetOffset < 0 {
            newTargetOffset = 0
        } else if CGFloat(newTargetOffset) > scrollView.contentSize.width {
            newTargetOffset = Float(scrollView.contentSize.width)
        }
        
        targetContentOffset.pointee.x = CGFloat(currentOffset)
        scrollView.setContentOffset(CGPoint(x: CGFloat(newTargetOffset), y: 0), animated: true)
        
        var index = Int(newTargetOffset / pageWidth)
        
        if index == 0 {
            // If first index
            var cell = self.cellForItem(at: IndexPath(item: index, section: 0))
            
            UIView.animate(withDuration: 0.2, animations: {
                cell?.transform = .identity
            })
            cell = self.cellForItem(at: IndexPath(item: index + 1, section: 0))
            UIView.animate(withDuration: 0.2, animations: {
                cell?.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            })
        } else {
            var cell = self.cellForItem(at: IndexPath(item: index, section: 0))
            UIView.animate(withDuration: 0.2, animations: {
                cell?.transform = .identity
            })
            
            index -= 1 // left
            cell = self.cellForItem(at: IndexPath(item: index, section: 0))
            UIView.animate(withDuration: 0.2, animations: {
                cell?.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            })
            
            index += 1
            index += 1 // right
            cell = self.cellForItem(at: IndexPath(item: index, section: 0))
            UIView.animate(withDuration: 0.2, animations: {
                cell?.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            })
        }
    }
}

extension WorkoutCollectionView: HomeCollectionCellDelegate {
    func didSelect() {
//        delegateCollection?.didsele
    }
}
