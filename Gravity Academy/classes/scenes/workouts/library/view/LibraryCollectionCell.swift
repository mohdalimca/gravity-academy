//
//  LibraryCollectionCell.swift
//  Gravity Academy
//
//  Created by Apple on 11/12/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class LibraryCollectionCell: UICollectionViewCell, Reusable {
    var didLikeDislikeWorkoutLibrary:((_ param:LikeDislikeParam, _ indexPath:IndexPath) -> Void)?
    let defaulImage = UIImage(named: "library_cell_bg")
    var section: Int!
    
    @IBOutlet weak var imageBG: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnLike: GravityAcademyButton!
    @IBOutlet weak var collection: MusclesCollectionView!

    var libraryWorkout: Workouts!
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        guard let workout = content as? Workouts else { return }
        libraryWorkout = workout
        lblTitle.text = workout.title ?? ""
        lblDate.text = workout.addedDate?.gravityDate()
        btnLike.isSelected = (workout.isLike == "1") ? true : false
        imageBG.downloadImageFrom(urlString: workout.woImgURL ?? "", with: defaulImage!)

//        if let _ = workout.woImgURL {
//            imageBG.downloadImageFrom(urlString: workout.woImgURL ?? "", with: defaulImage!)
//        } else {
//            imageBG.image = defaulImage
//        }
        collection.configure(musclesGroup: (workout.muscleGroups?.components(separatedBy: ","))!, showDetailCell: nil)
    }
    
    private func setup() {
        imageBG.layer.cornerRadius = 10.0
        btnLike.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
    }
    
    @objc func buttonPressed(_ sender: GravityAcademyButton) {
        if !sender.isSelected {
            print("like api call")
        } else {
            print("dis-like api call")
        }

        let param = LikeDislikeParam(type: .workout, type_id: libraryWorkout.workoutID)
        let indexPath = IndexPath(item: sender.tag, section: section)
        didLikeDislikeWorkoutLibrary?(param, indexPath)
    }
}
