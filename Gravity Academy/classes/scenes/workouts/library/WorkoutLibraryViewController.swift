//
//  WorkoutLibraryViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 26/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import HCVimeoVideoExtractor

class WorkoutLibraryViewController: UIViewController {
    
    @IBOutlet weak var tableView: WorkoutLibraryTableView!
    @IBOutlet weak var btnBack: GravityAcademyButton!
    @IBOutlet weak var btnIntro: GravityAcademyButton!
    
    weak var delegate:ControllerDismisser?
    weak var router: NextSceneDismisserPresenter?
    var homeViewModel: HomeViewModel!
    let viewModel = WorkoutLibraryViewModel(provider: WorkoutsServiceProvider())
    weak var delegateTable: WorkoutLibraryTableViewDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        startAnimation()
        viewModel.view = self
        tableView.delegateTable = delegateTable
        tableView.configure(viewModel)
        tableView.didLikeDislikeWorkoutLibrary = didLikeDislikeWorkoutLibrary
        tableView.presentDetailController = presentDetailController
        tableView.presentDetailControllerDayWorkout = presentDetailControllerDayWorkout
        [btnBack, btnIntro].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        viewModel.workoutLibrary()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWorkoutLibrary(_:)), name: .reloadWorkoutLibrary, object: nil)
    }
    
    private func reload() {
        tableView.reloadData()
        stopAnimating()
    }
    
    @objc func reloadWorkoutLibrary(_ notification: Notification) {
        viewModel.workoutLibrary()
    }
    
    func presentDetailControllerDayWorkout(_ indexPath:IndexPath) {
        viewModel.workoutCategory = viewModel.libraryWorkouts[indexPath.section].title
        router?.push(scene: .dayWorkoutDetail)
    }
    
    func presentDetailController(_ workout:Workouts, _ indexPath:IndexPath) {
        viewModel.workoutCategory = viewModel.libraryWorkouts[indexPath.section].title
        viewModel.workout = workout
        router?.push(scene: .detail)
    }
    
    func didLikeDislikeWorkoutLibrary(_ param:LikeDislikeParam, _ indexPath:IndexPath) {
        print("param is === \(param)")
        print("indexPath is === \(indexPath)")
        viewModel.likeDislikeIndexPath = indexPath
        viewModel.workoutLikeDislike(param)
    }
    
    @objc func buttonPressed(_ sender: GravityAcademyButton) {
        switch sender {
        case btnBack:
            router?.dismiss(controller: .workoutLibrary)
        case btnIntro:
            let url = URL(string: "https://vimeo.com/226190945")
            self.playVideo(url!)
        default:
            break
        }
    }

    
    private func reloadOnLikeDislike() {
        if viewModel.likeDislikeIndexPath.section == 0 {
            viewModel.workoutOfDay?.isLike = (viewModel.likeStatus) ? "1" : "0"
            NotificationCenter.default.post(name: .reloadWorkoutOfTheDay, object: nil)
        } else {
            viewModel.libraryWorkouts[viewModel.likeDislikeIndexPath.section  - 1].workouts![viewModel.likeDislikeIndexPath.item].isLike = (viewModel.likeStatus) ? "1" : "0"
        }
        let index = IndexPath(row: 0, section: viewModel.likeDislikeIndexPath.section)
        tableView.reloadRows(at: [index], with: .automatic)
        NotificationCenter.default.post(name: .handleWorkoutLikeDislike, object: nil)
    }
}

extension WorkoutLibraryViewController: WorkoutsViewRepresentable {
    func onAction(_ action: WorkoutsAction) {
        switch action {
        case let .errorMessage(text):
            showBannerWith(text: text, style: .danger)
            stopAnimating()
        case .library:
            reload()
        case .likeDislike:
            reloadOnLikeDislike()
        default:
            stopAnimating()
        }
    }
}
