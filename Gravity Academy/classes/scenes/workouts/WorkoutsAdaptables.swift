//
//  WorkoutsAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 05/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum WorkoutsDetailAction {
    case back
    case like
    case more
}

enum WorkoutsAction {
    case errorMessage(_ text:String)
    case library
    case detail
    case day
    case likeDislike
    case master
    case nointernet
    case dashBoardData
    case equipments
    case complete
    
}

protocol WorkoutsDetailHeaderViewDelegate: class {
    func didSelect(action: WorkoutsDetailAction)
}

protocol WorkoutsDetailTableViewDelegate: class {
    func didSelectAt(index: Int)
}

protocol WorkoutsViewRepresentable:class {
    func onAction(_ action: WorkoutsAction)
}

protocol WorkoutsServiceProviderDelegate:class {
    func completed<T>(for action:WorkoutsAction, with response:T?, error:APIError?)
}

protocol WorkoutsServiceProviderable: class {
    var delegate: WorkoutsServiceProviderDelegate? { get set }
    func workoutLibrary()
    func workoutOfTheDay()
    func workoutDetail(param: WorkoutsParams.WorkoutsDetail)
    func likeDislike(param:LikeDislikeParam)
    func masterData()
    func dashBoardData()
    func equipments()
    func completeWorkout(param:WorkoutsParams.Complete)
}

protocol AthleteCollectionViewDelegate: class {
    func sendAction(action:Scenes, with id:String)
}
