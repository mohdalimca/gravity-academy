//
//  WorkoutsViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 16/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import NotificationBannerSwift

class WorkoutsViewController: UIViewController {
    
    @IBOutlet weak var tableView: HomeTableView!
    weak var delegate:ControllerDismisser?
    weak var router: NextSceneDismisserPresenter?
    weak var delegateTable: HomeTableViewDelegate?
    let viewModel = HomeViewModel(provider: WorkoutsServiceProvider())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func setup() {
        startAnimation()
        setViewImages()
        viewModel.view = self
        viewModel.apiName = .masterData
        viewModel.masterData()
        viewModel.workoutOfTheDay()
        tableView.delegateTable = self
        tableView.didLikeDislikeWorkoutLibrary = didLikeDislikeWorkoutLibrary
        tableView.configure(viewModel)
        viewModel.dashboardData()
        NotificationCenter.default.addObserver(self, selector: #selector(likeDislikeUpdate(_:)), name: .handleWorkoutLikeDislike, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWorkoutOfDay(_:)), name: .reloadWorkoutOfTheDay, object: nil)
    }
    
    private func setViewImages() {
        if UserStore.gender == "male" {
            viewModel.imageArray = viewModel.maleImageArray
        } else {
            viewModel.imageArray = viewModel.femaleImageArray
        }
    }
    
    @objc func reloadWorkoutOfDay(_ notification: Notification) {
        viewModel.workoutOfTheDay()
    }
    
    @objc func likeDislikeUpdate(_ notification: Notification) {
        if let isLike = notification.object as? Bool {
            handleLike(isLike)
        }
    }
    
    private func didLikeDislikeWorkoutLibrary(_ param:LikeDislikeParam, _ indexPath:IndexPath) {
        viewModel.likeAPI(param: param)
    }

    func didTryAgain(_ api:AppAPI ) {
        switch api {
        case .masterData:
            startAnimation()
            viewModel.masterData()
            viewModel.workoutOfTheDay()
        case .likeUnlike:
            viewModel.likeAPI(param: LikeDislikeParam(type: .workout, type_id: viewModel.workoutOfDay?.workoutID))
        default: break
        }
        router?.dismiss(controller: .nointernet)
    }

    
    func reload() {
        tableView.reloadData()
        stopAnimating()
    }
    
    func handleLike(_ isLike: Bool) {
        viewModel.workoutOfDay?.isLike = (!isLike) ? "0" : "1"
        tableView.reloadSections(IndexSet(integer: 0), with: .none)
    }
}

extension WorkoutsViewController: HomeTableViewDelegate {
    
    func didShowDetailView(section: WorkoutsSection) {
        switch section {
        case .workout: router?.push(scene: .workoutDetail)
        case .program: router?.push(scene: .activityDetail)
        case .technique: router?.push(scene: .techniques)
        default: break
        }
    }
    
    func like(at index: Int, section: WorkoutsSection) {
    }
    
    func didSelect(at index: Int) {
        switch index {
        case 0: router?.push(scene: .dayWorkoutDetail)
        case 1: router?.push(scene: .workoutLibrary)
        case 2: router?.push(scene: .programLibrary)
        case 3: router?.push(scene: .techniquesList)
        case 4: router?.push(scene: .allExercise)
        case 5: router?.push(scene: .equipments)
        default: router?.push(scene: .techniquesList) // athlete list
        }
    }
    
    func sendAction(action: Scenes, id: String) {
        router?.push(scene: .athlete)
    }
}

extension WorkoutsViewController: WorkoutsViewRepresentable {
    func onAction(_ action: WorkoutsAction) {
        switch action {
        case .nointernet:
            router?.push(scene: .nointernet)
        case let .errorMessage(text):
            showBannerWith(text: text, style: .danger)
            stopAnimating()
        case .day, .dashBoardData:
            reload()
        case .likeDislike:
            handleLike(viewModel.isLike)
        default:
            stopAnimating()
        }
    }
}
