//
//  MusclesCollectionViewCell.swift
//  Gravity Academy
//
//  Created by Apple on 11/12/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
class MusclesCollectionViewCell: UICollectionViewCell, Reusable {
        
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure<T>(with content: T) {
        guard let title = content as? String else { return }
        lblTitle.text = title.capitalized
    }
}
