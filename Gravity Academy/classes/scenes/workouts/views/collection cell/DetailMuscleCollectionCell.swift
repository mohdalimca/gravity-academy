//
//  DetailMuscleCollectionCell.swift
//  Gravity Academy
//
//  Created by Apple on 16/04/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import UIKit

class DetailMuscleCollectionCell: UICollectionViewCell, Reusable {
        
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure<T>(with content: T) {
        guard let title = content as? String else { return }
        lblTitle.text = title.capitalized
    }
}
