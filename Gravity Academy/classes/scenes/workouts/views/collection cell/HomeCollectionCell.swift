//
//  HomeCollectionCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 22/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell, Reusable {

    weak var delegate: HomeCollectionCellDelegate?
    
    @IBOutlet weak var imageBG: UIImageView!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnLike: GravityAcademyButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        
    }
    
    private func setup() {
        imageIcon.layer.cornerRadius = imageIcon.frame.size.width / 2
        imageBG.layer.cornerRadius = 10.0
        btnLike.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
    }
    
    @objc func buttonPressed(_ sender: GravityAcademyButton) {
        if !sender.isSelected {
            sender.isSelected = true
            delegate?.didSelect()
            print("like api call")
        } else {
            sender.isSelected = false
            delegate?.didSelect()
            print("dis-like api call")
        }
    }
}
