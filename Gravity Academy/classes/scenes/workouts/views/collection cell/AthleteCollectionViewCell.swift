//
//  AthleteCollectionViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 06/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class AthleteCollectionViewCell: UICollectionViewCell, Reusable {
    
    @IBOutlet weak var imageUser: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure<T>(with content: T) {
        
    }
    
    private func setup() {
        
    }

}
