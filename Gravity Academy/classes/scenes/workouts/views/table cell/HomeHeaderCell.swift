//
//  HomeHeaderCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class HomeHeaderCell: UITableViewCell, Reusable {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var btnViewAll: UIButton!
    
    weak var delegate: HomeHeaderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        
        if let item = content as? RowSectionDisplayable {
            title.text = item.title.capitalized
        }
        
        if let item = content as? String {
            title.text = item.capitalized
        }
//        guard let item = content as? RowSectionDisplayable else { return }
//        title.text = item.title.capitalized
    }
    
    private func setup() {
        selectionStyle = .none
//        btnViewAll.addTarget(self, action: #selector(btnViewAllAction(_:)), for: .touchUpInside)
//        let tap = UITapGestureRecognizer(target: self, action:#selector(self.handleTap(_:)))
//        tap.numberOfTapsRequired = 1
//        btnViewAll.addGestureRecognizer(tap)
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        delegate?.didSelectViewAll(at: 0)
    }

    
    @objc func btnViewAllAction(_ sender: GravityAcademyButton) {
        delegate?.didSelectViewAll(at: sender.tag)
    }
}
