//
//  TechniqueGuideCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class TechniqueGuideCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var techniqueCollection: TechniqueCollectionView!
    let viewModel = HomeViewModel(provider: WorkoutsServiceProvider())
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setup() {
        selectionStyle = .none
        techniqueCollection.configure(viewModel: viewModel)
    }
}
