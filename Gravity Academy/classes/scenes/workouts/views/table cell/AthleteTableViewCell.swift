//
//  AthleteTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 06/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class AthleteTableViewCell: UITableViewCell, Reusable {

    @IBOutlet weak var btnViewAll: GravityAcademyButton!
    @IBOutlet weak var collectionView: AthleteCollectionView!
    var viewModel: HomeViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        
    }
    
    private func setup() {
        selectionStyle = .none
        collectionView.configure()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
