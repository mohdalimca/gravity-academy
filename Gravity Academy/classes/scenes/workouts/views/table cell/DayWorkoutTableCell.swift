//
//  DayWorkoutTableCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 21/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

protocol DayWorkoutTableCellDelegate:class {
    func likeUnlikeButtonAction()
}

class DayWorkoutTableCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imageBG: UIImageView!
    @IBOutlet weak var btnLike: GravityAcademyButton!
    @IBOutlet weak var collection: MusclesCollectionView!
    
    var workoutID: String!
    var viewModel:HomeViewModel!
    var delegate: DayWorkoutTableCellDelegate?
    var didLikeDislikeWorkoutLibrary:((_ param:LikeDislikeParam, _ indexPath:IndexPath) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure<T>(with content: T) {
        
        if let item = content as? WoOfTheDay {
            populate(item)
            return
        }
        
        if let item = content as? Workouts {
            populateWorkout(item)
            return
        }
    }
    
    private func populate(_ workoutOfDay:WoOfTheDay) {
        self.workoutID = workoutOfDay.workoutID
        lblCategory.text = workoutOfDay.title
        lblDate.text = workoutOfDay.addedDate?.gravityDate()
        btnLike.isSelected = (workoutOfDay.isLike != "0" ) ? true : false
        imageBG.downloadImageFrom(urlString: workoutOfDay.woImgURL ?? "", with: shadowImage)
        collection.configure(musclesGroup: (workoutOfDay.muscleGroups?.components(separatedBy: ","))!, showDetailCell: nil)
    }
    
    private func populateWorkout(_ workoutOfDay:Workouts) {
        self.workoutID = workoutOfDay.workoutID
        lblCategory.text = workoutOfDay.title
        lblDate.text = workoutOfDay.addedDate?.gravityDate()
        btnLike.isSelected = (workoutOfDay.isLike != "0" ) ? true : false
        collection.configure(musclesGroup: (workoutOfDay.muscleGroups?.components(separatedBy: ","))!, showDetailCell: nil)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setup() {
        selectionStyle = .none
        btnLike.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
    }
    
    @objc func buttonPressed(_ sender: GravityAcademyButton) {
        let param = LikeDislikeParam(type: .workout, type_id: workoutID)
        let index = IndexPath(row: 0, section: sender.tag)
        didLikeDislikeWorkoutLibrary?(param, index)
    }
}
