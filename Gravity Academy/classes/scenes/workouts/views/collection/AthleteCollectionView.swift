//
//  AthleteCollectionView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 06/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class AthleteCollectionView: UICollectionView {
    
    var viewModel: HomeViewModel!
    let layout = UICollectionViewFlowLayout()
    weak var delegateAthlete: AthleteCollectionViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
//    func configure(viewModel:HomeViewModel) {
//        self.viewModel = viewModel
//        setup()
//    }
    
    func configure() {
        setup()
    }
    
    private func setup() {
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 7
        collectionViewLayout = layout
        delegate = self
        dataSource = self
        reloadData()
    }
}

extension AthleteCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        return viewModel.count
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusable(indexPath) as AthleteCollectionViewCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath)
        print(collectionView.tag)
        delegateAthlete?.sendAction(action: .athlete, with: "\(indexPath.item)")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 48, height: 48)
    }
}




