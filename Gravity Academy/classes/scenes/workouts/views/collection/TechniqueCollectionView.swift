//
//  TechniqueCollectionView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 23/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class TechniqueCollectionView: UICollectionView {
    var viewModel: HomeViewModel!
    let layout = UICollectionViewFlowLayout()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(viewModel:HomeViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        collectionViewLayout = layout
        delegate = self
        dataSource = self
        reloadData()
    }
}

extension TechniqueCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        return viewModel.count
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusable(indexPath) as HomeCollectionCell
        cell.btnLike.tag = indexPath.row
        cell.imageBG.image = UIImage(named: HomeScreenImage.skill.rawValue)
        cell.configure(with: viewModel.item(at: indexPath.item))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath)
        print(collectionView.tag)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
}





