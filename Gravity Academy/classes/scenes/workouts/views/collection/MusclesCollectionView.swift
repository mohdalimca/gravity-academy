//
//  MusclesCollectionView.swift
//  Gravity Academy
//
//  Created by Apple on 11/12/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class MusclesCollectionView: UICollectionView {
    
    var homeViewModel: HomeViewModel!
    var detailViewModel: DetailViewModel!
    var showDetailCell: Bool?
    let layout = UICollectionViewFlowLayout()
    weak var delegateAthlete: AthleteCollectionViewDelegate?
    var musclesGroup: [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(musclesGroup:[String], showDetailCell:Bool?) {
        self.musclesGroup = musclesGroup
        self.showDetailCell = showDetailCell
        setup()
    }
    
    func configure(home:HomeViewModel?, detail:DetailViewModel?, showDetailCell:Bool?) {
        self.homeViewModel = home
        self.detailViewModel = detail
        self.showDetailCell = showDetailCell
        setup()
    }
    
    private func setup() {
        layout.minimumLineSpacing = 7
        layout.scrollDirection = .horizontal
        collectionViewLayout = layout
        delegate = self
        dataSource = self
        reloadData()
    }
}

extension MusclesCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return musclesGroup.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.showDetailCell != nil {
            let cell = dequeueReusable(indexPath) as DetailCollectionViewCell
            cell.configure(with: musclesGroup[indexPath.item])
            return cell
        } else {
            let cell = dequeueReusable(indexPath) as MusclesCollectionViewCell
            cell.configure(with: musclesGroup[indexPath.item])
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let lbl = UILabel()
        lbl.text = musclesGroup[indexPath.item]
        lbl.textAlignment = .center
        lbl.sizeToFit()
        layout.itemSize = CGSize(width: lbl.frame.size.width + 15, height: 25)
        return layout.itemSize
    }
}





