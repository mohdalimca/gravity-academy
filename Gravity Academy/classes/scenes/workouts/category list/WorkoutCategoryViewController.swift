//
//  WorkoutCategoryViewController.swift
//  Gravity Academy
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class WorkoutCategoryViewController: UIViewController {
    
    @IBOutlet weak var tableView: WorkoutCategoryTableView!
    @IBOutlet weak var header: WorkoutCategoryHeaderView!
    
    weak var delegate:ControllerDismisser?
    weak var router: NextSceneDismisserPresenter?
    let viewModel = WorkoutCategoryViewModel(provider: WorkoutsServiceProvider())
    weak var delegateTable: WorkoutLibraryTableViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
//        startAnimation()
        viewModel.view = self
        header.delegate = self
        tableView.delegateTable = delegateTable
        tableView.configure(viewModel)
        header.populateHeaderData(viewModel.id, viewModel.title)
//        viewModel.workoutDetail(param: WorkoutsParams.WorkoutsDetail(workout_id: "2", fn_level_id: "1"))
    }
    
    private func reload() {
        tableView.reloadData()
    }
    
    func didSelectFilters(_ fitnessLevel:String?, _ muscleID:String?, _ muscles:String?, _ equipment:String?) {
        if muscles != nil {
            header.viewFilter.isHidden = false
            header.lblFilter.text = muscles
            print("didSelectFilters")
        } else {
            print("no filter")
        }
    }
}

extension WorkoutCategoryViewController: WorkoutCategoryHeaderViewDelegate {
    func sendAction(action: WorkoutCategoryHeaderViewAction) {
        switch action {
        case .back:
            router?.dismiss(controller: .categoryList)
        case .backward:
            reload()
        case .forward:
            reload()
        case .filter:
            router?.push(scene: .filter)
        case .filterCancel:
            break
        }
    }
}

extension WorkoutCategoryViewController: WorkoutsViewRepresentable {
    func onAction(_ action: WorkoutsAction) {
        switch action {
        case let .errorMessage(text):
            showBannerWith(text: text, style: .danger)
            stopAnimating()
        case .detail:
            stopAnimating()
        default:
            break
        }
    }
}
