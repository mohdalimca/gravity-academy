//
//  WorkoutCategoryAdaptables.swift
//  Gravity Academy
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum WorkoutCategoryHeaderViewAction:String {
    case back
    case forward
    case backward
    case filter
    case filterCancel
}

protocol WorkoutCategoryHeaderViewDelegate:class {
    func sendAction(action: WorkoutCategoryHeaderViewAction)
}
