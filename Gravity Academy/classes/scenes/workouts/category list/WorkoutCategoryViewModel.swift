//
//  WorkoutCategoryViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class WorkoutCategoryViewModel {

    var id:String!
    var title:String!
    let provider: WorkoutsServiceProviderable
    weak var view: WorkoutsViewRepresentable?
    var detailWorkouts: [WoDetail] = []
    
    init(provider: WorkoutsServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    func workoutDetail(param: WorkoutsParams.WorkoutsDetail) {
        provider.workoutDetail(param: param)
    }
    
    var count: Int {
        return detailWorkouts.count
    }
}

extension WorkoutCategoryViewModel: WorkoutsServiceProviderDelegate {
    func completed<T>(for action: WorkoutsAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                guard let message = error?.responseData?.globalError else {
                    self.view?.onAction(.errorMessage(errorMessage))
                    return
                }
                self.view?.onAction(.errorMessage(message))
            } else {
                self.view?.onAction(.detail)
            }
        }
    }
}
