//
//  WorkoutCategoryTableView.swift
//  Gravity Academy
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class WorkoutCategoryTableView: UITableView {
    
    var viewModel: WorkoutCategoryViewModel!
    weak var delegateTable: WorkoutLibraryTableViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ viewModel: WorkoutCategoryViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
        reloadData()
    }
}

extension WorkoutCategoryTableView: UITableViewDelegate, UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return viewModel.count
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        let cell = tableView.dequeueReusable(indexPath) as WorkoutCategoryTableCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegateTable?.didSelect(at: indexPath.section)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}

