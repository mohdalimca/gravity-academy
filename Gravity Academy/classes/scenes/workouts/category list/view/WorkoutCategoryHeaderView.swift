//
//  WorkoutCategoryHeaderView.swift
//  Gravity Academy
//
//  Created by Apple on 15/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class WorkoutCategoryHeaderView: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblFilter: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var btnBackWard: UIButton!
    @IBOutlet weak var btnFilterCancel: UIButton!
    
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var viewFilter: UIView!
    
    var selectedDate = Date()
    weak var delegate: WorkoutCategoryHeaderViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        btnForward.isHidden = selectedDate.isCurrentDate
        lblDate.text = selectedDate.monthYear
        [btnBack, btnForward, btnBackWard, btnFilter, btnFilterCancel].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    func populateHeaderData(_ id:String, _ title:String) {
        lblTitle.text = title
        if id != "1" {
            viewTitle.isHidden = false
            viewDate.isHidden = true
            viewFilter.isHidden = true
        } else {
            viewDate.isHidden = false
            viewTitle.isHidden = true
            viewFilter.isHidden = true
            lblFilter.text = Date().monthYear
        }
    }
    
    private func forwardAction() {
        selectedDate = selectedDate.adding(months: 1)
        delegate?.sendAction(action: .forward)
        DispatchQueue.main.async {
            self.lblDate.text = self.selectedDate.monthYear
            self.btnForward.isHidden = self.selectedDate.isCurrentDate
        }
    }
    
    private func backwardAction() {
        selectedDate = selectedDate.adding(months: -1)
        delegate?.sendAction(action: .backward)
        DispatchQueue.main.async {
            self.lblDate.text = self.selectedDate.monthYear
            self.btnForward.isHidden = false
        }
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnBack:
            delegate?.sendAction(action: .back)
        case btnBackWard:
            backwardAction()
        case btnForward:
            forwardAction()
        case btnFilter:
            delegate?.sendAction(action: .filter)
        case btnFilterCancel:
            viewFilter.isHidden = true
            delegate?.sendAction(action: .filterCancel)
        default: break
        }
    }
}
