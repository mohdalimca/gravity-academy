//
//  WorkoutCategoryCoordinator.swift
//  Gravity Academy
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import  UIKit

final class WorkoutCategoryCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: WorkoutCategoryViewController = WorkoutCategoryViewController.from(from: .workouts, with: .categoryList)
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(controller, hideBar: true)
    }
    
    func start(id:String, title:String) {
        super.start()
        controller.viewModel.id = id
        controller.viewModel.title = title
        onStart()
        router.setRootModule(controller, hideBar: true)

    }
    
    private func onStart() {
        controller.router = self
    }
    
    private func startActivity() {
        let r = Router()
        let startActivity = StartActivityCoordinator(router: r)
        add(startActivity)
        startActivity.delegate = self
        startActivity.start()
        router.present(startActivity, animated: true)
    }
    
    private func startFilter() {
        let filter = FilterViewController()
        filter.router = self
        filter.transitioningDelegate = self
        filter.modalPresentationStyle = .custom
        filter.filterType = .workout
        filter.didSelectFilters = controller.didSelectFilters
        router.present(filter, animated: true)
    }
}

extension WorkoutCategoryCoordinator : `UIViewControllerTransitioningDelegate` {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return PresentationController(presentedViewController: presented, presenting: presenting)
    }
}


extension WorkoutCategoryCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .startActivity: startActivity()
        case .filter: startFilter()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension WorkoutCategoryCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
