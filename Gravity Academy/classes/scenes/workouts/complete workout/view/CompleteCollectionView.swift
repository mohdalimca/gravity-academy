
//
//  CompleteCollectionView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 23/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

protocol CompleteCollectionViewDelegate:class {
    func didSelectAtIndex(_ index: Int)
}

final class CompleteCollectionView: UICollectionView  {
    
    var viewModel: CompleteWorkoutViewModel!
    let layout = UICollectionViewFlowLayout()
    weak var delegateCollection:CompleteCollectionViewDelegate?
    var selectedIndex:Int = 0
    
    override func awakeFromNib() {
        
    }
    
    func configure(_ viewModel: CompleteWorkoutViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        layout.scrollDirection = .vertical
        collectionViewLayout = layout
        delegate = self
        dataSource = self
        reloadData()
    }
}

extension CompleteCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusable(indexPath) as CompleteCollectionViewCell
        cell.lblTitle.text = viewModel.item(at: indexPath.item)
        cell.imageEmoticon.image = UIImage(named: "complete_\(indexPath.item)")
        cell.viewContainer.layer.cornerRadius = 4
        if selectedIndex == indexPath.item {
            cell.viewContainer.backgroundColor = .black
            cell.lblTitle.textColor = .white
        } else {
            cell.lblTitle.textColor = .black
            cell.viewContainer.backgroundColor = UIColor.init(hex: "#EFEFEF")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        reloadData()
        delegateCollection?.didSelectAtIndex(indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.bounds.size.width - 40) / 3
        let height: CGFloat = 35.0
        return CGSize(width: width, height: height)
    }
}
