//
//  CompleteCollectionViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 23/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class CompleteCollectionViewCell: UICollectionViewCell, Reusable {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imageEmoticon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure<T>(with content: T) {
    }

}
