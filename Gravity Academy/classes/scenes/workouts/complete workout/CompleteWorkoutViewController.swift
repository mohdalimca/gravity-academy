//
//  CompleteWorkoutViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 23/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class CompleteWorkoutViewController: UIViewController {
    
    @IBOutlet weak var collection: CompleteCollectionView!
    @IBOutlet weak var containerView: GravityAcademyView!
    @IBOutlet weak var btnDismiss1: UIButton!
    @IBOutlet weak var btnDismiss2: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtNote: UITextView!
    
    var placeHolderText = "Leave a note to your training...."
    weak var router: NextSceneDismisserPresenter?
    var viewCenter: CGFloat = 0
    let viewModel = CompleteWorkoutViewModel(provider: WorkoutsServiceProvider())
    
    init() {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        viewModel.view = self
        viewCenter = self.view.center.y
        collection.delegateCollection = self
        collection.configure(viewModel)
        txtNote.delegate = self
        view.backgroundColor = .clear
        [btnDismiss1, btnDismiss2, btnSave].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:)))
//        containerView.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        switch sender {
        case btnDismiss1, btnDismiss2:
            dismiss(animated: true, completion: nil)
        case btnSave:
            startAnimation()
            viewModel.completeWorkout()
        default:
            break
        }
    }
    
    @objc func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: view?.window)
        var initialTouchPoint = CGPoint.zero
        
        switch sender.state {
        case .began:
            initialTouchPoint = touchPoint
        case .changed:
            if touchPoint.y > initialTouchPoint.y {
                view.frame.origin.y = touchPoint.y - initialTouchPoint.y
            }
        case .ended, .cancelled:
            if view.frame.origin.y < viewCenter {
                UIView.animate(withDuration: 0.5) {
                    self.view.frame.origin.y = 0.0
                }
            } else {
                dismiss(animated: true, completion: nil)
            }
        default: break
        }
    }
    
    private func dismissOnComplete() {
        dismiss(animated: true) {
            self.stopAnimating()
            self.router?.push(scene: .home)
        }
//        dismiss(animated: true, completion: nil)
    }
}

extension CompleteWorkoutViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeHolderText {
            textView.text = ""
            textView.textColor = .black
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            textView.text = placeHolderText
            textView.textColor = .lightGray
        }
        textView.resignFirstResponder()
    }

    func textViewDidChangeSelection(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = ""
            textView.textColor = .black
        }
        viewModel.userNote = textView.text
    }
}

extension CompleteWorkoutViewController: CompleteCollectionViewDelegate {
    func didSelectAtIndex(_ index: Int) {
        print(index as Any)
    }
}

extension CompleteWorkoutViewController: WorkoutsViewRepresentable {
    func onAction(_ action: WorkoutsAction) {
        switch action {
        case let .errorMessage(msg):
            showBannerWith(text: msg, style: .success)
            stopAnimating()
        case .complete:
            dismissOnComplete()
        default:
            stopAnimating()
        }
    }
}
