//
//  CompleteWorkoutViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 28/06/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation


final class CompleteWorkoutViewModel {
    
    weak var view: WorkoutsViewRepresentable?
    let provider: WorkoutsServiceProviderable
    var workoutId: String?
   
    var items: [String] = ["On fire", "Fresh", "Destroyed", "Confident", "Relaxed", "Dead","On fire", "Fresh", "Destroyed", "Confident", "Relaxed", "Dead", "Confident", "Relaxed", "Dead" , "Destroyed"]
    
    var userNote: String?

    init(provider: WorkoutsServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    var count: Int {
        return items.count
    }
    
    func item(at index:Int) -> String {
        return items[index]
    }
    
    func completeWorkout() {
        provider.completeWorkout(param: WorkoutsParams.Complete(workout_id: workoutId))
    }
}


extension CompleteWorkoutViewModel: WorkoutsServiceProviderDelegate {
    func completed<T>(for action: WorkoutsAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage(errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
//                    switch action {
////                    case .dashBoardData:
////                        self.dashBoardData = resp.data?.homeData
////                    case .day:
////                        self.workoutOfDay = resp.data?.woOfTheDay?.first
////                    default:
////                        break
//                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.error ?? errorMessage))
                }
            }
        }
    }
}
