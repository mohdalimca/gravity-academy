//
//  WorkoutsDetailTableViewCell.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 19/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class WorkoutsDetailTableViewCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var lblPart: UILabel!
    @IBOutlet weak var lblPartTile: UILabel!
    @IBOutlet weak var lblSession: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var imageCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configure<T>(with content: T) {
        progressView.layer.cornerRadius = 2.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
