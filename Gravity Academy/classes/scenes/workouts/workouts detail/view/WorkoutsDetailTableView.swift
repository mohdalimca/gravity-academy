//
//  WorkoutsDetailTableView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 19/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class WorkoutsDetailTableView: UITableView {
    
    var viewModel: HomeViewModel!
    weak var delegateTable: WorkoutsDetailTableViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func configure(_ viewModel: HomeViewModel) {
        self.viewModel = viewModel
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 50
        rowHeight = UITableView.automaticDimension
    }
    
}

extension WorkoutsDetailTableView: UITableViewDelegate, UITableViewDataSource  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.separatorStyle = .none
        let cell = tableView.dequeueReusable(indexPath) as WorkoutsDetailTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegateTable?.didSelectAt(index: indexPath.row)
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
    }
}

