//
//  WorkoutsDetailViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 19/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class WorkoutsDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: WorkoutsDetailTableView!
    @IBOutlet weak var header: WorkoutsDetailHeaderView!
    weak var delegate:ControllerDismisser?
    weak var router: NextSceneDismisserPresenter?
    weak var delegateTable: WorkoutsDetailTableViewDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        header.delegateHeader = self
        tableView.reloadData()
        tableView.delegateTable = delegateTable
        //        tableView.delegateTable = self
        //        tableView.configure(viewModel)
    }
}

extension WorkoutsDetailViewController: WorkoutsDetailHeaderViewDelegate {
    func didSelect(action: WorkoutsDetailAction) {
        if action == .back {
            router?.dismiss(controller: .workoutDetail)
        }
    }
}
