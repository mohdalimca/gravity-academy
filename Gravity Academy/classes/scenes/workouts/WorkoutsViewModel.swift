
//
//  WorkoutsViewModel.swift
//  Gravity Academy
//
//  Created by Apple on 08/12/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class WorkoutsViewModel {
    
    var workoutOfDay: WoOfTheDay?
    var dashBoardData: HomeData?
    weak var view: WorkoutsViewRepresentable?
    let provider: WorkoutsServiceProviderable
    
    init(provider: WorkoutsServiceProviderable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    func workoutLibrary() {
        provider.workoutLibrary()
    }
    
    func workoutDetail(param: WorkoutsParams.WorkoutsDetail) {
        provider.workoutDetail(param: param)
    }
    
    func workoutOfTheDay() {
        provider.workoutOfTheDay()
    }
}

extension WorkoutsViewModel: WorkoutsServiceProviderDelegate {
    func completed<T>(for action: WorkoutsAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage(errorMessage))
            } else {
                if let resp = response as? SuccessResponseModel, resp.responseCode == 200 {
                    switch action {
                    case .dashBoardData:
                        self.dashBoardData = resp.data?.homeData
                    case .day:
                        self.workoutOfDay = resp.data?.woOfTheDay?.first
                    default:
                        break
                    }
                    self.view?.onAction(action)
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.error ?? errorMessage))
                }
            }
        }
    }
}
