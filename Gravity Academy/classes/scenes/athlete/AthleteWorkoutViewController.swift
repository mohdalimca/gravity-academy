//
//  AthleteWorkoutViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class AthleteWorkoutViewController: UIViewController {

    @IBOutlet weak var header: AthleteWorkoutHeaderView!
    weak var router: NextSceneDismisserPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    private func setup() {
        header.delegateHeader = self
    }
}


extension AthleteWorkoutViewController: AthleteWorkoutHeaderViewDelegate {
    func sendAction(action: AthleteWorkoutHeaderButtonAction) {
        switch action {
        case .back: router?.dismiss(controller: .athlete)
        case .program: break
        default: break
        }
    }
}
