//
//  AthleteWorkoutHeaderView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class AthleteWorkoutHeaderView: UIView {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var lblMedia: UILabel!
    @IBOutlet weak var lblWorkout: UILabel!
    @IBOutlet weak var lblProgram: UILabel!
    @IBOutlet weak var lblSkills: UILabel!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var viewMedia: UIView!
    @IBOutlet weak var viewWorkout: UIView!
    @IBOutlet weak var viewProgram: UIView!
    @IBOutlet weak var viewSkills: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnMedia: UIButton!
    @IBOutlet weak var btnWorkout: UIButton!
    @IBOutlet weak var btnProgram: UIButton!
    @IBOutlet weak var btnSkills: UIButton!
    @IBOutlet weak var imageProfile: UIImageView!
    
    weak var delegateHeader:AthleteWorkoutHeaderViewDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        [btnBack, btnProfile, btnMedia, btnProgram, btnSkills, btnWorkout, btnMedia].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        updateButtons(btnProfile)
    }
    
    private func updateButtons(_ sender: UIButton) {
        switch sender {
        case btnProfile:
            viewProfile.alpha = 1.0
            viewMedia.alpha = 0.5
            viewWorkout.alpha = 0.5
            viewProgram.alpha = 0.5
            viewSkills.alpha = 0.5
        case btnMedia:
            viewProfile.alpha = 0.5
            viewMedia.alpha = 1.0
            viewWorkout.alpha = 0.5
            viewProgram.alpha = 0.5
            viewSkills.alpha = 0.5
        case btnWorkout:
            viewProfile.alpha = 0.5
            viewMedia.alpha = 0.5
            viewWorkout.alpha = 1.0
            viewProgram.alpha = 0.5
            viewSkills.alpha = 0.5
        case btnProgram:
            viewProfile.alpha = 0.5
            viewMedia.alpha = 0.5
            viewWorkout.alpha = 0.5
            viewProgram.alpha = 1.0
            viewSkills.alpha = 0.5
        case btnSkills:
            viewProfile.alpha = 0.5
            viewMedia.alpha = 0.5
            viewWorkout.alpha = 0.5
            viewProgram.alpha = 0.5
            viewSkills.alpha = 1.0
        default: break
        }
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        updateButtons(sender)
        switch sender {
        case btnBack: delegateHeader?.sendAction(action: .back)
        case btnProfile: delegateHeader?.sendAction(action: .profile)
        case btnMedia: delegateHeader?.sendAction(action: .media)
        case btnWorkout: delegateHeader?.sendAction(action: .workout)
        case btnProgram: delegateHeader?.sendAction(action: .program)
        case btnSkills: delegateHeader?.sendAction(action: .skills)
        default: break
        }
    }
}
