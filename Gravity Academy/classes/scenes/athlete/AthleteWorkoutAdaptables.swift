//
//  AthleteWorkoutAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum AthleteWorkoutHeaderButtonAction: String {
    case back
    case profile
    case media
    case workout
    case program
    case skills
}

protocol AthleteWorkoutHeaderViewDelegate:class {
    func sendAction(action: AthleteWorkoutHeaderButtonAction)
}
