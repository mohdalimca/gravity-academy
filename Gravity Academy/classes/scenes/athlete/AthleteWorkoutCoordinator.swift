
//
//  AthleteWorkoutCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 28/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class AthleteWorkoutCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let athlete: AthleteWorkoutViewController = AthleteWorkoutViewController.from(from: .workouts, with: .athlete)
    
    override func start() {
        super.start()
        onStart()
        router.setRootModule(athlete, hideBar: true)
    }
    
    private func onStart() {
        athlete.router = self
    }
    
    private func startActivity() {
        let r = Router()
        let startActivity = StartActivityCoordinator(router: r)
        add(startActivity)
        startActivity.delegate = self
        startActivity.start()
        router.present(startActivity, animated: true)
        
    }
}

extension AthleteWorkoutCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {}
    
    func push(scene: Scenes) {
        switch scene {
        case .startActivity: startActivity()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}


extension AthleteWorkoutCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

