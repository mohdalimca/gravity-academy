//
//  LoginViewController.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 08/10/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import NotificationBannerSwift

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginMainView: LoginMainView!
    let viewModel = LoginViewModel(provider: OnboardingServiceProvider())
    var router:NextSceneDismisserPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        loginMainView.delegate = self
        viewModel.view = self
    }
    
    func didTryAgain(_ api:AppAPI ) {
        switch api {
        case .login: didSelect(action: .login)
        case .verifyEmail: didSelect(action: .verify)
        default: break
        }
        router?.dismiss(controller: .nointernet)
    }
}

extension LoginViewController: LoginMainViewDelegate {
    func didSelect(action: LoginButtonAction) {
        switch action {
        case .forgot:
            router?.present(scene: .forgot)
        case .register: router?.push(scene: .register)
        case .login:
            startAnimation()
            viewModel.apiName = .login
            viewModel.onAction(action: .inputComplete(.login), for: .login)
        case .verify:
            viewModel.apiName = .verifyEmail
            startAnimation()
            viewModel.onAction(action: .validateEmail, for: .login)
        }
    }
}

extension LoginViewController: OnboardingViewRepresentable {
    func onAction(_ action: OnboardingAction) {
        switch action {
        case .noInternet:
            stopAnimating()
            router?.push(scene: .nointernet)
        case let .errorMessage(text), let .requireFields(text):
            showBannerWith(text: text, style: .danger)
            stopAnimating()
        case .userLogin:
            stopAnimating()
            showBannerWith(text: "Login successfully.", style: .success)
            router?.push(scene: .home)
        case .validateEmail:
            stopAnimating()
            router?.push(scene: .verifyOTP)
        default: break
        }
    }
}

