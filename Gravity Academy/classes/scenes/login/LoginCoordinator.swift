//
//  LandingCoordinator.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import UIKit

final class LoginCoordinator: Coordinator<Scenes> {
    weak var delegate: CoordinatorDimisser?
    let login: LoginViewController = LoginViewController.from(from: .login, with: .login)
    let verifyEmail: VerifyEmailViewController = VerifyEmailViewController.from(from: .register, with: .verifyEmail)
//    let verifyOTP: VerifyOTPViewController = VerifyOTPViewController.from(from: .register, with: .verifyOTP)
    let nointernet: NoInternetViewController = NoInternetViewController.from(from: .common, with: .nointernet)
    
    var forgot: ForgotCoordinator!
    var verifyOTP: VerifyEmailCoordinator!
    
    
    override func start() {
        super.start()
        login.router = self
        router.setRootModule(login, hideBar: true)
        onStart()
    }
    
    private func onStart() {
        nointernet.router = self
        nointernet.didTryAgain = login.didTryAgain
//        verifyOTP.router = self
    }
    
    private func forgotModule() {
        forgot = ForgotCoordinator(router: Router())
        add(forgot)
        forgot.delegate = self
        forgot.start()
        self.router.present(forgot, animated: true)
    }
    
    private func homeModule() {
        let router = Router()
        let home = HomeCoordinator(router: router)
        add(home)
        home.start()
        self.router.present(home, animated: true)
    }
    
    private func registerModule() {
        let router = Router()
        let register = RegistrationCoordinator(router: router)
        add(register)
        register.start()
        self.router.present(register, animated: true)
    }
    
    private func startVerificationModule() {
        let router = Router()
        verifyOTP = VerifyEmailCoordinator(router: router)
        add(verifyOTP)
        verifyOTP.start()
        self.router.present(verifyOTP, animated: true)
    }
    
    private func show(scene: Scenes) {
        switch scene {
        case .home:   homeModule()
        case .forgot: forgotModule()
        case .register: registerModule()
        default: break
        }
    }
}

extension LoginCoordinator: NextSceneDismisserPresenter {
    func present(scene: Scenes) {
        switch scene {
            case .home:   homeModule()
            case .forgot: forgotModule()
            case .register: registerModule()
            case .verifyOTP: startVerificationModule()
            default: break
        }
    }

    func push(scene: Scenes) {
        switch scene {
            case .nointernet:
                nointernet.apiName = login.viewModel.apiName
                self.router.present(nointernet, animated: true)
            case .home:   homeModule()
            case .forgot: forgotModule()
            case .register: registerModule()
            case .verifyOTP: startVerificationModule()
            default: break
        }
    }

    func dismiss(controller: Scenes) {
        switch controller {
        case .verifyOTP: router.popModule(animated: true)
        default:router.dismissModule(animated: true, completion: nil)
        }
    }
}

extension LoginCoordinator: LoginViewControllerDelegate {
    func onAction(action: Scenes) {
        show(scene: action)
    }
}


extension LoginCoordinator: CoordinatorDimisser {
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

func resetDefaults() {
    let dictionary = UserDefaults.standard.dictionaryRepresentation()
    dictionary.keys.forEach {
        key in UserDefaults.standard.removeObject(forKey: key)
    }
}
