//
//  OnboardingAdaptables.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 10/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

enum OnboardingScreenType {
    case login
    case register
    case forgot
    case new
    case otp
    case email
}

enum OnboardingAction {
    case inputComplete(_ screen:OnboardingScreenType)
    case editingDidEnd(_ filed:String, _ valiue:String)
    case editingDidChange(_ filed:String, _ valiue:String)
    case requireFields(_ text:String)
    case validEmail(text:String)
    case errorMessage(_ message:String)
    case successMessage(_ message:String)
    case userLogin
    case userRegistered
    case validateOTP
    case validateEmail
    case forgot
    case new
    case master
    case noInternet
}

struct OnboardingFielValues {
    static let email = "email"
    static let password = "password"
    static let gender = "gender"
    static let intro = "intro"
    static let bithdate = "birthdate"
}

protocol  OnboardingTableViewDelegate:class {
    func placeHolder(for item:Int, for screen:OnboardingScreenType) -> String
    func onAction(action:OnboardingAction, for screen:OnboardingScreenType)
    func numberOfItems(for screen:OnboardingScreenType) -> Int
}

protocol OnboardingViewRepresentable:class {
    func onAction(_ action:OnboardingAction)
}

protocol OnboardingServiceProviderDelegate:class {
    func completed<T>(for action:OnboardingAction, with response:T?, with error:APIError?)
}

protocol OnboardingServiceProviderable:class {
    var delegate: OnboardingServiceProviderDelegate? { get set }
    func register(param: UserParams.Register)
    func login(email:String, password:String)
    func masterData()
    func validateEmail(param: UserParams.ValidateEmail)
    func validateOTP(email:String, otp:String)
    func forgotPassword(email:String)
    func newPassword(password:String, otp:String)
}

protocol InputViewDelegate:class {
    func onAction(action:OnboardingAction, for screen:OnboardingScreenType)
}

enum TermsAndPrivacy {
    case termsAndCondition
    case privacyPolicy
}

enum TermsConditionText : String {
    case title = "I agree to the Terms and Privacy Policy."
}

struct WebUrl {
    static var privacyPolicy: String = "https://www.google.com/search?q=google&oq=google&aqs=chrome..69i57j69i60l3.1329j0j7&sourceid=chrome&ie=UTF-8"
    static var termsAndConditions: String = "https://www.google.com/search?q=google&oq=google&aqs=chrome..69i57j69i60l3.1329j0j7&sourceid=chrome&ie=UTF-8"
}
