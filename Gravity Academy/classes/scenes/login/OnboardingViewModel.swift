//
//  OnboardingViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 10/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation
import UIKit

final class OnboardingViewModel {
    weak var view:OnboardingServiceProviderable?
    var provider:OnboardingServiceProviderable
    private var regiterInputs: [String] {
        return ["Username" , "Full name", "Email", "Password"]
    }
    
    init(provider:OnboardingServiceProviderable) {
        self.provider = provider
    }
    
    func validEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func termsConditionAndPrivacyPolicyTextSetup(labelText: UILabel) -> NSMutableAttributedString {
        labelText.textColor =  UIColor.black
        let underlineAttriString = NSMutableAttributedString(string: TermsConditionText.title.rawValue)
        let rangeTerms = (TermsConditionText.title.rawValue as NSString).range(of: "Terms")
        let rangePrivacyPolicy = (TermsConditionText.title.rawValue as NSString).range(of: "Privacy Policy.")
        
        let myAttribute = [NSAttributedString.Key.foregroundColor: UIColor(red: 255.0/255.0, green: 42.0/255.0, blue: 42.0/255.0, alpha: 1.0), NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.underlineColor: UIColor.clear, NSAttributedString.Key.font: UIFont.init(name: "Avenir Book", size: 13.0)!] as [NSAttributedString.Key : Any]
        underlineAttriString.addAttributes(myAttribute, range: rangeTerms)
        underlineAttriString.addAttributes(myAttribute, range: rangePrivacyPolicy)
        return underlineAttriString
    }
    
 }

