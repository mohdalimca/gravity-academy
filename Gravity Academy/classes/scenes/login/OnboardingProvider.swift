//
//  OnboardingProvider.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 10/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation

final class OnboardingServiceProvider:OnboardingServiceProviderable {
  
    var delegate: OnboardingServiceProviderDelegate?
    private let task = UserTask()

    func register(param: UserParams.Register) {
        task.register(param: param, modeling: InformationResponseModel.self) { [weak self ](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .userRegistered, with: resp, with: err)
            }
            guard let _ = resp else { return }
            self?.delegate?.completed(for: .userRegistered, with: resp, with: err)
        }
    }
    
    func login(email: String, password: String) {
        task.login(param: UserParams.Login.init(email: email, password: password), responseModel: SuccessResponseModel.self) { [weak self ](resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .userLogin, with: resp, with: err)
            }
            guard let _ = resp else { return }
            self?.delegate?.completed(for: .userLogin, with: resp, with: err)
        }
    }
    
    func forgotPassword(email:String) {
        task.forgot(param: UserParams.Forgot(email: email), responseModel: InformationResponseModel.self) { [weak self] (resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .forgot, with: resp, with: err)
            }
            guard let _ = resp else { return }
            self?.delegate?.completed(for: .forgot, with: resp, with: err)
        }
    }
    
    func masterData() {
        task.masterData(modeling: SuccessResponseModel.self) { [weak self] (resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .master, with: resp, with: err)
            }
            guard let _ = resp else { return }
            self?.delegate?.completed(for: .master, with: resp, with: err)
        }
    }
    
    func validateOTP(email:String, otp:String) {
        task.validateOTP(param: UserParams.ValidateOTP(email: email, otp: otp), responseModel: OTPResponseModel.self) { [weak self] (resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .validateOTP, with: resp, with: err)
            }
            guard let _ = resp else { return }
            self?.delegate?.completed(for: .validateOTP, with: resp, with: err)
        }
    }
    
    func validateEmail(param: UserParams.ValidateEmail) {
        task.validateEmail(param: param, responseModel: InformationResponseModel.self) { [weak self] (resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .validateEmail, with: resp, with: err)
            }
            guard let _ = resp else { return }
            self?.delegate?.completed(for: .validateEmail, with: resp, with: err)
        }
    }
    
    func newPassword(password:String, otp:String) {
        task.newPassword(param: UserParams.NewPassword(password: password, key: otp), responseModel: InformationResponseModel.self) { [weak self] (resp, err) in
            if err != nil {
                self?.delegate?.completed(for: .new, with: resp, with: err)
            }
            guard let _ = resp else { return }
            self?.delegate?.completed(for: .new, with: resp, with: err)
        }
    }    
}
