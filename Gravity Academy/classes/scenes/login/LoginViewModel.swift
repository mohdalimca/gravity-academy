
//
//  LoginViewModel.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 10/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


final class LoginViewModel: InputViewDelegate {
    
    static var email = ""
    static var password = ""
    var apiName: AppAPI!
    var goals = [Goal]()
    var fitnessLevels = [FitnessLevel]()
    
    
    let provider:OnboardingServiceProviderable
    weak var view:OnboardingViewRepresentable?
    let onboarding = OnboardingViewModel(provider: OnboardingServiceProvider())
    
    init(provider:OnboardingServiceProviderable) {
        self.provider = provider
        provider.delegate = self
    }
    
    func onAction(action: OnboardingAction, for screen: OnboardingScreenType) {
        switch action {
        case let .editingDidEnd(field, input): setField(field: field, input: input)
        case .inputComplete(_ ): validateLogin()
        case .validateEmail: validateEmail()
        default: break
        }
    }
    
    func masterData() {
        provider.masterData()
    }
    
    func verifyEmail(email: String) {
        provider.validateEmail(param: UserParams.ValidateEmail(email: email))
    }
    
    private func setField(field:String, input:String) {
        if field == "Email"{
            LoginViewModel.email = input
        }
        if field == "Password" {
            LoginViewModel.password = input
        }
    }
    
    private func validateEmail() {
        if LoginViewModel.email == "" {
            view?.onAction(.requireFields("email is required"))
            return
        }
        if !onboarding.validEmail(email: LoginViewModel.email) {
            view?.onAction(.requireFields("invalid email"))
            return
        }
        provider.validateEmail(param: UserParams.ValidateEmail(email: LoginViewModel.email))
    }
    
    private func validateLogin() {
        if LoginViewModel.email == "" {
            view?.onAction(.requireFields("email is required"))
            return
        }
        if !onboarding.validEmail(email: LoginViewModel.email) {
            view?.onAction(.requireFields("invalid email"))
            return
        }
        if LoginViewModel.password == "" {
            view?.onAction(.requireFields("password is required"))
            return
        }
        provider.login(email: LoginViewModel.email, password: LoginViewModel.password)
    }
    
    private func masterAPI(with response: SuccessResponseModel?, with error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                guard let message = error?.responseData?.error else {
                    if error?.errorCode == .some(.network) {
                        self.view?.onAction(.noInternet)
                    } else {
                        self.view?.onAction(.errorMessage(errorMessage))
                    }
                    return
                }
                self.view?.onAction(.errorMessage(message))
            } else {
                self.fitnessLevels = response?.data?.masterData?.fitnessLevel ?? self.fitnessLevels
                self.goals = response?.data?.masterData?.goals ?? self.goals
                self.view?.onAction(.master)
            }
        }
    }
    
    private func loginAPI(with action: OnboardingAction, response: SuccessResponseModel?, with error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                guard let message = error?.responseData?.error else {
                    if error?.errorCode == .some(.network) {
                        self.view?.onAction(.noInternet)
                    } else {
                        self.view?.onAction(.errorMessage(errorMessage))
                    }
                    return
                }
                self.view?.onAction(.errorMessage(message))
            } else {
                UserStore.save(token: (response?.data?.loginData?.sessionkey)!)
                self.view?.onAction(action)
            }
        }
    }
    
    private func emailValidateAPI(with action: OnboardingAction, response: InformationResponseModel?, with error: APIError?) {
            DispatchQueue.main.async {
                if error != nil {
                    guard let message = error?.responseData?.error else {
                        if error?.errorCode == .some(.network) {
                            self.view?.onAction(.noInternet)
                        } else {
                            self.view?.onAction(.errorMessage(errorMessage))
                        }
                        return
                    }
                    self.view?.onAction(.errorMessage(message))
                } else {
                    self.view?.onAction(action)
                }
            }
        }
}

extension LoginViewModel:OnboardingServiceProviderDelegate {
    func completed<T>(for action: OnboardingAction, with response: T?, with error: APIError?) {
        switch action {
        case .master: masterAPI(with: (response as! SuccessResponseModel), with: error)
        case .userLogin: loginAPI(with: .userLogin, response: (response as? SuccessResponseModel ?? nil), with: error)
        case .validateEmail : emailValidateAPI(with: .validateEmail, response: (response as? InformationResponseModel ?? nil), with: error)
        default: break
        }
    }
}
