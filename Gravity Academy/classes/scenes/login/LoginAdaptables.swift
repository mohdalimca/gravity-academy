//
//  LoginAdaptanles.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 02/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import Foundation


enum LoginButtonAction: String {
    case login
    case forgot
    case register
    case verify
}

protocol LoginMainViewDelegate: class {
    func didSelect(action: LoginButtonAction)
}

protocol LoginViewControllerDelegate: class {
    func onAction(action: Scenes)
}
