//
//  LoginMainView.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

class LoginMainView: UIView {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgot: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnVerifyEmail: UIButton!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    weak var dismisser: ControllerDismisser?
    weak var delegate: LoginMainViewDelegate?
    weak var inputDelegate:InputViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setLabelAndButton()
    }
    
    private func setLabelAndButton() {
        [ btnBack, btnRegister, btnLogin, btnForgot, btnVerifyEmail, btnEye].forEach {
            $0?.layer.cornerRadius = 8
            $0?.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
        }
        
        [txtEmail, txtPassword].forEach {
            $0!.addTarget(self, action: #selector(onOnchange(sender:)), for: .editingChanged)
            $0!.addTarget(self, action: #selector(onDidEnd(sender:)), for: .editingDidEnd)
            $0!.addTarget(self, action: #selector(editingDidEndOnExit(sender:)), for: .editingDidEndOnExit)
        }
    }
    
    @objc func editingDidEndOnExit(sender: UITextField) {
        endEditing(true)
        setText(sender)
    }
    @objc func onDidEnd(sender:UITextField) {
        endEditing(true)
        setText(sender)
    }
    @objc func onOnchange(sender:UITextField){
        setText(sender)
    }
    
    private func setText(_ sender:UITextField) {
        if sender == txtEmail {
            LoginViewModel.email = sender.text!
        } else {
            LoginViewModel.password = sender.text!
        }
    }
    
    private func setPasswordField() {
        txtPassword.isSecureTextEntry = (!txtPassword.isSecureTextEntry) ? true : false
    }
    
    @objc func buttonPressed( sender: UIButton ) {
        switch sender {
        case btnEye: setPasswordField()
        case btnLogin: delegate?.didSelect(action: .login)
        case btnForgot: delegate?.didSelect(action: .forgot)
        case btnRegister: delegate?.didSelect(action: .register)
        case btnVerifyEmail: delegate?.didSelect(action: .verify)
        default: break
        }
    }
}
