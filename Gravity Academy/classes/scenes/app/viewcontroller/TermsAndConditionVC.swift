//
//  TermsAndConditionVC.swift
//  Gravity Academy
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit
import WebKit
import NVActivityIndicatorView

class TermsAndConditionVC: UIViewController {

    weak var router: NextSceneDismisserPresenter?
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        startAnimation()
        webView.navigationDelegate = self
        Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(removeAnimation), userInfo: nil, repeats: false)
        let request = URLRequest(url: URL(string: "https://www.gravity-academy.de/datenschutz-app")!)
        webView.load(request)
    }
    
    @objc func removeAnimation() {
        stopAnimating()
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        router?.dismiss(controller: .terms)
    }
}

extension TermsAndConditionVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        stopAnimating()
    }
}
