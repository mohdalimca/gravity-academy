
//
//  UserService.swift
//  Gravity Academy
//
//  Created by Apple on 19/02/20.
//  Copyright © 2020 W3Surface. All rights reserved.
//

import Foundation

final class UserService {
   
    private let task = UserTask()
    let notification = NotificationService()
   
//    func AuthenticateWithiCloud() {
//        guard (UserStore.id == nil), (UserStore.token == nil) else { return }
//        iCloudUserProvider.request {[weak self] in
//            guard let id = $0 else { return }
//            UserStore.save(id: id.recordName)
//            guard let appName = ProjectSettings.appName else { fatalError() }
//            self?.registeriCloud(id: id.recordName, app: appName)
//        }
//    }

    func registerPush(token:String) {
        guard let push = UserStore.pushToken else {
            notification.register(token)
            UserStore.save(pushToken: token)
            return
        }

        if push == token { return }
        notification.register(token)
        UserStore.save(pushToken: token)
    }

//   func registeriCloud(id:String, app:String) {
//    task.registeriCloud(params: UserParams.Auth.init(icloudId: id), responseModel: AuthResponse.self, completion:{resp,  err in
//         if err == nil {
//            print(resp!.fullOnboarded)
//            UserStore.save(auth: resp!.token.token)
//            UserStore.save(userID: resp!.userId)
//            UserStore.save(fullOnboarded: resp!.fullOnboarded)
//         }
//      })
//   }

}

