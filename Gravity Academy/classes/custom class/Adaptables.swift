//
//  Adaptables.swift
//  Treebetty
//
//  Created by Mohd Ali Khan on 4/18/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import UIKit

protocol ItemEditable {
    var name:String { get }
}

protocol Reusable {
    static var reuseIndentifier: String { get }
    static var nib: UINib? { get }
    func configure<T>(with content: T)
}

protocol ReusableReminder {
    static var reuseIndentifier: String { get }
    static var nib: UINib? { get }
    func configure<T>(with content: T, index: Int)
}

extension Reusable {
    static var reuseIndentifier: String { return String(describing: self) }
    static var nib: UINib? { return nil }
}

extension ReusableReminder {
    static var reuseIndentifier: String { return String(describing: self) }
    static var nib: UINib? { return nil }
}

protocol ControllerDismisser: class {
    func dismiss(controller:Scenes)
}

protocol PushNextController:class {
    func push(scene:Scenes)
}

@objc protocol PopPreviousController: class {
    @objc optional func popController()
}

protocol CoordinatorDimisser:class {
    func dismiss(coordinator:Coordinator<Scenes>)
}

protocol ScenePresenter {
    func present(scene:Scenes)
}

protocol RowSectionDisplayable {
    var title: String { get }
    var content: [Row] { get }
}

protocol RowJournalSectionDisplayable {
    var title: Date { get }
    var content: [RowJournal] { get }
}

public enum ViewType: String {
    case popup
    case xib
    case alert
    case controller
    case action
    case externalLink
    case hyperLinkController
}

typealias Dismisser = ControllerDismisser & CoordinatorDimisser
typealias NextSceneDismisser = PushNextController & ControllerDismisser & PopPreviousController
typealias NextSceneDismisserPresenter = NextSceneDismisser & ScenePresenter
