//
//  Gravity_AcademyTests.swift
//  Gravity AcademyTests
//
//  Created by Mohd Ali Khan on 01/09/19.
//  Copyright © 2019 m@k. All rights reserved.
//

import XCTest
@testable import Gravity_Academy

class Gravity_AcademyTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func test_username_placeholder() {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let login = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
        let _ = login.view
        XCTAssertEqual("Email", login.loginMainView.txtEmail.placeholder)
    }

}
